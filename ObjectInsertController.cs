﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyItemsModule.Services;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule
{
    public class ObjectInsertController : AppController
    {
        public async Task<ResultOItem<ClassObject>> GetOItem(string idObject)
        {
            var serviceElastic = new ElasticAgentObjectInsert(Globals);
            var result = await serviceElastic.GetClassObject(idObject);
            return result;
        }

        public ObjectInsertController(Globals globals) : base(globals)
        {
        }
        
    }
}
