﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Validations
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateCopyRelationsRequest(CopyRelationsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The request is not valid! It's empty!";
                return result;
            }

            if (!globals.is_GUID( request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The request is not valid! IdConfig is no valid Guid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetCopyRelationsModel(CopyRelationsModel model, string propertyName, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.Config))
            {
                if (model.Config == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Config found!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.SrcObject))
            {
                if (model.SrcObject == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No source-element!";
                    return result;
                }

                if (model.SrcObject.Type != globals.Type_Object)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The dest-element must be an Object, but is a {model.SrcObject.Type}";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.DstObject))
            {
                if (model.DstObject == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No dest-element!";
                    return result;
                }

                if (model.SrcObject.Type != globals.Type_Object)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The dest-element must be an Object, but is a {model.DstObject.Type}";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.Ontology))
            {
                if (model.Ontology == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No ontology!";
                    return result;
                }

                if (model.Ontology.GUID_Parent != globals.Class_Ontologies.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The ontology is not of correct class!";
                    return result;
                }
            }


            return result;
        }

        public static clsOntologyItem  ValidateMergeObjectsRequest(MergeObjectsRequestByClass request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateMergeObjectsByClassModel(MergeObjectsByClassModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();


            if (propertyName == null || propertyName == nameof(MergeObjectsByClassModel.ConfigItem))
            {
                if (model.ConfigItem == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Configuration found!";
                    return result;
                }

                if (model.ConfigItem.GUID_Parent != MergeObjects.Config.LocalData.Class_Merge_Objects_By_Class.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "This is no Merge-Objects configuration!";
                    return result;
                }
            }
            
            if (propertyName == null || propertyName != nameof(MergeObjectsByClassModel.ConfigItem))
            {
                if (!model.ConfigsToClasses.Any() && !model.Excludes.Any() && !model.Includes.Any() && !model.ObjectsToMerge.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have to provide classes, excludes or includes or a list of objects!";
                    return result;
                }

                if (model.ConfigsToClasses.Any(cls => cls.Ontology != globals.Type_Class))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The related classes are invalid!";
                    return result;
                }

                foreach (var include in model.Includes)
                {
                    try
                    {
                        include.Regex = new System.Text.RegularExpressions.Regex(include.Attribute.Val_String);
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = $"Attribute {include.Attribute.Val_String} is invalid!";
                        return result;
                        
                    }
                }

                foreach (var exclude in model.Excludes)
                {
                    try
                    {
                        exclude.Regex = new System.Text.RegularExpressions.Regex(exclude.Attribute.Val_String);
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = $"Attribute {exclude.Attribute.Val_String} is invalid!";
                        return result;

                    }
                }

                if (model.ObjectsToMerge.Any(obj => obj.Ontology != globals.Type_Object))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The related objects are invalid!";
                    return result;
                }
            }

            

            return result;
        }

        public static clsOntologyItem ValidateMergeObjectsByItemsRequest(MergeObjectsRequestByItems request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateMergeObjectsByItemsModel(MergeObjectsByItemsModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == null || propertyName == nameof(MergeObjectsByItemsModel.RootMergeConfig))
            {
                if (model.RootMergeConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"No Root-Config found!";
                    return result;
                }

                if (model.RootMergeConfig.GUID_Parent != MergeObjects.Config.LocalData.Class_Merge_Objects_By_Items.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The Root-Config is not of Type {MergeObjects.Config.LocalData.Class_Merge_Objects_By_Items.Name}";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(MergeObjectsByItemsModel.MergeConfigs))
            {
                
            }

            if (propertyName == null || propertyName == nameof(MergeObjectsByItemsModel.MergeHeads))
            {
                if (model.MergeHeads.Count != model.MergeConfigs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There have {model.MergeHeads.Count} Head-Items, but only {model.MergeConfigs.Count} configurations!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(MergeObjectsByItemsModel.MergeMerges))
            {
                if (model.MergeMerges.Count < model.MergeConfigs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There have {model.MergeMerges.Count} Merge-Items, but {model.MergeConfigs.Count} configurations. You need more Merge-Items than configurations!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(MergeObjectsByItemsModel.NamingRules))
            {
                if (model.NamingRules.Count != model.MergeConfigs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There have {model.NamingRules.Count} naming rules, but {model.MergeConfigs.Count} configurations. You need one naming rule per configuration!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(MergeObjectsByItemsModel.ArchiveClasses))
            {
                if (!model.ArchiveClasses.All(cls => cls.Ontology == globals.Type_Class))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Not all related items for the archive-classes are of type Class!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(MergeObjectsByItemsModel.MergeHeads))
            {
                if (!model.MergeHeads.All(cls => cls.Ontology == globals.Type_Object))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Not all related items for the heads are of type object!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(MergeObjectsByItemsModel.MergeMerges))
            {
                if (!model.MergeHeads.All(cls => cls.Ontology == globals.Type_Object))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Not all related items for the merges are of type object!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetObjectRelationTreeModel(ObjectRelationTreeModel model, OntologyModDBConnector dbReader, Globals globals, string propertyName)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == nameof(ObjectRelationTreeModel.ReferenceItemsToShowRelationObjects))
            {
                model.ReferenceItemsToShowRelationObjects = dbReader.ObjectRels;
                model.ShowRelationObjects = model.ReferenceItemsToShowRelationObjects.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).ToList();
            }
            else if (propertyName == nameof(ObjectRelationTreeModel.ShowRelationObjects))
            {
                if (dbReader.Objects1.Any())
                {
                    if (!dbReader.Objects1.All(obj => obj.GUID_Parent == ObjectRelationTree.Config.LocalData.Class_Relation_to_Show.GUID))
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = $"Not all objects of the Relations To Show list are of the class {ObjectRelationTree.Config.LocalData.Class_Relation_to_Show.Name}";
                        return result;
                    }
                }

                model.ShowRelationObjects = dbReader.Objects1;
            }
            else if (propertyName == nameof(ObjectRelationTreeModel.HelpLeftRight))
            {
                var objAtts = dbReader.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == ObjectRelationTree.Config.LocalData.AttributeType_Help_Left_Right.GUID).ToList();
                if (objAtts.Count != model.ShowRelationObjects.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The Count of Attributes {ObjectRelationTree.Config.LocalData.AttributeType_Help_Left_Right.Name} is incorrect!";
                    return result;
                }

                model.HelpLeftRight = objAtts;
            }
            else if (propertyName == nameof(ObjectRelationTreeModel.LeftRight))
            {
                var objAtts = dbReader.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == ObjectRelationTree.Config.LocalData.AttributeType_Left_Right.GUID).ToList();
                if (objAtts.Count != model.ShowRelationObjects.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The Count of Attributes {ObjectRelationTree.Config.LocalData.AttributeType_Left_Right.Name} is incorrect!";
                    return result;
                }

                model.LeftRight = objAtts;
            }
            else if (propertyName == nameof(ObjectRelationTreeModel.UseNameprovider))
            {
                var objAtts = dbReader.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == ObjectRelationTree.Config.LocalData.AttributeType_Use_Nameprovider.GUID).ToList();
                if (objAtts.Count != model.ShowRelationObjects.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The Count of Attributes {ObjectRelationTree.Config.LocalData.AttributeType_Use_Nameprovider.Name} is incorrect!";
                    return result;
                }

                model.UseNameprovider = objAtts;
            }
            else if (propertyName == nameof(ObjectRelationTreeModel.ShowRelationToCssClasses))
            {
                if (dbReader.ObjectRels.Count > model.ShowRelationObjects.Count)
                {
                    result = globals.LState_Success.Clone();
                    result.Additional1 = $"The Count of Relations between  {ObjectRelationTree.Config.LocalData.Class_Relation_to_Show.Name} and {ObjectRelationTree.Config.LocalData.Class_CSS_Class.Name} is incorrect! You have more CSS-Classes then show-Objects.";
                    return result;
                }

                model.ShowRelationToCssClasses = dbReader.ObjectRels;
            }
            else if (propertyName == nameof(ObjectRelationTreeModel.ShowRelationToMainHelpClasses))
            {
                if (dbReader.ObjectRels.Count > model.ShowRelationObjects.Count)
                {
                    result = globals.LState_Success.Clone();
                    result.Additional1 = $"The Count of Relations with {nameof(ObjectRelationTree.Config.LocalData.RelationType_Main_Help_Class.Name)} is incorrect!";
                    return result;
                }

                if (!dbReader.ObjectRels.All(rel => rel.Ontology == globals.Type_Class))
                {
                    result = globals.LState_Success.Clone();
                    result.Additional1 = $"Not all items related by {nameof(ObjectRelationTree.Config.LocalData.RelationType_Main_Help_Class.Name)} are of type class!";
                    return result;
                }

                model.ShowRelationToMainHelpClasses = dbReader.ObjectRels;
            }
            else if (propertyName == nameof(ObjectRelationTreeModel.ShowRelationToRelationClassesFirst))
            {
                var results = (from showRelationObject in model.ShowRelationObjects
                               join firstClass in dbReader.ObjectRels.Where(rel => rel.OrderID == 1) on showRelationObject.GUID equals firstClass.ID_Object
                               join secondClass in dbReader.ObjectRels.Where(rel => rel.OrderID == 2) on showRelationObject.GUID equals secondClass.ID_Object
                               select new { showRelationObject, firstClass, secondClass }).ToList();

                if (results.Count != model.ShowRelationObjects.Count)
                {
                    result = globals.LState_Success.Clone();
                    result.Additional1 = $"The Count of Relations with {nameof(ObjectRelationTree.Config.LocalData.RelationType_Relation_Classes.Name)} is incorrect!";
                    return result;
                }

                if (!dbReader.ObjectRels.All(rel => rel.Ontology == globals.Type_Class))
                {
                    result = globals.LState_Success.Clone();
                    result.Additional1 = $"Not all items related by {nameof(ObjectRelationTree.Config.LocalData.RelationType_Main_Help_Class.Name)} are of type class!";
                    return result;
                }

                model.ShowRelationToRelationClassesFirst = results.Select(res => res.firstClass).ToList();
                model.ShowRelationToRelationClassesSecond = results.Select(res => res.secondClass).ToList();
            }
            else if (propertyName == nameof(ObjectRelationTreeModel.ShowRelationToRelationTypesMainHelp))
            {
                if (dbReader.ObjectRels.Count > model.ShowRelationObjects.Count)
                {
                    result = globals.LState_Success.Clone();
                    result.Additional1 = $"The Count of Relation-Types for Main-Help Classes is incorrect!";
                    return result;
                }

                if (!dbReader.ObjectRels.All(rel => rel.Ontology == globals.Type_RelationType))
                {
                    result = globals.LState_Success.Clone();
                    result.Additional1 = $"Not all items related by {nameof(ObjectRelationTree.Config.LocalData.RelationType_Relationtype_Main_Help.Name)} are of type Relation-Type!";
                    return result;
                }

                model.ShowRelationToRelationTypesMainHelp = dbReader.ObjectRels;
            }
            else if (propertyName == nameof(ObjectRelationTreeModel.ShowRelationToRelationTypesRelationClasses))
            {
                if (dbReader.ObjectRels.Count > model.ShowRelationObjects.Count)
                {
                    result = globals.LState_Success.Clone();
                    result.Additional1 = $"The Count of Relation-Types for Relation-Classes is incorrect!";
                    return result;
                }

                if (!dbReader.ObjectRels.All(rel => rel.Ontology == globals.Type_RelationType))
                {
                    result = globals.LState_Success.Clone();
                    result.Additional1 = $"Not all items related by {nameof(ObjectRelationTree.Config.LocalData.RelationType_Relationtype_Relation_Clases.Name)} are of type Relation-Type!";
                    return result;
                }

                model.ShowRelationToRelationTypesRelationClasses = dbReader.ObjectRels;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetObjectRelationTreeRequest(GetObjectRelationTreeRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty( request.IdObject))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(request.IdObject))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid Id!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateDeleteObjectsRequest(DeleteObjectsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid Id!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetDeleteObjectsModel(DeleteObjectsModel model, OntologyModDBConnector dbReader, Globals globals, string propertyName)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == nameof(DeleteObjectsModel.Config))
            {
                if (dbReader.Objects1.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact 1 Object, but there are {dbReader.Objects1.Count}!";
                    return result;
                }

                model.Config = dbReader.Objects1.First();

                if (model.Config.GUID_Parent != DeleteObjects.Config.LocalData.Class_Delete_Objects.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided object is not of class {DeleteObjects.Config.LocalData.Class_Delete_Objects.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(DeleteObjectsModel.ClassItems))
            {
                if (dbReader.ObjectRels.Any())
                {
                    if (!dbReader.ObjectRels.All(rel => rel.Ontology == globals.Type_Class))
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = $"The provided classes are other types!";
                        return result;
                    }

                    model.ClassItems = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).ToList();
                }
            }
            else if (propertyName == nameof(DeleteObjectsModel.ObjectsToDelete))
            {
                if (!model.ClassItems.Any())
                {
                    model.ObjectsToDelete = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).ToList();
                }
                else
                {
                    model.ObjectsToDelete = dbReader.Objects1;
                }
            }

            return result;
        }
    }
}
