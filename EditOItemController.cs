﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Services;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule
{
    public class EditOItemController: AppController
    {
        private object controllerLocker = new object();

        private ElasticAgentOItems elasticAgent;
        private List<clsOntologyItem> dataTypes;
        public List<clsOntologyItem> DataTypes
        {
            get { return dataTypes; }
        }


        private clsOntologyItem oItem;
        public clsOntologyItem OItem
        {
            get
            {
                lock(controllerLocker)
                {
                    return oItem;
                }
            }
            set
            {
                lock(controllerLocker)
                {
                    oItem = value;
                }
            }
        }

        private clsOntologyItem relationTypeExists;
        public clsOntologyItem RelationTypeExists
        {
            get
            {
                lock (controllerLocker)
                {
                    return relationTypeExists;
                }
            }
            set
            {
                lock (controllerLocker)
                {
                    relationTypeExists = value;
                }
            }
        }

        private clsOntologyItem classExists;
        public clsOntologyItem ClassExists
        {
            get
            {
                lock (controllerLocker)
                {
                    return classExists;
                }
            }
            set
            {
                lock (controllerLocker)
                {
                    classExists = value;
                }
            }
        }

        private clsOntologyItem objectExists;
        public clsOntologyItem ObjectExists
        {
            get
            {
                lock (controllerLocker)
                {
                    return objectExists;
                }
            }
            set
            {
                lock (controllerLocker)
                {
                    objectExists = value;
                }
            }
        }

        private clsOntologyItem attributeTypeExists;
        public clsOntologyItem AttributeTypeExists
        {
            get
            {
                lock (controllerLocker)
                {
                    return attributeTypeExists;
                }
            }
            set
            {
                lock (controllerLocker)
                {
                    attributeTypeExists = value;
                }
            }
        }

        private ResultGetAttribute resultGetAttribute;
        public ResultGetAttribute ResultGetAttribute
        {
            get
            {
                lock (controllerLocker)
                {
                    return resultGetAttribute;
                }
            }
            set
            {
                lock (controllerLocker)
                {
                    resultGetAttribute = value;
                }
            }
        }

        private clsOntologyItem resultSaveAttribute;
        public clsOntologyItem ResultSaveAttribute
        {
            get
            {
                lock (controllerLocker)
                {
                    return resultSaveAttribute;
                }
            }
            set
            {
                lock (controllerLocker)
                {
                    resultSaveAttribute = value;
                }
            }
        }

        public async Task<ResultOItem<clsOntologyItem>> GetOItem(string idItem, string type)
        {
            var taskResult = await Task.Run<ResultOItem<clsOntologyItem>>(async() =>
            {
                var serviceAgent = new ElasticAgentOItems(Globals);
                var resultGetOItem = await serviceAgent.GetOItem(idItem, type);
                return resultGetOItem;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> SaveObjects(List<clsOntologyItem> objects)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = objects
                };

                var serviceAgent = new ElasticAgentOItems(Globals);

                var saveResult = await serviceAgent.SaveObjects(objects);
                result.ResultState = saveResult;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the objects!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckObjects(List<string> objectNames, string idClass)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var serviceAgent = new ElasticAgentOItems(Globals);
                var objectsResult = await serviceAgent.GetObjects(objectNames, idClass);
                result.ResultState = objectsResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var objects = (from name in objectNames
                                   join obj in objectsResult.Result on name equals obj.Name into objs
                                   from obj in objs.DefaultIfEmpty()
                                   select new { name, obj }).Select(nameObj =>
                                     {
                                         if (nameObj != null)
                                         {
                                             nameObj.obj.New_Item = false;
                                             return nameObj.obj;
                                         }
                                         else
                                         {
                                             return new clsOntologyItem
                                             {
                                                 GUID = Globals.NewGUID,
                                                 Name = nameObj.name,
                                                 GUID_Parent = idClass,
                                                 Type = Globals.Type_Object,
                                                 New_Item = true
                                             };
                                         }
                                     }).ToList();
                result.Result = objects;         
                
                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> IsExistingRelationType(string nameRelationType)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var serviceAgent = new ElasticAgentOItems(Globals);
                var relationTypeExistsTask = serviceAgent.IsExistingRelationType(nameRelationType);
                relationTypeExistsTask.Wait();
                RelationTypeExists = relationTypeExistsTask.Result;
                return relationTypeExistsTask.Result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> IsExistingClass(string nameObject)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var serviceAgent = new ElasticAgentOItems(Globals);
                var classExistsTask = serviceAgent.IsExistingClass(nameObject);
                classExistsTask.Wait();
                ClassExists = classExistsTask.Result;
                return classExistsTask.Result;
            });

            return taskResult;

        }

        public async Task<clsOntologyItem> IsExistingObject(string nameObject, string idClass)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var serviceAgent = new ElasticAgentOItems(Globals);
                var objectdExistsTask = serviceAgent.IsExistingObject(nameObject, idClass);
                objectdExistsTask.Wait();
                ObjectExists = objectdExistsTask.Result;
                return objectdExistsTask.Result;
            });

            return taskResult;

        }

        public async Task<clsOntologyItem> IsExistingAttributeType(string nameAttributeType)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var serviceAgent = new ElasticAgentOItems(Globals);
                var attributeTypeExistsTask = serviceAgent.IsExistingAttributeType(nameAttributeType);
                attributeTypeExistsTask.Wait();
                AttributeTypeExists = attributeTypeExistsTask.Result;
                return attributeTypeExistsTask.Result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveOItem(clsOntologyItem oItem)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var transaction = new clsTransaction(Globals);
                var result = transaction.do_Transaction(oItem);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> SaveObject(clsOntologyItem objectItem, clsOntologyItem classItem, string newName, string guid)
        {
            if (objectItem == null)
            {
                var names = new List<string>();
                if (newName.Contains('\n'))
                {
                    names = newName.Split('\n').ToList();
                }
                else
                {
                    names.Add(newName);
                }

                var objectItems = names.Select(name => new clsOntologyItem
                {
                    GUID = names.Count == 1 ? guid : Globals.NewGUID,
                    Name = name,
                    GUID_Parent = classItem.GUID,
                    Type = Globals.Type_Object
                }).ToList();

                
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = objectItems
                };

                var elasticAgent = new ElasticAgentOItems(Globals);
                result.ResultState = await elasticAgent.SaveObjects(objectItems);

                return result;
            }
            else
            {
                objectItem.Name = newName;
                var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
                {
                    var result = new ResultItem<List<clsOntologyItem>>
                    {
                        ResultState = Globals.LState_Success.Clone(),
                        Result = new List<clsOntologyItem> { objectItem }
                    };

                    var transaction = new clsTransaction(Globals);

                    result.ResultState = transaction.do_Transaction(objectItem);

                    return result;
                });
                return taskResult;
            }

            

            

            
        }
        public async Task<ResultGetAttribute> GetAttribute(string idAttribute)
        {
            var taskResult = await Task.Run<ResultGetAttribute>(() =>
            {
                var result = new ResultGetAttribute
                {
                    Result = Globals.LState_Success.Clone()
                };

                var resultTask = elasticAgent.GetAttribute(idAttribute);
                resultTask.Wait();
                result = resultTask.Result;

                return result;
            });

            return taskResult;
        }

      
        public EditOItemController(Globals globals) : base(globals)
        {   
            Initialize();
        }

        private void Initialize()
        {
            dataTypes = new List<clsOntologyItem>();
            elasticAgent = new ElasticAgentOItems(Globals);

            lock(controllerLocker)
            {
                var dbReader = new OntologyModDBConnector(Globals);
                var searchDataTypes = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_DataTypes.GUID
                    }
                };
                var result = dbReader.GetDataObjects(searchDataTypes);

                if (result.GUID == Globals.LState_Success.GUID)
                {
                    dataTypes = dbReader.Objects1;
                    dataTypes.ForEach(dType =>
                    {
                        if (dType.GUID == "0f7cbdaef27c4f998bcbf6d4659120f0")
                        {
                            dType.GUID_Related = Globals.DType_Bool.GUID;
                        }
                        else if (dType.GUID == "0da2776e471540088097111de9d3ca30")
                        {
                            dType.GUID_Related = Globals.DType_Int.GUID;
                        }
                        else if (dType.GUID == "c1cf6835aaea456b85df193700b18dbe")
                        {
                            dType.GUID_Related = Globals.DType_Real.GUID;
                        }
                        else if (dType.GUID == "704bad09434e4ee4ad9b4d2ac28ac1cd")
                        {
                            dType.GUID_Related = Globals.DType_DateTime.GUID;
                        }
                        else if (dType.GUID == "f9fcc62c61ea4ea3ba206c61610f8604")
                        {
                            dType.GUID_Related = Globals.DType_String.GUID;
                        }
                    });
                }
            }
            

        }

        public async Task<ResultItem<clsObjectAtt>> SaveObjectAttribute(clsOntologyItem objectItem, clsOntologyItem attributeTypeItem, object value, string idAttribute = null)
        {
            var relationConfig = new clsRelationConfig(Globals);
    
            var objectAttribute = relationConfig.Rel_ObjectAttribute(objectItem, attributeTypeItem, value);

            objectAttribute.ID_Attribute = string.IsNullOrEmpty(idAttribute) ? Globals.NewGUID : idAttribute;

            var resultTask = await elasticAgent.SaveAttribute(objectAttribute);

            var result = new ResultItem<clsObjectAtt>
            {
                ResultState = resultTask,
                Result = objectAttribute
            };

            return result;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjects(List<string> names, string idClass)
        {
            var elasticAgent = new ElasticAgentOItems(Globals);
            var searchObjects = names.Select(name => new clsOntologyItem
                {
                    Name = name,
                    GUID_Parent = idClass
                }).ToList();

            var serviceResult = await elasticAgent.GetObjects(searchObjects);
            var result = new ResultItem<List<clsOntologyItem>>
            {
                ResultState = serviceResult.ResultState,
                Result = (from obj in  serviceResult.Result
                          join name in names on obj.Name.ToLower() equals name.ToLower()
                          select obj).ToList()
            };

            return result;
        }
    }


    public class ResultOItem
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem OItem { get; set; }
    }

    
}
