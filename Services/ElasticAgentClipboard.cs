﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Services
{
    public class ElasticAgentClipboard : ElasticBaseAgent
    {
        private clsRelationConfig relationManager;

        public async Task<ResultItem<List<clsObjectRel>>> GetClipboardItemsRaw(clsOntologyItem refItem = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var dbReaderClipboardItems = new OntologyModDBConnector(globals);
                if (refItem != null)
                {
                    if (refItem.Type == globals.Type_AttributeType)
                    {
                        var searchRelated = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                                ID_RelationType = globals.RelationType_belongingAttribute.GUID,
                                Ontology = globals.Type_AttributeType
                            }
                        };

                        result.ResultState = dbReaderClipboardItems.GetDataObjectRel(searchRelated);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Result = dbReaderClipboardItems.ObjectRels;
                    }
                    else if (refItem.Type == globals.Type_RelationType)
                    {
                        var searchRelated = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                                ID_RelationType = globals.RelationType_belongingRelationType.GUID,
                                Ontology = globals.Type_RelationType
                            }
                        };

                        result.ResultState = dbReaderClipboardItems.GetDataObjectRel(searchRelated);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Result = dbReaderClipboardItems.ObjectRels;
                    }
                    else if (refItem.Type == globals.Type_Object)
                    {
                        var searchRelated = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                                ID_RelationType = globals.RelationType_belongingObject.GUID,
                                Ontology = globals.Type_Object
                            }
                        };

                        result.ResultState = dbReaderClipboardItems.GetDataObjectRel(searchRelated);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        if (!string.IsNullOrEmpty(refItem.GUID_Parent))
                        {
                            result.Result = dbReaderClipboardItems.ObjectRels.Where(clipItm => clipItm.ID_Parent_Other == refItem.GUID_Parent).ToList();
                        }
                        else
                        {
                            result.Result = dbReaderClipboardItems.ObjectRels;
                        }


                    }
                    else if (refItem.Type == globals.Type_Class)
                    {
                        var searchRelated = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                                ID_RelationType = globals.RelationType_belongingClass.GUID,
                                ID_Other = refItem.GUID,
                                Ontology = globals.Type_Class
                            }
                        };

                        result.ResultState = dbReaderClipboardItems.GetDataObjectRel(searchRelated);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Result = dbReaderClipboardItems.ObjectRels;
                    }
                }
                else
                {
                    var searchRelated = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                            ID_RelationType = globals.RelationType_belongingAttribute.GUID,
                        },
                        new clsObjectRel
                        {
                            ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                            ID_RelationType = globals.RelationType_belongingRelationType.GUID,
                        },
                        new clsObjectRel
                        {
                            ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                            ID_RelationType = globals.RelationType_belongingObject.GUID,
                        },
                        new clsObjectRel
                        {
                            ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                            ID_RelationType = globals.RelationType_belongingClass.GUID,
                        }

                    };

                    result.ResultState = dbReaderClipboardItems.GetDataObjectRel(searchRelated);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result = dbReaderClipboardItems.ObjectRels;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> RemoveItems(List<clsObjectRel> clipboardItemsToDelete)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var dbWriter = new OntologyModDBConnector(globals);

                
                if (clipboardItemsToDelete.Any())
                {
                    result.ResultState = dbWriter.DelObjectRels(clipboardItemsToDelete);
                    result.Result = clipboardItemsToDelete;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> ClearClipboard(clsOntologyItem refItem = null)
        {

            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();
                var delete = new List<clsObjectRel>();

                if (refItem != null)
                {
                    if (refItem.Type == globals.Type_AttributeType)
                    {
                        delete.Add(new clsObjectRel
                        {
                            ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                            ID_RelationType = globals.RelationType_belongingAttribute.GUID
                        });
                        



                    }
                    else if (refItem.Type == globals.Type_RelationType)
                    {
                       
                        delete.Add(new clsObjectRel
                        {
                            ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                            ID_RelationType = globals.RelationType_belongingRelationType.GUID
                        });
                        


                    }
                    else if (refItem.Type == globals.Type_Object)
                    {
                        delete.Add(new clsObjectRel
                        {
                            ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                            ID_RelationType = globals.RelationType_belongingObject.GUID
                        });

                    }
                    else if (refItem.Type == globals.Type_Class)
                    {
                        delete.Add(new clsObjectRel
                        {
                            ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                            ID_RelationType = globals.RelationType_belongingClass.GUID
                        });

                    }
                }
                else
                {
                    delete.Add(new clsObjectRel
                    {
                        ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                        ID_RelationType = globals.RelationType_belongingAttribute.GUID
                    });
                    delete.Add(new clsObjectRel
                    {
                        ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                        ID_RelationType = globals.RelationType_belongingClass.GUID
                    });
                    delete.Add(new clsObjectRel
                    {
                        ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                        ID_RelationType = globals.RelationType_belongingRelationType.GUID
                    });
                    delete.Add(new clsObjectRel
                    {
                        ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                        ID_RelationType = globals.RelationType_belongingObject.GUID
                    });

                }

                var dbWriter = new OntologyModDBConnector(globals);
                result = dbWriter.DelObjectRels(delete);

                return result;
            });

            return taskResult;
            
        }

        public async Task<ResultItem<List<clsObjectRel>>> AddClipboardItems(List<clsOntologyItem> itemsToAdd)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var dbWriter = new OntologyModDBConnector(globals);
                var dbReader = new OntologyModDBConnector(globals);

                var attributeTypeItems = itemsToAdd.Where(itm => itm.Type == globals.Type_AttributeType).Select(itm => relationManager.Rel_ObjectRelation(Config.LocalData.Object_Baseconfig, itm, globals.RelationType_belongingAttribute)).ToList();
                var relationTypeItems = itemsToAdd.Where(itm => itm.Type == globals.Type_RelationType).Select(itm => relationManager.Rel_ObjectRelation(Config.LocalData.Object_Baseconfig, itm, globals.RelationType_belongingRelationType)).ToList();
                var classItems = itemsToAdd.Where(itm => itm.Type == globals.Type_Class).Select(itm => relationManager.Rel_ObjectRelation(Config.LocalData.Object_Baseconfig, itm, globals.RelationType_belongingClass)).ToList();
                var objectItems = itemsToAdd.Where(itm => itm.Type == globals.Type_Object).Select(itm => relationManager.Rel_ObjectRelation(Config.LocalData.Object_Baseconfig, itm, globals.RelationType_belongingObject)).ToList();

                if (attributeTypeItems.Any())
                {
                    result.ResultState = dbWriter.SaveObjRel(attributeTypeItems);
                    if (result.ResultState.GUID != globals.LState_Error.GUID)
                    {
                        result.Result.AddRange(attributeTypeItems);
                    }
                }

                if (relationTypeItems.Any())
                {
                    result.ResultState = dbWriter.SaveObjRel(relationTypeItems);
                    if (result.ResultState.GUID != globals.LState_Error.GUID)
                    {
                        result.Result.AddRange(relationTypeItems);
                    }
                }

                if (classItems.Any())
                {
                    result.ResultState = dbWriter.SaveObjRel(classItems);
                    if (result.ResultState.GUID != globals.LState_Error.GUID)
                    {
                        result.Result.AddRange(classItems);
                    }
                }

                if (objectItems.Any())
                {
                    result.ResultState = dbWriter.SaveObjRel(objectItems);
                    if (result.ResultState.GUID != globals.LState_Error.GUID)
                    {
                        result.Result.AddRange(objectItems);
                    }
                }

                if (result.Result.Any())
                {
                    result.ResultState = dbReader.GetDataObjectRel(result.Result);
                    if (result.ResultState.GUID == globals.LState_Success.GUID)
                    {
                        result.Result = dbReader.ObjectRels;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetOItems(List<string> idItems)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>()
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var dbReader = new OntologyModDBConnector(globals);

                var resultOItems = dbReader.GetOItems(idItems);

                result.ResultState = resultOItems.ResultState;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = resultOItems.Result;

                return result;
            });
            return taskResult;
        }

        public ElasticAgentClipboard(Globals globals) : base(globals)
        {
            this.globals = globals;
            relationManager = new clsRelationConfig(globals);
        }
    }

}
