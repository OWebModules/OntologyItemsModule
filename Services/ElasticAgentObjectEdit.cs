﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntologyItemsModule.Validations;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Services
{
    public class ElasticAgentObjectEdit : ElasticBaseAgent
    {
        public async Task<ResultItem<CopyRelationsToDestResult>> CopyRelationsToDest(CopyRelationsModel model, List<clsObjectAtt> searchAtts, List<clsObjectRel> searchRels)
        {
            var taskResult = await Task.Run<ResultItem<CopyRelationsToDestResult>>(() =>
            {
                var result = new ResultItem<CopyRelationsToDestResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new CopyRelationsToDestResult()
                };


                var dbWriter = new OntologyModDBConnector(globals);

                var dbReaderAtts = new OntologyModDBConnector(globals);

                if (searchAtts.Any())
                {
                    result.ResultState = dbReaderAtts.GetDataObjectAtt(searchAtts);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Attributes!";
                        return result;
                    }
                }

                var dbReaderRels = new OntologyModDBConnector(globals);

                if (searchRels.Any())
                {
                    result.ResultState = dbReaderRels.GetDataObjectRel(searchRels);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Relations!";
                        return result;
                    }
                }

                var saveAtts = dbReaderAtts.ObjAtts.Select(att => new clsObjectAtt
                {
                    ID_AttributeType = att.ID_AttributeType,
                    ID_DataType = att.ID_DataType,
                    ID_Object = model.DstObject.GUID,
                    ID_Class = model.DstObject.GUID_Parent,
                    OrderID = att.OrderID,
                    Val_Bit = att.Val_Bit,
                    Val_Datetime = att.Val_Datetime,
                    Val_Real = att.Val_Double,
                    Val_Lng = att.Val_Lng,
                    Val_Named = att.Val_Named,
                    Val_String = att.Val_String
                }).ToList();


                var saveRels = dbReaderRels.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object == model.SrcObject.GUID ? model.DstObject.GUID : rel.ID_Object,
                    ID_Parent_Object = rel.ID_Object == model.SrcObject.GUID ? model.DstObject.GUID_Parent : rel.ID_Parent_Object,
                    ID_Other = rel.ID_Other == model.SrcObject.GUID ? model.DstObject.GUID : rel.ID_Other,
                    ID_Parent_Other = rel.ID_Other == model.SrcObject.GUID ? model.DstObject.GUID_Parent : rel.ID_Parent_Other,
                    ID_RelationType = rel.ID_RelationType,
                    OrderID = rel.OrderID,
                    Ontology = rel.Ontology
                }).ToList();

                if (saveAtts.Any())
                {
                    result.ResultState = dbWriter.SaveObjAtt(saveAtts);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the attributes!";
                        return result;
                    }
                }

                if (saveRels.Any())
                {
                    result.ResultState = dbWriter.SaveObjRel(saveRels);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the relations!";
                        return result;
                    }
                }

                result.Result.CountAttributes = saveAtts.Count;
                result.Result.CountRelations = saveRels.Count;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<CopyRelationsModel>> GetCopyRelationsModel(CopyRelationsRequest request, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<CopyRelationsModel>>(() =>
           {
               var result = new ResultItem<CopyRelationsModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new CopyRelationsModel()
               };

               var searchConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = CopyRelations.Config.LocalData.Class_Copy_Relations.GUID
                   }
               };

               var dbReader = new OntologyModDBConnector(globals);

               result.ResultState = dbReader.GetDataObjects(searchConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the config!";
                   return result;
               }

               result.Result.Config = dbReader.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateGetCopyRelationsModel(result.Result, nameof(result.Result.Config), globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchSource = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = CopyRelations.Config.LocalData.ClassRel_Copy_Relations_src.ID_RelationType
                   }
               };

               var dbReaderSource = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderSource.GetDataObjectRel(searchSource);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the source-element!";
                   return result;
               }

               result.Result.SrcObject = dbReaderSource.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               result.ResultState = ValidationController.ValidateGetCopyRelationsModel(result.Result, nameof(result.Result.SrcObject), globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchDest = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = CopyRelations.Config.LocalData.ClassRel_Copy_Relations_dst.ID_RelationType
                   }
               };

               var dbReaderDest = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderDest.GetDataObjectRel(searchDest);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Dest-element!";
                   return result;
               }

               result.Result.DstObject = dbReaderDest.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               result.ResultState = ValidationController.ValidateGetCopyRelationsModel(result.Result, nameof(result.Result.DstObject), globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchOntology = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = CopyRelations.Config.LocalData.ClassRel_Copy_Relations_belonging_Ontologies.ID_RelationType,
                       ID_Parent_Other = CopyRelations.Config.LocalData.ClassRel_Copy_Relations_belonging_Ontologies.ID_Class_Right
                   }
               };

               var dbReaderOntology = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderOntology.GetDataObjectRel(searchOntology);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the ontology!";
                   return result;
               }

               result.Result.Ontology = dbReaderOntology.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               result.ResultState = ValidationController.ValidateGetCopyRelationsModel(result.Result, nameof(result.Result.Ontology), globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<MoveObjectListItems>> GetMoveObjectListItems(MoveObjectListRequest request)
        {
            var taskResult = await Task.Run<ResultItem<MoveObjectListItems>>(() =>
           {
               var result = new ResultItem<MoveObjectListItems>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new MoveObjectListItems()
               };

               if (string.IsNullOrEmpty(request.IdConfig) && !request.ObjectIds.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The config-id is invalid or no list of object-ids provided!";
                   return result;
               }

               if (!string.IsNullOrEmpty(request.IdConfig) && !globals.is_GUID(request.IdConfig))
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The config-id is invalid!";
                   return result;
               }

               var dbReaderConfig = new OntologyModDBConnector(globals);

               if (!string.IsNullOrEmpty(request.IdConfig))
               {
                   var searchConfig = new List<clsOntologyItem>
                   {
                       new clsOntologyItem
                       {
                           GUID = request.IdConfig
                       }
                   };

                   result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Config!";
                       return result;
                   }

                   result.Result.Config = dbReaderConfig.Objects1.FirstOrDefault();

                   if (result.Result.Config == null)
                   {
                       result.ResultState = globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "No Config found!";
                       return result;
                   }

                   var searchClasses = new List<clsObjectRel>
                   {
                       new clsObjectRel
                       {
                           ID_Object = result.Result.Config.GUID,
                           ID_RelationType = MoveObjectLists.Config.LocalData.ClassRel_Move_Objectlists_belonging_Class.ID_RelationType
                       }
                   };

                   var dbReaderClasses = new OntologyModDBConnector(globals);

                   result.ResultState = dbReaderClasses.GetDataObjectRel(searchClasses);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Class!";
                       return result;
                   }

                   result.Result.DestClass = dbReaderClasses.ObjectRels.Select(rel => new clsOntologyItem
                   {
                       GUID = rel.ID_Other
                   }).FirstOrDefault();

                   if (result.Result.DestClass == null)
                   {
                       result.ResultState = globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "No Dest-Class found!";
                       return result;
                   }

                   var searchDestClass = new List<clsOntologyItem>
                   {
                       new clsOntologyItem
                       {
                           GUID = result.Result.DestClass.GUID
                       }
                   };

                   result.ResultState = dbReaderClasses.GetDataClasses(searchDestClass);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Class!";
                       return result;
                   }

                   result.Result.DestClass = dbReaderClasses.Classes1.FirstOrDefault();

                   if (result.Result.DestClass == null)
                   {
                       result.ResultState = globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "No Dest-Class found!";
                       return result;
                   }

                   if (result.Result.DestClass.Type != globals.Type_Class)
                   {
                       result.ResultState = globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "The Dest-Class is no Class!";
                       return result;
                   }

                   var searchObjects = new List<clsObjectRel>
                   {
                       new clsObjectRel
                       {
                           ID_Object = result.Result.Config.GUID,
                           ID_RelationType = MoveObjectLists.Config.LocalData.ClassRel_Move_Objectlists_belonging_Object.ID_RelationType
                       }
                   };

                   var dbReaderObjects = new OntologyModDBConnector(globals);

                   result.ResultState = dbReaderObjects.GetDataObjectRel(searchObjects);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the objects!";
                       return result;
                   }

                   result.Result.ObjectList = dbReaderObjects.ObjectRels.Select(rel => new clsOntologyItem
                   {
                       GUID = rel.ID_Other,
                       Name = rel.Name_Other,
                       GUID_Parent = rel.ID_Parent_Other,
                       Type = rel.Ontology
                   }).ToList();
               }
               else
               {
                   if (request.IdDestClass == null || !globals.is_GUID(request.IdDestClass))
                   {
                       result.ResultState = globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "IdDestClass is invlid!";
                       return result;
                   }

                   var searchObjects = request.ObjectIds.Select(objId => new clsOntologyItem
                   {
                       GUID = objId
                   }).ToList();

                   var dbReaderObjects = new OntologyModDBConnector(globals);

                   result.ResultState = dbReaderObjects.GetDataObjects(searchObjects);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the objects!";
                       return result;
                   }

                   result.Result.ObjectList = dbReaderObjects.Objects1;

                   var searchClass = new List<clsOntologyItem>
                   {
                       new clsOntologyItem
                       {
                           GUID = request.IdDestClass
                       }
                   };

                   var dbReaderDestClass = new OntologyModDBConnector(globals);
                   result.ResultState = dbReaderDestClass.GetDataClasses(searchClass);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the dest-class!";
                       return result;
                   }

                   result.Result.DestClass = dbReaderDestClass.Classes1.FirstOrDefault();

                   if (result.Result.DestClass ==null)
                   {
                       result.ResultState = globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "No dest-class found!";
                       return result;
                   }
               }               

               if (!result.Result.ObjectList.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No objects found!";
                   return result;
               }

               if (!result.Result.ObjectList.All(obj => obj.Type == globals.Type_Object))
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You have to provide only objects in the object-List!";
                   return result;
               }

               var searchAttributes = result.Result.ObjectList.Select(obj => new clsObjectAtt
               {
                   ID_Object = obj.GUID
               }).ToList();

               var dbReaderAttributes = new OntologyModDBConnector(globals);

               if (searchAttributes.Any())
               {
                   result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Attributes!";
                       return result;
                   }

                   result.Result.ObjectAttributes = dbReaderAttributes.ObjAtts;
               }

               var searchRelations = result.Result.ObjectList.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID
               }).ToList();

               var dbReaderRelationsLeftRight = new OntologyModDBConnector(globals);

               if (searchRelations.Any())
               {
                   result.ResultState = dbReaderRelationsLeftRight.GetDataObjectRel(searchRelations);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Left-right relations!";
                       return result;
                   }

                   result.Result.ObjectRelationsLeftRight = dbReaderRelationsLeftRight.ObjectRels;
               }

               searchRelations = result.Result.ObjectList.Select(obj => new clsObjectRel
               {
                   ID_Other = obj.GUID
               }).ToList();

               var dbReaderRelationsRightLeft = new OntologyModDBConnector(globals);

               if (searchRelations.Any())
               {
                   result.ResultState = dbReaderRelationsRightLeft.GetDataObjectRel(searchRelations);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Right-left relations!";
                       return result;
                   }

                   result.Result.ObjectRelationsRightLeft = dbReaderRelationsRightLeft.ObjectRels;
               }


               return result;
           });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveOntologyItems(OntologyBaseItemLists baseItemLists)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                var classes = baseItemLists.GetClasses(globals);
                if (classes.Any())
                {
                    result = dbWriter.SaveClass(classes);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the classes!";
                        return result;
                    }
                }

                var attributeTypes = baseItemLists.GetAttributeTypes(globals);
                if (attributeTypes.Any())
                {
                    result = dbWriter.SaveAttributeTypes(attributeTypes);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the attributetypes!";
                        return result;
                    }
                }

                var relationTypes = baseItemLists.GetRelationTypes(globals);
                if (relationTypes.Any())
                {
                    result = dbWriter.SaveRelationTypes(relationTypes);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the relationtypes!";
                        return result;
                    }
                }

                var objects = baseItemLists.GetObjects(globals);
                if (objects.Any())
                {
                    result = dbWriter.SaveObjects(objects);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the objects!";
                        return result;
                    }
                }

                if (baseItemLists.ObjectAttributes.Any())
                {
                    result = dbWriter.SaveObjAtt(baseItemLists.ObjectAttributes);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the objectattributes!";
                        return result;
                    }
                }

                if (baseItemLists.ObjectRelations.Any())
                {
                    result = dbWriter.SaveObjRel(baseItemLists.ObjectRelations);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the objectrelations!";
                        return result;
                    }
                }

                if (baseItemLists.ClassAttributes.Any())
                {
                    result = dbWriter.SaveClassAtt(baseItemLists.ClassAttributes);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the classattributes!";
                        return result;
                    }
                }

                if (baseItemLists.ClassRelations.Any())
                {
                    result = dbWriter.SaveClassRel(baseItemLists.ClassRelations);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the classrelations!";
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public ElasticAgentObjectEdit(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}
