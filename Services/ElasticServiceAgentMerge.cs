﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntologyItemsModule.Validations;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Services
{
    public class ElasticServiceAgentMerge : ElasticBaseAgent
    {
        public async Task<ResultItem<MergeObjectsByClassModel>> GetMergeObjectsByClassModel(MergeObjectsRequestByClass request)
        {
            var taskResult = await Task.Run<ResultItem<MergeObjectsByClassModel>>(() =>
           {
               var result = new ResultItem<MergeObjectsByClassModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new MergeObjectsByClassModel()
               };

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<MergeObjectsByItemsModel>> GetMergeObjectsByItemsModel(MergeObjectsRequestByItems request)
        {
            var taskResult = await Task.Run<ResultItem<MergeObjectsByItemsModel>>(() =>
            {
                var result = new ResultItem<MergeObjectsByItemsModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new MergeObjectsByItemsModel()
                };

                var searchRootConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderRoot = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRoot.GetDataObjects(searchRootConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Root-Config!";
                    return result;
                }

                result.Result.RootMergeConfig = dbReaderRoot.Objects1.FirstOrDefault();

                result.ResultState = ValidationController.ValidateMergeObjectsByItemsModel(result.Result, globals, nameof(MergeObjectsByItemsModel.RootMergeConfig));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchSubConfigs = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.RootMergeConfig.GUID,
                        ID_RelationType = MergeObjects.Config.LocalData.ClassRel_Merge_Objects_By_Items_contains_Merge_Objects_By_Items.ID_RelationType,
                        ID_Parent_Other = MergeObjects.Config.LocalData.ClassRel_Merge_Objects_By_Items_contains_Merge_Objects_By_Items.ID_Class_Right
                    }
                };

                var dbReaderSubConfigs = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the sub-configs!";
                    return result;
                }

                result.Result.MergeConfigs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                if (!result.Result.MergeConfigs.Any())
                {
                    result.Result.MergeConfigs.Add(result.Result.RootMergeConfig);
                }

                var searchNamingRules = result.Result.MergeConfigs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = MergeObjects.Config.LocalData.ClassRel_Merge_Objects_By_Class_belonging_Merge_Naming_Rule.ID_RelationType,
                    ID_Parent_Other = MergeObjects.Config.LocalData.ClassRel_Merge_Objects_By_Class_belonging_Merge_Naming_Rule.ID_Class_Right
                }).ToList();

                var dbReaderNamingRules = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderNamingRules.GetDataObjectRel(searchNamingRules);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the naming rules!";
                    return result;
                }

                result.Result.NamingRules = dbReaderNamingRules.ObjectRels;

                result.ResultState = ValidationController.ValidateMergeObjectsByItemsModel(result.Result, globals, nameof(MergeObjectsByItemsModel.NamingRules));
                
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchArchiveClasses = result.Result.MergeConfigs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = MergeObjects.Config.LocalData.ClassRel_Merge_Objects_By_Class_archive_class.ID_RelationType
                }).ToList();

                var dbReaderArchiveClasses = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderArchiveClasses.GetDataObjectRel(searchArchiveClasses);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the archive-classes!";
                    return result;
                }

                result.Result.ArchiveClasses = dbReaderArchiveClasses.ObjectRels;

                result.ResultState = ValidationController.ValidateMergeObjectsByItemsModel(result.Result, globals, nameof(MergeObjectsByItemsModel.ArchiveClasses));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchHeads = result.Result.MergeConfigs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = MergeObjects.Config.LocalData.ClassRel_Merge_Objects_By_Items_head.ID_RelationType,
                }).ToList();

                var dbReaderHeads = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderHeads.GetDataObjectRel(searchHeads);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the head-items!";
                    return result;
                }

                result.Result.MergeHeads = dbReaderHeads.ObjectRels;

                result.ResultState = ValidationController.ValidateMergeObjectsByItemsModel(result.Result, globals, nameof(MergeObjectsByItemsModel.MergeHeads));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchMerges = result.Result.MergeConfigs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = MergeObjects.Config.LocalData.ClassRel_Merge_Objects_By_Items_merge.ID_RelationType,
                }).ToList();

                var dbReaderMerges = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderMerges.GetDataObjectRel(searchMerges);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the head-items!";
                    return result;
                }

                result.Result.MergeMerges = dbReaderMerges.ObjectRels;

                result.ResultState = ValidationController.ValidateMergeObjectsByItemsModel(result.Result, globals, nameof(MergeObjectsByItemsModel.MergeMerges));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                return result;
            });

            return taskResult;
        }

        public ElasticServiceAgentMerge(Globals globals):base(globals)
        {
            
        }
    }
}
