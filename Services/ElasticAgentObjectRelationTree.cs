﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntologyItemsModule.Validations;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Services
{
    public class ElasticAgentObjectRelationTree : ElasticBaseAgent
    {
        public async Task<ResultItem<ObjectRelationTreeModel>> GetObjectRelationTreeModel(GetObjectRelationTreeRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<ObjectRelationTreeModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ObjectRelationTreeModel()
                };

                if (!string.IsNullOrEmpty(request.IdObject))
                {
                    var getOItemResult = await GetOItem(request.IdObject, globals.Type_Object);

                    result.ResultState = getOItemResult.ResultState;

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.ReferenceItem = getOItemResult.Result;
                }

                var dbReaderRelationsToShow = new OntologyModDBConnector(globals);
                if (result.Result.ReferenceItem != null)
                {
                    var searchRelationsToShow = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Other = result.Result.ReferenceItem.GUID_Parent,
                            ID_RelationType = ObjectRelationTree.Config.LocalData.ClassRel_Relation_to_Show_Main_Help_Class.ID_RelationType,
                            ID_Parent_Object = ObjectRelationTree.Config.LocalData.ClassRel_Relation_to_Show_Main_Help_Class.ID_Class_Left
                        },
                        new clsObjectRel
                        {
                            ID_Other = result.Result.ReferenceItem.GUID_Parent,
                            ID_RelationType = ObjectRelationTree.Config.LocalData.ClassRel_Relation_to_Show_Relation_Classes.ID_RelationType,
                            ID_Parent_Object = ObjectRelationTree.Config.LocalData.ClassRel_Relation_to_Show_Relation_Classes.ID_Class_Left
                        }
                    };

                    result.ResultState = dbReaderRelationsToShow.GetDataObjectRel(searchRelationsToShow);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the relations to show objects";
                        return result;
                    }

                    result.ResultState = ValidationController.ValidateAndSetObjectRelationTreeModel(result.Result, dbReaderRelationsToShow, globals, nameof(ObjectRelationTreeModel.ReferenceItemsToShowRelationObjects));
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
                else
                {
                    var searchRelationsToShow = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            GUID_Parent = ObjectRelationTree.Config.LocalData.Class_Relation_to_Show.GUID
                        }
                    };

                    result.ResultState = dbReaderRelationsToShow.GetDataObjects(searchRelationsToShow);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the relations to show objects";
                        return result;
                    }

                    result.ResultState = ValidationController.ValidateAndSetObjectRelationTreeModel(result.Result, dbReaderRelationsToShow, globals, nameof(ObjectRelationTreeModel.ShowRelationObjects));
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                var searchAttributes = result.Result.ShowRelationObjects.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.GUID,
                    ID_AttributeType = ObjectRelationTree.Config.LocalData.ClassAtt_Relation_to_Show_Help_Left_Right.ID_AttributeType
                }).ToList();

                searchAttributes.AddRange( result.Result.ShowRelationObjects.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.GUID,
                    ID_AttributeType = ObjectRelationTree.Config.LocalData.ClassAtt_Relation_to_Show_Left_Right.ID_AttributeType
                }));

                searchAttributes.AddRange(result.Result.ShowRelationObjects.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.GUID,
                    ID_AttributeType = ObjectRelationTree.Config.LocalData.ClassAtt_Relation_to_Show_Use_Nameprovider.ID_AttributeType
                }));

                var dbReaderAttributes = new OntologyModDBConnector(globals);

                if (searchAttributes.Any())
                {
                    result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the relations to show objects";
                        return result;
                    }

                }

                result.ResultState = ValidationController.ValidateAndSetObjectRelationTreeModel(result.Result, dbReaderAttributes, globals, nameof(ObjectRelationTreeModel.HelpLeftRight));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                result.ResultState = ValidationController.ValidateAndSetObjectRelationTreeModel(result.Result, dbReaderAttributes, globals, nameof(ObjectRelationTreeModel.LeftRight));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                result.ResultState = ValidationController.ValidateAndSetObjectRelationTreeModel(result.Result, dbReaderAttributes, globals, nameof(ObjectRelationTreeModel.UseNameprovider));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchCssClass = result.Result.ShowRelationObjects.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = ObjectRelationTree.Config.LocalData.ClassRel_Relation_to_Show_Image_Class_CSS_Class.ID_RelationType,
                    ID_Parent_Other = ObjectRelationTree.Config.LocalData.ClassRel_Relation_to_Show_Image_Class_CSS_Class.ID_Class_Right
                }).ToList();

                var dbReaderCssClass = new OntologyModDBConnector(globals);

                if (searchCssClass.Any())
                {
                    result.ResultState = dbReaderCssClass.GetDataObjectRel(searchCssClass);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Css-Classes!";
                        return result;
                    }
                }
                result.ResultState = ValidationController.ValidateAndSetObjectRelationTreeModel(result.Result, dbReaderCssClass, globals, nameof(ObjectRelationTreeModel.ShowRelationToCssClasses));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchMainHelpClasses = result.Result.ShowRelationObjects.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = ObjectRelationTree.Config.LocalData.ClassRel_Relation_to_Show_Main_Help_Class.ID_RelationType
                }).ToList();

                var dbReaderMainHelpClasses = new OntologyModDBConnector(globals);

                if (searchMainHelpClasses.Any())
                {
                    result.ResultState = dbReaderMainHelpClasses.GetDataObjectRel(searchMainHelpClasses);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Main-Help-Classes!";
                        return result;
                    }
                }
                result.ResultState = ValidationController.ValidateAndSetObjectRelationTreeModel(result.Result, dbReaderMainHelpClasses, globals, nameof(ObjectRelationTreeModel.ShowRelationToMainHelpClasses));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchRelationClasses = result.Result.ShowRelationObjects.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = ObjectRelationTree.Config.LocalData.ClassRel_Relation_to_Show_Relation_Classes.ID_RelationType
                }).ToList();

                var dbReaderRelationClasses = new OntologyModDBConnector(globals);

                if (searchRelationClasses.Any())
                {
                    result.ResultState = dbReaderRelationClasses.GetDataObjectRel(searchRelationClasses);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Relation-Classes!";
                        return result;
                    }
                }
                result.ResultState = ValidationController.ValidateAndSetObjectRelationTreeModel(result.Result, dbReaderRelationClasses, globals, nameof(ObjectRelationTreeModel.ShowRelationToRelationClassesFirst));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchRelationTypesMainHelp = result.Result.ShowRelationObjects.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = ObjectRelationTree.Config.LocalData.ClassRel_Relation_to_Show_Relationtype_Main_Help.ID_RelationType
                }).ToList();

                var dbReaderRelationTypesMainHelp = new OntologyModDBConnector(globals);

                if (searchRelationTypesMainHelp.Any())
                {
                    result.ResultState = dbReaderRelationTypesMainHelp.GetDataObjectRel(searchRelationTypesMainHelp);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the RelationTypes  for Main-Help-Classes!";
                        return result;
                    }
                }
                result.ResultState = ValidationController.ValidateAndSetObjectRelationTreeModel(result.Result, dbReaderRelationTypesMainHelp, globals, nameof(ObjectRelationTreeModel.ShowRelationToRelationTypesMainHelp));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchRelationTypesRelationClasses = result.Result.ShowRelationObjects.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = ObjectRelationTree.Config.LocalData.ClassRel_Relation_to_Show_Relationtype_Relation_Clases.ID_RelationType
                }).ToList();

                var dbReaderRelationTypesRelationClasses = new OntologyModDBConnector(globals);

                if (searchRelationTypesRelationClasses.Any())
                {
                    result.ResultState = dbReaderRelationTypesRelationClasses.GetDataObjectRel(searchRelationTypesRelationClasses);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the RelationTypes  for Relation-Classes!";
                        return result;
                    }
                }
                result.ResultState = ValidationController.ValidateAndSetObjectRelationTreeModel(result.Result, dbReaderRelationTypesRelationClasses, globals, nameof(ObjectRelationTreeModel.ShowRelationToRelationTypesRelationClasses));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.ResultState = await EnrichObjectRelationTreeModel(request, result.Result);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        private async Task<clsOntologyItem> EnrichObjectRelationTreeModel(GetObjectRelationTreeRequest request, ObjectRelationTreeModel model)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbReaderHelpMain = new OntologyModDBConnector(globals);

                foreach (var searchObject in model.ShowRelationObjects)
                {
                    var helpLeftRight = model.HelpLeftRight.FirstOrDefault(helpLeftR => helpLeftR.ID_Object == searchObject.GUID).Val_Bool.Value;
                    var leftRight = model.LeftRight.FirstOrDefault(leftR => leftR.ID_Object == searchObject.GUID).Val_Bool.Value;

                    var relationClass1 = model.ShowRelationToRelationClassesFirst.First(cls => cls.ID_Object == searchObject.GUID);
                    var relationClass2 = model.ShowRelationToRelationClassesSecond.First(cls => cls.ID_Object == searchObject.GUID);
                    var mainHelpClass = model.ShowRelationToMainHelpClasses.FirstOrDefault(mainHelp => mainHelp.ID_Object == searchObject.GUID);
                    var mainHelpRelationType = model.ShowRelationToRelationTypesMainHelp.FirstOrDefault(mainHelp => mainHelp.ID_Object == searchObject.GUID);
                    var relationClassRelationType = model.ShowRelationToRelationTypesRelationClasses.FirstOrDefault(mainHelp => mainHelp.ID_Object == searchObject.GUID);
                    var searchHelpMainRelations = new List<clsObjectRel>();
                    if (mainHelpClass != null && mainHelpRelationType != null)
                    {
                        if (helpLeftRight)
                        {
                            searchHelpMainRelations.Add(new clsObjectRel
                            {
                                ID_Other = model.ReferenceItem?.GUID,
                                ID_Parent_Other = model.ReferenceItem?.GUID_Parent ?? mainHelpClass.ID_Object,
                                ID_RelationType = mainHelpRelationType.ID_Other,
                                ID_Parent_Object = relationClass1.ID_Other
                            });
                        }
                        else
                        {
                            searchHelpMainRelations.Add(new clsObjectRel
                            {
                                ID_Object = model.ReferenceItem?.GUID,
                                ID_Parent_Object = model.ReferenceItem?.GUID_Parent ?? mainHelpClass.ID_Object,
                                ID_RelationType = mainHelpRelationType.ID_Other,
                                ID_Parent_Other = relationClass1.ID_Other
                            });
                        }
                    }

                    result = dbReaderHelpMain.GetDataObjectRel(searchHelpMainRelations);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = $"Error while Enrich of object {searchObject.Name}!";
                        return result;
                    }

                    var searchRelated = new List<clsObjectRel>();
                    if (dbReaderHelpMain.ObjectRels.Any())
                    {
                        if (helpLeftRight)
                        {
                            if (leftRight)
                            {
                                searchRelated.AddRange(dbReaderHelpMain.ObjectRels.Select(rel => new clsObjectRel
                                {
                                    ID_Object = rel.ID_Object,
                                    ID_RelationType = relationClassRelationType.ID_Other,
                                    ID_Parent_Other = relationClass2.ID_Other
                                }));
                            }
                            else
                            {
                                searchRelated.AddRange(dbReaderHelpMain.ObjectRels.Select(rel => new clsObjectRel
                                {
                                    ID_Other = rel.ID_Object,
                                    ID_RelationType = relationClassRelationType.ID_Other,
                                    ID_Parent_Object = relationClass2.ID_Other
                                }));
                            }
                        }
                        else
                        {
                            if (leftRight)
                            {
                                searchRelated.AddRange(dbReaderHelpMain.ObjectRels.Select(rel => new clsObjectRel
                                {
                                    ID_Object = rel.ID_Other,
                                    ID_RelationType = relationClassRelationType.ID_Other,
                                    ID_Parent_Other = relationClass2.ID_Other
                                }));
                            }
                            else
                            {
                                searchRelated.AddRange(dbReaderHelpMain.ObjectRels.Select(rel => new clsObjectRel
                                {
                                    ID_Other = rel.ID_Other,
                                    ID_RelationType = relationClassRelationType.ID_Other,
                                    ID_Parent_Object = relationClass2.ID_Other
                                }));
                            }
                        }
                    }

                    var dbReaderRelated = new OntologyModDBConnector(globals);

                    if (searchRelated.Any())
                    {
                        result = dbReaderRelated.GetDataObjectRel(searchRelated);

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            result.Additional1 = $"Error while getting the related items: {result.Additional1 ?? string.Empty}";
                            return result;
                        }
                    }

                    if (dbReaderRelated.ObjectRels.Any())
                    {
                        model.RelatedItems.AddRange(dbReaderRelated.ObjectRels.Select(rel => new RelatedItem
                        {
                            ReferenceObject = model.ReferenceItem,
                            ShowObjectItem = searchObject,
                            Related = leftRight ? new clsOntologyItem
                            {
                                GUID = rel.ID_Other,
                                Name = rel.Name_Other,
                                GUID_Parent = rel.ID_Parent_Other,
                                Type = rel.Ontology
                            } : new clsOntologyItem
                            {
                                GUID = rel.ID_Object,
                                Name = rel.Name_Object,
                                GUID_Parent = rel.Name_Parent_Object,
                                Type = globals.Type_Object
                            },
                            OrderId = rel.OrderID.Value
                        }));
                    }
                }

                return result;
            });

            return taskResult;
        }

        public ElasticAgentObjectRelationTree(Globals globals) : base(globals)
        {

        }
    }
}
