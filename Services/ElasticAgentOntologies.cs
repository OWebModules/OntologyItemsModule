﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Services
{
    public class ElasticAgentOntologies : ElasticBaseAgent
    {
        private clsRelationConfig relationConfig;

        public ResultItem<clsOntologyItem> GetOItem(string id, string type)
        {

            var dbReader = new OntologyModDBConnector(globals);
            var resultOItem = dbReader.GetOItem(id, type);
            var result = new ResultItem<clsOntologyItem>()
            {
                ResultState = globals.LState_Success.Clone()
            };
            if (resultOItem.GUID_Related == globals.LState_Error.GUID)
            {
                result.ResultState = globals.LState_Error.Clone();
            }

            result.Result = resultOItem;
            return result;

        }

        public async Task<clsOntologyItem> RelateOItems(Ontology ontology, List<clsOntologyItem> refItems)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbWriter = new OntologyModDBConnector(globals);
                var result = globals.LState_Success.Clone();

                var oItems = refItems.Select(refItem => new clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = refItem.Name,
                    GUID_Parent = globals.Class_OntologyItems.GUID,
                    Type = globals.Type_Object,
                    GUID_Related = refItem.GUID
                }).ToList();

                if (oItems.Any())
                {
                    result = dbWriter.SaveObjects(oItems);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                }

                var relOntologyToOItems = oItems.Select(oItem => relationConfig.Rel_ObjectRelation(ontology.OntologyEntity, oItem, globals.RelationType_contains)).ToList();



                if (relOntologyToOItems.Any())
                {




                    result = dbWriter.SaveObjRel(relOntologyToOItems);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var oItemsToRefItem = (from oItem in oItems
                                           join refItem in refItems on oItem.GUID_Related equals refItem.GUID
                                           select new { oItem, refItem });

                    var oItemsToObjectItems = oItemsToRefItem.Where(oToRef => oToRef.refItem.Type == globals.Type_Object).Select(oToRef => relationConfig.Rel_ObjectRelation(oToRef.oItem, oToRef.refItem, globals.RelationType_belongingObject)).ToList();
                    var oItemsToClassItems = oItemsToRefItem.Where(oToRef => oToRef.refItem.Type == globals.Type_Class).Select(oToRef => relationConfig.Rel_ObjectRelation(oToRef.oItem, oToRef.refItem, globals.RelationType_belongingClass)).ToList();
                    var oItemsToAttributeTypeItems = oItemsToRefItem.Where(oToRef => oToRef.refItem.Type == globals.Type_AttributeType).Select(oToRef => relationConfig.Rel_ObjectRelation(oToRef.oItem, oToRef.refItem, globals.RelationType_belongingAttribute)).ToList();
                    var oItemsToRelationTypeItems = oItemsToRefItem.Where(oToRef => oToRef.refItem.Type == globals.Type_RelationType).Select(oToRef => relationConfig.Rel_ObjectRelation(oToRef.oItem, oToRef.refItem, globals.RelationType_belongingRelationType)).ToList();

                    if (oItemsToObjectItems.Any())
                    {
                        result = dbWriter.SaveObjRel(oItemsToObjectItems);
                    }

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    if (oItemsToClassItems.Any())
                    {
                        result = dbWriter.SaveObjRel(oItemsToClassItems);
                    }

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    if (oItemsToAttributeTypeItems.Any())
                    {
                        result = dbWriter.SaveObjRel(oItemsToAttributeTypeItems);
                    }

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    if (oItemsToRelationTypeItems.Any())
                    {
                        result = dbWriter.SaveObjRel(oItemsToRelationTypeItems);
                    }

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    ontology.OntologyItems = oItemsToRefItem.Select(oItem => new OntologyAppDBConnector.Models.OntologyItem
                    {
                        OItem = oItem.oItem,
                        RefItem = oItem.refItem
                    }).ToList();
                }



                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> CheckOntology(Ontology ontology)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                if (ontology.OntologyEntity.New_Item != null && ontology.OntologyEntity.New_Item.Value)
                {
                    var dbWriter = new OntologyModDBConnector(globals);
                    var result = dbWriter.SaveObjects(new List<clsOntologyItem> { ontology.OntologyEntity });

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    if (ontology.OntologyEntity.New_Item != null && ontology.OntologyEntity.New_Item.Value)
                    {
                        var saveRefItem = relationConfig.Rel_ObjectRelation(ontology.OntologyEntity, ontology.RefItem, globals.RelationType_belongingResource);
                        result = dbWriter.SaveObjRel(new List<clsObjectRel> { saveRefItem });
                    }

                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        ontology.OntologyEntity.New_Item = false;
                    }
                    return result;
                }
                else
                {
                    return globals.LState_Success.Clone();
                }
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetClassesByGuid(List<string> idsClasses)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);

                var searchClasses = idsClasses.Select(id => new clsOntologyItem
                {
                    GUID = id
                }).ToList();

                var result = new ResultItem<List<clsOntologyItem>>()
                {
                    ResultState = dbReader.GetDataClasses(searchClasses)
                };

                result.Result = dbReader.Classes1;
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<OntologyRaw>> GetOntology(GetOntologyRequest getOntologyRequest)
        {
            var taskResult = await Task.Run<ResultItem<OntologyRaw>>(() =>
            {

                
                var result = new ResultItem<OntologyRaw>
                {
                    Result = new OntologyRaw
                    {
                        RefItem = getOntologyRequest.RefItem
                    },
                    ResultState = globals.LState_Success.Clone()
                };

                var getOItemResult = GetOItem(getOntologyRequest.RefItem.GUID, globals.Type_Object);
                result.ResultState = getOItemResult.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.RefItem = getOItemResult.Result;

                if (getOntologyRequest.DirectionRefOntology == null)
                {
                    getOntologyRequest.DirectionRefOntology = globals.Direction_RightLeft;
                }

                if (getOntologyRequest.RelationTypeRefOntology == null)
                {
                    getOntologyRequest.RelationTypeRefOntology = globals.RelationType_belongingResource;
                }


                if (getOntologyRequest.RefItem.GUID_Parent != globals.Class_Ontologies.GUID)
                {
                    var dbReaderOntologyToRef = new OntologyModDBConnector(globals);
                    if (getOntologyRequest.GetClassOntology)
                    {
                        var classItemResult = GetOItem(getOntologyRequest.RefItem.GUID_Parent, globals.Type_Class);

                        if (classItemResult.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState = globals.LState_Error.Clone();

                            return result;
                        }

                        result.Result.RefItem = classItemResult.Result;

                    }

                    var searchOntology = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Object = getOntologyRequest.DirectionRefOntology.GUID == globals.Direction_LeftRight.GUID ? result.Result.RefItem.GUID : null,
                                ID_Other = getOntologyRequest.DirectionRefOntology.GUID == globals.Direction_LeftRight.GUID ? null : result.Result.RefItem.GUID,
                                ID_RelationType = getOntologyRequest.RelationTypeRefOntology.GUID,
                                ID_Parent_Object = getOntologyRequest.DirectionRefOntology.GUID == globals.Direction_LeftRight.GUID ? null : globals.Class_Ontologies.GUID,
                                ID_Parent_Other = getOntologyRequest.DirectionRefOntology.GUID == globals.Direction_LeftRight.GUID ? globals.Class_Ontologies.GUID : null
                            }
                        };



                    result.ResultState = dbReaderOntologyToRef.GetDataObjectRel(searchOntology);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    
                    if (dbReaderOntologyToRef.ObjectRels.Any())
                    {

                        result.Result.Ontologies = dbReaderOntologyToRef.ObjectRels.Select(ontoRel => new clsOntologyItem
                        {
                            GUID = getOntologyRequest.DirectionRefOntology.GUID == globals.Direction_RightLeft.GUID ? ontoRel.ID_Object : ontoRel.ID_Other,
                            Name = getOntologyRequest.DirectionRefOntology.GUID == globals.Direction_RightLeft.GUID ? ontoRel.Name_Object : ontoRel.Name_Other,
                            GUID_Parent = getOntologyRequest.DirectionRefOntology.GUID == globals.Direction_RightLeft.GUID ? ontoRel.ID_Parent_Object : ontoRel.ID_Parent_Other,
                            Type = globals.Type_Object
                        }).ToList();


                    }
                    else
                    {
                        result.Result.Ontologies = new List<clsOntologyItem> {
                            new clsOntologyItem
                            {
                                GUID = globals.NewGUID,
                                Name = result.Result.RefItem.Name,
                                GUID_Parent = globals.Class_Ontologies.GUID,
                                Type = globals.Type_Object,
                                New_Item = true
                            }
                        };
                    }
                }
                else
                {
                    result.Result.Ontologies = new List<clsOntologyItem>
                    {
                        getOItemResult.Result
                    };
                }


                var searchOJoins = result.Result.Ontologies.Select(onto => new clsObjectRel
                {

                    ID_Object = onto.GUID,
                    ID_RelationType = globals.RelationType_contains.GUID,
                    ID_Parent_Other = globals.Class_OntologyJoin.GUID


                }).ToList();

                var dbReaderJoin = new OntologyModDBConnector(globals);

                if (searchOJoins.Any())
                {
                    result.ResultState = dbReaderJoin.GetDataObjectRel(searchOJoins);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
                
                result.Result.OntologiesToJoins = dbReaderJoin.ObjectRels;

                var searchOItemsJoins = dbReaderJoin.ObjectRels.Select(oJoinRel => new clsObjectRel
                {
                    ID_Object = oJoinRel.ID_Other,
                    ID_RelationType = globals.RelationType_contains.GUID,
                    ID_Parent_Other = globals.Class_OntologyItems.GUID
                }).ToList();

                var dbReaderOItemsJoins = new OntologyModDBConnector(globals);
                if (searchOItemsJoins.Any())
                {
                    result.ResultState = dbReaderOItemsJoins.GetDataObjectRel(searchOItemsJoins);
                }

                result.Result.JoinsToOItems = dbReaderOItemsJoins.ObjectRels;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchOItemsOntology = result.Result.Ontologies.Select(onto => new clsObjectRel
                { 
                        ID_Object = onto.GUID,
                        ID_RelationType = globals.RelationType_contains.GUID,
                        ID_Parent_Other = globals.Class_OntologyItems.GUID
                
                }).ToList();

                var dbReaderOItemsOntology = new OntologyModDBConnector(globals);

                if (searchOItemsOntology.Any())
                {
                    result.ResultState = dbReaderOItemsOntology.GetDataObjectRel(searchOItemsOntology);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.OntologiesToOItems = dbReaderOItemsOntology.ObjectRels;

                var oItems = dbReaderOItemsJoins.ObjectRels;
                oItems.AddRange(dbReaderOItemsOntology.ObjectRels);
                var searchRefItems = oItems.Select(oItem => new clsObjectRel
                {
                    ID_Object = oItem.ID_Other,
                    ID_RelationType = globals.RelationType_belongingAttribute.GUID

                }).ToList();

                searchRefItems.AddRange(oItems.Select(oItem => new clsObjectRel
                {
                    ID_Object = oItem.ID_Other,
                    ID_RelationType = globals.RelationType_belongingClass.GUID

                }));

                searchRefItems.AddRange(oItems.Select(oItem => new clsObjectRel
                {
                    ID_Object = oItem.ID_Other,
                    ID_RelationType = globals.RelationType_belongingObject.GUID

                }));

                searchRefItems.AddRange(oItems.Select(oItem => new clsObjectRel
                {
                    ID_Object = oItem.ID_Other,
                    ID_RelationType = globals.RelationType_belongingRelationType.GUID

                }));

                var dbReaderOitemsToRef = new OntologyModDBConnector(globals);

                if (searchRefItems.Any())
                {
                    result.ResultState = dbReaderOitemsToRef.GetDataObjectRel(searchRefItems);
                }

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.OItemsToRefs = dbReaderOitemsToRef.ObjectRels;

                var searchJoinToRule = result.Result.OntologiesToJoins.Select(join => new clsObjectRel
                {
                    ID_Object = join.ID_Other,
                    ID_RelationType = globals.RelationType_contains.GUID,
                    ID_Parent_Other = globals.Class_OntologyRelationRule.GUID
                }).ToList();

                var dbReaderJoinsToRules = new OntologyModDBConnector(globals);

                if (searchJoinToRule.Any())
                {
                    result.ResultState = dbReaderJoinsToRules.GetDataObjectRel(searchJoinToRule);
                }

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.JoinsToRule = dbReaderJoinsToRules.ObjectRels;

                var searchOItemToRule = result.Result.OntologiesToOItems.Select(oItem => new clsObjectRel
                {
                    ID_Object = oItem.ID_Other,
                    ID_RelationType = globals.RelationType_contains.GUID,
                    ID_Parent_Other = globals.Class_OntologyRelationRule.GUID
                }).ToList();

                var dbReaderOItemsToRule = new OntologyModDBConnector(globals);

                if (searchOItemToRule.Any())
                {
                    result.ResultState = dbReaderOItemsToRule.GetDataObjectRel(searchOItemToRule);
                }

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.OItemsToRule = dbReaderOItemsToRule.ObjectRels;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                if (getOntologyRequest.RefItem.Type == globals.Type_Object)
                {
                    var searchClassAttributes = new List<clsClassAtt>
                    {
                        new clsClassAtt
                        {
                            ID_Class = getOntologyRequest.RefItem.GUID_Parent
                        }
                    };

                    var dbReaderClassAttributes = new OntologyModDBConnector(globals);
                    if (searchClassAttributes.Any())
                    {
                        result.ResultState = dbReaderClassAttributes.GetDataClassAtts(searchClassAttributes);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Result.ClassAttributes = dbReaderClassAttributes.ClassAtts;
                    }

                    var searchClassRels = new List<clsClassRel>
                    {
                        new clsClassRel
                        {
                            ID_Class_Left = getOntologyRequest.RefItem.GUID_Parent
                        }
                    };

                    var dbReaderClassRels = new OntologyModDBConnector(globals);
                    if (searchClassRels.Any())
                    {
                        result.ResultState = dbReaderClassRels.GetDataClassRel(searchClassRels);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Result.ClassRelations = dbReaderClassRels.ClassRels;

                    }

                    searchClassRels = new List<clsClassRel>
                    {
                        new clsClassRel
                        {
                            ID_Class_Right = getOntologyRequest.RefItem.GUID_Parent
                        }
                    };

                    var dbReaderRightLeftClasses = new OntologyModDBConnector(globals);
                    if (searchClassRels.Any())
                    {
                        result.ResultState = dbReaderRightLeftClasses.GetDataClassRel(searchClassRels);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Result.ClassRelations.AddRange(dbReaderRightLeftClasses.ClassRels);

                    }
                }

                return result;
            });
            return taskResult;
        }

        public ElasticAgentOntologies(Globals globals) : base(globals)
        {
            this.relationConfig = new clsRelationConfig(globals);
        }
    }

    public class OntologyRaw
    {
        public List<clsOntologyItem> Ontologies { get; set; }
        public clsOntologyItem RefItem { get; set; }
        public List<clsObjectRel> OntologiesToJoins { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> JoinsToOItems { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> JoinsToRule { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> OntologiesToOItems { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> OItemsToRefs { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> OItemsToRule { get; set; } = new List<clsObjectRel>();

        public List<clsClassAtt> ClassAttributes { get; set; } = new List<clsClassAtt>();
        public List<clsClassRel> ClassRelations { get; set; } = new List<clsClassRel>();
    }

    public class GetOntologyRequest
    {
        public clsOntologyItem RefItem { get; set; }
        public bool GetClassOntology { get; set; }
        public clsOntologyItem DirectionRefOntology { get; set; }
        public clsOntologyItem RelationTypeRefOntology { get; set; }

        public GetOntologyRequest(clsOntologyItem refItem)
        {
            RefItem = refItem;
        }
    }

    public class ExportOntologyRequest : GetOntologyRequest
    {
        public string ExportPath { get; set; }
        public bool ZipFiles { get; set; }
        public ExportOntologyRequest(clsOntologyItem refItem) : base(refItem)
        {

        }
    }
}
