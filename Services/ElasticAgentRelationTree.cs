﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Services
{

    public class ElasticAgentRelationTree : ElasticBaseAgent
    {
        private object serviceLocker = new object();


        private OItemResult resultOItem;
        public OItemResult ResultOItem
        {
            get
            {
                lock (serviceLocker)
                {
                    return resultOItem;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    resultOItem = value;
                }
                //RaisePropertyChanged(nameof(ResultOItem));
            }
        }

        private ResultRelationTreeAtt resultRelationTreeAtt;
        public ResultRelationTreeAtt ResultRelationTreeAtt
        {
            get
            {
                lock (serviceLocker)
                {
                    return resultRelationTreeAtt;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    resultRelationTreeAtt = value;
                }
                //RaisePropertyChanged(nameof(ResultRelationTreeAtt));
            }
        }

        private ResultRelationTreeRel _resultRelationTreeRelLeftRight;
        public ResultRelationTreeRel ResultRelationTreeRelLeftRight
        {
            get
            {
                lock (serviceLocker)
                {
                    return _resultRelationTreeRelLeftRight;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    _resultRelationTreeRelLeftRight = value;
                }
                //RaisePropertyChanged(nameof(ResultRelationTreeRelLeftRight));
            }
        }

        private ResultRelationTreeRel _resultRelationTreeRelRightLeft;
        public ResultRelationTreeRel ResultRelationTreeRelRightLeft
        {
            get
            {
                lock (serviceLocker)
                {
                    return _resultRelationTreeRelRightLeft;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    _resultRelationTreeRelRightLeft = value;
                }
                //RaisePropertyChanged(nameof(ResultRelationTreeRelRightLeft));
            }
        }

        private ResultRelationTreeRel _resultRelationTreeRelLeftOther;
        public ResultRelationTreeRel ResultRelationTreeRelLeftOther
        {
            get
            {
                lock (serviceLocker)
                {
                    return _resultRelationTreeRelLeftOther;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    _resultRelationTreeRelLeftOther = value;
                }
                //RaisePropertyChanged(nameof(ResultRelationTreeRelLeftOther));
            }
        }

        private ResultRelationTreeRel _resultRelationTreeRelRightOther;
        public ResultRelationTreeRel ResultRelationTreeRelRightOther
        {
            get
            {
                lock (serviceLocker)
                {
                    return _resultRelationTreeRelRightOther;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    _resultRelationTreeRelRightOther = value;
                }
                //RaisePropertyChanged(nameof(ResultRelationTreeRelRightOther));
            }
        }

        private ResultOItems _resultObjects;
        public ResultOItems ResultOItems
        {
            get
            {
                lock (serviceLocker)
                {
                    return _resultObjects;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    _resultObjects = value;
                }
                //RaisePropertyChanged(nameof(ResultOItems));
            }
        }

        private ResultRelateExists _resultRelateExists;
        public ResultRelateExists ResultRelateExists
        {
            get
            {
                lock (serviceLocker)
                {
                    return _resultRelateExists;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    _resultRelateExists = value;
                }
                //RaisePropertyChanged(nameof(ResultRelateExists));
            }
        }

        private ResultRelated _resultRelated;
        public ResultRelated ResultRelated
        {
            get
            {
                lock (serviceLocker)
                {
                    return _resultRelated;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    _resultRelated = value;
                }
                //RaisePropertyChanged(nameof(ResultRelated));
            }
        }

        public async Task<OItemResult> GetOItem(string guid, string type)
        {
            var taskResult = await Task.Run<OItemResult>(() =>
            {
                var result = new OItemResult
                {
                    Result = globals.LState_Success.Clone()
                };

                var dbReader = new OntologyModDBConnector(globals);
                var oItem = dbReader.GetOItem(guid, type);
                result.OItem = oItem;
                if (oItem == null || oItem.GUID == globals.LState_Error.GUID)
                {
                    result.Result = globals.LState_Error.Clone();
                }
                ResultOItem = result;
                return result;
            });

            return taskResult;
        }

        public async Task<ResultOItems> GetObjects(clsOntologyItem[] items)
        {
            var taskResult = await Task.Run<ResultOItems>(() =>
            {
                var result = new ResultOItems
                {
                    Result = globals.LState_Success.Clone()
                };

                var dbReader = new OntologyModDBConnector(globals);

                var resultDbReader = dbReader.GetDataObjects(items.ToList());

                if (resultDbReader.GUID == globals.LState_Error.GUID)
                {
                    result.Result = globals.LState_Error.Clone();
                    return result;
                }

                result.OItems = (from obj in items
                                      join objDb in dbReader.Objects1 on obj.GUID equals objDb.GUID
                                      select objDb).ToList();
                ResultOItems = result;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultOItems> GetAttributeTypes(clsOntologyItem[] items)
        {
            var taskResult = await Task.Run<ResultOItems>(() =>
            {
                var result = new ResultOItems
                {
                    Result = globals.LState_Success.Clone()
                };

                var dbReader = new OntologyModDBConnector(globals);

                var resultDbReader = dbReader.GetDataAttributeType(items.ToList());

                if (resultDbReader.GUID == globals.LState_Error.GUID)
                {
                    result.Result = globals.LState_Error.Clone();
                    return result;
                }

                result.OItems = (from obj in items
                                      join objDb in dbReader.AttributeTypes on obj.GUID equals objDb.GUID
                                      select objDb).ToList();
                ResultOItems = result;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultOItems> GetRelationTypes(clsOntologyItem[] items)
        {
            var taskResult = await Task.Run<ResultOItems>(() =>
            {
                var result = new ResultOItems
                {
                    Result = globals.LState_Success.Clone()
                };

                var dbReader = new OntologyModDBConnector(globals);

                var resultDbReader = dbReader.GetDataRelationTypes(items.ToList());

                if (resultDbReader.GUID == globals.LState_Error.GUID)
                {
                    result.Result = globals.LState_Error.Clone();
                    return result;
                }

                result.OItems = (from obj in items
                                 join objDb in dbReader.RelationTypes on obj.GUID equals objDb.GUID
                                 select objDb).ToList();
                ResultOItems = result;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultOItems> GetClasses(clsOntologyItem[] items)
        {
            var taskResult = await Task.Run<ResultOItems>(() =>
            {
                var result = new ResultOItems
                {
                    Result = globals.LState_Success.Clone()
                };

                var dbReader = new OntologyModDBConnector(globals);

                var resultDbReader = dbReader.GetDataClasses(items.ToList());

                if (resultDbReader.GUID == globals.LState_Error.GUID)
                {
                    result.Result = globals.LState_Error.Clone();
                    return result;
                }

                result.OItems = (from obj in items
                                 join objDb in dbReader.Classes1 on obj.GUID equals objDb.GUID
                                 select objDb).ToList();
                ResultOItems = result;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultRelateExists> GetExistingRelations(List<clsObjectRel> toRelate)
        {
            var taskResult = await Task.Run<ResultRelateExists>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);

                var result = new ResultRelateExists
                {
                    Result = globals.LState_Success.Clone()
                };
                result.Result = dbReader.GetDataObjectRel(toRelate);
                result.Related = dbReader.ObjectRels;

                return result;
            });

            return taskResult;
            
        }

        public async Task<ResultRelationTreeAtt> GetRelationTreeAttributeClass(string idClass)
        {
            var taskResult = await Task.Run<ResultRelationTreeAtt>(() =>
            {
                var result = new ResultRelationTreeAtt
                {
                    Result = globals.LState_Success.Clone(),
                    ClassAtts = new List<ResultClassAtt>()
                };

                var searchClassAttribute = new List<clsClassAtt>
                {
                    new clsClassAtt
                    {
                        ID_Class = idClass
                    }
                };

                var dbReaderClassAtt = new OntologyModDBConnector(globals);
                result.Result = dbReaderClassAtt.GetDataClassAtts(searchClassAttribute);

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    ResultRelationTreeAtt = result;
                    return result;
                }


                result.ClassAtts.AddRange(dbReaderClassAtt.ClassAtts.Select(clsAtt => new ResultClassAtt
                {
                    ClassAtt = clsAtt
                }));

                return result;
            });

            return taskResult;

        }

        public async Task<ResultRelationTreeAtt> GetRelationTreeAttribute(string idObject)
        {
            var taskResult = await Task.Run<ResultRelationTreeAtt>(() =>
            {
                var result = new ResultRelationTreeAtt
                {
                    Result = globals.LState_Success.Clone(),
                    ClassAtts = new List<ResultClassAtt>()
                };

                var resultTaskObject = GetOItem(idObject, globals.Type_Object);
                resultTaskObject.Wait();

                var oItem = resultTaskObject.Result.OItem;

                var searchClassAttribute = new List<clsClassAtt>
            {
                new clsClassAtt
                {
                    ID_Class = oItem.GUID_Parent
                }
            };

                var dbReaderClassAtt = new OntologyModDBConnector(globals);
                result.Result = dbReaderClassAtt.GetDataClassAtts(searchClassAttribute);

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    ResultRelationTreeAtt = result;
                    return result;
                }

                var dbReaderObjAtt = new OntologyModDBConnector(globals);

                foreach (var clsAtt in dbReaderClassAtt.ClassAtts)
                {
                    var searchObjAtt = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_AttributeType = clsAtt.ID_AttributeType,
                        ID_Object = idObject
                    }
                };

                    result.Result = dbReaderObjAtt.GetDataObjectAtt(searchObjAtt, doCount: true);

                    if (result.Result.GUID == globals.LState_Error.GUID)
                    {
                        ResultRelationTreeAtt = result;
                        return result;
                    }

                    result.ClassAtts.Add(new ResultClassAtt
                    {
                        ClassAtt = clsAtt,
                        CountItems = result.Result.Count.Value
                    });
                }

                return result;
            });

            return taskResult;
            
        }

        public async Task<ResultRelationTreeRel> GetRelationTreeRelationOtherLeft(string idObject)
        {
            var taskResult = await Task.Run<ResultRelationTreeRel>(async () =>
            {
                var result = new ResultRelationTreeRel
                {
                    Result = globals.LState_Success.Clone(),
                    ClassRels = new List<ResultClassRel>()
                };

                var resultTaskObject = await GetOItem(idObject, globals.Type_Object);

                var oItem = resultTaskObject.OItem;

                var searchClassRelations = new List<clsClassRel>
            {
                new clsClassRel
                {
                    ID_Class_Left = oItem.GUID_Parent
                }
            };

                var dbReaderClassRel = new OntologyModDBConnector(globals);
                result.Result = dbReaderClassRel.GetDataClassRel(searchClassRelations, doOr: true);

                if (result.Result.GUID == globals.LState_Error.GUID)
                {

                    ResultRelationTreeRelLeftOther = result;

                    return result;
                }

                var dbReaderObjRel = new OntologyModDBConnector(globals);

                foreach (var clsRel in dbReaderClassRel.ClassRels.Where(clsRel => string.IsNullOrEmpty(clsRel.ID_Class_Right)))
                {
                    var searchObjRel = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idObject,
                        ID_RelationType = clsRel.ID_RelationType
                    }
                };

                    result.Result = dbReaderObjRel.GetDataObjectRel(searchObjRel, doCount: true);

                    if (result.Result.GUID == globals.LState_Error.GUID)
                    {
                        ResultRelationTreeRelLeftRight = result;
                        return result;
                    }

                    result.ClassRels.Add(new ResultClassRel
                    {
                        ClassRel = clsRel,
                        CountItems = result.Result.Count.Value
                    });
                }

                ResultRelationTreeRelLeftRight = result;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultRelationTreeRel> GetRelationTreeRelationOtherRight(string idObject)
        {
            var taskResult = await Task.Run<ResultRelationTreeRel>(async () =>
            {
                var result = new ResultRelationTreeRel
                {
                    Result = globals.LState_Success.Clone(),
                    ClassRels = new List<ResultClassRel>()
                };

                var resultTaskObject = await GetOItem(idObject, globals.Type_Object);

                var oItem = resultTaskObject.OItem;

                var searchRightLeft = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = idObject
                }
            };

                var dbReaderObjRel = new OntologyModDBConnector(globals);
                result.Result = dbReaderObjRel.GetDataObjectRel(searchRightLeft);

                result.ClassRels =
                    dbReaderObjRel.ObjectRels.GroupBy(
                        objRel =>
                            new
                            {
                                IdParentObject = objRel.ID_Parent_Object,
                                NameParentObject = objRel.Name_Parent_Object,
                                IdRelationType = objRel.ID_RelationType,
                                NameRelationType = objRel.Name_RelationType,
                                IdParentOther = oItem.GUID_Parent
                            }).Select(objRelGrp => new ResultClassRel
                            {
                                ClassRel = new clsClassRel
                                {
                                    ID_Class_Left = objRelGrp.Key.IdParentObject,
                                    Name_Class_Left = objRelGrp.Key.NameParentObject,
                                    ID_Class_Right = objRelGrp.Key.IdParentOther,
                                    ID_RelationType = objRelGrp.Key.IdRelationType,
                                    Name_RelationType = objRelGrp.Key.NameRelationType
                                },
                                CountItems = objRelGrp.Count()
                            }).ToList();

                ResultRelationTreeRelRightLeft = result;

                return result;
            });

            return taskResult;
            
        }

        public async Task<ResultRelated> RelateItems(List<clsObjectRel> toRelate)
        {
            var taskResult = await Task.Run<ResultRelated>(() =>
            {
                var result = new ResultRelated
                {
                    Result = globals.LState_Success.Clone()
                };

                if (toRelate.Count > 0)
                {
                    var dbWriter = new OntologyModDBConnector(globals);

                    result.Result = dbWriter.SaveObjRel(toRelate);
                    result.Count = toRelate.Count;
                }
                else
                {
                    result.Count = 0;
                }


                ResultRelated = result;
                return result;
            });

            return taskResult;
        }

        public async Task<ResultRelationTreeRel> GetRelationTreeRelationFullQualifiedClass(string idClass, bool leftRight)
        {
            var taskResult = await Task.Run<ResultRelationTreeRel>(() =>
            {
                var result = new ResultRelationTreeRel
                {
                    Result = globals.LState_Success.Clone(),
                    ClassRels = new List<ResultClassRel>()
                };

                var searchClassRelations = new List<clsClassRel>
                {
                    new clsClassRel
                    {
                        ID_Class_Left = leftRight ? idClass  : null,
                        ID_Class_Right = leftRight ? null: idClass
                    }
                };

                var dbReaderClassRel = new OntologyModDBConnector(globals);
                result.Result = dbReaderClassRel.GetDataClassRel(searchClassRelations);

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    if (leftRight)
                    {
                        ResultRelationTreeRelLeftRight = result;
                    }
                    else
                    {
                        ResultRelationTreeRelRightLeft = result;
                    }

                    return result;
                }


                result.ClassRels.AddRange(dbReaderClassRel.ClassRels.Select(clsRel => new ResultClassRel
                {
                    ClassRel = clsRel
                }));

                return result;
            });

            return taskResult;
        }

        public async Task<ResultRelationTreeRel> GetRelationTreeRelationFullQualified(string idObject, bool leftRight)
        {
            var taskResult = await Task.Run<ResultRelationTreeRel>(() =>
            {
                var result = new ResultRelationTreeRel
                {
                    Result = globals.LState_Success.Clone(),
                    ClassRels = new List<ResultClassRel>()
                };

                var resultTaskObject = GetOItem(idObject, globals.Type_Object);
                resultTaskObject.Wait();

                var oItem = resultTaskObject.Result.OItem;

                var searchClassRelations = new List<clsClassRel>
                {
                    new clsClassRel
                    {
                        ID_Class_Left = leftRight ? oItem.GUID_Parent : null,
                        ID_Class_Right = leftRight ? null: oItem.GUID_Parent
                    }
                };

                var dbReaderClassRel = new OntologyModDBConnector(globals);
                result.Result = dbReaderClassRel.GetDataClassRel(searchClassRelations);

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    if (leftRight)
                    {
                        ResultRelationTreeRelLeftRight = result;
                    }
                    else
                    {
                        ResultRelationTreeRelRightLeft = result;
                    }

                    return result;
                }

                var dbReaderObjRel = new OntologyModDBConnector(globals);

                foreach (var clsRel in dbReaderClassRel.ClassRels.Where(clsRel => !string.IsNullOrEmpty(clsRel.ID_Class_Right)))
                {
                    var searchObjRel = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = leftRight ? idObject : null,
                        ID_Other = leftRight ? null : idObject,
                        ID_RelationType = clsRel.ID_RelationType,
                        ID_Parent_Other = leftRight ? clsRel.ID_Class_Right : null,
                        ID_Parent_Object = leftRight ? null : clsRel.ID_Class_Left
                    }
                };

                    result.Result = dbReaderObjRel.GetDataObjectRel(searchObjRel, doCount: true);

                    if (result.Result.GUID == globals.LState_Error.GUID)
                    {
                        ResultRelationTreeRelLeftRight = result;
                        return result;
                    }

                    result.ClassRels.Add(new ResultClassRel
                    {
                        ClassRel = clsRel,
                        CountItems = result.Result.Count.Value
                    });
                }

                if (leftRight)
                {
                    ResultRelationTreeRelLeftRight = result;
                }
                else
                {
                    ResultRelationTreeRelRightLeft = result;
                }
                return result;
            });

            return taskResult;
        }

        public ElasticAgentRelationTree(Globals globals) : base(globals)
        {
        }
    }


    public class OItemResult
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem OItem { get; set; }
    }

    public class ResultRelationTreeAtt
    {
        public clsOntologyItem Result { get; set; }
        public List<ResultClassAtt> ClassAtts { get; set; }
    }

    public class ResultRelationTreeRel
    {
        public clsOntologyItem Result { get; set; }
        public List<ResultClassRel> ClassRels { get; set; }
    }


    public class ResultClassAtt
    {
        public clsClassAtt ClassAtt { get; set; }
        public long CountItems { get; set; }
    }

    public class ResultClassRel
    {
        public clsClassRel ClassRel { get; set; }
        public long CountItems { get; set; }
    }

    public class ResultOItems
    {
        public clsOntologyItem Result { get; set; }
        public List<clsOntologyItem> OItems { get; set; }
    }

    public class ResultRelateExists
    {
        public clsOntologyItem Result { get; set; }
        public List<clsObjectRel> Related { get; set; }
    }

    public class ResultRelated
    {
        public clsOntologyItem Result { get; set; }
        public long Count { get; set; }
    }
}
