﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OntologyItemsModule.Services
{
    [Flags]
    public enum ListLoadStateItem
    {

        None = 0,
        ListLoadPending = 1,
        ListLoadLoaded = 2,
        AdvancedFilterPending = 4,
        AdvancedFilterLoaded = 8
    }
    public class ListAdapter : NotifyPropertyChange
    {
        private Globals globals;
        private object listAdapterLocker = new object();

        private Thread threadListLoader;

        private clsOntologyItem filterItemNote;
        public clsOntologyItem FilterItemNote
        {
            get { return filterItemNote; }
            set
            {
                filterItemNote = value;

            }
        }

        private clsClassAtt filterItemClassAtt;
        public clsClassAtt FilterItemClassAtt
        {
            get { return filterItemClassAtt; }
            set
            {
                filterItemClassAtt = value;
            }
        }

        private clsClassRel filterItemClassRel;
        public clsClassRel FilterItemClassRel
        {
            get { return filterItemClassRel; }
            set
            {
                filterItemClassRel = value;
            }
        }

        private clsObjectAtt filterItemObjectAttribute;
        public clsObjectAtt FilterItemObjectAttribute
        {
            get { return filterItemObjectAttribute; }
            set
            {
                filterItemObjectAttribute = value;
            }
        }

        private clsObjectRel filterItemObjectRelation;
        public clsObjectRel FilterItemObjectRelation
        {
            get { return filterItemObjectRelation; }
            set
            {
                filterItemObjectRelation = value;
            }
        }

        private ListType listType;

        private bool exact;
        public bool Exact
        {
            get { return exact; }
            set
            {
                exact = value;
            }
        }

        private ListLoadStateItem listLoadState;
        public ListLoadStateItem ListLoadState
        {
            get
            {
                lock(listAdapterLocker)
                {
                    return listLoadState;
                }
            }
            set
            {
                lock(listAdapterLocker)
                {
                    listLoadState = value;
                }
                RaisePropertyChanged(nameof(ListLoadState));
            }
        }

        private ResultViewItems resultListLoader;
        public ResultViewItems ResultListLoader
        {
            get
            {
                lock(listAdapterLocker)
                {
                    return resultListLoader;
                }
            }
            set
            {
                lock(listAdapterLocker)
                {
                    resultListLoader = value;
                }
                if (resultListLoader.Result.GUID != globals.LState_Nothing.GUID)
                {
                    ListLoadStateItem listLoadStatePre = ListLoadState;
                    if (ListLoadState.HasFlag(ListLoadStateItem.AdvancedFilterPending))
                    {
                        listLoadStatePre &= ~ListLoadStateItem.AdvancedFilterPending;
                        listLoadStatePre |= ListLoadStateItem.AdvancedFilterLoaded;
                    }

                    if (ListLoadState.HasFlag(ListLoadStateItem.ListLoadPending))
                    {
                        listLoadStatePre &= ~ListLoadStateItem.ListLoadPending;
                        listLoadStatePre |= ListLoadStateItem.ListLoadLoaded;
                    }

                    ListLoadState = listLoadStatePre;
                }
                RaisePropertyChanged(nameof(ResultListLoader));
            }
        }
        
        public DeleteCounter DelClassAttributes(List<clsClassAtt> classAttributes)
        {
            var dbWriteConnector = new OntologyModDBConnector(globals);
            var counter = new DeleteCounter
            {
                CountToDo = classAttributes.Count,
                CountDone = 0,
                CountError = 0,
                CountRelated = 0
            };
            
            classAttributes.ForEach(classAtt =>
            {
                var result = dbWriteConnector.DelClassAttType(new clsOntologyItem { GUID = classAtt.ID_Class, Type = globals.Type_Class }, new clsOntologyItem { GUID = classAtt.ID_AttributeType, Type = globals.Type_AttributeType });
                if (result.GUID == globals.LState_Success.GUID)
                {
                    counter.CountDone++;
                }
                else if (result.GUID == globals.LState_Relation.GUID)
                {
                    counter.CountRelated++;
                }
                else if (result.GUID == globals.LState_Error.GUID)
                {
                    counter.CountError++;
                }
            });

            return counter;
        }

        public DeleteCounter DelClassRelations(List<clsClassRel> classRelations)
        {
            var dbWriteConnector = new OntologyModDBConnector(globals);
            var counter = new DeleteCounter
            {
                CountToDo = classRelations.Count,
                CountDone = 0,
                CountError = 0,
                CountRelated = 0
            };

            var result = dbWriteConnector.DelClassRel(classRelations);
            if (result.GUID == globals.LState_Success.GUID)
            {
                counter.CountDone = (int)result.Max1.Value;
            }
            else if (result.GUID == globals.LState_Relation.GUID)
            {
                counter.CountRelated = (int)result.Max1.Value;
            }
            else if (result.GUID == globals.LState_Error.GUID)
            {
                counter.CountError = (int)result.Max1.Value;
            }
            return counter;
        }

        public clsOntologyItem OItem(string guid, string type)
        {
            var dbWriteConnector = new OntologyModDBConnector(globals);
            return dbWriteConnector.GetOItem(guid, type);
        }

        public ListAdapter(Globals globals)
        {
            this.globals = globals;
            Initialize();
        }

        public clsOntologyItem SaveClassAttributeList(List<clsClassAtt> classAttributes)
        {
            var dbWriteConnector = new OntologyModDBConnector(globals);
            return dbWriteConnector.SaveClassAtt(classAttributes);
        }

        public async Task<ResultViewItems> Initialize_AdvancedFilter(ListType listType,
            clsOntologyItem rootItem,
            clsOntologyItem advancedFilter_Class = null,
            clsOntologyItem advancedFilter_Object = null,
            clsOntologyItem advancedFilter_RelationType = null,
            clsOntologyItem advancedFilter_Direction = null)
        {
            var taskResult = await Task.Run<ResultViewItems>(() =>
            {
                var dbReadConnector_Classes = new OntologyModDBConnector(globals);
                var dbReadConnector_AdvancedFilter_Conscious = new OntologyModDBConnector(globals);
                var dbReadConnector_AdvancedFilter_Subconscious = new OntologyModDBConnector(globals);

                var result = new ResultViewItems
                {
                    ListType = listType,
                    Result = globals.LState_Nothing.Clone()
                };

                ListLoadState |= ListLoadStateItem.AdvancedFilterPending;
                ResultListLoader = result;

                var idAdvancedFilterObject = advancedFilter_Object != null ? advancedFilter_Object.GUID : null;
                var idAdvancedFilterClass = advancedFilter_Object != null ? advancedFilter_Object.GUID_Parent : advancedFilter_Class != null ? advancedFilter_Class.GUID : null;

                switch (listType)
                {
                    case ListType.Instances:
                        List<clsObjectRel> searchConscious = null;
                        List<clsObjectRel> searchSubconscious = null;
                        FilterItemNote = rootItem;

                        result.Result = dbReadConnector_Classes.GetDataClasses();

                        if (advancedFilter_Direction == null)
                        {
                            searchConscious = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Object = FilterItemNote.Type == globals.Type_Class ? null : FilterItemNote.GUID,
                                ID_Parent_Object = FilterItemNote.Type == globals.Type_Class ? FilterItemNote.GUID : FilterItemNote.GUID_Parent,
                                ID_RelationType = advancedFilter_RelationType != null ? advancedFilter_RelationType.GUID : null,
                                ID_Other = idAdvancedFilterObject,
                                ID_Parent_Other = idAdvancedFilterClass
                            }
                        };

                            searchSubconscious = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Other = FilterItemNote.Type == globals.Type_Class ? null : FilterItemNote.GUID,
                                ID_Parent_Other = FilterItemNote.Type == globals.Type_Class ? FilterItemNote.GUID : FilterItemNote.GUID_Parent,
                                ID_RelationType = advancedFilter_RelationType != null ? advancedFilter_RelationType.GUID : null,
                                ID_Object = idAdvancedFilterObject,
                                ID_Parent_Object = idAdvancedFilterClass
                            }
                        };

                        }
                        else if (advancedFilter_Direction.GUID == globals.Direction_LeftRight.GUID)
                        {
                            searchConscious = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Object = FilterItemNote.Type == globals.Type_Class ? null : FilterItemNote.GUID,
                                ID_Parent_Object = FilterItemNote.Type == globals.Type_Class ? FilterItemNote.GUID : FilterItemNote.GUID_Parent,
                                ID_RelationType = advancedFilter_RelationType != null ? advancedFilter_RelationType.GUID : null,
                                ID_Other = idAdvancedFilterObject,
                                ID_Parent_Other = idAdvancedFilterClass
                            }
                        };
                        }
                        else if (advancedFilter_Direction.GUID == globals.Direction_RightLeft.GUID)
                        {
                            searchSubconscious = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Other = FilterItemNote.Type == globals.Type_Class ? null : FilterItemNote.GUID,
                                ID_Parent_Other = FilterItemNote.Type == globals.Type_Class ? FilterItemNote.GUID : FilterItemNote.GUID_Parent,
                                ID_RelationType = advancedFilter_RelationType != null ? advancedFilter_RelationType.GUID : null,
                                ID_Object = idAdvancedFilterObject,
                                ID_Parent_Object = idAdvancedFilterClass
                            }
                        };
                        }


                        if (searchConscious != null)
                        {
                            result.Result = dbReadConnector_AdvancedFilter_Conscious.GetDataObjectRel(searchConscious);

                        }

                        if (result.Result.GUID != globals.LState_Error.GUID)
                        {
                            if (searchSubconscious != null)
                            {
                                result.Result = dbReadConnector_AdvancedFilter_Subconscious.GetDataObjectRel(searchSubconscious);

                            }
                        }


                        if (result.Result.GUID != globals.LState_Error.GUID)
                        {
                            if (rootItem.Type == globals.Type_Object)
                            {
                                var classItem = dbReadConnector_Classes.Classes1.FirstOrDefault(cls => cls.GUID == rootItem.GUID_Parent);
                                result.InstanceViewItems = new List<InstanceViewItem> { new InstanceViewItem(rootItem, classItem) };
                            }
                            else
                            {
                                if (advancedFilter_Direction == null)
                                {

                                    result.InstanceViewItems = (from objectItem in dbReadConnector_AdvancedFilter_Conscious.ObjectRels.Where(cons => cons.ID_Parent_Object == idAdvancedFilterClass).Select(cons => new clsOntologyItem
                                    {
                                        GUID = cons.ID_Object,
                                        Name = cons.Name_Object,
                                        GUID_Parent = cons.ID_Parent_Object,
                                        Type = globals.Type_Object
                                    })
                                                                join classItem in dbReadConnector_Classes.Classes1 on objectItem.GUID_Parent equals classItem.GUID
                                                                select new InstanceViewItem(objectItem, classItem)).ToList();

                                    var instanceViewItemsAdd = (from objectItem in dbReadConnector_AdvancedFilter_Subconscious.ObjectRels.Where(cons => cons.ID_Parent_Other == idAdvancedFilterClass).Select(cons => new clsOntologyItem
                                    {
                                        GUID = cons.ID_Other,
                                        Name = cons.Name_Other,
                                        GUID_Parent = cons.ID_Parent_Other,
                                        Type = globals.Type_Object
                                    })
                                                                join classItem in dbReadConnector_Classes.Classes1 on objectItem.GUID_Parent equals classItem.GUID
                                                                select new InstanceViewItem(objectItem, classItem)).ToList();

                                    result.InstanceViewItems.AddRange(from instanceViewItemToAdd in instanceViewItemsAdd
                                                                      join instanceViewItem in result.InstanceViewItems on instanceViewItemToAdd.IdInstance equals instanceViewItem.IdInstance into instanceViewItems
                                                                      from instanceViewItem in instanceViewItems.DefaultIfEmpty()
                                                                      where instanceViewItem == null
                                                                      select instanceViewItemToAdd);

                                }
                                else if (advancedFilter_Direction.GUID == globals.Direction_LeftRight.GUID)
                                {
                                    result.InstanceViewItems = (from objectItem in dbReadConnector_AdvancedFilter_Conscious.ObjectRels.Where(cons => cons.ID_Parent_Object == idAdvancedFilterClass).Select(cons => new clsOntologyItem
                                    {
                                        GUID = cons.ID_Object,
                                        Name = cons.Name_Object,
                                        GUID_Parent = cons.ID_Parent_Object,
                                        Type = globals.Type_Object
                                    })
                                                                join classItem in dbReadConnector_Classes.Classes1 on objectItem.GUID_Parent equals classItem.GUID
                                                                select new InstanceViewItem(objectItem, classItem)).ToList();
                                }
                                else if (advancedFilter_Direction.GUID == globals.Direction_RightLeft.GUID)
                                {
                                    result.InstanceViewItems = (from objectItem in dbReadConnector_AdvancedFilter_Subconscious.ObjectRels.Where(cons => cons.ID_Parent_Object == idAdvancedFilterClass).Select(cons => new clsOntologyItem
                                    {
                                        GUID = cons.ID_Other,
                                        Name = cons.Name_Other,
                                        GUID_Parent = cons.ID_Parent_Other,
                                        Type = globals.Type_Object
                                    })
                                                                join classItem in dbReadConnector_Classes.Classes1 on objectItem.GUID_Parent equals classItem.GUID
                                                                select new InstanceViewItem(objectItem, classItem)).ToList();
                                }
                            }

                        }

                        if (result.Result.GUID != globals.LState_Error.GUID)
                        {
                            if (advancedFilter_Direction == null)
                            {



                                result.InstanceViewItems = (from objectItem in result.InstanceViewItems
                                                            join relConscious in dbReadConnector_AdvancedFilter_Conscious.ObjectRels on objectItem.IdInstance equals relConscious.ID_Object
                                                            join relSubsconscious in dbReadConnector_AdvancedFilter_Subconscious.ObjectRels on objectItem.IdInstance equals relSubsconscious.ID_Other
                                                            group objectItem by objectItem.IdInstance into objectItemGroup
                                                            select objectItemGroup.First()).ToList();
                            }
                            else if (advancedFilter_Direction.GUID == globals.Direction_LeftRight.GUID)
                            {
                                result.InstanceViewItems = (from objectItem in result.InstanceViewItems
                                                            join relConscious in dbReadConnector_AdvancedFilter_Conscious.ObjectRels on objectItem.IdInstance equals relConscious.ID_Object
                                                            group objectItem by objectItem.IdInstance into objectItemGroup
                                                            select objectItemGroup.First()).ToList();
                            }
                            else if (advancedFilter_Direction.GUID == globals.Direction_RightLeft.GUID)
                            {
                                result.InstanceViewItems = (from objectItem in result.InstanceViewItems
                                                            join relSubsconscious in dbReadConnector_AdvancedFilter_Subconscious.ObjectRels on objectItem.IdInstance equals relSubsconscious.ID_Other
                                                            group objectItem by objectItem.IdInstance into objectItemGroup
                                                            select objectItemGroup.First()).ToList();
                            }

                            result.Result = globals.LState_Success.Clone();
                            ResultListLoader = result;
                        }
                        else
                        {
                            result.Result = globals.LState_Error.Clone();
                            ResultListLoader = result;
                        }

                        break;
                    case ListType.InstanceAttribute_Conscious:

                        break;
                    case ListType.InstanceInstance_Conscious:

                        break;
                    case ListType.InstanceInstance_Subconscious:

                        break;
                    case ListType.InstanceInstance_Omni:

                        break;
                    case ListType.InstanceOther_Conscious:

                        break;
                    case ListType.InstanceOther_Subconscious:

                        break;
                    default:
                        result.Result = globals.LState_Error.Clone();
                        ResultListLoader = result;
                        break;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultViewItems> Initialize_ClassList(clsOntologyItem filterItem, bool exact = false)
        {
            var taskResult = await Task.Run<ResultViewItems>(() =>
            {
                ListLoadState = ListLoadStateItem.ListLoadPending;
                var result = new ResultViewItems
                {
                    ListType = ListType.Classes,
                    Result = globals.LState_Nothing.Clone()
                };

                ResultListLoader = result;

                FilterItemNote = filterItem;
                Exact = exact;

                result = LoadClassList();
                ResultListLoader = result;
                return result;
            });

            return taskResult;
        }

        private ResultViewItems LoadClassList()
        {
            var dbReadConnector_Classes = new OntologyModDBConnector(globals);
            var result = new ResultViewItems
            {
                ListType = ListType.Classes,
                Result = globals.LState_Nothing.Clone()
            };

            result.Result = dbReadConnector_Classes.GetDataClasses(filterItemNote != null ? new List<clsOntologyItem> { FilterItemNote } : null);
            if (result.Result.GUID == globals.LState_Success.GUID)
            {

                if (Exact && FilterItemNote != null && !string.IsNullOrEmpty(FilterItemNote.Name))
                {
                    result.ClassViewItems = (from classItem in dbReadConnector_Classes.Classes1.Where(classItem => classItem.Name == FilterItemNote.Name)
                                     join parentClassItem in dbReadConnector_Classes.Classes1 on classItem.GUID_Parent equals parentClassItem.GUID
                                     select new ClassViewItem(classItem, parentClassItem)).ToList();

                }
                else
                {
                    result.ClassViewItems = (from classItem in dbReadConnector_Classes.Classes1
                                      join parentClassItem in dbReadConnector_Classes.Classes1 on classItem.GUID_Parent equals parentClassItem.GUID
                                      select new ClassViewItem(classItem, parentClassItem)).ToList();
                }
            }
            else
            {
                result.ClassViewItems = new List<ClassViewItem>();
            }

            return result;
        }

        public async Task<ResultViewItems> Initialize_RelationTypeList(clsOntologyItem filterItem, bool exact = false)
        {
            var taskResult = await Task.Run<ResultViewItems>(() =>
            {
                ListLoadState = ListLoadStateItem.ListLoadPending;
                var result = new ResultViewItems
                {
                    ListType = ListType.RelationTypes,
                    Result = globals.LState_Nothing.Clone()
                };

                ResultListLoader = result;

                FilterItemNote = filterItem;
                Exact = exact;

                result = LoadRelationTypeList();
                ResultListLoader = result;
                return result;
            });

            return taskResult;
        }

        private ResultViewItems LoadRelationTypeList()
        {
            var dbReadConnector_RelationType = new OntologyModDBConnector(globals);

            var result = new ResultViewItems
            {
                ListType = ListType.RelationTypes,
                Result = globals.LState_Nothing.Clone()
            };

            result.Result = dbReadConnector_RelationType.GetDataRelationTypes(filterItemNote != null ? new List<clsOntologyItem> { FilterItemNote } : null);
            if (result.Result.GUID == globals.LState_Success.GUID)
            {

                if (Exact && FilterItemNote != null && !string.IsNullOrEmpty(FilterItemNote.Name))
                {
                    result.RelationTypeViewItems = (from relTypeItem in dbReadConnector_RelationType.RelationTypes.Where(classItem => classItem.Name == FilterItemNote.Name)
                                      select new RelationTypeViewItem(relTypeItem)).ToList();

                }
                else
                {
                    result.RelationTypeViewItems = (from relTypeItem in dbReadConnector_RelationType.RelationTypes
                                             select new RelationTypeViewItem(relTypeItem)).ToList();
                }
            }
            else
            {
                result.RelationTypeViewItems = new List<RelationTypeViewItem>();
            }

            return result;
        }

        public async Task<ResultViewItems> Initialize_AttributeTypeList(clsOntologyItem filterItem, bool exact = false)
        {
            var taskResult = await Task.Run<ResultViewItems>(() =>
            {
                ListLoadState = ListLoadStateItem.ListLoadPending;
                var result = new ResultViewItems
                {
                    ListType = ListType.AttributeTypes,
                    Result = globals.LState_Nothing.Clone()
                };

                ResultListLoader = result;

                FilterItemNote = filterItem;
                Exact = exact;

                result = LoadAttributeTypeList();
                ResultListLoader = result;

                return result;
            });
            return taskResult;
        }

        private ResultViewItems LoadAttributeTypeList()
        {
            var dbReadConnector_AttributeTypes = new OntologyModDBConnector(globals);

            var result = new ResultViewItems
            {
                ListType = ListType.AttributeTypes,
                Result = globals.LState_Nothing.Clone()
            };
            result.Result = dbReadConnector_AttributeTypes.GetDataAttributeType(filterItemNote != null ? new List<clsOntologyItem> { FilterItemNote } : null);
            if (result.Result.GUID == globals.LState_Success.GUID)
            {
                
                if (Exact && FilterItemNote != null && !string.IsNullOrEmpty(FilterItemNote.Name))
                {
                    result.AttributeTypeViewItems = (from attType in dbReadConnector_AttributeTypes.AttributeTypes.Where(attType => attType.Name == FilterItemNote.Name)
                                       join dataType in globals.DataTypes.DataTypes on attType.GUID_Parent equals dataType.GUID
                                       select new AttributeTypeViewItem(attType, dataType)).ToList();
                    
                }
                else
                {
                    result.AttributeTypeViewItems = (from attType in dbReadConnector_AttributeTypes.AttributeTypes
                                              join dataType in globals.DataTypes.DataTypes on attType.GUID_Parent equals dataType.GUID
                                              select new AttributeTypeViewItem(attType, dataType)).ToList();
                }
            }
            else
            {
                result.AttributeTypeViewItems = new List<AttributeTypeViewItem>();
            }

            return result;
        }

        public async Task<ResultViewItems> Initialize_InstanceList(clsOntologyItem filterItem, bool exact = false)
        {
            var taskResult = await Task.Run<ResultViewItems>(() =>
            {
                ListLoadState = ListLoadStateItem.ListLoadPending;
                var result = new ResultViewItems
                {
                    ListType = ListType.Instances,
                    Result = globals.LState_Nothing.Clone()
                };

                ResultListLoader = result;

                FilterItemNote = filterItem;
                Exact = exact;

                result = LoadInstanceList();
                ResultListLoader = result;
                return result;
            });

            return taskResult;
        }

        private ResultViewItems LoadInstanceList()
        {
            var result = new ResultViewItems
            {
                ListType = ListType.Instances,
                Result = globals.LState_Nothing.Clone()
            };
            result = GetDataInstances();
            ResultListLoader = result;
            return result;
        }

        private ResultViewItems GetDataInstances()
        {
            var dbReadConnector_Classes = new OntologyModDBConnector(globals);
            var dbReadConnector_Instance = new OntologyModDBConnector(globals);

            var result = new ResultViewItems
            {
                ListType = ListType.Instances,
                Result = globals.LState_Nothing.Clone()
            };
            result.Result = dbReadConnector_Classes.GetDataClasses();
            if (result.Result.GUID == globals.LState_Success.GUID)
            {
                result.Result = dbReadConnector_Instance.GetDataObjects(filterItemNote != null ? new List<clsOntologyItem> { FilterItemNote } : null);
                if (result.Result.GUID == globals.LState_Success.GUID)
                {
                    if (Exact && FilterItemNote != null && !string.IsNullOrEmpty(FilterItemNote.Name))
                    {
                        result.InstanceViewItems = (from instanceItem in dbReadConnector_Instance.Objects1.Where(objectItem => objectItem.Name == FilterItemNote.Name)
                                             join classItem in dbReadConnector_Classes.Classes1 on instanceItem.GUID_Parent equals classItem.GUID
                                             select new InstanceViewItem(instanceItem, classItem)).ToList();

                    }
                    else
                    {
                        result.InstanceViewItems = (from instanceItem in dbReadConnector_Instance.Objects1
                                             join classItem in dbReadConnector_Classes.Classes1 on instanceItem.GUID_Parent equals classItem.GUID
                                             select new InstanceViewItem(instanceItem, classItem)).ToList();

                    }
                }
                else
                {
                    result.InstanceViewItems = new List<InstanceViewItem>();
                }


            }
            else
            {
                result.InstanceViewItems = new List<InstanceViewItem>();
            }


            return result;
        }

        public async Task<ResultViewItems> Initialize_ClassAttributeList(clsClassAtt filterItem, bool exact = false)
        {
            var taskResult = await Task.Run<ResultViewItems>(() =>
            {
                ListLoadState = ListLoadStateItem.ListLoadPending;
                var result = new ResultViewItems
                {
                    ListType = ListType.ClassAttributeRelation,
                    Result = globals.LState_Nothing.Clone()
                };
                ResultListLoader = result;

                FilterItemClassAtt = filterItem;
                Exact = exact;

                result = LoadClassAttList();
                ResultListLoader = result;
                return result;
            });

            return taskResult;
        }

        private ResultViewItems LoadClassAttList()
        {
            var dbReadConnector_ClassAttributes = new OntologyModDBConnector(globals);

            var result = new ResultViewItems
            {
                ListType = ListType.ClassAttributeRelation,
                Result = globals.LState_Nothing.Clone()
            };
            result.Result = dbReadConnector_ClassAttributes.GetDataClassAtts(FilterItemClassAtt != null ? new List<clsClassAtt> { FilterItemClassAtt } : null);
            if (result.Result.GUID == globals.LState_Success.GUID)
            {
                
               
                if (Exact && FilterItemClassAtt != null && (!string.IsNullOrEmpty(FilterItemClassAtt.Name_AttributeType) ||
                                                            !string.IsNullOrEmpty(FilterItemClassAtt.Name_Class) ||
                                                            !string.IsNullOrEmpty(FilterItemClassAtt.Name_DataType)))
                {
                    result.ClassAttViewItems = (from classAttItem in dbReadConnector_ClassAttributes.ClassAtts.Where(classAttItem => (!string.IsNullOrEmpty(FilterItemClassAtt.Name_AttributeType) ? classAttItem.Name_AttributeType == FilterItemClassAtt.Name_AttributeType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassAtt.Name_Class) ? classAttItem.Name_Class == FilterItemClassAtt.Name_Class : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassAtt.Name_DataType) ? classAttItem.Name_DataType == FilterItemClassAtt.Name_DataType : 1 == 1))
                                         select new ClassAttributeViewItem(classAttItem)).ToList();

                }
                else if (!Exact && FilterItemClassAtt != null && (!string.IsNullOrEmpty(FilterItemClassAtt.Name_AttributeType) ||
                                                            !string.IsNullOrEmpty(FilterItemClassAtt.Name_Class) ||
                                                            !string.IsNullOrEmpty(FilterItemClassAtt.Name_DataType)))
                {
                    result.ClassAttViewItems = (from classAttItem in dbReadConnector_ClassAttributes.ClassAtts.Where(classAttItem => (!string.IsNullOrEmpty(FilterItemClassAtt.Name_AttributeType) ? classAttItem.Name_AttributeType.ToLower().Contains(FilterItemClassAtt.Name_AttributeType.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassAtt.Name_Class) ? classAttItem.Name_Class.ToLower().Contains(FilterItemClassAtt.Name_Class.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassAtt.Name_DataType) ? classAttItem.Name_DataType.ToLower().Contains(FilterItemClassAtt.Name_DataType.ToLower()) : 1 == 1))
                                         select new ClassAttributeViewItem(classAttItem)).ToList();
                }
                else
                {
                    result.ClassAttViewItems = (from classAttItem in dbReadConnector_ClassAttributes.ClassAtts
                                         select new ClassAttributeViewItem(classAttItem)).ToList();

                }
                
               

            }
            else
            {
                result.ClassAttViewItems = new List<ClassAttributeViewItem>();
            }

            return result;
        }

        public async Task<ResultViewItems> Initialize_ClassRelationList(clsClassRel filterItem, ListType listType, bool exact = false)
        {
            var taskResult = await Task.Run<ResultViewItems>(() =>
            {
                ListLoadState = ListLoadStateItem.ListLoadPending;
                var result = new ResultViewItems
                {
                    ListType = listType,
                    Result = globals.LState_Nothing.Clone()
                };
                ResultListLoader = result;

                FilterItemClassRel = filterItem;
                Exact = exact;
                this.listType = listType;

                result = LoadClassRelList(listType);
                ResultListLoader = result;
                return result;
            });

            return taskResult;
        }


        private ResultViewItems AddClassRelsToList(bool doOr, ListType listType)
        {
            var dbReadConnector_ClassRelations = new OntologyModDBConnector(globals);
            var result = new ResultViewItems
            {
                ListType = listType,
                Result = globals.LState_Nothing.Clone()
            };
            result.Result = dbReadConnector_ClassRelations.GetDataClassRel(FilterItemClassRel != null ? new List<clsClassRel> { FilterItemClassRel } : null, doIds: false, doOr: doOr);
            if (result.Result.GUID == globals.LState_Success.GUID)
            {


                if (Exact && FilterItemClassRel != null && (!string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Left) ||
                                                            !string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Right) ||
                                                            !string.IsNullOrEmpty(FilterItemClassRel.Name_RelationType) ||
                                                            !string.IsNullOrEmpty(FilterItemClassRel.Ontology) ||
                                                            FilterItemClassRel.Min_Forw != null ||
                                                            FilterItemClassRel.Max_Forw != null ||
                                                            FilterItemClassRel.Max_Backw != null))
                {

                    var classRelViewItems = dbReadConnector_ClassRelations.ClassRels.Where(classRelItem => (!string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Left) ? classRelItem.Name_Class_Left == FilterItemClassRel.Name_Class_Left : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Right) ? classRelItem.Name_Class_Right == FilterItemClassRel.Name_Class_Right : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassRel.Name_RelationType) ? classRelItem.Name_RelationType == FilterItemClassRel.Name_RelationType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassRel.Ontology) ? classRelItem.Ontology == FilterItemClassRel.Ontology : 1 == 1) &&
                                                                                                                       (FilterItemClassRel.Min_Forw != null ? classRelItem.Min_Forw == FilterItemClassRel.Min_Forw : 1 == 1) &&
                                                                                                                       (FilterItemClassRel.Max_Forw != null ? classRelItem.Max_Forw == FilterItemClassRel.Max_Forw : 1 == 1) &&
                                                                                                                       (FilterItemClassRel.Max_Backw != null ? classRelItem.Max_Backw == FilterItemClassRel.Max_Backw : 1 == 1)).ToList();

                    switch (listType)
                    {
                        case ListType.ClassOtherRelation_Conscious:
                        case ListType.ClassClassRelation_Conscious:
                            result.ClassRelConciousViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationConsciousViewItem(clsRel)).ToList());
                            break;
                        case ListType.ClassClassRelation_Subconscious:
                            result.ClassRelSubconsciousViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationSubconsciousViewItem(clsRel)).ToList());
                            break;
                        case ListType.ClassClassRelation_Omni:
                            result.ClassRelOmniViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationOmniViewItem(clsRel)).ToList());
                            break;
                        
                    }



                }
                else if (!Exact && FilterItemClassRel != null && (!string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Left) ||
                                                            !string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Right) ||
                                                            !string.IsNullOrEmpty(FilterItemClassRel.Name_RelationType) ||
                                                            !string.IsNullOrEmpty(FilterItemClassRel.Ontology) ||
                                                            FilterItemClassRel.Min_Forw != null ||
                                                            FilterItemClassRel.Max_Forw != null ||
                                                            FilterItemClassRel.Max_Backw != null))
                {
                    var classRelViewItems = dbReadConnector_ClassRelations.ClassRels.Where(classRelItem => (!string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Left) ? classRelItem.Name_Class_Left.ToLower().Contains(FilterItemClassRel.Name_Class_Left.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassRel.Name_Class_Right) ? classRelItem.Name_Class_Right.ToLower().Contains(FilterItemClassRel.Name_Class_Right.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassRel.Name_RelationType) ? classRelItem.Name_RelationType.ToLower().Contains(FilterItemClassRel.Name_RelationType.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemClassRel.Ontology) ? classRelItem.Ontology.ToLower().Contains(FilterItemClassRel.Ontology.ToLower()) : 1 == 1) &&
                                                                                                                       (FilterItemClassRel.Min_Forw != null ? classRelItem.Min_Forw == FilterItemClassRel.Min_Forw : 1 == 1) &&
                                                                                                                       (FilterItemClassRel.Max_Forw != null ? classRelItem.Max_Forw == FilterItemClassRel.Max_Forw : 1 == 1) &&
                                                                                                                       (FilterItemClassRel.Max_Backw != null ? classRelItem.Max_Backw == FilterItemClassRel.Max_Backw : 1 == 1)).ToList();

                    switch (listType)
                    {
                        case ListType.ClassOtherRelation_Conscious:
                        case ListType.ClassClassRelation_Conscious:
                            result.ClassRelConciousViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationConsciousViewItem(clsRel)).ToList());
                            break;
                        case ListType.ClassClassRelation_Subconscious:
                            result.ClassRelSubconsciousViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationSubconsciousViewItem(clsRel)).ToList());
                            break;

                        case ListType.ClassClassRelation_Omni:
                            result.ClassRelOmniViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationOmniViewItem(clsRel)).ToList());
                            break;
                    }
                }
                else
                {
                    var classRelViewItems = dbReadConnector_ClassRelations.ClassRels;
                    switch (listType)
                    {
                        case ListType.ClassOtherRelation_Conscious:
                        case ListType.ClassClassRelation_Conscious:
                            result.ClassRelConciousViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationConsciousViewItem(clsRel)).ToList());
                            break;
                        case ListType.ClassClassRelation_Subconscious:
                            result.ClassRelSubconsciousViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationSubconsciousViewItem(clsRel)).ToList());
                            break;

                        case ListType.ClassClassRelation_Omni:
                            result.ClassRelOmniViewItems.AddRange(classRelViewItems.Select(clsRel => new ClassRelationOmniViewItem(clsRel)).ToList());
                            break;
                    }
                }



            }
            return result;
        }

        public clsOntologyItem SaveObjectAttributes(List<clsObjectAtt> objectAttributes)
        {

            var dbWriteConnector = new OntologyModDBConnector(globals);
            var result = dbWriteConnector.SaveObjAtt(objectAttributes);

            return result;
        }

        public clsOntologyItem SaveObjectRelations(List<clsObjectRel> objectRelations)
        {
            var dbWriteConnector = new OntologyModDBConnector(globals);
            var result = dbWriteConnector.SaveObjRel(objectRelations);

            return result;
        }

        private ResultViewItems LoadClassRelList(ListType listType)
        {
            var result = new ResultViewItems
            {
                ListType = listType,
                Result = globals.LState_Nothing.Clone()
            };

            ResultListLoader = result;
            
            result.Result = globals.LState_Error.Clone();
            
            switch (listType)
            {
                case ListType.ClassClassRelation_Conscious:
                    result = AddClassRelsToList(false, listType);
                    break;
                case ListType.ClassClassRelation_Subconscious:
                    result = AddClassRelsToList(false, listType);
                    break;
                case ListType.ClassClassRelation_Omni:
                    result = AddClassRelsToList(false, listType);
                    if (result.Result.GUID == globals.LState_Success.GUID)
                    {
                        result = AddClassRelsToList(true, listType);
                    }
                    
                    break;
                case ListType.ClassOtherRelation_Conscious:
                    result = AddClassRelsToList(true, listType);
                    break;
            }

            return result;
        }

        public async Task<ResultViewItems> Initialize_ObjectAttributeList(clsObjectAtt filterItem, bool exact = false)
        {
            ListLoadState = ListLoadStateItem.ListLoadPending;
            var result = new ResultViewItems
            {
                ListType = ListType.InstanceAttribute_Conscious,
                Result = globals.LState_Nothing.Clone()
            };

            ResultListLoader = result;


            FilterItemObjectAttribute = filterItem;
            Exact = exact;

            result = LoadObjAttList();
            ResultListLoader = result;
            return result;
        }

        private ResultViewItems LoadObjAttList()
        {
            var dbReadConnector_ObjectAttributes = new OntologyModDBConnector(globals);

            var result = new ResultViewItems
            {
                ListType = ListType.InstanceAttribute_Conscious,
                Result = globals.LState_Nothing.Clone()
            };
            result.Result = dbReadConnector_ObjectAttributes.GetDataObjectAtt(FilterItemObjectAttribute != null ? new List<clsObjectAtt> { FilterItemObjectAttribute } : null);
            if (result.Result.GUID == globals.LState_Success.GUID)
            {


                if (Exact && FilterItemObjectAttribute != null && (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Attribute) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_AttributeType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Class) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_DataType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_AttributeType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Class) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_DataType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Object) ||
                                                            FilterItemObjectAttribute.OrderID != null ||
                                                            FilterItemObjectAttribute.Val_Bit != null ||
                                                            FilterItemObjectAttribute.Val_Date != null ||
                                                            FilterItemObjectAttribute.Val_Double != null ||
                                                            FilterItemObjectAttribute.Val_Int != null) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Val_String))
                {
                    result.ObjectAttributeViewItems = (from objectAttribute in dbReadConnector_ObjectAttributes.ObjAtts.Where(objAtt => (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Attribute) ? objAtt.ID_Attribute == FilterItemObjectAttribute.ID_Attribute : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_AttributeType) ? objAtt.ID_AttributeType == FilterItemObjectAttribute.ID_AttributeType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Class) ? objAtt.ID_Class == FilterItemObjectAttribute.ID_Class : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_DataType) ? objAtt.ID_DataType == FilterItemObjectAttribute.ID_DataType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Object) ? objAtt.ID_Object == FilterItemObjectAttribute.ID_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_AttributeType) ? objAtt.Name_AttributeType == FilterItemObjectAttribute.Name_AttributeType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Class) ? objAtt.Name_Class == FilterItemObjectAttribute.Name_Class : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_DataType) ? objAtt.Name_DataType == FilterItemObjectAttribute.Name_DataType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Object) ? objAtt.Name_Object == FilterItemObjectAttribute.Name_Object : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.OrderID != null ? objAtt.OrderID == FilterItemObjectAttribute.OrderID : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Bit != null ? objAtt.Val_Bit == FilterItemObjectAttribute.Val_Bit : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Date != null ? objAtt.Val_Date == FilterItemObjectAttribute.Val_Date : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Double != null ? objAtt.Val_Double == FilterItemObjectAttribute.Val_Double : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Int != null ? objAtt.Val_Int == FilterItemObjectAttribute.Val_Int : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_String != null ? objAtt.Val_String.ToLower().Contains(FilterItemObjectAttribute.Val_String.ToLower()) : 1 == 1))
                                         select new ObjectAttributeViewItem(objectAttribute)).ToList();

                }
                else if (!Exact && FilterItemObjectAttribute != null && (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Attribute) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_AttributeType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Class) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_DataType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_AttributeType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Class) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_DataType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Object) ||
                                                            FilterItemObjectAttribute.OrderID != null ||
                                                            FilterItemObjectAttribute.Val_Bit != null ||
                                                            FilterItemObjectAttribute.Val_Date != null ||
                                                            FilterItemObjectAttribute.Val_Double != null ||
                                                            FilterItemObjectAttribute.Val_Int != null) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectAttribute.Val_String))
                {
                    result.ObjectAttributeViewItems = (from objectAttribute in dbReadConnector_ObjectAttributes.ObjAtts.Where(objAtt => (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Attribute) ? objAtt.ID_Attribute == FilterItemObjectAttribute.ID_Attribute : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_AttributeType) ? objAtt.ID_AttributeType == FilterItemObjectAttribute.ID_AttributeType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Class) ? objAtt.ID_Class == FilterItemObjectAttribute.ID_Class : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_DataType) ? objAtt.ID_DataType == FilterItemObjectAttribute.ID_DataType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.ID_Object) ? objAtt.ID_Object == FilterItemObjectAttribute.ID_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_AttributeType) ? objAtt.Name_AttributeType.ToLower().Contains(FilterItemObjectAttribute.Name_AttributeType.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Class) ? objAtt.Name_Class.ToLower().Contains(FilterItemObjectAttribute.Name_Class.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_DataType) ? objAtt.Name_DataType.ToLower().Contains(FilterItemObjectAttribute.Name_DataType.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectAttribute.Name_Object) ? objAtt.Name_Object.ToLower().Contains(FilterItemObjectAttribute.Name_Object.ToLower()) : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.OrderID != null ? objAtt.OrderID == FilterItemObjectAttribute.OrderID : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Bit != null ? objAtt.Val_Bit == FilterItemObjectAttribute.Val_Bit : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Date != null ? objAtt.Val_Date == FilterItemObjectAttribute.Val_Date : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Double != null ? objAtt.Val_Double == FilterItemObjectAttribute.Val_Double : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_Int != null ? objAtt.Val_Int == FilterItemObjectAttribute.Val_Int : 1 == 1) &&
                                                                                                                       (FilterItemObjectAttribute.Val_String != null ? objAtt.Val_String.ToLower().Contains(FilterItemObjectAttribute.Val_String.ToLower()) : 1 == 1))
                                         select new ObjectAttributeViewItem(objectAttribute)).ToList();
                }
                else
                {
                    result.ObjectAttributeViewItems = (from objectAttribute in dbReadConnector_ObjectAttributes.ObjAtts
                                                select new ObjectAttributeViewItem(objectAttribute)).ToList();

                }



            }
            else
            {
                result.ObjectAttributeViewItems = new List<ObjectAttributeViewItem>();
            }

            return result;
        }

        public async Task<ResultViewItems> Initialize_ObjectRelationList(ListType listType, clsObjectRel filterItem, bool exact = false)
        {
            ListLoadState = ListLoadStateItem.ListLoadPending;
            var result = new ResultViewItems
            {
                ListType = listType,
                Result = globals.LState_Nothing.Clone()
            };

            ResultListLoader = result;

            this.listType = listType;
            FilterItemObjectRelation = filterItem;
            Exact = exact;

            result = LoadObjRelList(listType);
            ResultListLoader = result;
            return result;
        }

        private ResultViewItems LoadObjRelList(ListType listType)
        {
            var dbReadConnector_ObjectRelations = new OntologyModDBConnector(globals);
            var dbReadConnector_ObjectRelationsOmni = new OntologyModDBConnector(globals);

            var result = new ResultViewItems
            {
                ListType = listType,
                Result = globals.LState_Nothing.Clone()
            };

            if (listType == ListType.InstanceInstance_Omni)
            {
                result.Result = dbReadConnector_ObjectRelations.GetDataObjectRel(FilterItemObjectRelation != null ? new List<clsObjectRel> { new clsObjectRel { ID_Object = FilterItemObjectRelation.ID_Object } } : null);
                result.Result = dbReadConnector_ObjectRelationsOmni.GetDataObjectRel(FilterItemObjectRelation != null ? new List<clsObjectRel> { new clsObjectRel { ID_Other = FilterItemObjectRelation.ID_Object } } : null);
            }
            else
            {
                result.Result = dbReadConnector_ObjectRelations.GetDataObjectRel(FilterItemObjectRelation != null ? new List<clsObjectRel> { FilterItemObjectRelation } : null);
            }
            
            if (result.Result.GUID == globals.LState_Success.GUID)
            {

                var objectRelationList = new List<clsObjectRel>();
                if (listType == ListType.InstanceInstance_Omni)
                {
                    objectRelationList = dbReadConnector_ObjectRelations.ObjectRels;
                    objectRelationList.AddRange(dbReadConnector_ObjectRelationsOmni.ObjectRels);
                }
                else if (Exact && FilterItemObjectRelation != null && (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_RelationType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_RelationType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Ontology) ||
                                                            FilterItemObjectRelation.OrderID != null))
                {
                    objectRelationList = dbReadConnector_ObjectRelations.ObjectRels.Where(objRel => (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Object) ? objRel.ID_Object == FilterItemObjectRelation.ID_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Other) ? objRel.ID_Other == FilterItemObjectRelation.ID_Other : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Object) ? objRel.ID_Parent_Object == FilterItemObjectRelation.ID_Parent_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Other) ? objRel.ID_Parent_Other == FilterItemObjectRelation.ID_Parent_Other : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_RelationType) ? objRel.ID_RelationType == FilterItemObjectRelation.ID_RelationType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Object) ? objRel.Name_Object == FilterItemObjectRelation.Name_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Other) ? objRel.Name_Other == FilterItemObjectRelation.Name_Other : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Object) ? objRel.Name_Parent_Object == FilterItemObjectRelation.Name_Parent_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Other) ? objRel.Name_Parent_Other == FilterItemObjectRelation.Name_Parent_Other : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_RelationType) ? objRel.Name_RelationType == FilterItemObjectRelation.Name_RelationType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Ontology) ? objRel.Ontology == FilterItemObjectRelation.Ontology : 1 == 1) &&
                                                                                                                       (FilterItemObjectRelation.OrderID != null ? objRel.OrderID == FilterItemObjectRelation.OrderID : 1 == 1)).ToList();

                }
                else if (!Exact && FilterItemObjectRelation != null && (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.ID_RelationType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Object) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Other) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Name_RelationType) ||
                                                            !string.IsNullOrEmpty(FilterItemObjectRelation.Ontology) ||
                                                            FilterItemObjectRelation.OrderID != null))
                {
                    objectRelationList = dbReadConnector_ObjectRelations.ObjectRels.Where(objRel => (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Object) ? objRel.ID_Object == FilterItemObjectRelation.ID_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Other) ? objRel.ID_Other == FilterItemObjectRelation.ID_Other : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Object) ? objRel.ID_Parent_Object == FilterItemObjectRelation.ID_Parent_Object : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_Parent_Other) ? objRel.ID_Parent_Other == FilterItemObjectRelation.ID_Parent_Other : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.ID_RelationType) ? objRel.ID_RelationType == FilterItemObjectRelation.ID_RelationType : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Object) ? objRel.Name_Object.ToLower().Contains(FilterItemObjectRelation.Name_Object.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Other) ? objRel.Name_Other.ToLower().Contains(FilterItemObjectRelation.Name_Other.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Object) ? objRel.Name_Parent_Object.ToLower().Contains(FilterItemObjectRelation.Name_Parent_Object.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_Parent_Other) ? objRel.Name_Parent_Other.ToLower().Contains(FilterItemObjectRelation.Name_Parent_Other.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Name_RelationType) ? objRel.Name_RelationType.ToLower().Contains(FilterItemObjectRelation.Name_RelationType.ToLower()) : 1 == 1) &&
                                                                                                                       (!string.IsNullOrEmpty(FilterItemObjectRelation.Ontology) ? objRel.Ontology.ToLower().Contains(FilterItemObjectRelation.Ontology.ToLower()) : 1 == 1) &&
                                                                                                                       (FilterItemObjectRelation.OrderID != null ? objRel.OrderID == FilterItemObjectRelation.OrderID : 1 == 1)).ToList();
                }
                

                switch (listType)
                {
                    case ListType.InstanceInstance_Conscious:
                        result.ObjectObjectConsciousViewItems = objectRelationList.Select(objRel => new ObjectObject_Conscious(objRel)).ToList();
                        break;
                    case ListType.InstanceInstance_Subconscious:
                        result.ObjectObjectSubconsciousViewItems = objectRelationList.Select(objRel => new ObjectObject_Subconscious(objRel)).ToList();
                        break;
                    case ListType.InstanceInstance_Omni:
                        result.ObjectObjectOmniViewItems = objectRelationList.Select(objRel => new ObjectObject_Omni(objRel)).ToList();
                        break;
                    case ListType.InstanceOther_Conscious:
                        //ObjectOtherConsciousViewItems = objectRelationList.Where(objRel => objRel.Ontology != globals.Type_Object).Select(objRel => new ObjectObject_Conscious(objRel)).ToList();
                        result.ObjectOtherConsciousViewItems = objectRelationList.Select(objRel => new ObjectObject_Conscious(objRel)).ToList();
                        break;
                    case ListType.InstanceOther_Subconscious:
                        //ObjectOtherSubconsciousViewItems = objectRelationList.Where(objRel => objRel.Ontology != globals.Type_Object).Select(objRel => new ObjectObject_Subconscious(objRel)).ToList();
                        result.ObjectOtherSubconsciousViewItems = objectRelationList.Select(objRel => new ObjectObject_Subconscious(objRel)).ToList();
                        break;
                }
                

            }
            else
            {
                result.ObjectAttributeViewItems = new List<ObjectAttributeViewItem>();
            }

            return result;
        }

        private void Initialize()
        {
            
        }
    }

    public class ResultViewItems
    {
        public ListType ListType { get; set; }
        public clsOntologyItem Result { get; set; }
        public List<ClassViewItem> ClassViewItems { get; set; }
        public List<AttributeTypeViewItem> AttributeTypeViewItems { get; set; }
        public List<RelationTypeViewItem> RelationTypeViewItems { get; set; }
        public List<InstanceViewItem> InstanceViewItems { get; set; }
        public List<ClassAttributeViewItem> ClassAttViewItems { get; set; }
        public List<ClassRelationConsciousViewItem> ClassRelConciousViewItems { get; set; }
        public List<ClassRelationSubconsciousViewItem> ClassRelSubconsciousViewItems { get; set; }
        public List<ClassRelationOmniViewItem> ClassRelOmniViewItems { get; set; }
        public List<ObjectAttributeViewItem> ObjectAttributeViewItems { get; set; }
        public List<ObjectObject_Conscious> ObjectObjectConsciousViewItems { get; set; }
        public List<ObjectObject_Subconscious> ObjectObjectSubconsciousViewItems { get; set; }
        public List<ObjectObject_Conscious> ObjectOtherConsciousViewItems { get; set; }
        public List<ObjectObject_Subconscious> ObjectOtherSubconsciousViewItems { get; set; }
        public List<ObjectObject_Omni> ObjectObjectOmniViewItems { get; set; }

        public ResultViewItems()
        {
            ClassViewItems = new List<ClassViewItem>();
            AttributeTypeViewItems = new List<AttributeTypeViewItem>();
            RelationTypeViewItems = new List<RelationTypeViewItem>();
            InstanceViewItems = new List<InstanceViewItem>();
            ClassAttViewItems = new List<ClassAttributeViewItem>();
            ClassRelConciousViewItems = new List<ClassRelationConsciousViewItem>();
            ClassRelSubconsciousViewItems = new List<ClassRelationSubconsciousViewItem>();
            ClassRelOmniViewItems = new List<ClassRelationOmniViewItem>();
            ObjectAttributeViewItems = new List<ObjectAttributeViewItem>();
            ObjectObjectConsciousViewItems = new List<ObjectObject_Conscious>();
            ObjectObjectSubconsciousViewItems = new List<ObjectObject_Subconscious>();
            ObjectOtherConsciousViewItems = new List<ObjectObject_Conscious>();
            ObjectOtherSubconsciousViewItems = new List<ObjectObject_Subconscious>();
            ObjectObjectOmniViewItems = new List<ObjectObject_Omni>();
        }
    }

}
