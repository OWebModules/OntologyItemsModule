﻿using ImportExport_Module.Base;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntologyItemsModule.Validations;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Services
{
    public class ElasticAgentOItems : ElasticBaseAgent
    {

        public async Task<clsOntologyItem> ChangeAttributes(List<clsObjectAtt> objRels)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbWriter = new OntologyModDBConnector(globals);

                var result = dbWriter.SaveObjAtt(objRels);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> ChangeRelations(List<clsObjectRel> objRels)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbWriter = new OntologyModDBConnector(globals);

                var result = dbWriter.SaveObjRel(objRels);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveItems(List<clsOntologyItem> items)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbWriter = new OntologyModDBConnector(globals);

                var attributeTypes = items.Where(itm => itm.Type == globals.Type_AttributeType).ToList();
                var relationTypes = items.Where(itm => itm.Type == globals.Type_RelationType).ToList();
                var classes = items.Where(itm => itm.Type == globals.Type_Class).ToList();
                var objects = items.Where(itm => itm.Type == globals.Type_Object).ToList();


                var result = globals.LState_Success.Clone();
                if (attributeTypes.Any())
                {
                    result = dbWriter.SaveAttributeTypes(attributeTypes);
                }
                else if (result.GUID == globals.LState_Success.GUID && relationTypes.Any())
                {
                    result = dbWriter.SaveRelationTypes(relationTypes);
                }
                else if (result.GUID == globals.LState_Success.GUID && classes.Any())
                {
                    result = dbWriter.SaveClass(classes);
                }
                else if (result.GUID == globals.LState_Success.GUID && objects.Any())
                {
                    result = dbWriter.SaveObjects(objects);
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectAtt>>> GetObjectAttributes(List<clsObjectAtt> objRels)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectAtt>>>(() =>
            {
                var result = new ResultItem<List<clsObjectAtt>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchRelations = objRels.Select(rel => new clsObjectAtt
                {
                    ID_Attribute = rel.ID_Attribute,
                    ID_AttributeType = rel.ID_AttributeType
                }).ToList();

                var dbReaderRels = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderRels.GetDataObjectAtt(searchRelations);

                if (result.ResultState.GUID == globals.LState_Success.GUID)
                {
                    result.Result = dbReaderRels.ObjAtts;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<long>> GetObjectAttributesCount(List<clsObjectAtt> objRels)
        {
            var taskResult = await Task.Run<ResultItem<long>>(() =>
            {
                var result = new ResultItem<long>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchRelations = objRels.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Object,
                    ID_Attribute = rel.ID_Attribute,
                    ID_AttributeType = rel.ID_AttributeType
                }).ToList();

                var dbReaderRels = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderRels.GetDataObjectAtt(searchRelations, doCount: true);

                if (result.ResultState.GUID == globals.LState_Success.GUID)
                {
                    result.Result = result.ResultState.Count.Value;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetObjectRelations(List<clsObjectRel> objRels)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchRelations = objRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = rel.ID_RelationType,
                    ID_Other = rel.ID_Other
                }).ToList();

                var dbReaderRels = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderRels.GetDataObjectRel(searchRelations);

                if (result.ResultState.GUID == globals.LState_Success.GUID)
                {
                    result.Result = dbReaderRels.ObjectRels;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<long>> GetObjectRelationsCount(List<clsObjectRel> objRels)
        {
            var taskResult = await Task.Run<ResultItem<long>>(() =>
            {
                var result = new ResultItem<long>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchRelations = objRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = rel.ID_RelationType,
                    ID_Other = rel.ID_Other
                }).ToList();

                var dbReaderRels = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderRels.GetDataObjectRel(searchRelations, doCount:true);

                if (result.ResultState.GUID == globals.LState_Success.GUID)
                {
                    result.Result = result.ResultState.Count.Value;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjects(List<clsOntologyItem> objects)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchObjects = objects.Select(obj => new clsOntologyItem
                {
                    GUID = obj.GUID,
                    Name = obj.Name,
                    GUID_Parent = obj.GUID_Parent,
                    Type = globals.Type_Object
                }).ToList();

                var dbReaderObjs = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderObjs.GetDataObjects(searchObjects);

                if (result.ResultState.GUID == globals.LState_Success.GUID)
                {
                    result.Result = dbReaderObjs.Objects1;
                }

                return result;
            });

            return taskResult;
        }
        
        public async Task<ResultItem<List<clsOntologyItem>>> GetClassesOfObjects(List<clsOntologyItem> objects)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };

               var searchClasses = objects.GroupBy(obj => obj.GUID_Parent).Select(objGrp => new clsOntologyItem { GUID = objGrp.Key }).ToList();

               if (!searchClasses.Any()) return result;
               var dbReaderClasses = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderClasses.GetDataClasses(searchClasses);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the classes!";
                   return result;
               }

               result.Result = dbReaderClasses.Classes1;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjects(string idClass)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var dbReader = new OntologyModDBConnector(globals);

                var search = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = idClass
                    }
                };

                result.ResultState = dbReader.GetDataObjects(search);

                result.Result = dbReader.Objects1;

                return result;
            });

            return taskResult;
        }
        
        public async Task<ResultItem<List<clsOntologyItem>>> GetObjects(List<string> objectNames, string idClass)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };

               var dbReaderObjects = new OntologyModDBConnector(globals);

               var searchObjects = objectNames.Select(name => new clsOntologyItem
               {
                   Name = name,
                   GUID_Parent = idClass
               }).ToList();

               result.ResultState = dbReaderObjects.GetDataObjects(searchObjects);
               
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the objects!";
                   return result;
               }

               result.Result = (from name in objectNames
                                join obj in dbReaderObjects.Objects1 on name.ToLower() equals obj.Name.ToLower()
                                select obj).ToList();

               return result;
           });

            return taskResult;
        }      

        public async Task<ResultItem<clsObjectRel>> GetRelation(clsObjectRel searchRel)
        {
            var taskResult = await Task.Run<ResultItem<clsObjectRel>>(() =>
            {
                var result = new ResultItem<clsObjectRel>
                {
                    ResultState = globals.LState_Success.Clone()
                };
                
                var dbReader = new OntologyModDBConnector(globals);
                result.ResultState = dbReader.GetDataObjectRel(new List<clsObjectRel> { searchRel });

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Relations!";
                    return result;
                }

                result.Result = dbReader.ObjectRels.FirstOrDefault();

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> DeleteItems(List<clsObjectAtt> itemsToDelete)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbDeletor = new OntologyModDBConnector(globals);

                itemsToDelete = itemsToDelete.Select(itm => new clsObjectAtt { ID_Attribute = itm.ID_Attribute }).ToList();
                result = dbDeletor.DelObjectAtts(itemsToDelete);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> DeleteItems(List<clsObjectRel> itemsToDelete)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbDeletor = new OntologyModDBConnector(globals);

                result = dbDeletor.DelObjectRels(itemsToDelete);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> DeleteItems(List<clsOntologyItem> itemsToDelete)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbDeletor = new OntologyModDBConnector(globals);

                result = dbDeletor.DelObjects(itemsToDelete);
                if (result.Count > 0)
                {
                    result = globals.LState_Error.Clone();
                }
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetRelatedObjects(GetMultiselectConfigRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                if (!request.IsValid(globals))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = request.ValidationMessage;
                    return result;
                }

                var dbReaderRelation = new OntologyModDBConnector(globals);

                var search = new List<clsObjectRel>();

                if (request.IdDirection == globals.Direction_LeftRight.GUID)
                {
                    search.Add(new clsObjectRel
                    {
                        ID_Object = request.IdObject,
                        ID_RelationType = request.IdRelationType,
                        ID_Parent_Other = request.IdClassOther
                    });
                }
                else
                {
                    search.Add(new clsObjectRel
                    {
                        ID_Other = request.IdObject,
                        ID_RelationType = request.IdRelationType,
                        ID_Parent_Object = request.IdClassOther
                    });
                }

                result.ResultState = dbReaderRelation.GetDataObjectRel(search);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the object-relations!";
                    return result;
                }

                if (request.IdDirection == globals.Direction_LeftRight.GUID)
                {
                    result.Result = dbReaderRelation.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).ToList();
                }
                else
                {
                    result.Result = dbReaderRelation.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Object,
                        Name = rel.Name_Object,
                        GUID_Parent = rel.ID_Parent_Object,
                        Type = globals.Type_Object
                    }).ToList();
                }
                

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsClassRel>> GetClassRel(GetMultiselectConfigRequest request)
        {
            var taskResult = await Task.Run<ResultItem<clsClassRel>>(async() =>
            {
                var result = new ResultItem<clsClassRel>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                if (!request.IsValid(globals))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = request.ValidationMessage;
                    return result;
                }

                var dbReader = new OntologyModDBConnector(globals);

                var getOItemResult = await GetOItem(request.IdObject, globals.Type_Object);

                result.ResultState = getOItemResult.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                if (result.ResultState.GUID == globals.LState_Nothing.GUID)
                {
                    return result;
                }

                var search = new List<clsClassRel>();

                if (request.IdDirection == globals.Direction_LeftRight.GUID)
                {
                    search.Add(new clsClassRel
                    {
                        ID_Class_Left = getOItemResult.Result.GUID_Parent,
                        ID_RelationType = request.IdRelationType,
                        ID_Class_Right = request.IdClassOther
                    });
                }
                else
                {
                    search.Add(new clsClassRel
                    {
                        ID_Class_Right = getOItemResult.Result.GUID_Parent,
                        ID_RelationType = request.IdRelationType,
                        ID_Class_Left = request.IdClassOther
                    });
                }

                result.ResultState = dbReader.GetDataClassRel(search);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Classrelation!";
                    return result;
                }

                result.Result = dbReader.ClassRels.FirstOrDefault();

                if (result.Result == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Classrelation found!";
                    return result;
                }

                return result;
            });
            return taskResult;
        }

        public async Task<ResultClassPath> GetClassPath(clsOntologyItem objectItem)
        {
            var taskResult = await Task.Run<ResultClassPath>(() =>
            {
                var result = new ResultClassPath
                {
                    Result = globals.LState_Success.Clone(),
                    Path = ""
                };
                var dbReader = new OntologyModDBConnector(globals);
                var resultRead = dbReader.GetDataClasses(new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID = objectItem.GUID_Parent
                }
            });

                if (resultRead.GUID == globals.LState_Error.GUID)
                {

                    result.Result = resultRead;
                    return result;
                }

                var classItem = dbReader.Classes1.FirstOrDefault();
                if (classItem == null)
                {
                    result.Result = globals.LState_Error.Clone();
                    return result;
                }

                while (classItem.GUID_Parent != globals.Root.GUID)
                {
                    result.Path = $"{ classItem.Name }\\{result.Path}";

                    if (classItem.GUID_Parent != globals.Root.GUID)
                    {
                        resultRead = dbReader.GetDataClasses(new List<clsOntologyItem>
                        {
                            new clsOntologyItem
                            {
                                GUID = classItem.GUID_Parent
                            }
                        });

                        if (resultRead.GUID == globals.LState_Error.GUID)
                        {

                            result.Result = resultRead;
                            return result;
                        }

                        classItem = dbReader.Classes1.FirstOrDefault();
                    }

                }

                result.Path = $"{ globals.Root.Name }\\{result.Path}";

                return result;
            });


            return taskResult;
        }

        public async Task<clsOntologyItem> IsExistingRelationType(string name)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);
                var result = dbReader.GetDataRelationTypes(new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    Name = name
                }
            });

                if (result.GUID == globals.LState_Error.GUID)
                {
                    var resultAgent = globals.LState_Error.Clone();
                    return resultAgent;
                }

                if (dbReader.RelationTypes.Any(relType => relType.Name.ToLower() == name.ToLower()))
                {
                    var resultAgent = globals.LState_Exists.Clone();
                    return resultAgent;
                }
                else
                {
                    var resultAgent = globals.LState_Success.Clone();
                    return resultAgent;
                }
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> IsExistingClass(string name)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);
                var result = dbReader.GetDataClasses(new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    Name = name
                }
            });

                if (result.GUID == globals.LState_Error.GUID)
                {
                    var resultAgent = globals.LState_Error.Clone();
                    return resultAgent;
                }

                if (dbReader.Classes1.Any(relType => relType.Name.ToLower() == name.ToLower()))
                {
                    var resultAgent = globals.LState_Exists.Clone();
                    return resultAgent;
                }
                else
                {
                    var resultAgent = globals.LState_Success.Clone();
                    return resultAgent;
                }
            });

            return taskResult;

        }

        public async Task<clsOntologyItem> IsExistingObject(string name, string idClass)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);
                var result = dbReader.GetDataObjects(new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    Name = name,
                    GUID_Parent = idClass
                }
            });

                if (result.GUID == globals.LState_Error.GUID)
                {
                    var resultAgent = globals.LState_Error.Clone();
                    return resultAgent;
                }

                if (dbReader.Objects1.Any(relType => relType.Name.ToLower() == name.ToLower()))
                {
                    var resultAgent = globals.LState_Exists.Clone();
                    return resultAgent;
                }
                else
                {
                    var resultAgent = globals.LState_Success.Clone();
                    return resultAgent;
                }
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> IsExistingAttributeType(string name)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);
                var result = dbReader.GetDataAttributeType(new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    Name = name
                }
            });

                if (result.GUID == globals.LState_Error.GUID)
                {
                    var resultAgent = globals.LState_Error.Clone();
                    return resultAgent;
                }

                if (dbReader.AttributeTypes.Any(relType => relType.Name.ToLower() == name.ToLower()))
                {
                    var resultAgent = globals.LState_Exists.Clone();
                    return resultAgent;
                }
                else
                {
                    var resultAgent = globals.LState_Success.Clone();
                    return resultAgent;
                }
            });
            return taskResult;
        }

        public async Task<ResultGetAttribute> GetAttribute(string idAttribute)
        {
            var taskResult = await Task.Run<ResultGetAttribute>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);
                var result = new ResultGetAttribute
                {
                    Result = globals.LState_Success.Clone()
                };

                result.Result = dbReader.GetDataObjectAtt(new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Attribute = idAttribute
                    }
                });

                if (result.Result.GUID != globals.LState_Error.GUID)
                {
                    result.ObjectAttribute = dbReader.ObjAtts.FirstOrDefault();


                }

                return result;
            });

            if (taskResult.ObjectAttribute != null)
            {
                taskResult.ObjectItem = (await GetOItem(taskResult.ObjectAttribute.ID_Object, globals.Type_Object)).Result;
                taskResult.AttributeTypeItem = (await GetOItem(taskResult.ObjectAttribute.ID_AttributeType, globals.Type_AttributeType)).Result;
            }

            return taskResult;
        }

        public async Task<clsOntologyItem> SetOrderId(List<OrderIdChange> orderIdChanges)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();



                var dbReader = new OntologyModDBConnector(globals);

                var searchItems = orderIdChanges.Where(change => !string.IsNullOrEmpty(change.IdObject) && !string.IsNullOrEmpty(change.IdOther) && !string.IsNullOrEmpty(change.IdRelationType)).Select(change => new clsObjectRel
                {
                    ID_Object = change.IdObject,
                    ID_Other = change.IdOther,
                    ID_RelationType = change.IdRelationType
                }).ToList();

                result = dbReader.GetDataObjectRel(searchItems);

                if (result.GUID == globals.LState_Error.GUID || !dbReader.ObjectRels.Any())
                {
                    result = globals.LState_Error.Clone();
                    return result;
                }

                var changes = (from toChange in orderIdChanges
                               join existing in dbReader.ObjectRels on new { toChange.IdObject, toChange.IdOther, toChange.IdRelationType } equals new { IdObject = existing.ID_Object, IdOther = existing.ID_Other, IdRelationType = existing.ID_RelationType }
                               where toChange.OrderId != existing.OrderID
                               select new { toChange, existing }).ToList();

                changes.ForEach(change =>
                {
                    change.existing.OrderID = change.toChange.OrderId;
                });

                if (changes.Any())
                {
                    result = dbReader.SaveObjRel(changes.Select(change => change.existing).ToList());
                }


                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveAttribute(clsObjectAtt objectAttribute)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);
                var result = dbReader.SaveObjAtt(new List<clsObjectAtt> { objectAttribute });

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveObjects(List<clsOntologyItem> objects)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbWriter = new OntologyModDBConnector(globals);
                var result = dbWriter.SaveObjects(objects);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetRelationTypes()
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    Result = new List<clsOntologyItem>()
                };

                var dbReader = new OntologyModDBConnector(globals);
                result.ResultState = dbReader.GetDataRelationTypes(null);

                if (result.ResultState.GUID == globals.LState_Success.GUID)
                {
                    result.Result = dbReader.RelationTypes;
                }
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetClasses()
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    Result = new List<clsOntologyItem>()
                };

                var dbReader = new OntologyModDBConnector(globals);
                result.ResultState = dbReader.GetDataClasses();

                if (result.ResultState.GUID == globals.LState_Success.GUID)
                {
                    result.Result = dbReader.Classes1;
                }
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsClassAtt>>> GetClassAttributes(List<clsClassAtt> classAttributes)
        {
            var taskResult = await Task.Run<ResultItem<List<clsClassAtt>>>(() =>
            {
                var result = new ResultItem<List<clsClassAtt>>
                {
                    Result = new List<clsClassAtt>()
                };

                if (!classAttributes.Any())
                {
                    return result;
                }
                var dbReader = new OntologyModDBConnector(globals);
                result.ResultState = dbReader.GetDataClassAtts(classAttributes, doIds: false);

                if (result.ResultState.GUID == globals.LState_Success.GUID)
                {
                    result.Result = dbReader.ClassAtts;
                }
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectAtt>>> GetClassAttributes(List<clsObjectAtt> objectAttributes)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectAtt>>>(() =>
            {
                var result = new ResultItem<List<clsObjectAtt>>
                {
                    Result = new List<clsObjectAtt>()
                };

                if (!objectAttributes.Any())
                {
                    return result;
                }
                var dbReader = new OntologyModDBConnector(globals);
                result.ResultState = dbReader.GetDataObjectAtt(objectAttributes, doIds: true);

                if (result.ResultState.GUID == globals.LState_Success.GUID)
                {
                    result.Result = dbReader.ObjAtts;
                }
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjects(GetMultiselectConfigRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                if (!request.IsValid(globals))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = request.ValidationMessage;
                    return result;
                }

                var dbReader = new OntologyModDBConnector(globals);
                var search = new List<clsOntologyItem>();
                
                search.Add(new clsOntologyItem
                {
                    GUID_Parent = request.IdClassOther
                });

                result.ResultState = dbReader.GetDataObjects(search);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting objects!";
                    return result;
                }

                result.Result = dbReader.Objects1;
                
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<MergeObjectsByClassModel>> GetMergeObjectsByClassModel(MergeObjectsRequestByClass request)
        {
            var taskResult = await Task.Run<ResultItem<MergeObjectsByClassModel>>(() =>
           {
               var result = new ResultItem<MergeObjectsByClassModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new MergeObjectsByClassModel()
               };

               var searchConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig
                   }
               };

               var dbReaderConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the config!";
                   return result;
               }

               result.Result.ConfigItem = dbReaderConfig.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateMergeObjectsByClassModel(result.Result, globals, nameof(MergeObjectsByClassModel.ConfigItem));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchClasses = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = MergeObjects.Config.LocalData.ClassRel_Merge_Objects_By_Class_belonging_Class.ID_RelationType
                   }
               };




               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<DeleteObjectsModel>> GetDeleteObjectsModel(DeleteObjectsRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<DeleteObjectsModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new DeleteObjectsModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Config!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetDeleteObjectsModel(result.Result, dbReaderConfig, globals, nameof(DeleteObjectsModel.Config));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchClasses = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = OntologyItemsModule.DeleteObjects.Config.LocalData.RelationType_belonging_Class.GUID
                    }
                };

                var dbReaderClasses = new OntologyModDBConnector(globals);

                if (searchClasses.Any())
                {
                    result.ResultState = dbReaderClasses.GetDataObjectRel(searchClasses);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the classes!";
                        return result;
                    }
                }

                result.ResultState = ValidationController.ValidateAndSetDeleteObjectsModel(result.Result, dbReaderClasses, globals, nameof(DeleteObjectsModel.ClassItems));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                if (result.Result.ClassItems.Any())
                {
                    var searchObjects = result.Result.ClassItems.Select(cls => new clsOntologyItem
                    {
                        GUID_Parent = cls.GUID
                    }).ToList();

                    var dbReaderObjects = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderObjects.GetDataObjects(searchObjects);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = $"Error while getting the Objects of classes!";
                        return result;
                    }

                    result.ResultState = ValidationController.ValidateAndSetDeleteObjectsModel(result.Result, dbReaderObjects, globals, nameof(DeleteObjectsModel.ObjectsToDelete));

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
                else
                {
                    var searchObjects = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = result.Result.Config.GUID,
                            ID_RelationType = OntologyItemsModule.DeleteObjects.Config.LocalData.RelationType_belonging_Object.GUID
                        }
                    };

                    var dbReaderObjects = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderObjects.GetDataObjectRel(searchObjects);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = $"Error while getting the Objects of classes!";
                        return result;
                    }

                    result.ResultState = ValidationController.ValidateAndSetDeleteObjectsModel(result.Result, dbReaderObjects, globals, nameof(DeleteObjectsModel.ObjectsToDelete));
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public ElasticAgentOItems(Globals globals) : base(globals)
        {
        }
    }

    public class ResultClassPath
    {
        public clsOntologyItem Result { get; set; }
        public string Path { get; set; }
    }

    public class ResultGetAttribute
    {
        public clsOntologyItem Result { get; set; }
        public clsObjectAtt ObjectAttribute { get; set; }
        public clsOntologyItem ObjectItem { get; set; }
        public clsOntologyItem AttributeTypeItem { get; set; }
    }
}
