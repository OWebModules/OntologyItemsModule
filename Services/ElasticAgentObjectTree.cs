﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Services
{
    public class ElasticAgentObjectTree
    {
        public Globals globals;

        public async Task<clsOntologyItem> GetOItem(string id, string type)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);
                return dbReader.GetOItem(id, type);
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetJoinedRelList(List<clsOntologyItem> objects, string idRelationType)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>> (() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var searchRel = objects.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = idRelationType
                }).ToList();

                searchRel.AddRange(objects.Select(obj => new clsObjectRel
                {
                    ID_Other = obj.GUID,
                    ID_RelationType = idRelationType
                }));

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjectRel(searchRel);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the object-relations!";
                    return result;
                }

                result.Result = dbReader.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjects(List<string> nameList, string idClass)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var dbReader = new OntologyModDBConnector(globals);
                var oitems = nameList.Select(name => new clsOntologyItem
                {
                    Name = name,
                    GUID_Parent = idClass
                }).ToList();

                result.ResultState = dbReader.GetDataObjects(oitems);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the objects!";
                    return result;
                }

                result.Result = (from obj in dbReader.Objects1
                                join name in nameList on obj.Name equals name
                                select obj).ToList();
                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveRelations(List<clsObjectRel> objectRelLists)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var dbWriter = new OntologyModDBConnector(globals);
                return dbWriter.SaveObjRel(objectRelLists);
            });

            return taskResult;
        }

        public async Task<ResultItem<long>> GetNextOrderId(string idClass, string idRelationType)
        {
            var taskResult = await Task.Run<ResultItem<long>>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);
                var resultNextOrderId = dbReader.GetDataRelOrderId(new clsOntologyItem { GUID_Parent = idClass }, new clsOntologyItem { GUID_Parent = idClass }, new clsOntologyItem { GUID = idRelationType }, doAsc: false);
                var result = new ResultItem<long>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = resultNextOrderId + 1
                };
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ObjectsAndObjectRels>> GetTree(string idObject, string idClass, string idRelationType, string idDirection)
        {
            var classItem = await GetOItem(idClass, globals.Type_Class);
            clsOntologyItem objectItem = null;
            if (!string.IsNullOrEmpty(idObject))
            {
                objectItem = await GetOItem(idObject, globals.Type_Object);
            }
            var relationTypeItem = await GetOItem(idRelationType, globals.Type_RelationType);

            var taskResult = await Task.Run<ResultItem<ObjectsAndObjectRels>>(() =>
            {
                var result = new ResultItem<ObjectsAndObjectRels>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ObjectsAndObjectRels
                    {
                        Objects = new List<clsOntologyItem>(),
                        ObjectRelations = new List<clsObjectRel>()
                    }
                };


                if (classItem.GUID == globals.LState_Error.GUID || relationTypeItem.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();

                    return result;
                }

                var dbReader = new OntologyModDBConnector(globals);
                result.ResultState = dbReader.GetDataObjects(new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = idClass
                    }
                });

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the command line runs!";
                    return result;
                }

                var idObjects = new List<string>();
                if(!string.IsNullOrEmpty(idObject))
                {
                    idObjects.Add(idObject);
                }
                var resultObjectRelations = GetObjectRelations(idObject, idClass, idRelationType, idDirection, idObjects);
                
                if (resultObjectRelations.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();

                    return result;
                }

                var objectsWithoutRelations = new List<clsOntologyItem>();
                if (string.IsNullOrEmpty(idObject))
                {
                    if (idDirection == globals.Direction_LeftRight.GUID)
                    {
                        objectsWithoutRelations = (from obj in dbReader.Objects1
                                                   join rel in resultObjectRelations.Result.ObjectRelations on obj.GUID equals rel.ID_Object into rels
                                                   from rel in rels.DefaultIfEmpty()
                                                   where rel == null
                                                   select obj).ToList();
                    }
                    else
                    {
                        objectsWithoutRelations = (from obj in dbReader.Objects1
                                                   join rel in resultObjectRelations.Result.ObjectRelations on obj.GUID equals rel.ID_Other into rels
                                                   from rel in rels.DefaultIfEmpty()
                                                   where rel == null
                                                   select obj).ToList();
                    }
                }
                
                result.Result = resultObjectRelations.Result;
                result.Result.Objects.AddRange(objectsWithoutRelations);
                if (result.Result.ObjectRelations.Any() && objectItem != null && !result.Result.Objects.Any(obj => obj.GUID == objectItem.GUID))
                {
                    result.Result.Objects.Add(objectItem);
                    
                }
                return result;
            });

            return taskResult;
        }

        public ResultItem<ObjectsAndObjectRels> GetObjectRelations(string idObject, string idClass, string idRelationType, string idDirection, List<string> idObjects)
        {
            var result = new ResultItem<ObjectsAndObjectRels>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new ObjectsAndObjectRels
                {
                    ObjectRelations = new List<clsObjectRel>(),
                    Objects = new List<clsOntologyItem>()
                }
            };

            var searchObjectRelations = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = idDirection == globals.Direction_LeftRight.GUID ? idObject : null,
                    ID_RelationType = idRelationType,
                    ID_Parent_Object = idClass,
                    ID_Other = idDirection == globals.Direction_RightLeft.GUID ? idObject : null,
                    ID_Parent_Other = idClass
                }
            };

            var dbReader = new OntologyModDBConnector(globals);
            result.ResultState = dbReader.GetDataObjectRel(searchObjectRelations);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }
            result.Result.Objects.AddRange(dbReader.Objects1);
            result.Result.ObjectRelations.AddRange(dbReader.ObjectRels.OrderBy(rel => rel.OrderID).ThenBy(rel => idDirection == globals.Direction_LeftRight.GUID ? rel.Name_Other : rel.Name_Object));
            if (!string.IsNullOrEmpty(idObject))
            {
                foreach (var relItem in dbReader.ObjectRels)
                {
                    var idSubObject = idDirection == globals.Direction_LeftRight.GUID ? relItem.ID_Other : relItem.ID_Object;
                    if (!idObjects.Contains(idSubObject))
                    {
                        var resultSubItems = GetObjectRelations(idSubObject, idClass, idRelationType, idDirection, idObjects);

                        if (resultSubItems.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState = resultSubItems.ResultState;
                            return result;

                        }
                        result.Result.ObjectRelations.AddRange(resultSubItems.Result.ObjectRelations.OrderBy(rel => rel.OrderID).ThenBy(rel => idDirection == globals.Direction_LeftRight.GUID ? rel.Name_Other : rel.Name_Object));
                        result.Result.Objects.AddRange(resultSubItems.Result.Objects);
                    }
                }
                
            }
            

            return result;
        }

        public async Task<ResultItem<clsOntologyItem>> GetMostImportantRelationType(string idClass)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchObjRel = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = idClass,
                        ID_Parent_Other = idClass
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjectRel(searchObjRel, doIds:true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var objectRelOrder = dbReader.ObjectRelsId.GroupBy(relItm => new { IdRelationType = relItm.ID_RelationType, NameRelationType = relItm.Name_RelationType }).
                    Select(grpItm => new { IdRelationType = grpItm.Key.IdRelationType, NameRelationType = grpItm.Key.NameRelationType, Count = grpItm.Count() }).OrderByDescending(orderItm => orderItm.Count);

                result.Result = objectRelOrder.Select(relItm => new clsOntologyItem
                {
                    GUID = relItm.IdRelationType,
                    Name = relItm.NameRelationType,
                    Type = globals.Type_RelationType
                }).FirstOrDefault();

                if (result.Result != null)
                {
                    var resultRelationType = dbReader.GetOItem(result.Result.GUID, globals.Type_RelationType);
                    if (resultRelationType.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        return result;
                    }

                    result.Result = resultRelationType;
                }

                return result;
            });

            return taskResult;
        }

        public ElasticAgentObjectTree(Globals globals)
        {
            this.globals = globals;
        }

    }

    public class ObjectsAndObjectRels
    {
        public List<clsOntologyItem> Objects { get; set; }
        public List<clsObjectRel> ObjectRelations { get; set; }
    }

}
