﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Factories;
using OntologyItemsModule.Models;
using OntologyItemsModule.Services;
using OntologyItemsModule.Validations;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule
{
    public class OItemListController : AppController
    {
        private object controllerLocker = new object();

        private clsOntologyItem oItem;
        public clsOntologyItem OItem
        {
            get
            {
                lock (controllerLocker)
                {
                    return oItem;
                }
            }
            set
            {
                lock (controllerLocker)
                {
                    oItem = value;
                }
            }
        }

        private ResultClassPath resultClassPath;
        public ResultClassPath ResultClassPath
        {
            get
            {
                lock (controllerLocker)
                {
                    return resultClassPath;
                }
            }
            set
            {
                lock (controllerLocker)
                {
                    resultClassPath = value;
                }
            }
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckExistance(List<clsOntologyItem> itemsToCheck)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var elasticAgent = new ElasticAgentOItems(Globals);

                var serviceResult = await elasticAgent.CheckExistance(itemsToCheck);
                result.ResultState = serviceResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var classesOrObjectsToCheck = itemsToCheck.Where(itm => itm.Type == Globals.Type_Class || itm.Type == Globals.Type_Object).ToList();
                var attributeTypesOrRelationTypesToCheck = itemsToCheck.Where(itm => itm.Type == Globals.Type_AttributeType || itm.Type == Globals.Type_RelationType).ToList();

                (from itemToCheck in classesOrObjectsToCheck
                 join itemExist in serviceResult.Result on new { itemToCheck.Type, itemToCheck.GUID_Parent, Name = itemToCheck.Name.ToLower() } equals new { itemExist.Type, itemExist.GUID_Parent, Name = itemExist.Name.ToLower() } into itemsExist
                 from itemExist in itemsExist.DefaultIfEmpty()
                 select new { itemToCheck, itemExist }).ToList().ForEach(itm =>
                 {
                     if (itm.itemExist != null)
                     {
                         itm.itemToCheck.GUID = itm.itemExist.GUID;
                         itm.itemToCheck.Name = itm.itemExist.Name;
                         itm.itemToCheck.GUID_Parent = itm.itemExist.GUID_Parent;
                         itm.itemToCheck.Type = itm.itemExist.Type;
                     }

                     itm.itemToCheck.Mark = itm.itemExist != null;
                 });

                (from itemToCheck in attributeTypesOrRelationTypesToCheck
                 join itemExist in serviceResult.Result on itemToCheck.Name.ToLower() equals itemExist.Name.ToLower() into itemsExist
                 from itemExist in itemsExist.DefaultIfEmpty()
                 select new { itemToCheck, itemExist }).ToList().ForEach(itm =>
                 {
                     if (itm.itemExist != null)
                     {
                         itm.itemToCheck.GUID = itm.itemExist.GUID;
                         itm.itemToCheck.Name = itm.itemExist.Name;
                         itm.itemToCheck.GUID_Parent = itm.itemExist.GUID_Parent;
                         itm.itemToCheck.Type = itm.itemExist.Type;
                     }
                     itm.itemToCheck.Mark = itm.itemExist != null;
                 });


                result.Result = itemsToCheck;
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<InstanceViewItem>>> GetInstanceViewItems(List<clsOntologyItem> objects)
        {
            var taskResult = await Task.Run<ResultItem<List<InstanceViewItem>>>(async() =>
            {
                var result = new ResultItem<List<InstanceViewItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<InstanceViewItem>()
                };

                var serviceAgent = new ElasticAgentOItems(Globals);

                var getClassesResult = await serviceAgent.GetClassesOfObjects(objects);

                result.ResultState = getClassesResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var objClsList = (from obj in objects
                                 join cls in getClassesResult.Result on obj.GUID_Parent equals cls.GUID
                                 select new { obj, cls }).ToList();

                result.Result = objClsList.Select(objCls => new InstanceViewItem(objCls.obj, objCls.cls)).ToList();
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<AttributeTypeViewItem>>> GetAttributeTypeViewItems(List<clsOntologyItem> attributeTypes)
        {
            var taskResult = await Task.Run<ResultItem<List<AttributeTypeViewItem>>>(async () =>
            {
                var result = new ResultItem<List<AttributeTypeViewItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<AttributeTypeViewItem>()
                };

                var serviceAgent = new ElasticAgentOItems(Globals);

                var getDataTypeResult = await serviceAgent.GetDataTypes();

                result.ResultState = getDataTypeResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var objAttDatList = (from attType in attributeTypes
                                  join datType in getDataTypeResult.Result on attType.GUID_Parent equals datType.GUID
                                  select new { attType, datType }).ToList();

                result.Result = objAttDatList.Select(attType => new AttributeTypeViewItem(attType.attType, attType.datType )).ToList();
                return result;
            });

            return taskResult;
        }

        public async Task<DataGridResultWeb<InstanceViewItem>> GetObjectList(string IdParent, string filter)
        {
            var result = Globals.LState_Success.Clone();

            var viewModel = new Models.ViewModelOItemList(Globals);
            
            if (!string.IsNullOrEmpty(IdParent))
            {
                var resultTask = await viewModel.Initialize_ItemList(Models.ListType.Instances, new clsOntologyItem { GUID_Parent = IdParent, Name = filter });

                var dataGridResult = new DataGridResultWeb<InstanceViewItem> { Result = resultTask.Result, GridItems = resultTask.InstanceViewItems };
                return dataGridResult;
            }
            else
            {
                var dataGridResult = new DataGridResultWeb<InstanceViewItem> { Result = result, GridItems = new List<InstanceViewItem>() };
                return dataGridResult;
            }
            

            
        }

        public async Task<DataGridResultWeb<ObjectObject_Omni>> GetObjectRelation(string idObject)
        {
            var result = Globals.LState_Success.Clone();

            var viewModel = new Models.ViewModelOItemList(Globals);

            if (!string.IsNullOrEmpty(idObject))
            {
                var objRelItem = new clsObjectRel
                {
                    ID_Object = idObject
                };

                var resultTask = await viewModel.Initialize_ItemList(ListType.InstanceInstance_Omni, objRelItem);

                var dataGridResult = new DataGridResultWeb<ObjectObject_Omni> { Result = result, GridItems = resultTask.ObjectObjectOmniViewItems };
                return dataGridResult;
            }
            else
            {
                var dataGridResult = new DataGridResultWeb<ObjectObject_Omni> { Result = result, GridItems = new List<ObjectObject_Omni>() };
                return dataGridResult;
            }
            
            
        }

        public async Task<ResultItem<List<ObjectObject_Conscious>>> UpdateObjectRelationsLeftRight(List<ObjectObject_Conscious> relList)
        {
            var service = new ElasticAgentOItems(Globals);
            var getRelationsResult = await service.GetObjectRelations(relList.Select(rel => rel.GetProxyItem()).ToList());

            var result = new ResultItem<List<ObjectObject_Conscious>>()
            {
                ResultState = Globals.LState_Success.Clone()
            };

            if (getRelationsResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = getRelationsResult.ResultState;
                return result;
            }

            var toChange = new List<clsObjectRel>();
            var toChangeNames = new List<clsOntologyItem>();

            (from relDb in getRelationsResult.Result
             join rel in relList on new { relDb.ID_Object, relDb.ID_RelationType, relDb.ID_Other } equals new { ID_Object = rel.IdObject, ID_RelationType = rel.IdRelationType, ID_Other = rel.IdOther }
             select new { relDb, rel }).ToList().ForEach(rel =>
               {
                   if (rel.relDb.Name_Other != rel.rel.NameOther)
                   {
                       rel.relDb.Name_Other = rel.rel.NameOther;
                       toChangeNames.Add(new clsOntologyItem
                       {
                           GUID = rel.relDb.ID_Other,
                           Name = rel.rel.NameOther,
                           GUID_Parent = rel.relDb.ID_Parent_Other,
                           Type = rel.relDb.Ontology
                       });
                   }
                   
                   if (rel.relDb.OrderID != rel.rel.OrderId)
                   {
                       rel.relDb.OrderID = rel.rel.OrderId;
                       toChange.Add(rel.relDb);
                   }
                   
               });

            if (toChange.Any())
            {
                result.ResultState = await service.ChangeRelations(toChange);
            }
            
            if (toChangeNames.Any() && result.ResultState.GUID == Globals.LState_Success.GUID)
            {
                result.ResultState = await service.SaveItems(toChangeNames);
            }

            result.Result = getRelationsResult.Result.Select(rel => new ObjectObject_Conscious(rel)).ToList();

            return result;
        }

        public async Task<ResultItem<List<ObjectObject_Subconscious>>> UpdateObjectRelationsRightLeft(List<ObjectObject_Subconscious> relList)
        {
            var service = new ElasticAgentOItems(Globals);
            var getRelationsResult = await service.GetObjectRelations(relList.Select(rel => rel.GetProxyItem()).ToList());

            var result = new ResultItem<List<ObjectObject_Subconscious>>()
            {
                ResultState = Globals.LState_Success.Clone()
            };

            if (getRelationsResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = getRelationsResult.ResultState;
                return result;
            }

            var toChange = new List<clsObjectRel>();
            var toChangeNames = new List<clsOntologyItem>();

            (from relDb in getRelationsResult.Result
             join rel in relList on new { relDb.ID_Object, relDb.ID_RelationType, relDb.ID_Other } equals new { ID_Object = rel.IdObject, ID_RelationType = rel.IdRelationType, ID_Other = rel.IdOther }
             select new { relDb, rel }).ToList().ForEach(rel =>
             {
                 if (rel.relDb.Name_Object != rel.rel.NameObject)
                 {
                     rel.relDb.Name_Object = rel.rel.NameObject;
                     toChangeNames.Add(new clsOntologyItem
                     {
                         GUID = rel.relDb.ID_Object,
                         Name = rel.rel.NameObject,
                         GUID_Parent = rel.relDb.ID_Parent_Object,
                         Type = Globals.Type_Object
                     });
                 }

                 if (rel.relDb.OrderID != rel.rel.OrderId)
                 {
                     rel.relDb.OrderID = rel.rel.OrderId;
                     toChange.Add(rel.relDb);
                 }

             });

            if (toChange.Any())
            {
                result.ResultState = await service.ChangeRelations(toChange);
            }

            if (toChangeNames.Any() && result.ResultState.GUID == Globals.LState_Success.GUID)
            {
                result.ResultState = await service.SaveItems(toChangeNames);
            }

            result.Result = getRelationsResult.Result.Select(rel => new ObjectObject_Subconscious(rel)).ToList();

            return result;
        }

        public async Task<ResultItem<List<ObjectAttributeViewItem>>> UpdateObjectAttribute(List<ObjectAttributeViewItem> relList)
        {
            var service = new ElasticAgentOItems(Globals);
            var getRelationsResult = await service.GetObjectAttributes(relList.Select(rel => rel.GetProxyItem()).ToList());

            var result = new ResultItem<List<ObjectAttributeViewItem>>()
            {
                ResultState = Globals.LState_Success.Clone()
            };

            if (getRelationsResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = getRelationsResult.ResultState;
                return result;
            }

            var toChange = new List<clsObjectAtt>();

            (from relDb in getRelationsResult.Result
             join rel in relList on relDb.ID_Attribute equals rel.IdAttribute
             select new { relDb, rel }).ToList().ForEach(rel =>
             {
                 if (rel.relDb.OrderID != rel.rel.OrderId)
                 {
                     rel.relDb.OrderID = rel.rel.OrderId;
                     toChange.Add(rel.relDb);
                 }

             });

            if (toChange.Any())
            {
                result.ResultState = await service.ChangeAttributes(toChange);
            }

            result.Result = getRelationsResult.Result.Select(rel => new ObjectAttributeViewItem(rel)).ToList();

            return result;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> SaveNewItems(List<clsOntologyItem> itemsToSave)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };

               if (!itemsToSave.Any())
               {
                   return result;
               }

               itemsToSave.ForEach(itm => { if (itm.GUID == null) { itm.GUID = Globals.NewGUID; } }) ;

               var elasticAgent = new ElasticAgentOItems(Globals);

               var saveResult = await elasticAgent.SaveItems(itemsToSave);
               result.ResultState = saveResult;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while saving the Items!";
                   return result;
               }

               result.Result = itemsToSave;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<InstanceViewItem>>> UpdateInstanceItem(List<clsOntologyItem> itemList)
        {
            var service = new ElasticAgentOItems(Globals);
            var getObjectsResult = await service.GetObjects(itemList);

            var result = new ResultItem<List<InstanceViewItem>>()
            {
                ResultState = Globals.LState_Success.Clone()
            };

            if (getObjectsResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = getObjectsResult.ResultState;
                return result;
            }

            var toChangeNames = new List<clsOntologyItem>();

            (from objDb in getObjectsResult.Result
             join obj in itemList on objDb.GUID equals obj.GUID
             select new { objDb, obj }).ToList().ForEach(objItem =>
             {
                 if (objItem.objDb.Name != objItem.obj.Name)
                 {
                     objItem.objDb.Name = objItem.obj.Name;
                     toChangeNames.Add(new clsOntologyItem
                     {
                         GUID = objItem.objDb.GUID,
                         Name = objItem.obj.Name,
                         GUID_Parent = objItem.objDb.GUID_Parent,
                         Type = objItem.objDb.Type
                     });
                 }

             });

            if (toChangeNames.Any() && result.ResultState.GUID == Globals.LState_Success.GUID)
            {
                result.ResultState = await service.SaveItems(toChangeNames);
            }

            var getClassesResult = await service.GetClassesOfObjects(getObjectsResult.Result);

            result.ResultState = getClassesResult.ResultState;
            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var objectClassList = (from obj in getObjectsResult.Result
                             join cls in getClassesResult.Result on obj.GUID_Parent equals cls.GUID
                             select new { obj, cls }).ToList();
            result.Result = objectClassList.Select(obj => new InstanceViewItem(obj.obj, obj.cls)).ToList();

            return result;
        }

        public async Task<DataGridResultWeb<ObjectObject_Conscious>> GetObjectRelationConscious(ObjectRelNode relationNode, bool isOther)
        {
            var result = Globals.LState_Success.Clone();

            var viewModel = new Models.ViewModelOItemList(Globals);
            var objRelItem = new clsObjectRel();
            objRelItem.ID_RelationType = relationNode.IdRelationType;

            if (relationNode.NodeType == NodeType.NodeForwardFormal ||
                            relationNode.NodeType == NodeType.NodeForwardInformal ||
                            relationNode.NodeType == NodeType.AttributeRel)
            {
                if (relationNode.NodeType == NodeType.NodeForwardFormal)
                {
                    objRelItem.ID_Parent_Other = relationNode.IdRight;
                }
                objRelItem.ID_Object = relationNode.IdLeft;
                
            }
            else
            {
                if (relationNode.NodeType == NodeType.NodeBackwardFormal)
                {
                    objRelItem.ID_Parent_Object = relationNode.IdLeft;
                }
                objRelItem.ID_Other = relationNode.IdRight;
                
            }

            var resultTask = await viewModel.Initialize_ItemList(isOther ? Models.ListType.InstanceOther_Conscious : Models.ListType.InstanceInstance_Conscious,
                    objRelItem);

            var dataGridResult = new DataGridResultWeb<ObjectObject_Conscious> { Result = result, GridItems = isOther ? resultTask.ObjectOtherConsciousViewItems : resultTask.ObjectObjectConsciousViewItems };


            return dataGridResult;
        }

        
        public async Task<DataGridResultWeb<ObjectObject_Subconscious>> GetObjectRelationSubConscious(ObjectRelNode relationNode, bool isOther)
        {
            var result = Globals.LState_Success.Clone();

            var viewModel = new Models.ViewModelOItemList(Globals);
            var objRelItem = new clsObjectRel();
            objRelItem.ID_RelationType = relationNode.IdRelationType;
            if (relationNode.NodeType == NodeType.NodeForwardFormal ||
                            relationNode.NodeType == NodeType.NodeForwardInformal ||
                            relationNode.NodeType == NodeType.AttributeRel)
            {
                if (relationNode.NodeType == NodeType.NodeForwardFormal)
                {
                    objRelItem.ID_Parent_Object = relationNode.IdRight;
                }
                objRelItem.ID_Other = relationNode.IdLeft;
                
            }
            else
            {
                
                objRelItem.ID_Parent_Object = relationNode.IdLeft;
                
                objRelItem.ID_Other = relationNode.IdRight;
                
            }

            var resultTask = await viewModel.Initialize_ItemList(isOther ? Models.ListType.InstanceOther_Subconscious  : Models.ListType.InstanceInstance_Subconscious,
                    objRelItem);
            var dataGridResult = new DataGridResultWeb<ObjectObject_Subconscious> { Result = result, GridItems = isOther ? resultTask.ObjectOtherSubconsciousViewItems : resultTask.ObjectObjectSubconsciousViewItems };
            
            return dataGridResult;
        }

        public async Task<DataGridResultWeb<ObjectAttributeViewItem>> GetObjectAttributeList(ObjectRelNode relationNode)
        {
            var result = Globals.LState_Success.Clone();

            var viewModel = new Models.ViewModelOItemList(Globals);

            var resultTask = await viewModel.Initialize_ItemList(new clsObjectAtt { ID_Object = relationNode.IdLeft, ID_AttributeType = relationNode.IdRight });

            var dataGridResult = new DataGridResultWeb<ObjectAttributeViewItem> { Result = result, GridItems = resultTask.ObjectAttributeViewItems };

            return dataGridResult;
        }

        public async Task<DataGridResultWeb<AttributeTypeViewItem>> GetAttributeTypeList(clsOntologyItem filterItem = null)
        {
            var result = Globals.LState_Success.Clone();

            var viewModel = new Models.ViewModelOItemList(Globals);

            var resultTask = await viewModel.Initialize_ItemList(Models.ListType.AttributeTypes, filterItem);

            var dataGridResult = new DataGridResultWeb<AttributeTypeViewItem> { Result = result, GridItems = resultTask.AttributeTypeViewItems };

            return dataGridResult;
        }

        public async Task<DataGridResultWeb<RelationTypeViewItem>> GetRelationTypeList(clsOntologyItem filterItem = null)
        {
            var result = Globals.LState_Success.Clone();

            var viewModel = new Models.ViewModelOItemList(Globals);

            var resultTask = await viewModel.Initialize_ItemList(Models.ListType.RelationTypes, filterItem);

            var dataGridResult = new DataGridResultWeb<RelationTypeViewItem> { Result = result, GridItems = resultTask.RelationTypeViewItems };

            return dataGridResult;
        }

        public async Task<clsOntologyItem> SetOrderId(List<OrderIdChange> orderIdChanges)
        {
            var serviceAgent = new ElasticAgentOItems(Globals);
            var result = await serviceAgent.SetOrderId(orderIdChanges);
            return result;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjects(List<string> ids)
        {
            var serviceAgent = new ElasticAgentOItems(Globals);
            var result = await serviceAgent.GetObjects(ids.Select(id => new clsOntologyItem { GUID = id }).ToList());
            return result;
        }


        public async Task<clsOntologyItem> GetOItem(string idItem, string type)
        {
            var serviceAgent = new ElasticAgentOItems(Globals);
            var oItemTask = await serviceAgent.GetOItem(idItem, type);
            if (oItemTask.ResultState.GUID == Globals.LState_Error.GUID ||
                oItemTask.ResultState.GUID == Globals.LState_Nothing.GUID)
            {
                return null;
            }
            OItem = oItemTask.Result;
            return oItem;
        }

        public async Task<ResultItem<clsObjectRel>> GetRelation(clsObjectRel searchRel)
        {
            var serviceAgent = new ElasticAgentOItems(Globals);
            return await serviceAgent.GetRelation(searchRel);
        }

        public async Task<ResultClassPath> GetClassPath(clsOntologyItem objectItem)
        {
            var serviceAgent = new ElasticAgentOItems(Globals);
            var agentTask = await serviceAgent.GetClassPath(objectItem);
            return agentTask;
        }

        public async Task<clsOntologyItem> DeleteItems(List<ObjectAttributeViewItem> relationItems)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var serviceAgent = new ElasticAgentOItems(Globals);

                var deleteItems = relationItems.Select(rel => rel.GetProxyItem()).ToList();
                result = await serviceAgent.DeleteItems(deleteItems);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> DeleteItems(List<ObjectObject_Conscious> relationItems)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async() =>
            {
                var result = Globals.LState_Success.Clone();

                var serviceAgent = new ElasticAgentOItems(Globals);

                var deleteItems = relationItems.Select(rel => rel.GetProxyItem()).ToList();
                result = await serviceAgent.DeleteItems(deleteItems);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> DeleteItems(List<ObjectObject_Subconscious> relationItems)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var serviceAgent = new ElasticAgentOItems(Globals);

                var deleteItems = relationItems.Select(rel => rel.GetProxyItem()).ToList();
                result = await serviceAgent.DeleteItems(deleteItems);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<MergeObjectsResult>> MergeObjects(MergeObjectsRequestByItems request)
        {
            var taskResult = await Task.Run<ResultItem<MergeObjectsResult>>(async () =>
            {
                var result = new ResultItem<MergeObjectsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new MergeObjectsResult()
                };

                var attributesToDelete = new List<clsObjectAtt>();
                var relationsToDelete = new List<clsObjectRel>();
                var objectsToDelete = new List<clsOntologyItem>();
                var relationsToSave = new List<clsObjectRel>();
                var attributesToSave = new List<clsObjectAtt>();
                var objectsToSave = new List<clsOntologyItem>();

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = ValidationController.ValidateMergeObjectsByItemsRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");


                request.MessageOutput?.OutputInfo("Get Model...");
                var elasticAgent = new ElasticServiceAgentMerge(Globals);

                var getModelResult = await elasticAgent.GetMergeObjectsByItemsModel(request);

                result.ResultState = getModelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                

                request.MessageOutput?.OutputInfo($"Have Model, {getModelResult.Result.MergeConfigs.Count} configurations");

                var relationConfig = new clsRelationConfig(Globals);
                var transformProviders = NameTransformManager.GetNameTransformProviders(Globals);

                foreach (var config in getModelResult.Result.MergeConfigs)
                {
                    attributesToSave.Clear();
                    attributesToDelete.Clear();
                    relationsToSave.Clear();
                    relationsToDelete.Clear();
                    objectsToSave.Clear();
                    objectsToDelete.Clear();

                    request.MessageOutput?.OutputInfo($"Merge-Config: {config.Name}");

                    var headObject = getModelResult.Result.MergeHeads.Select(haedI => new clsOntologyItem
                    {
                        GUID = haedI.ID_Other,
                        Name = haedI.Name_Other,
                        GUID_Parent = haedI.ID_Parent_Other,
                        Type = Globals.Type_Object
                    }).FirstOrDefault();

                    if (headObject == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = $"No merge-head for config { config.Name } found!";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                    }

                    var mergeObjects = getModelResult.Result.MergeMerges.Where(merge => merge.ID_Object == config.GUID).Select(merge => new clsOntologyItem
                    {
                        GUID = merge.ID_Other,
                        Name = merge.Name_Other,
                        GUID_Parent = merge.ID_Parent_Other,
                        Type = merge.Ontology
                    }).ToList();

                    if (!mergeObjects.Any())
                    {
                        result.ResultState = Globals.LState_Error.Clone();

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = $"No merge-merges for config { config.Name } found!";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                    }

                    if (mergeObjects.Any(merge => merge.GUID == headObject.GUID))
                    {
                        result.ResultState.Additional1 = $"Head is in merge-list. This is not allowed!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var namingRule = getModelResult.Result.NamingRules.FirstOrDefault(nameR => nameR.ID_Object == config.GUID);

                    if (namingRule == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = $"No naming-rule for config { config.Name } found!";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                    }

                    var archiveClass = getModelResult.Result.ArchiveClasses.FirstOrDefault(cls => cls.ID_Object == config.GUID);

                    var searchAttributes = mergeObjects.Select(merge => new clsObjectAtt
                    {
                        ID_Object = merge.GUID
                    }).ToList();

                    var searchRelationsLeftRight = mergeObjects.Select(merge => new clsObjectRel
                    {
                        ID_Object = merge.GUID
                    }).ToList();

                    var searchRelationsRightLeft = mergeObjects.Select(merge => new clsObjectRel
                    {
                        ID_Other = merge.GUID
                    }).ToList();


                    if (namingRule.ID_Other == OntologyItemsModule.MergeObjects.Config.LocalData.Object_Take_Merge_Name.GUID)
                    {
                        var nameMerge = mergeObjects.OrderBy(merge => merge.Name).Select(merge => merge.Name).FirstOrDefault();
                        headObject.Name = nameMerge;
                        objectsToSave.Add(headObject);
                    }
                    else if (namingRule.ID_Other == OntologyItemsModule.MergeObjects.Config.LocalData.Object_Take_Name_Transform_Name.GUID)
                    {
                        var provider = transformProviders.Where(prov => prov.IsResponsible(headObject.GUID_Parent)).FirstOrDefault();
                        if (provider != null)
                        { 
                            var transformResult = await provider.TransformNames(new List<clsOntologyItem> { headObject }, false, idClass: headObject.GUID_Parent, request.MessageOutput);
                            result.ResultState = transformResult.ResultState;
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                return result;
                            }
                            objectsToSave.Add(headObject);
                        }
                    }

                    if (archiveClass != null)
                    {
                        foreach (var mergeObject in mergeObjects)
                        {
                            mergeObject.GUID_Parent = archiveClass.ID_Other;
                            objectsToSave.Add(mergeObject);
                            relationsToSave.Add(relationConfig.Rel_ObjectRelation(config, mergeObject, OntologyItemsModule.MergeObjects.Config.LocalData.RelationType_archived_objects));
                        }
                    }

                    var getAttributesResult = await elasticAgent.GetAttributes(searchAttributes);
                    result.ResultState = getAttributesResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var getRelationsLeftRightResult = await elasticAgent.GetRelationItems(searchRelationsLeftRight);
                    result.ResultState = getAttributesResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var getRelationsRightLeftResult = await elasticAgent.GetRelationItems(searchRelationsRightLeft);
                    result.ResultState = getAttributesResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    

                    if (archiveClass == null)
                    {
                        attributesToDelete = getAttributesResult.Result.Select(att => new clsObjectAtt
                        {
                            ID_Attribute = att.ID_Attribute
                        }).ToList();

                        relationsToDelete = getRelationsLeftRightResult.Result.Select(rel => new clsObjectRel
                        {
                            ID_Object = rel.ID_Object,
                            ID_RelationType = rel.ID_RelationType,
                            ID_Other = rel.ID_Other
                        }).ToList();

                        relationsToDelete.AddRange(getRelationsRightLeftResult.Result.Select(rel => new clsObjectRel
                        {
                            ID_Object = rel.ID_Object,
                            ID_RelationType = rel.ID_RelationType,
                            ID_Other = rel.ID_Other
                        }));

                        objectsToDelete = mergeObjects.Select(obj => new clsOntologyItem
                        {
                            GUID = obj.GUID
                        }).ToList();
                    }

                    attributesToSave = getAttributesResult.Result.Select(att => new clsObjectAtt
                    {
                        ID_Attribute = Globals.NewGUID,
                        ID_AttributeType = att.ID_AttributeType,
                        ID_DataType = att.ID_DataType,
                        ID_Class = headObject.GUID_Parent,
                        OrderID = att.OrderID,
                        Val_Named = att.Val_Named,
                        Val_Bool = att.Val_Bool,
                        Val_Datetime = att.Val_Datetime,
                        Val_Double = att.Val_Double,
                        Val_Lng = att.Val_Lng,
                        Val_String = att.Val_String
                    }).ToList();

                    relationsToSave.AddRange(getRelationsLeftRightResult.Result.Select(rel => new clsObjectRel
                    {
                        ID_Object = headObject.GUID,
                        ID_Parent_Object = headObject.GUID_Parent,
                        ID_Other = rel.ID_Other,
                        ID_Parent_Other = rel.ID_Parent_Other,
                        OrderID = rel.OrderID,
                        Ontology = rel.Ontology,
                        ID_RelationType = rel.ID_RelationType
                    }).ToList());

                    relationsToSave.AddRange(getRelationsRightLeftResult.Result.Select(rel => new clsObjectRel
                    {
                        ID_Object = rel.ID_Object,
                        ID_Parent_Object = rel.ID_Parent_Object,
                        ID_Other = headObject.GUID,
                        ID_Parent_Other = headObject.GUID_Parent,
                        OrderID = rel.OrderID,
                        Ontology = rel.Ontology,
                        ID_RelationType = rel.ID_RelationType
                    }));
                    
                    if (attributesToSave.Any())
                    {
                        var saveResult = await elasticAgent.SaveAttributes(attributesToSave);
                        result.ResultState = saveResult;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = $"Error while saving the attributes of head {headObject.Name}!";
                            return result;
                        }
                    }

                    if (relationsToSave.Any())
                    {
                        var saveResult = await elasticAgent.SaveRelations(relationsToSave);
                        result.ResultState = saveResult;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = $"Error while saving the left-right relations of head {headObject.Name}!";
                            return result;
                        }
                    }

                    if (objectsToSave.Any())
                    {
                        var saveResult = await elasticAgent.SaveObjects(objectsToSave);
                        result.ResultState = saveResult;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = $"Error while saving objects!";
                            return result;
                        }
                    }

                    if (attributesToDelete.Any())
                    {
                        var delResult = await elasticAgent.DeleteAttributes(attributesToDelete);
                        result.ResultState = delResult;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = $"Error while deleting the attributes of merges (head: {headObject.Name}!";
                            return result;
                        }
                    }

                    if (relationsToDelete.Any())
                    {
                        var delResult = await elasticAgent.DeleteRelations(relationsToDelete);
                        result.ResultState = delResult;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = $"Error while deleting the relations of merges (head: {headObject.Name}!";
                            return result;
                        }
                    }

                    if (objectsToDelete.Any())
                    {
                        var delResult = await elasticAgent.DeleteObjects(objectsToDelete);
                        result.ResultState = delResult;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = $"Error while deleting the relations of merges (head: {headObject.Name}!";
                            return result;
                        }
                    }


                    result.Result.MergedObjects.AddRange(mergeObjects);
                    result.Result.MergedAttributes.AddRange(attributesToSave);
                    result.Result.MergedRelations.AddRange(relationsToSave);
                    result.Result.DeletedObjects.AddRange(objectsToDelete);
                    result.Result.DeletedAttributes.AddRange(attributesToDelete);
                    result.Result.DeletedRelations.AddRange(relationsToDelete);
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<MergeObjectsResult>> MergeObjects(MergeObjectsRequestByClass request)
        {
            var taskResult = await Task.Run<ResultItem<MergeObjectsResult>>(async() =>
            {
                var result = new ResultItem<MergeObjectsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new MergeObjectsResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");
                result.ResultState = ValidationController.ValidateMergeObjectsRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated request.");






                return result;

            });

            return taskResult;
        }

        public async Task<ResultItem<KendoTreeNode>> RelateObjectRelationTreeItem(string idReference, List<KendoTreeNode> treeNodes, ObjectRelationTreeModel treeModel)
        {
            var taskResult = await Task.Run<ResultItem<KendoTreeNode>>(() =>
            {
                var result = new ResultItem<KendoTreeNode>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                var treeNode = treeNodes.Select(node => node.GetTreeNode(idReference)).FirstOrDefault();

                if (treeNode == null)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"TreeNode cannot be found by idReference {idReference}";
                    return result;
                }

                return result;
            });

            return taskResult;
        }


        public async Task<clsOntologyItem> DeleteItems(List<clsOntologyItem> objectItems)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var serviceAgent = new ElasticAgentOItems(Globals);

                var deleteItems = objectItems;
                result = await serviceAgent.DeleteItems(deleteItems);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<HasObjectRelationsResult>>> HasObjectRelations(List<string> objectItemIds)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<List<HasObjectRelationsResult>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<HasObjectRelationsResult>()
                };

                var elasticAgent = new ElasticAgentOItems(Globals);

                foreach (var objectItemId in objectItemIds)
                {
                    var hasRelationsResult = new HasObjectRelationsResult();
                    hasRelationsResult.IdObject = objectItemId;
                    var hasAttributeRelationsResult = await elasticAgent.GetObjectAttributesCount(new List<clsObjectAtt>
                    {
                        new clsObjectAtt
                        {
                            ID_Object = objectItemId
                        }
                    }); 

                    result.ResultState = hasAttributeRelationsResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the count of attributes!";
                        return result;
                    }

                    hasRelationsResult.CountAttributeRelations = hasAttributeRelationsResult.Result;

                    var hasRelationsLeftRightResult = await elasticAgent.GetObjectRelationsCount(new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = objectItemId
                        }
                    });

                    result.ResultState = hasRelationsLeftRightResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the count of left-right relations!";
                        return result;
                    }

                    hasRelationsResult.CountLeftRightRelations = hasRelationsLeftRightResult.Result;


                    var hasRelationsrightLeftResult = await elasticAgent.GetObjectRelationsCount(new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Other = objectItemId
                        }
                    });

                    result.ResultState = hasRelationsrightLeftResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the count of right-left relations!";
                        return result;
                    }

                    hasRelationsResult.CountRightLeftRelations = hasRelationsrightLeftResult.Result;

                    result.Result.Add(hasRelationsResult);
                }

                

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<HasRelationsAttributeTypeResult>>> HasAttributeTypeRelations(List<string> attributeTypeIds)
        {
            var taskResult = await Task.Run<ResultItem<List<HasRelationsAttributeTypeResult>>>(async() =>
            {
                var result = new ResultItem<List<HasRelationsAttributeTypeResult>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<HasRelationsAttributeTypeResult>()
                };

                var elasticAgent = new ElasticAgentOItems(Globals);

                var searchClassAttributes = attributeTypeIds.Select(attributeTypeId => new clsClassAtt
                {
                    ID_AttributeType = attributeTypeId
                }).ToList();

                var searchObjectAttributes = attributeTypeIds.Select(attributeTypeId => new clsObjectAtt
                {
                    ID_AttributeType = attributeTypeId
                }).ToList();

                var getClassAttributes = await elasticAgent.GetClassAttributes(searchClassAttributes);
                result.ResultState = getClassAttributes.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Class-Attributes!";
                    return result;
                }

                var getObjectAttributes = await elasticAgent.GetObjectAttributes(searchObjectAttributes);
                result.ResultState = getObjectAttributes.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Object-Attributes!";
                    return result;
                }

                foreach (var attributeTypeId in attributeTypeIds)
                {
                    result.Result.Add(new HasRelationsAttributeTypeResult
                    {
                        IdAttributeType = attributeTypeId,
                        CountClassAttributeRelations = getClassAttributes.Result.Where(clsAtt => clsAtt.ID_AttributeType == attributeTypeId).Count(),
                        CountObjectAttributeRelations = getObjectAttributes.Result.Where(objAtt => objAtt.ID_AttributeType == attributeTypeId).Count()
                    });
                }

                return result;
            });

            return taskResult;
             
        }

        public async Task<ResultItem<DeleteObjectsAndRelationsResult>> DeleteObjectsAndRelations(DeleteObjectsRequest request)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<DeleteObjectsAndRelationsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new DeleteObjectsAndRelationsResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = ValidationController.ValidateDeleteObjectsRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                var elasticAgent = new ElasticAgentOItems(Globals);

                request.MessageOutput?.OutputInfo("Get model...");

                var serviceResult = await elasticAgent.GetDeleteObjectsModel(request);

                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (!serviceResult.Result.ObjectsToDelete.Any())
                {
                    request.MessageOutput?.OutputInfo("No Objects to Delete! Finished");
                    return result;
                }

                request.MessageOutput?.OutputInfo($"{serviceResult.Result.ObjectsToDelete.Count} Objects to delete...");
                var deleteRequest = new DeleteObjectsAndRelationsRequest { ObjectsToDelete = serviceResult.Result.ObjectsToDelete };
                result = await DeleteObjectsAndRelations(deleteRequest);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<DeleteObjectsAndRelationsResult>> DeleteObjectsAndRelations(DeleteObjectsAndRelationsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<DeleteObjectsAndRelationsResult>>(() =>
            {
                var result = new ResultItem<DeleteObjectsAndRelationsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new DeleteObjectsAndRelationsResult()
                };

                if (!request.ObjectsToDelete.Any()) return result;

                var dbReader = new OntologyModDBConnector(Globals);
                var dbDeletor = new OntologyModDBConnector(Globals);

                var searchAttributes = request.ObjectsToDelete.Select(obj => new clsObjectAtt
                {
                    ID_Object = obj.GUID
                }).ToList();

                result.ResultState = dbReader.GetDataObjectAtt(searchAttributes);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading the attributes";
                    return result;
                }

                var deleteAtt = dbReader.ObjAtts.Select(objAtt => new clsObjectAtt
                {
                    ID_Attribute = objAtt.ID_Attribute
                }).ToList();

                if (deleteAtt.Any())
                {
                    result.ResultState = dbDeletor.DelObjectAtts(deleteAtt);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting attributes";
                        return result;
                    }

                    result.Result.CountDeletedAttributes = deleteAtt.Count;
                }

                var searchRelLeftRight = request.ObjectsToDelete.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID
                }).ToList();

                result.ResultState = dbReader.GetDataObjectRel(searchRelLeftRight);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading the left-side relations";
                    return result;
                }

                var deleteRel = dbReader.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = rel.ID_RelationType,
                    ID_Other = rel.ID_Other
                }).ToList();

                if (deleteRel.Any())
                {
                    result.ResultState = dbDeletor.DelObjectRels(deleteRel);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting left-side-relations";
                        return result;
                    }

                    result.Result.CountDeletedRelations += deleteRel.Count;
                }

                var searchRelRightLeft = request.ObjectsToDelete.Select(obj => new clsObjectRel
                {
                    ID_Other = obj.GUID
                }).ToList();

                result.ResultState = dbReader.GetDataObjectRel(searchRelRightLeft);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading the right-side relations";
                    return result;
                }

                deleteRel = dbReader.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = rel.ID_RelationType,
                    ID_Other = rel.ID_Other
                }).ToList();

                if (deleteRel.Any())
                {
                    result.ResultState = dbDeletor.DelObjectRels(deleteRel);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting right-side-relations";
                        return result;
                    }

                    result.Result.CountDeletedRelations += deleteRel.Count;
                }

                var deleteItems = request.ObjectsToDelete.Select(obj => new clsOntologyItem
                {
                    GUID = obj.GUID
                }).ToList();

                result.ResultState = dbDeletor.DelObjects(deleteItems);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while deleting objects";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public OItemListController(Globals Globals) : base(Globals)
        {
        }
    }

    public class DataGridResultWeb<TType>
    {
        public clsOntologyItem Result { get; set; }
        public List<TType> GridItems { get; set; }
    }

    
}
