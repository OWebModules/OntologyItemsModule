﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Factories;
using OntologyItemsModule.Services;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule
{
    public class ClassTreeController : AppController
    {
        private object controllerLocker = new object();

        private ElasticAgentClasses serviceAgent;

        private ClassTreeFactory classTreeFactory;

        private ViewTreeNode searchNode;

        private ResultClassSelect resultClassSelect { get; set; }
        public ResultClassSelect ResultClassSelect
        {
            get
            {
                lock(controllerLocker)
                {
                    return resultClassSelect;
                }
            }
            set
            {
                lock(controllerLocker)
                {
                    resultClassSelect = value;
                }
            }
        }

        public async Task<ResultItem<ClassTreeConfiguration>> GetClassTreeConfiguration()
        {
            var taskResult = await Task.Run<ResultItem<ClassTreeConfiguration>>(() =>
            {
                var result = new ResultItem<ClassTreeConfiguration>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ClassTreeConfiguration("search-string")
                };

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> CreateClass(clsOntologyItem classItem)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = Globals.LState_Success.Clone();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultClassSelect> GetClassList(ClassSelectItem classSelectItem)
        {
            var taskResult = await Task.Run<ResultClassSelect>(async() =>
            {
                var resultItem = new ResultClassSelect
                {
                    Result = Globals.LState_Success.Clone()
                };


                if (string.IsNullOrEmpty(classSelectItem.SearchTerm))
                {

                    try
                    {
                        var resultTaskPath = await serviceAgent.GetPathItems(string.IsNullOrEmpty(classSelectItem.IdClass) ? Globals.Root.GUID : classSelectItem.IdClass);
                        resultItem.ClassPathNodes = resultTaskPath.PathItems;


                        var resultTaskClasses = await serviceAgent.GetDataTwoLevels(classSelectItem.IdClass);

                        if (resultTaskClasses.Result.GUID == Globals.LState_Success.GUID)
                        {
                            if (serviceAgent.ResultData.Result.GUID == Globals.LState_Error.GUID)
                            {
                                resultItem.Result = Globals.LState_Error.Clone();
                                ResultClassSelect = resultItem;
                                return resultItem;


                            }

                            var classTreeFactory = new ClassTreeFactory(Globals, searchNode);

                            var resultTaskFactory = await classTreeFactory.CreateTreeNodes(serviceAgent.ClassList, string.IsNullOrEmpty(classSelectItem.IdClass) ? -1 : 2, classSelectItem.IdClass);
                            if (resultTaskFactory.Result.GUID == Globals.LState_Error.GUID)
                            {
                                resultItem.Result = Globals.LState_Error.Clone();
                                ResultClassSelect = resultItem;
                                return resultItem;
                            }

                            resultItem.TreeNodes = resultTaskFactory.TreeNodes.OrderBy(treeNode => treeNode.Name).ToList();

                            //var resultJson = classTreeFactory.GetJson(resultTaskFactory.Result.RootTreeNodes, resultTaskFactory.Result.TreeNodes);
                            //if (resultJson.GUID == Globals.LState_Error.GUID)
                            //{
                            //    resultItem.Result = Globals.LState_Error.Clone();
                            //    ResultClassSelect = resultItem;
                            //    return resultItem;
                            //}

                            //resultItem.Json = resultJson.Additional1;
                        }

                    }
                    catch (Exception ex)
                    {

                        resultItem.Result = Globals.LState_Success.Clone();
                        resultItem.Result.Additional1 = ex.Message;
                    }

                }
                else
                {
                    try
                    {
                        resultItem.ClassPathNodes = new List<clsOntologyItem> { Globals.Root, new clsOntologyItem { GUID = searchNode.Id, Name = classSelectItem.SearchTerm } };


                        var resultTaskClasses = serviceAgent.SearchClasses(classSelectItem.SearchTerm);
                        resultTaskClasses.Wait();

                        if (resultTaskClasses.Result.Result.GUID == Globals.LState_Success.GUID)
                        {
                            if (serviceAgent.ResultData.Result.GUID == Globals.LState_Error.GUID)
                            {
                                resultItem.Result = Globals.LState_Error.Clone();
                                ResultClassSelect = resultItem;
                                return resultItem;


                            }

                            var classTreeFactory = new ClassTreeFactory(Globals, searchNode);

                            var resultTaskFactory = await classTreeFactory.CreateTreeNodes(serviceAgent.ClassList, -1, searchNode.Id);
                            if (resultTaskFactory.Result.GUID == Globals.LState_Error.GUID)
                            {
                                resultItem.Result = Globals.LState_Error.Clone();
                                ResultClassSelect = resultItem;
                                return resultItem;
                            }

                            resultItem.TreeNodes = resultTaskFactory.TreeNodes.OrderBy(treeNode => treeNode.Name).ToList();

                            //var resultJson = classTreeFactory.GetJson(resultTaskFactory.Result.RootTreeNodes, resultTaskFactory.Result.TreeNodes);
                            //if (resultJson.GUID == Globals.LState_Error.GUID)
                            //{
                            //    resultItem.Result = Globals.LState_Error.Clone();
                            //    ResultClassSelect = resultItem;
                            //    return resultItem;
                            //}

                            //resultItem.Json = resultJson.Additional1;
                        }

                    }
                    catch (Exception ex)
                    {

                        resultItem.Result = Globals.LState_Success.Clone();
                        resultItem.Result.Additional1 = ex.Message;
                    }
                }

                ResultClassSelect = resultItem;
                return resultItem;
            });

            return taskResult;
        }

        public async Task<ResultWriteJson> GetJsonClassTree(ClassSelectItem classSelectItem)
        {
            var taskResult = await Task.Run<ResultWriteJson>(() =>
            {
                var resultTask = GetClassList(classSelectItem);
                resultTask.Wait();

                var classTreeFactory = new ClassTreeFactory(Globals, searchNode);
                var result = classTreeFactory.GetJson(resultTask.Result.RootTreeNodes, resultTask.Result.TreeNodes);
                ;
                if (result.GUID == Globals.LState_Success.GUID)
                {

                    return new ResultWriteJson
                    {
                        Result = result,
                        Json = result.Additional1
                    };

                }
                else
                {
                    return new ResultWriteJson
                    {
                        Result = result,
                        Json = "[]"
                    };
                }
            });

            return taskResult;
            
        }

        public ClassTreeController(Globals globals  ) : base(globals)
        {            
            Initialize();
        }

        private void Initialize()
        {
            serviceAgent = new ElasticAgentClasses(Globals);
            classTreeFactory = new ClassTreeFactory(Globals, searchNode);
            searchNode = classTreeFactory.CrateSearchNode();
        }
    }

    public class ResultClassSelect
    {
        public clsOntologyItem Result { get; set; }
        public List<clsOntologyItem> ClassPathNodes { get; set; }
        public List<ViewTreeNode> RootTreeNodes { get; set; }
        public List<ViewTreeNode> TreeNodes { get; set; }
        public string Json { get; set; }
    }

    public class ClassSelectItem
    {
        public string IdClass { get; set; }
        public string SearchTerm { get; set; }
        public bool OnlyPathItems { get; set; }
    }

    public class ResultWriteJson
    {
        public clsOntologyItem Result { get; set; }
        public string Json { get; set; }
    }
}
