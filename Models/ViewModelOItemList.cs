﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    [Flags]
    public enum ListType
    {
        AttributeTypes = 0,
        RelationTypes = 2,
        Classes = 4,
        Instances = 8,
        ClassAttributeRelation = 16,
        ClassClassRelation_Conscious = 32,
        ClassClassRelation_Subconscious = 64,
        ClassClassRelation_Omni = 128,
        ClassOtherRelation_Conscious = 256,
        InstanceAttribute_Conscious = 512,
        InstanceInstance_Conscious = 1024,
        InstanceInstance_Subconscious = 2048,
        InstanceInstance_Omni = 4096,
        InstanceOther_Conscious = 8192,
        InstanceOther_Subconscious = 16384
    }
    [Flags]
    public enum ListError
    {
        None = 0,
        WrongListType = 1,
        LoadError = 2
    }
    [Flags]
    public enum DestinationType
    {
        None = 0,
        WinForms = 1
    }
    public class ViewModelOItemList : NotifyPropertyChange
    {

        internal Globals globals;

        private object listAdapterLocker = new object();

        private clsOntologyItem oItem_Class_AdvancedFilter;
        public clsOntologyItem OItem_Class_AdvancedFilter
        {
            get { return oItem_Class_AdvancedFilter; }
            set
            {
                oItem_Class_AdvancedFilter = value;
                RaisePropertyChanged(nameof(OItem_Class_AdvancedFilter));
            }
        }
        private clsOntologyItem oItem_RelationType_AdvancedFilter;
        public clsOntologyItem OItem_RelationType_AdvancedFilter
        {
            get { return oItem_RelationType_AdvancedFilter; }
            set
            {
                oItem_RelationType_AdvancedFilter = value;
                RaisePropertyChanged(nameof(OItem_RelationType_AdvancedFilter));
            }
        }
        private clsOntologyItem oItem_Object_AdvancedFilter;
        public clsOntologyItem OItem_Object_AdvancedFilter
        {
            get { return oItem_Object_AdvancedFilter; }
            set
            {
                oItem_Object_AdvancedFilter = value;
                RaisePropertyChanged(nameof(OItem_Object_AdvancedFilter));
            }
        }
        private clsOntologyItem oItem_Direction_AdvancedFilter;
        public clsOntologyItem OItem_Direction_AdvancedFilter
        {
            get { return oItem_Direction_AdvancedFilter; }
            set
            {
                oItem_Direction_AdvancedFilter = value;
                RaisePropertyChanged(nameof(OItem_Direction_AdvancedFilter));
            }
        }

        private ListLoadStateItem listLoadState;
        public ListLoadStateItem ListLoadState
        {
            get { return listLoadState; }
            set
            {
                listLoadState = value;
                AddAllowed = ListLoadState.HasFlag(ListLoadStateItem.AdvancedFilterLoaded) || ListLoadState.HasFlag(ListLoadStateItem.ListLoadLoaded);

                RaisePropertyChanged(nameof(ListLoadState));
            }
        }

        private ListAdapter listAdapter;
        public ListAdapter ListAdapter
        {
            get { return listAdapter; }
            set
            {
                listAdapter = value;
            }
        }

        private bool addAllowed;
        public bool AddAllowed
        {
            get { return addAllowed; }
            set
            {
                addAllowed = value;
                RaisePropertyChanged(nameof(AddAllowed));
            }
        }


        private clsOntologyItem noteListFilter;
        public clsOntologyItem NoteListFilter
        {
            get { return noteListFilter; }
            set
            {
                noteListFilter = value;
                RaisePropertyChanged(nameof(NoteListFilter));
            }
        }

        private clsClassAtt classAttFilter;
        public clsClassAtt ClassAttFilter
        {
            get { return classAttFilter; }
            set
            {
                classAttFilter = value;
                RaisePropertyChanged(nameof(ClassAttFilter));
            }
        }

        private clsClassRel classRelFilter;
        public clsClassRel ClassRelFilter
        {
            get { return classRelFilter; }
            set
            {
                classRelFilter = value;
                RaisePropertyChanged(nameof(ClassRelFilter));
            }
        }

        private ListType listType;
        public ListType ListType
        {
            get { return listType; }
            set
            {
                if (listType == value) return;

                listType = value;
                RaisePropertyChanged(nameof(ListType));
            }
        }

        private ResultOItemList resultListLoader;
        public ResultOItemList ResultListLoader
        {
            get
            {
                lock (listAdapterLocker)
                {
                    return resultListLoader;
                }
            }
            set
            {
                lock (listAdapterLocker)
                {
                    resultListLoader = value;
                }
                RaisePropertyChanged(nameof(ResultListLoader));
            }
        }

        public ViewModelOItemList(Globals globals)
        {
            this.globals = globals;
            listAdapter = new ListAdapter(globals);
            listAdapter.PropertyChanged += ListAdapter_PropertyChanged;
            
        }

        private void ListAdapter_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(e.PropertyName);
        }

        private DestinationType destinationType;
        public DestinationType DestinationType
        {
            get
            {
                return destinationType;
            }
            set
            {
                destinationType = value;
            }
        }

        public bool IsAllowedChangeOrderIdForListType
        {
            get
            {
                if (ListType == ListType.ClassAttributeRelation ||
                    ListType == ListType.ClassClassRelation_Conscious ||
                    ListType == ListType.ClassClassRelation_Omni ||
                    ListType == ListType.ClassClassRelation_Subconscious ||
                    ListType == ListType.ClassOtherRelation_Conscious ||
                    ListType == ListType.InstanceAttribute_Conscious ||
                    ListType == ListType.InstanceInstance_Conscious ||
                    ListType == ListType.InstanceInstance_Omni ||
                    ListType == ListType.InstanceInstance_Subconscious ||
                    ListType == ListType.InstanceOther_Conscious ||
                    ListType == ListType.InstanceOther_Subconscious)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public DeleteCounter DelClassAttributes(List<clsClassAtt> classAttributes)
        {
            return ListAdapter.DelClassAttributes(classAttributes);
        }


        public DeleteCounter DelClassRelations(List<clsClassRel> classRelations)
        {
            return ListAdapter.DelClassRelations(classRelations);
        }
        public clsOntologyItem MoveItemInList(object objectMoved, object objectMovedTo, bool directionUp)
        {
            clsOntologyItem result = globals.LState_Nothing.Clone();
            switch (ListType)
            {
                case ListType.InstanceAttribute_Conscious:
                    var itemSrc1 = (ObjectAttributeViewItem)objectMoved;
                    var itemDst1 = (ObjectAttributeViewItem)objectMovedTo;
                    if (directionUp)
                    {
                        itemSrc1.OrderId = itemDst1.OrderId - 1;
                    }
                    else
                    {
                        itemSrc1.OrderId = itemDst1.OrderId + 1;
                    }

                    result = itemSrc1.SaveItem(globals);
                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        result = itemDst1.SaveItem(globals);
                    }
                    break;
                case ListType.InstanceInstance_Conscious:
                    var itemSrc2 = (ObjectObject_Conscious)objectMoved;
                    var itemDst2 = (ObjectObject_Conscious)objectMovedTo;
                    if (directionUp)
                    {
                        itemSrc2.OrderId = itemDst2.OrderId - 1;
                    }
                    else
                    {
                        itemSrc2.OrderId = itemDst2.OrderId + 1;
                    }

                    result = itemSrc2.SaveItem(globals);
                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        result = itemDst2.SaveItem(globals);
                    }
                    break;
                case ListType.InstanceInstance_Subconscious:
                    var itemSrc3 = (ObjectObject_Subconscious)objectMoved;
                    var itemDst3 = (ObjectObject_Subconscious)objectMovedTo;
                    if (directionUp)
                    {
                        itemSrc3.OrderId = itemDst3.OrderId - 1;
                    }
                    else
                    {
                        itemSrc3.OrderId = itemDst3.OrderId + 1;
                    }

                    result = itemSrc3.SaveItem(globals);
                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        result = itemDst3.SaveItem(globals);
                    }
                    break;

                case ListType.InstanceInstance_Omni:
                    var itemSrc4 = (ObjectObject_Omni)objectMoved;
                    var itemDst4 = (ObjectObject_Omni)objectMovedTo;
                    if (directionUp)
                    {
                        itemSrc4.OrderId = itemDst4.OrderId - 1;
                    }
                    else
                    {
                        itemSrc4.OrderId = itemDst4.OrderId + 1;
                    }

                    result = itemSrc4.SaveItem(globals);
                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        result = itemDst4.SaveItem(globals);
                    }
                    break;
                case ListType.InstanceOther_Conscious:
                    var itemSrc5 = (ObjectObject_Conscious)objectMoved;
                    var itemDst5 = (ObjectObject_Conscious)objectMovedTo;
                    if (directionUp)
                    {
                        itemSrc5.OrderId = itemDst5.OrderId - 1;
                    }
                    else
                    {
                        itemSrc5.OrderId = itemDst5.OrderId + 1;
                    }

                    result = itemSrc5.SaveItem(globals);
                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        result = itemDst5.SaveItem(globals);
                    }
                    break;
                case ListType.InstanceOther_Subconscious:
                    var itemSrc6 = (ObjectObject_Subconscious)objectMoved;
                    var itemDst6 = (ObjectObject_Subconscious)objectMovedTo;
                    if (directionUp)
                    {
                        itemSrc6.OrderId = itemDst6.OrderId - 1;
                    }
                    else
                    {
                        itemSrc6.OrderId = itemDst6.OrderId + 1;
                    }

                    result = itemSrc6.SaveItem(globals);
                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        result = itemDst6.SaveItem(globals);
                    }
                    break;

            }

            return result;
        }

        public bool IsAllowed_DataGridDragAndDrop
        {
            get
            {
                return ListType == ListType.InstanceAttribute_Conscious ||
                    ListType == ListType.InstanceInstance_Conscious ||
                    ListType == ListType.InstanceInstance_Omni ||
                    ListType == ListType.InstanceInstance_Subconscious ||
                    ListType == ListType.InstanceOther_Conscious ||
                    ListType == ListType.InstanceOther_Subconscious;
            }
        }

        public async Task<ResultOItemList> Initialize_AdvancedFilter(ListType listType,
            clsOntologyItem rootItem,
            clsOntologyItem advancedFilter_Class = null, 
            clsOntologyItem advancedFilter_Object = null,
            clsOntologyItem advancedFilter_RelationType = null,
            clsOntologyItem advancedFilter_Direction = null)
        {
            ListType = listType;
            OItem_Class_AdvancedFilter = advancedFilter_Class;
            OItem_Object_AdvancedFilter = advancedFilter_Object;
            OItem_RelationType_AdvancedFilter = advancedFilter_RelationType;
            OItem_Direction_AdvancedFilter = advancedFilter_Direction;

            var resultTask = await listAdapter.Initialize_AdvancedFilter(ListType,
                rootItem,
                OItem_Class_AdvancedFilter,
                OItem_Object_AdvancedFilter,
                OItem_RelationType_AdvancedFilter,
                OItem_Direction_AdvancedFilter);

            var resultOItems = new ResultOItemList(resultTask) { ListError = ListError.None };
            ResultListLoader = resultOItems;
            return resultOItems;
        }

        

        

        public async Task<ResultOItemList> Initialize_ItemList(ListType listType, clsClassRel filterItem, bool exact = false, DestinationType destinationType = DestinationType.None)
        {
            
            DestinationType = destinationType;
            ListType = listType;

            var result = new ResultOItemList
            {
                ListType = listType,
                ListError = ListError.WrongListType,
                Result = globals.LState_Success.Clone()
            };

            switch (ListType)
            {
                case ListType.ClassClassRelation_Conscious:
                case ListType.ClassClassRelation_Subconscious:
                case ListType.ClassClassRelation_Omni:
                case ListType.ClassOtherRelation_Conscious:
                    var resultTask = await ListAdapter.Initialize_ClassRelationList(filterItem, listType, exact);
                    result = new ResultOItemList(resultTask);

                    break;
                default:
                    break;
            }

            ResultListLoader = result;
            return result;
            
        }

        public async Task<ResultOItemList> Initialize_ItemList(ListType listType, clsObjectRel filterItem, bool exact = false, DestinationType destinationType = DestinationType.None)
        {
            
            DestinationType = destinationType;
            ListType = listType;
            var result = new ResultOItemList
            {
                ListType = listType,
                ListError = ListError.WrongListType,
                Result = globals.LState_Success.Clone()
            };

            switch (ListType)
            {
                case ListType.InstanceInstance_Conscious:
                case ListType.InstanceInstance_Subconscious:
                case ListType.InstanceInstance_Omni:
                case ListType.InstanceOther_Conscious:
                case ListType.InstanceOther_Subconscious:
                    var resultTask = await ListAdapter.Initialize_ObjectRelationList(listType, filterItem, exact);
                    result = new ResultOItemList(resultTask);

                    break;
                default:
                    break;
            }

            ResultListLoader = result;
            return result;
        }

        public async Task<ResultOItemList> Initialize_ItemList(clsObjectAtt filterItem, bool exact = false, DestinationType destinationType = DestinationType.None)
        {
            DestinationType = destinationType;
            ListType = ListType.InstanceAttribute_Conscious;
            var result = new ResultOItemList
            {
                ListType = listType,
                ListError = ListError.None,
                Result = globals.LState_Success.Clone()
            };
            
            var resultTask = await ListAdapter.Initialize_ObjectAttributeList(filterItem, exact);
            result = new ResultOItemList(resultTask);

            ResultListLoader = result;
            return result;

        }

        public async Task<ResultOItemList> Initialize_ItemList(clsClassAtt filterItem, bool exact = false, DestinationType destinationType = DestinationType.None)
        {
            DestinationType = destinationType;
            ListType = ListType.ClassAttributeRelation;
            var result = new ResultOItemList
            {
                ListType = listType,
                ListError = ListError.None,
                Result = globals.LState_Success.Clone()
            };
            var resultTask = await ListAdapter.Initialize_ClassAttributeList(filterItem, exact);
            result = new ResultOItemList(resultTask);

            ResultListLoader = result;
            return result;

        }
        public async Task<ResultOItemList> Initialize_ItemList(ListType listType, clsOntologyItem filterItem, bool exact = false, DestinationType destinationType = DestinationType.None)
        {
            DestinationType = destinationType;
            ListType = listType;
            var result = new ResultOItemList
            {
                ListType = listType,
                ListError = ListError.None,
                Result = globals.LState_Error.Clone()
            };

            switch (ListType)
            {
                case ListType.AttributeTypes:
                    NoteListFilter = filterItem;

                    var resultTask1 = await ListAdapter.Initialize_AttributeTypeList(filterItem, exact);

                    result = new ResultOItemList(resultTask1) { ListError = ListError.None };
                    break;
                case ListType.Classes:
                    NoteListFilter = filterItem;

                    var resultTask2 = await ListAdapter.Initialize_ClassList(filterItem, exact);
                    result = new ResultOItemList(resultTask2) { ListError = ListError.None };
                    break;
                case ListType.RelationTypes:
                    NoteListFilter = filterItem;
                    var resultTask3 = await ListAdapter.Initialize_RelationTypeList(filterItem, exact);
                    result = new ResultOItemList(resultTask3) { ListError = ListError.None };
                    break;
                case ListType.Instances:
                    NoteListFilter = filterItem;
                    var resultTask4 = await ListAdapter.Initialize_InstanceList(filterItem, exact);
                    result = new ResultOItemList(resultTask4) { ListError = ListError.None };
                    break;
                default:
                    break;
            }


            ResultListLoader = result;
            return result;
        }

    }

    public class ResultOItemList 
    {
        public ListError ListError { get; set; }
        public ListType ListType { get; set; }
        public clsOntologyItem Result { get; set; }
        public List<ClassViewItem> ClassViewItems { get; set; }
        public List<AttributeTypeViewItem> AttributeTypeViewItems { get; set; }
        public List<RelationTypeViewItem> RelationTypeViewItems { get; set; }
        public List<InstanceViewItem> InstanceViewItems { get; set; }
        public List<ClassAttributeViewItem> ClassAttViewItems { get; set; }
        public List<ClassRelationConsciousViewItem> ClassRelConciousViewItems { get; set; }
        public List<ClassRelationSubconsciousViewItem> ClassRelSubconsciousViewItems { get; set; }
        public List<ClassRelationOmniViewItem> ClassRelOmniViewItems { get; set; }
        public List<ObjectAttributeViewItem> ObjectAttributeViewItems { get; set; }
        public List<ObjectObject_Conscious> ObjectObjectConsciousViewItems { get; set; }
        public List<ObjectObject_Subconscious> ObjectObjectSubconsciousViewItems { get; set; }
        public List<ObjectObject_Conscious> ObjectOtherConsciousViewItems { get; set; }
        public List<ObjectObject_Subconscious> ObjectOtherSubconsciousViewItems { get; set; }

        public List<ObjectObject_Omni> ObjectObjectOmniViewItems { get; set; }

        public ResultOItemList()
        {
            ClassViewItems = new List<ClassViewItem>();
            AttributeTypeViewItems = new List<AttributeTypeViewItem>();
            RelationTypeViewItems = new List<RelationTypeViewItem>();
            InstanceViewItems = new List<InstanceViewItem>();
            ClassAttViewItems = new List<ClassAttributeViewItem>();
            ClassRelConciousViewItems = new List<ClassRelationConsciousViewItem>();
            ClassRelSubconsciousViewItems = new List<ClassRelationSubconsciousViewItem>();
            ClassRelOmniViewItems = new List<ClassRelationOmniViewItem>();
            ObjectAttributeViewItems = new List<ObjectAttributeViewItem>();
            ObjectObjectConsciousViewItems = new List<ObjectObject_Conscious>();
            ObjectObjectSubconsciousViewItems = new List<ObjectObject_Subconscious>();
            ObjectOtherConsciousViewItems = new List<ObjectObject_Conscious>();
            ObjectOtherSubconsciousViewItems = new List<ObjectObject_Subconscious>();
            ObjectObjectOmniViewItems = new List<ObjectObject_Omni>();
        }
        public ResultOItemList(ResultViewItems resultViewItems)
        {
            Result = resultViewItems.Result;
            ListType = resultViewItems.ListType;
            ClassViewItems = resultViewItems.ClassViewItems;
            AttributeTypeViewItems = resultViewItems.AttributeTypeViewItems;
            RelationTypeViewItems = resultViewItems.RelationTypeViewItems;
            InstanceViewItems = resultViewItems.InstanceViewItems;
            ClassAttViewItems = resultViewItems.ClassAttViewItems;
            ClassRelConciousViewItems = resultViewItems.ClassRelConciousViewItems;
            ClassRelSubconsciousViewItems = resultViewItems.ClassRelSubconsciousViewItems;
            ClassRelOmniViewItems = resultViewItems.ClassRelOmniViewItems;
            ObjectAttributeViewItems = resultViewItems.ObjectAttributeViewItems;
            ObjectObjectConsciousViewItems = resultViewItems.ObjectObjectConsciousViewItems;
            ObjectObjectSubconsciousViewItems = resultViewItems.ObjectObjectSubconsciousViewItems;
            ObjectOtherConsciousViewItems = resultViewItems.ObjectOtherConsciousViewItems;
            ObjectOtherSubconsciousViewItems = resultViewItems.ObjectOtherSubconsciousViewItems;
            ObjectObjectOmniViewItems = resultViewItems.ObjectObjectOmniViewItems;
        }
    }
}
