﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class CheckObjectsResult
    {
        public int ItemsToSave { get; set; }
        public int CountExisting { get; set; }

        private List<string> existingNames = new List<string>();
        public IReadOnlyList<string> ExistingNames => existingNames.AsReadOnly();
        
        public CheckObjectsResult(List<clsOntologyItem> objects)
        {
            this.existingNames = objects.Where(obj => obj.New_Item.Value == false).Select(obj => obj.Name).ToList();
            this.ItemsToSave = objects.Count;
        }


    }
}
