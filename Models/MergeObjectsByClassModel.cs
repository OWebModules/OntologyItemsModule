﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class MergeObjectsByClassModel
    {
        public clsOntologyItem ConfigItem { get; set; }

        public List<clsObjectRel> ConfigsToClasses { get; set; } = new List<clsObjectRel>();
        public List<AttributeAndRegex> Excludes { get; set; } = new List<AttributeAndRegex>();
        public List<AttributeAndRegex> Includes { get; set; } = new List<AttributeAndRegex>();

        public List<clsObjectRel> ObjectsToMerge { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> NamingRules { get; set; } = new List<clsObjectRel>();
    }

    public class AttributeAndRegex
    {
        public Regex Regex { get; set; }
        public clsObjectAtt Attribute { get; set; }
    }
}
