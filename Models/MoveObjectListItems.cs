﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class MoveObjectListItems
    {
        public clsOntologyItem Config { get; set; }
        public clsOntologyItem DestClass { get; set; }

        public List<clsOntologyItem> ObjectList { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectAtt> ObjectAttributes { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> ObjectRelationsLeftRight { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ObjectRelationsRightLeft { get; set; } = new List<clsObjectRel>();
    }
}
