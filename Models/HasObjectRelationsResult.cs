﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class HasObjectRelationsResult
    {
        public string IdObject { get; set; }
        public long CountAttributeRelations { get; set; }
        public long CountRightLeftRelations { get; set; }
        public long CountLeftRightRelations { get; set; }

        public bool HasObjectRelations
        {
            get
            {
                return (CountAttributeRelations > 0 ||
                    CountLeftRightRelations > 0 ||
                    CountRightLeftRelations > 0);
            }
        }
    }

    public class HasRelationsAttributeTypeResult
    {
        public string IdAttributeType { get; set; }
        public long CountObjectAttributeRelations { get; set; }
        public long CountClassAttributeRelations { get; set; }

        public bool HasRelations
        {
            get
            {
                return CountObjectAttributeRelations > 0 ||
                    CountClassAttributeRelations > 0;
            }
        }
    }

}
