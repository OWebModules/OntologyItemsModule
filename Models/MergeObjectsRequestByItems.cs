﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class MergeObjectsRequestByItems
    {
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutput { get; set; }


        public MergeObjectsRequestByItems(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
