﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class OntologyBaseItemLists
    {
        public List<clsOntologyItem> OItems { get; set; } = new List<clsOntologyItem>();

        public List<clsOntologyItem> GetClasses(Globals globals)
        {
            return OItems.Where(oItem => oItem.Type == globals.Type_Class).ToList();
        }
        public List<clsOntologyItem> GetRelationTypes(Globals globals)
        {
            return OItems.Where(oItem => oItem.Type == globals.Type_RelationType).ToList();
        }

        public List<clsOntologyItem> GetAttributeTypes(Globals globals)
        {
            return OItems.Where(oItem => oItem.Type == globals.Type_AttributeType).ToList();
        }

        public List<clsOntologyItem> GetObjects(Globals globals)
        {
            return OItems.Where(oItem => oItem.Type == globals.Type_Object).ToList();
        }

        public List<clsObjectAtt> ObjectAttributes { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> ObjectRelations { get; set; } = new List<clsObjectRel>();

        public List<clsClassAtt> ClassAttributes { get; set; } = new List<clsClassAtt>();

        public List<clsClassRel> ClassRelations { get; set; } = new List<clsClassRel>();
    }
}
