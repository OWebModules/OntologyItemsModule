﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class DeleteObjectsAndRelationsResult
    {
        public long CountDeletedObjects { get; set; }
        public long CountDeletedAttributes { get; set; }

        public long CountDeletedRelations { get; set; }
    }
}
