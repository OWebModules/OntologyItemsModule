﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;

using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class ObjectRelationViewItem : NotifyPropertyChange
    {
        internal clsObjectRel proxyItem;

        [KendoColumn(hidden = true)]
        [DataViewColumn(IsVisible = false)]
        public string IdObject
        {
            get { return proxyItem.ID_Object; }
            set
            {
                proxyItem.ID_Object = value;
                RaisePropertyChanged(nameof(IdObject));
            }
        }

        [KendoColumn(hidden = true)]
        [DataViewColumn(IsVisible = false)]
        public string IdOther
        {
            get { return proxyItem.ID_Other; }
            set
            {
                proxyItem.ID_Other = value;
                RaisePropertyChanged(nameof(IdOther));
            }
        }

        [KendoColumn(hidden = true)]
        public string IdParentObject
        {
            get { return proxyItem.ID_Parent_Object; }
            set
            {
                proxyItem.ID_Parent_Object = value;
                RaisePropertyChanged(nameof(IdParentObject));
            }
        }

        [KendoColumn(hidden = true)]
        public string IdParentOther
        {
            get { return proxyItem.ID_Parent_Other; }
            set
            {
                proxyItem.ID_Parent_Other = value;
                RaisePropertyChanged(nameof(IdParentOther));
            }
        }

        [KendoColumn(hidden = true)]
        [DataViewColumn(IsVisible = false)]
        public string IdRelationType
        {
            get { return proxyItem.ID_RelationType; }
            set
            {
                proxyItem.ID_RelationType = value;
                RaisePropertyChanged(nameof(IdRelationType));
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveObjRel(new List<clsObjectRel> { proxyItem });

            return result;
        }

        public clsObjectRel GetProxyItem()
        {
            return proxyItem;
        }


        public ObjectRelationViewItem(clsObjectRel proxyItem)
        {
            this.proxyItem = proxyItem;
        }
    }
}
