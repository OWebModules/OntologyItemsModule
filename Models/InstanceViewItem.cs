﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;

using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.incell,
        height = "100%")]

    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[]{ 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class InstanceViewItem : NotifyPropertyChange
    {
        private clsOntologyItem proxyItem;

        [DataViewColumn(IsVisible = false)]
        [KendoColumn(hidden =true)]
        [KendoModel(IsId = true)]
        public string IdInstance
        {
            get { return proxyItem.GUID; }
            set
            {
                proxyItem.GUID = value;
                RaisePropertyChanged(nameof(IdInstance));
            }
        }

        private bool apply;
        [DataViewColumn(IsVisible = false, DisplayOrder = 0, CellType = CellType.Boolean)]
        [KendoColumn(hidden = false
            ,Order = 0
            ,filterable = true
            ,title = "Apply"
            ,headerTemplate = "<input type='checkbox' class='k-checkbox header-checkbox'><label class='k-checkbox-label' for='header-chb'></label>"
            ,template = "<input type=\"checkbox\" #= Apply ? 'checked=\"checked\"' : \"\" # class=\"chkbxApply\" />", width = "80px",
            type = ColType.BooleanType)]
        [KendoColumnSortable(sortable = false)]
        public bool Apply
        {
            get { return apply; }
            set
            {
                apply = value;
                RaisePropertyChanged(nameof(Apply));
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 1, CellType = CellType.String)]
        [KendoColumn(hidden = false, 
            Order = 1, 
            filterable = true, 
            title = "Name", 
            delay = 400,
            cssClass = "name-cell")]
        [KendoModel(editable = true)]
        public string NameInstance
        {
            get { return proxyItem.Name; }
            set
            {
                proxyItem.Name = value;
                RaisePropertyChanged(nameof(NameInstance));
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 2, CellType = CellType.Integer)]
        [KendoColumn(hidden = false,
            Order = 2,
            filterable = false,
            title = "Action",
            width = "120px",
            template = "<button type='button' class='button-object-edit'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button><button type='button' class='button-module-starter'><i class='fa fa-external-link-square' aria-hidden='true'></i></button><button type='button' class='button-object-duplicate'><i class='fa fa-files-o' aria-hidden='true'></i></button>")]
        [KendoModel(editable = false)]
        public int Edit { get; set; }

        [KendoColumn(hidden = true)]
        public string IdClass
        {
            get { return proxyItem.GUID_Parent; }
            set
            {
                proxyItem.GUID_Parent = value;
                RaisePropertyChanged(nameof(IdClass));
            }
        }

        

        [DataViewColumn(IsVisible = true, DisplayOrder = 2, CellType = CellType.String, Width = 250)]
        [KendoColumn(hidden = true, Order = 2, filterable = true, title = "Class", width = "200px")]
        public string NameClass
        {
            get { return proxyItem.Name_Parent; }
            set
            {
                proxyItem.Name_Parent = value;
                RaisePropertyChanged(nameof(NameClass));
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveClass(new List<clsOntologyItem> { proxyItem });

            return result;
        }

        [DataViewColumn(IsVisible = false, DisplayOrder = 3, CellType = CellType.String, Width = 250)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.GUID : null;
            }
        }

        public clsOntologyItem GetProxyItem()
        {
            return proxyItem;
        }

        public InstanceViewItem(clsOntologyItem proxyItem, clsOntologyItem classItem)
        {
            this.proxyItem = proxyItem;
            this.proxyItem.Name_Parent = classItem.Name;
        }
    }

   
}
