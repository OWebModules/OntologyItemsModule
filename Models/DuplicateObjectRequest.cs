﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class DuplicateObjectRequest
    {
        public string IdObject { get; private set; }

        public string NewObjectName { get; set; }
        public IMessageOutput MessageOutput { get; set;}

        public DuplicateObjectRequest(string idObject)
        {
            IdObject = idObject;
        }

    }
}
