﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class CheckHierarchicalItemResult
    {
        public CheckHierarchicalItemRequest Request { get; set; }
        public HierarchicalOItem HierarchicalItem { get; set; }
    }
}
