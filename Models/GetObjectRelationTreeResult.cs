﻿using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class GetObjectRelationTreeResult
    {
        public List<KendoTreeNode> TreeNodes { get; set; } = new List<KendoTreeNode>();
        public ObjectRelationTreeModel Model { get; set; }
    }
}
