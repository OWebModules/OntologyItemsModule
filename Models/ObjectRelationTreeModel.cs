﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class ObjectRelationTreeModel
    {
        public clsOntologyItem ReferenceItem { get; set; }
        public List<clsObjectRel> ReferenceItemsToShowRelationObjects { get; set; } = new List<clsObjectRel>();
        public List<clsOntologyItem> ShowRelationObjects { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectAtt> HelpLeftRight { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> LeftRight { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> UseNameprovider { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> ShowRelationToCssClasses { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ShowRelationToMainHelpClasses { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ShowRelationToRelationClassesFirst { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ShowRelationToRelationClassesSecond { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ShowRelationToRelationTypesMainHelp { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ShowRelationToRelationTypesRelationClasses { get; set; } = new List<clsObjectRel>();

        public List<RelatedItem> RelatedItems { get; set; } = new List<RelatedItem>();
    }

    public class RelatedItem
    {
        public clsOntologyItem ReferenceObject { get; set; }
        public clsOntologyItem ShowObjectItem { get; set; }

        public clsOntologyItem Related { get; set; }

        public long OrderId { get; set; }

    }
}
