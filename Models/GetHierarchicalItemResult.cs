﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class GetHierarchicalItemResult
    {
        public GetHierarchicalItemRequest Request { get; set; }
        public HierarchicalOItem HierarchicalItem { get; set; }
        
    }
}
