﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class CopyRelationsRequest
    {
        public string IdConfig { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public CopyRelationsRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
