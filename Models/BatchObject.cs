﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class BatchObject
    {
        public string IdClass { get; set; }
        public string NameClass { get; set; }

        public List<RelationItem> RelationItems { get; set; }

    }

    public class RelationItem
    {
        public clsObjectRel ObjectRelation { get; set; }
        public clsObjectAtt AttributeRelation { get; set; }
    }

}
