﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class ClassRelationViewItem : NotifyPropertyChange
    {
        internal clsClassRel proxyItem;

        public string IdClassLeft
        {
            get { return proxyItem.ID_Class_Left; }
            set
            {
                proxyItem.ID_Class_Left = value;
                RaisePropertyChanged(nameof(IdClassLeft));
            }
        }

        public string IdClassRight
        {
            get { return proxyItem.ID_Class_Right; }
            set
            {
                proxyItem.ID_Class_Left = value;
                RaisePropertyChanged(nameof(IdClassRight));
            }
        }

        public string IdRelationType
        {
            get { return proxyItem.ID_RelationType; }
            set
            {
                proxyItem.ID_RelationType = value;
                RaisePropertyChanged(nameof(IdRelationType));
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveClassRel(new List<clsClassRel> { proxyItem });

            return result;
        }


        public ClassRelationViewItem(clsClassRel proxyItem)
        {
            this.proxyItem = proxyItem;
        }
    }
}
