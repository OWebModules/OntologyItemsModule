﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class GetObjectRelationTreeRequest
    {
        public string IdObject { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public GetObjectRelationTreeRequest(string idObject)
        {
            IdObject = idObject;
        }
    }
}
