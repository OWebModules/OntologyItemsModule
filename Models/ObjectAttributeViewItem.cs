﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;

using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.cell,
        editable = EditableType.incell,
        height = "100%")]

    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class ObjectAttributeViewItem : NotifyPropertyChange
    {
        private clsObjectAtt proxyItem;

        [KendoColumn(hidden = true)]
        [DataViewColumn(IsVisible = false)]
        public string IdAttribute
        {
            get { return proxyItem.ID_Attribute; }
            set
            {
                proxyItem.ID_Attribute = value;
                RaisePropertyChanged(nameof(IdAttribute));
            }
        }

        [KendoColumn(hidden = true)]
        public string IdAttributeType
        {
            get { return proxyItem.ID_AttributeType; }
            set
            {
                proxyItem.ID_AttributeType = value;
                RaisePropertyChanged(nameof(IdAttributeType));
            }
        }

        private bool apply;
        [DataViewColumn(IsVisible = false, DisplayOrder = 0, CellType = CellType.Boolean)]
        [KendoColumn(hidden = false, 
            Order = 0, 
            filterable = true, 
            title = "Apply", 
            template = "<input type=\"checkbox\" #= Apply ? 'checked=\"checked\"' : \"\" # class=\"chkbxApply\" />", 
            width = "80px", 
            type = ColType.BooleanType,
            headerTemplate = "<input type='checkbox' class='k-checkbox header-checkbox'><label class='k-checkbox-label' for='header-chb'></label>")]
        [KendoColumnSortable(sortable = false)]
        public bool Apply
        {
            get { return apply; }
            set
            {
                apply = value;
                RaisePropertyChanged(nameof(Apply));
            }
        }

        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Attribute-Type")]
        [DataViewColumn(IsVisible = true, DisplayOrder = 1, CellType = CellType.String)]
        public string NameAttributeType
        {
            get { return proxyItem.Name_AttributeType; }
            set
            {
                proxyItem.Name_AttributeType = value;
                RaisePropertyChanged(nameof(NameAttributeType));
            }
        }

        [KendoColumn(hidden = true)]
        public string IdClass
        {
            get { return proxyItem.ID_Class; }
            set
            {
                proxyItem.ID_Class = value;
                RaisePropertyChanged(nameof(IdClass));
            }
        }

        [KendoColumn(hidden = true)]
        public string NameClass
        {
            get { return proxyItem.Name_Class; }
            set
            {
                proxyItem.Name_Class = value;
                RaisePropertyChanged(nameof(NameClass));
            }
        }

        [KendoColumn(hidden = true)]
        public string IdObject
        {
            get { return proxyItem.ID_Object; }
            set
            {
                proxyItem.ID_Object = value;
                RaisePropertyChanged(nameof(IdObject));
            }
        }

        [KendoColumn(hidden = true)]
        public string NameObject
        {
            get { return proxyItem.Name_Object; }
            set
            {
                proxyItem.Name_Object = value;
                RaisePropertyChanged(nameof(NameObject));
            }
        }

        [KendoColumn(hidden = true)]
        public string IdDataType
        {
            get { return proxyItem.ID_DataType; }
            set
            {
                proxyItem.ID_DataType = value;
                RaisePropertyChanged(nameof(IdDataType));
            }
        }

        [KendoColumn(hidden = false, Order = 4, filterable = true, title = "Data-Type")]
        [DataViewColumn(IsVisible = true, DisplayOrder = 4, CellType = CellType.String)]
        public string NameDataType
        {
            get { return proxyItem.Name_DataType; }
            set
            {
                proxyItem.Name_DataType = value;
                RaisePropertyChanged(nameof(NameDataType));
            }
        }

        [KendoColumn(hidden = false, Order = 5, filterable = true, title = "Order", type = ColType.NumberType)]
        [DataViewColumn(IsVisible = true, DisplayOrder = 5, CellType = CellType.Integer)]
        [KendoModel(editable = true)]
        public long OrderId
        {
            get { return proxyItem.OrderID ?? 0; }
            set
            {
                proxyItem.OrderID = value;
                RaisePropertyChanged(nameof(OrderId));
            }
        }

        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "Value")]
        [DataViewColumn(IsVisible = true, DisplayOrder = 2, CellType = CellType.String)]
        public string Value
        {
            get { return proxyItem.Val_Named; }
            set
            {
                proxyItem.Val_Named = value;
                RaisePropertyChanged(nameof(Value));
            }
        }

        public DateTime? Value_DateTime
        {
            get { return proxyItem.Val_Date; }
            set
            {
                proxyItem.Val_Date = value;
                RaisePropertyChanged(nameof(Value_DateTime));
            }
        }

        public long? Value_Int
        {
            get { return proxyItem.Val_Int; }
            set
            {
                proxyItem.Val_Int = value;
                RaisePropertyChanged(nameof(Value_Int));
            }
        }

        public double? Value_Double
        {
            get { return proxyItem.Val_Double; }
            set
            {
                proxyItem.Val_Double = value;
                RaisePropertyChanged(nameof(Value_Double));
            }
        }

        public string Value_String
        {
            get { return proxyItem.Val_String; }
            set
            {
                proxyItem.Val_String = value;
                RaisePropertyChanged(nameof(Value_String));
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveObjAtt(new List<clsObjectAtt> { proxyItem });

            return result;
        }

        [DataViewColumn(IsVisible = false, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.ID_Object + proxyItem.ID_AttributeType + proxyItem.OrderID : null;
            }
        }


        public clsObjectAtt GetProxyItem()
        {
            return proxyItem;
        }

        public ObjectAttributeViewItem(clsObjectAtt proxyItem)
        {
            this.proxyItem = proxyItem;
        }
    }
}
