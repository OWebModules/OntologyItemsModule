﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class OrderIdChange
    {
        public string IdObject { get; set; }
        public string IdOther { get; set; }

        public string IdRelationType { get; set; }

        public long OrderId { get; set; }
    }
}
