﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class MergeObjectsByItemsModel
    {
        public clsOntologyItem RootMergeConfig { get; set; }
        public List<clsOntologyItem> MergeConfigs { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> MergeHeads { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> MergeMerges { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ArchiveClasses { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> NamingRules { get; set; } = new List<clsObjectRel>();

    }
}
