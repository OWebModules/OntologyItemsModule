﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.incell,
        height = "100%")]

    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class ObjectObject_Conscious : ObjectRelationViewItem
    {
        [KendoColumn(hidden = true)]
        public string NameObject
        {
            get { return proxyItem.Name_Object; }
            set
            {
                proxyItem.Name_Object = value;
                RaisePropertyChanged(nameof(NameObject));
            }
        }

        private bool apply;
        [DataViewColumn(IsVisible = false, DisplayOrder = 0, CellType = CellType.Boolean)]
        [KendoColumn(hidden = false, 
            Order = 0, 
            filterable = true, 
            title = "Apply", 
            template = "<input type=\"checkbox\" #= Apply ? 'checked=\"checked\"' : \"\" # class=\"chkbxApply\" />", 
            width = "80px", 
            type = ColType.BooleanType,
            headerTemplate = "<input type='checkbox' class='k-checkbox header-checkbox'><label class='k-checkbox-label' for='header-chb'></label>")]
        [KendoColumnSortable(sortable = false)]
        public bool Apply
        {
            get { return apply; }
            set
            {
                apply = value;
                RaisePropertyChanged(nameof(Apply));
            }
        }

        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "Name")]
        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 2, CellType = CellType.String)]
        [KendoModel(editable = true)]
        public string NameOther
        {
            get { return proxyItem.Name_Other; }
            set
            {
                proxyItem.Name_Other = value;
                RaisePropertyChanged(nameof(NameOther));
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 3, CellType = CellType.Integer)]
        [KendoColumn(hidden = false,
            Order = 3,
            filterable = false,
            title = "Action",
            width = "80px",
            template = "<button type='button' class='button-object-edit-left'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button><button type='button' class='button-module-starter-left'><i class='fa fa-external-link-square' aria-hidden='true'></i></button>")]
        [KendoModel(editable = false)]
        public int Edit { get; set; }

        [KendoColumn(hidden = false, Order = 5, filterable = true, title = "RelationType")]
        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 5, CellType = CellType.String)]
        public string NameRelationType
        {
            get { return proxyItem.Name_RelationType; }
            set
            {
                proxyItem.Name_RelationType = value;
                RaisePropertyChanged(nameof(NameRelationType));
            }
        }

        [KendoColumn(hidden = false, Order = 4, filterable = true, title = "Parent")]
        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 4, CellType = CellType.String)]
        public string NameParentOther
        {
            get { return proxyItem.Name_Parent_Other; }
            set
            {
                proxyItem.Name_Parent_Other = value;
                RaisePropertyChanged(nameof(NameParentOther));
            }
        }

        [KendoColumn(hidden = true)]
        public string NameParentObject
        {
            get { return proxyItem.Name_Parent_Object; }
            set
            {
                proxyItem.Name_Parent_Object = value;
                RaisePropertyChanged(nameof(NameParentObject));
            }
        }

        [KendoColumn(hidden = false, Order = 6, filterable = true, title = "Order", type = ColType.NumberType)]
        [DataViewColumn(IsVisible = true,
                   DisplayOrder = 6, CellType = CellType.Integer)]
        [KendoModel(editable = true)]
        public long OrderId
        {
            get { return proxyItem.OrderID ?? 0; }
            set
            {
                proxyItem.OrderID = value;
                RaisePropertyChanged(nameof(OrderId));
            }
        }

        [KendoColumn(hidden = false, Order = 7, filterable = true, title = "Ontology")]
        [DataViewColumn(IsVisible = true,
                   DisplayOrder = 7, CellType = CellType.String)]
        public string Ontology
        {
            get { return proxyItem.Ontology; }
            set
            {
                proxyItem.Ontology = value;
                RaisePropertyChanged(nameof(Ontology));
            }
        }

        [KendoColumn(hidden = true)]
        [DataViewColumn(IsVisible = false, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        [KendoModel(IsId = true)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.ID_Object + proxyItem.ID_Other + proxyItem.ID_RelationType : null;
            }
        }

        public ObjectObject_Conscious(clsObjectRel proxyItem) : base(proxyItem)
        {

        }
    }
}
