﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class DeleteObjectsModel
    {
        public clsOntologyItem Config { get; set; }
        public List<clsOntologyItem> ObjectsToDelete { get; set; } = new List<clsOntologyItem>();
        public List<clsOntologyItem> ClassItems { get; set; } = new List<clsOntologyItem>();
    }
}
