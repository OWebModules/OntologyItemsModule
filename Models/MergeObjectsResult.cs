﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class MergeObjectsResult
    {
        public List<clsOntologyItem> MergedObjects { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectAtt> MergedAttributes { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> MergedRelations { get; set; } = new List<clsObjectRel>();
        public List<clsOntologyItem> DeletedObjects { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectAtt> DeletedAttributes { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> DeletedRelations { get; set; } = new List<clsObjectRel>();
    }
}
