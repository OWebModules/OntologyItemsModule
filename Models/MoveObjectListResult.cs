﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class MoveObjectListResult
    {
        public OntologyBaseItemLists MovedItems { get; set; }
    }
}
