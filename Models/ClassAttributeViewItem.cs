﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;

using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.incell,
        height = "100%")]

    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class ClassAttributeViewItem : NotifyPropertyChange
    {

        private clsClassAtt proxyItem;

        [DataViewColumn(IsVisible = false)]
        [KendoColumn(hidden = true)]
        public string IdClass
        {
            get { return proxyItem.ID_Class; }
            set
            {
                proxyItem.ID_Class = value;
                RaisePropertyChanged(nameof(IdClass));
            }
        }

        [KendoColumn(hidden = true)]
        public string NameClass
        {
            get { return proxyItem.Name_Class; }
            set
            {
                proxyItem.Name_Class = value;
                RaisePropertyChanged(nameof(NameClass));
            }
        }

        [DataViewColumn(IsVisible = false)]
        [KendoColumn(hidden = true)]
        public string IdAttributeType
        {
            get { return proxyItem.ID_AttributeType; }
            set
            {
                proxyItem.ID_AttributeType = value;
                RaisePropertyChanged(nameof(IdAttributeType));
            }
        }

        private bool apply;
        [DataViewColumn(IsVisible = false, DisplayOrder = 0, CellType = CellType.Boolean)]
        [KendoColumn(hidden = false,
            Order = 0,
            filterable = true,
            title = "Apply",
            template = "<input type=\"checkbox\" #= Apply ? 'checked=\"checked\"' : \"\" # class=\"chkbxApply\" />",
            width = "80px",
            type = ColType.BooleanType,
            headerTemplate = "<input type='checkbox' class='k-checkbox header-checkbox'><label class='k-checkbox-label' for='header-chb'></label>")]
        [KendoColumnSortable(sortable = false)]
        public bool Apply
        {
            get { return apply; }
            set
            {
                apply = value;
                RaisePropertyChanged(nameof(Apply));
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 0, CellType = CellType.String)]
        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Name")]
        public string NameAttributeType
        {
            get { return proxyItem.Name_AttributeType; }
            set
            {
                proxyItem.Name_AttributeType = value;
                RaisePropertyChanged(nameof(NameAttributeType));
            }
        }

        [KendoColumn(hidden = true)]
        public string IdDataType
        {
            get { return proxyItem.ID_DataType; }
            set
            {
                proxyItem.ID_DataType = value;
                RaisePropertyChanged(nameof(IdDataType));
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 1, CellType = CellType.String)]
        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "DataType")]
        public string NameDataTpye
        {
            get { return proxyItem.Name_DataType; }
            set
            {
                proxyItem.Name_DataType = value;
                RaisePropertyChanged(nameof(NameDataTpye));
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 2, CellType = CellType.Integer)]
        [KendoColumn(hidden = false, Order = 3, filterable = true, title = "Min")]
        [KendoModel(editable = true)]
        public long Min
        {
            get { return proxyItem.Min ?? 0; }
            set
            {
                proxyItem.Min = value;
                RaisePropertyChanged(nameof(Min));
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 3, CellType = CellType.Integer)]
        [KendoColumn(hidden = false,
            Order = 4,
            filterable = false,
            title = "Action",
            width = "100px",
            template = "<button type='button' class='button-min-set-0'>0</button><button type='button' class='button-min-decrease'><i class='fa fa-caret-down' aria-hidden='true'></i></button><button type='button' class='button-min-decrease'><i class='fa fa-caret-up' aria-hidden='true'></i></button>")]
        [KendoModel(editable = false)]
        public int EditMin { get; set; }

        [DataViewColumn(IsVisible = true, DisplayOrder = 5, CellType = CellType.Integer)]
        [KendoColumn(hidden = false, Order = 5, filterable = true, title = "Max")]
        [KendoModel(editable = true)]
        public long Max
        {
            get { return proxyItem.Max ?? 0; }
            set
            {
                proxyItem.Max = value;
                RaisePropertyChanged(nameof(Max));
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 3, CellType = CellType.Integer)]
        [KendoColumn(hidden = false,
            Order = 6,
            filterable = false,
            title = "Action",
            width = "100px",
            template = "<button type='button' class='button-max-set-inifinite'><i class='fa fa-superpowers' aria-hidden='true'></i></button><button type='button' class='button-max-decrease'><i class='fa fa-caret-down' aria-hidden='true'></i></button><button type='button' class='button-max-decrease'><i class='fa fa-caret-up' aria-hidden='true'></i></button>")]
        [KendoModel(editable = false)]
        public int EditMax { get; set; }

        [DataViewColumn(IsVisible = false, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        [KendoColumn(hidden = true)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.ID_Class + proxyItem.ID_AttributeType : null;
            }
        }

        public clsClassAtt GetProxyItem()
        {
            return proxyItem;
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveClassAtt(new List<clsClassAtt> { proxyItem });

            return result;
        }

        public ClassAttributeViewItem(clsClassAtt proxyItem)
        {
            this.proxyItem = proxyItem;
        }
    }
}
