﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Services
{
    public class DeleteCounter
    {
        public int CountToDo { get; set; }
        public int CountDone { get; set; }
        public int CountError { get; set; }
        public int CountRelated { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorCaption { get; set; }
    }
}
