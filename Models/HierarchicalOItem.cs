﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class HierarchicalOItem
    {
        private clsOntologyItem oItem;
        public string IdItem
        {
            get
            {
                return oItem.GUID;
            }
        }
        public string NameItem
        {
            get
            {
                return oItem.Name;
            }
        }
        public string IdClass
        {
            get
            {
                return oItem.GUID_Parent;
            }
        }

        public List<HierarchicalOItem> ChildItems { get; set; } = new List<HierarchicalOItem>();
        public HierarchicalOItem HierarchicalParentItem { get; set; }

        public string GetFullPath(string splitter)
        {
            var fullPath = NameItem;
            if (HierarchicalParentItem != null)
            {
                var fullPathParent = HierarchicalParentItem.GetFullPath(splitter);
                fullPath = $"{fullPathParent}{splitter}{fullPath}";
                return fullPath;
            }
            else
            {
                return NameItem;
            }
        }

        public HierarchicalOItem GetRootItem()
        {
            if (HierarchicalParentItem == null)
            {
                return this;
            }
            else
            {
                return HierarchicalParentItem.GetRootItem();
            }
        }

        public HierarchicalOItem Clone()
        {
            var result = new HierarchicalOItem(oItem)
            {
                HierarchicalParentItem = this.HierarchicalParentItem
            };
            result.ChildItems.AddRange(this.ChildItems);

            return result;
        }

        public HierarchicalOItem(clsOntologyItem item)
        {
            oItem = item;
        }
    }
}
