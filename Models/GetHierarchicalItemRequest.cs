﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class GetHierarchicalItemRequest
    {
        public string IdClass { get; private set; }
        public string IdRelationType { get; private set; }
        public string FullName { get; private set; }
        public string FullNameSplitter { get; private set; }

        public List<string> NameSpaceIdsToExclude { get; set; } = new List<string>();

        public IMessageOutput MessageOutput { get; set; }

        public GetHierarchicalItemRequest(string idClass, string idRelationType, string fullName, string fullNameSplitter)
        {
            IdClass = idClass;
            IdRelationType = idRelationType;
            FullName = fullName;
            FullNameSplitter = fullNameSplitter;
        }
    }
}
