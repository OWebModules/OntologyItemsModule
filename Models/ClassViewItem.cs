﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;

using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class ClassViewItem : NotifyPropertyChange
    {
        private clsOntologyItem proxyItem;

        [DataViewColumn(IsVisible = false)]
        public string IdClass
        {
            get { return proxyItem.GUID; }
            set
            {
                proxyItem.GUID = value;
                RaisePropertyChanged(nameof(IdClass));
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 0, CellType = CellType.String)]
        public string NameClass
        {
            get { return proxyItem.Name; }
            set
            {
                proxyItem.Name = value;
                RaisePropertyChanged(nameof(NameClass));
            }
        }

        public string IdParentClass
        {
            get { return proxyItem.GUID_Parent; }
            set
            {
                proxyItem.GUID_Parent = value;
                RaisePropertyChanged(nameof(IdParentClass));
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 1, CellType = CellType.String)]
        public string NameParentClass
        {
            get { return proxyItem.Name_Parent; }
            set
            {
                proxyItem.Name_Parent = value;
                RaisePropertyChanged(nameof(NameParentClass));
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveClass(new List<clsOntologyItem> { proxyItem });

            return result;
        }

        [DataViewColumn(IsVisible = false, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.GUID : null;
            }
        }

        public ClassViewItem(clsOntologyItem proxyItem, clsOntologyItem parentClass)
        {
            this.proxyItem = proxyItem;
            this.proxyItem.Name_Parent = parentClass.Name;
        }
    }
}
