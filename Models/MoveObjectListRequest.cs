﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class MoveObjectListRequest
    {
        public string IdConfig { get; set; }

        public List<string> ObjectIds { get; set; } = new List<string>();

        public string IdDestClass { get; set; }

        public IMessageOutput MessageOutput { get; set; }

        public MoveObjectListRequest()
        {
        }
    }
}
