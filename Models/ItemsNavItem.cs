﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class ItemsNavItem
    {
        
        public int ItemIndex { get; set; }
        public int ItemCount
        {
            get
            {
                return ItemList != null ? ItemList.Count : 0;
            }
        }
        public List<NavItem> ItemList { get; set; }

        public NavItem CurrentNavItem
        {
            get
            {
                if (ItemList == null || !ItemList.Any()) return null;

                return ItemList[ItemIndex];
            }
        }

        public string CurrentItemName
        {
            get
            {
                if (ItemList == null || !ItemList.Any()) return null;
                
                var item = ItemList[ItemIndex];

                return item.Name;
            }
        }

        public int CurrentPos
        {
            get
            {
                if (ItemCount == 0) return 0;

                return ItemIndex + 1;
            }
            
        }

        public string NavString
        {
            get
            {
                return $"{CurrentPos.ToString()}/{ItemCount}";
            }
        }

        public bool NextPossible
        {
            get
            {
                return ItemIndex < ItemCount - 1;
            }
        }

        public bool PreviousPossible
        {
            get
            {
                return ItemIndex > 0;
            }
        }

        public bool MoveFirst()
        {
            if (!PreviousPossible) return false;

            ItemIndex = 0;

            return true;
        }

        public bool MoveNext()
        {
            
            if (!NextPossible) return false;
                
            ItemIndex++;

            return true;
            
        }

        public bool MoveLast()
        {

            if (!NextPossible) return false;

            ItemIndex = ItemCount-1;

            return true;

        }

        public bool MovePrevious()
        {
            
            if (!PreviousPossible) return false;

            ItemIndex--;

            return true;
            
        }

        public bool MovePosition(int position)
        {
            if (position >= ItemCount - 1) return false;
            if (position < 0) return false;
            if (ItemCount == 0) return false;

            ItemIndex = position;
            return true;
        }
    }

    public class NavItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
