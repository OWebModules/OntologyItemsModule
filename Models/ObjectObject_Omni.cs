﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    [KendoGridConfig(
       groupbable = true,
       autoBind = false,
       scrollable = true,
       resizable = true,
       selectable = SelectableType.row,
       editable = EditableType.False,
       height = "100%")]

    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class ObjectObject_Omni : ObjectRelationViewItem
    {

        private bool apply;
        [DataViewColumn(IsVisible = false, DisplayOrder = 0, CellType = CellType.Boolean)]
        [KendoColumn(hidden = false,
            Order = 0,
            filterable = true,
            title = "Apply",
            template = "<input type=\"checkbox\" #= Apply ? 'checked=\"checked\"' : \"\" # class=\"chkbxApply\" />",
            width = "80px",
            type = ColType.BooleanType,
            headerTemplate = "<input type='checkbox' class='k-checkbox header-checkbox'><label class='k-checkbox-label' for='header-chb'></label>")]
        [KendoColumnSortable(sortable = false)]
        public bool Apply
        {
            get { return apply; }
            set
            {
                apply = value;
                RaisePropertyChanged(nameof(Apply));
            }
        }

        [KendoColumn(hidden = false, Order = 0, filterable = true, title = "Name (Object)")]
        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 0, CellType = CellType.String)]
        public string NameObject
        {
            get { return proxyItem.Name_Object; }
            set
            {
                proxyItem.Name_Object = value;
                RaisePropertyChanged(nameof(NameObject));
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 1, CellType = CellType.Integer)]
        [KendoColumn(hidden = false,
            Order = 1,
            filterable = false,
            title = "Action",
            width = "80px",
            template = "<button type='button' class='button-object-edit-left-omni'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button><button type='button' class='button-module-starter-left-omni'><i class='fa fa-external-link-square' aria-hidden='true'></i></button>")]
        [KendoModel(editable = false)]
        public int EditLeft { get; set; }

        [KendoColumn(hidden = false, Order = 4, filterable = true, title = "Name (Other)")]
        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 4, CellType = CellType.String)]
        public string NameOther
        {
            get { return proxyItem.Name_Other; }
            set
            {
                proxyItem.Name_Other = value;
                RaisePropertyChanged(nameof(NameOther));
            }
        }

        [DataViewColumn(IsVisible = true, DisplayOrder = 5, CellType = CellType.Integer)]
        [KendoColumn(hidden = false,
            Order = 5,
            filterable = false,
            title = "Action",
            width = "80px",
            template = "<button type='button' class='button-object-edit-right-omni'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button><button type='button' class='button-module-starter-right-omni'><i class='fa fa-external-link-square' aria-hidden='true'></i></button>")]
        [KendoModel(editable = false)]
        public int EditRight { get; set; }

        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "Relationtype")]
        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 2, CellType = CellType.String)]
        public string NameRelationType
        {
            get { return proxyItem.Name_RelationType; }
            set
            {
                proxyItem.Name_RelationType = value;
                RaisePropertyChanged(nameof(NameRelationType));
            }
        }

        [KendoColumn(hidden = false, Order = 6, filterable = true, title = "Parent (Other)")]
        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 6, CellType = CellType.String)]
        public string NameParentOther
        {
            get { return proxyItem.Name_Parent_Other; }
            set
            {
                proxyItem.Name_Parent_Other = value;
                RaisePropertyChanged(nameof(NameParentOther));
            }
        }

        [KendoColumn(hidden = false, Order = 3, filterable = true, title = "Parent (Object)")]
        [DataViewColumn(IsVisible = true,
                    DisplayOrder = 3, CellType = CellType.String)]
        public string NameParentObject
        {
            get { return proxyItem.Name_Parent_Object; }
            set
            {
                proxyItem.Name_Parent_Object = value;
                RaisePropertyChanged(nameof(NameParentObject));
            }
        }

        [KendoColumn(hidden = false, Order = 7, filterable = true, title = "Order", type = ColType.NumberType)]
        [DataViewColumn(IsVisible = true,
                   DisplayOrder = 7, CellType = CellType.Integer)]
        [KendoModel(editable = true)]
        public long OrderId
        {
            get { return proxyItem.OrderID ?? 0; }
            set
            {
                proxyItem.OrderID = value;
                RaisePropertyChanged(nameof(OrderId));
            }
        }

        [KendoColumn(hidden = false, Order = 8, filterable = true, title = "Type")]
        [DataViewColumn(IsVisible = true,
                   DisplayOrder = 8, CellType = CellType.String)]
        public string Ontology
        {
            get { return proxyItem.Ontology; }
            set
            {
                proxyItem.Ontology = value;
                RaisePropertyChanged(nameof(Ontology));
            }
        }

        [KendoColumn(hidden = true)]
        [DataViewColumn(IsVisible = false, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.ID_Object + proxyItem.ID_Other + proxyItem.ID_RelationType : null;
            }
        }

        public ObjectObject_Omni(clsObjectRel proxyItem) : base(proxyItem)
        {

        }
    }
}
