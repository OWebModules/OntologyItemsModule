﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class CopyRelationsModel
    {
        public clsOntologyItem Config { get; set; }
        public clsOntologyItem SrcObject { get; set; }
        public clsOntologyItem DstObject { get; set; }
        public clsOntologyItem Ontology { get; set; }
    }
}
