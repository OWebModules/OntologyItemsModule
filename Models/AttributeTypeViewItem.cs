﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;

using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.False,
        height = "100%")]

    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class AttributeTypeViewItem : NotifyPropertyChange
    {
        private clsOntologyItem proxyItem;

        private bool apply;
        [DataViewColumn(IsVisible = false, DisplayOrder = 0, CellType = CellType.Boolean)]
        [KendoColumn(hidden = false, Order = 0, filterable = true, title = "Apply", template = "<input type=\"checkbox\" #= Apply ? 'checked=\"checked\"' : \"\" # class=\"chkbxApply\" />", width = "80px", type = ColType.BooleanType)]
        public bool Apply
        {
            get { return apply; }
            set
            {
                apply = value;
                RaisePropertyChanged(nameof(Apply));
            }
        }

        [KendoColumn(hidden = true)]
        [DataViewColumn(IsVisible = false)]
        public string IdAttributeType
        {
            get { return proxyItem.GUID; }
            set
            {
                proxyItem.GUID = value;
                RaisePropertyChanged(nameof(IdAttributeType));
            }
        }

        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Name")]
        [DataViewColumn(IsVisible =true, DisplayOrder = 0, CellType = CellType.String)]
        public string NameAttributeType
        {
            get { return proxyItem.Name; }
            set
            {
                proxyItem.Name = value;
                RaisePropertyChanged(nameof(NameAttributeType));
            }
        }

        [KendoColumn(hidden = true)]
        [DataViewColumn(IsVisible = false)]
        public string IdDataType
        {
            get { return proxyItem.GUID_Parent; }
            set
            {
                proxyItem.GUID_Parent = value;
                RaisePropertyChanged(nameof(IdDataType));
            }
        }

        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "Datatype", width = "300px")]
        [DataViewColumn(IsVisible = true, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string NameDataType
        {
            get { return proxyItem.Name_Parent; }
            set
            {
                proxyItem.Name_Parent = value;
                RaisePropertyChanged(nameof(NameDataType));
            }
        }

        [KendoColumn(hidden = true)]
        [DataViewColumn(IsVisible = false, DisplayOrder = 1, CellType = CellType.String, Width = 250)]
        public string IdItem
        {
            get
            {
                return proxyItem != null ? proxyItem.GUID : null;
            }
        }

        public clsOntologyItem SaveItem(Globals globals)
        {
            var dbConnectorSave = new OntologyModDBConnector(globals);

            var result = dbConnectorSave.SaveAttributeTypes(new List<clsOntologyItem> { proxyItem });

            return result;
        }

        
        public AttributeTypeViewItem(clsOntologyItem proxyItem, clsOntologyItem dataTypeItem)
        {
            this.proxyItem = proxyItem;
            this.proxyItem.Name_Parent = dataTypeItem.Name;
        }
    }
}
