﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Services
{
    public class GetObjectsByClassModel
    {
        public clsOntologyItem Config { get; set; }
        public clsObjectAtt ExcludeRegex { get; set; }
        public clsObjectAtt IncludeRegex { get; set; }

        public List<clsObjectRel> BelongingClasses { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> BelongingObjects { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> BelongingNamingRules { get; set; } = new List<clsObjectRel>();
    }
}
