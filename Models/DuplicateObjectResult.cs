﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class DuplicateObjectResult
    {
        public clsOntologyItem OldObject { get; set; }
        public clsOntologyItem NewObject { get; set; }
        public List<clsObjectAtt> NewAttributes { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> NewRelations { get; set; } = new List<clsObjectRel>();
    }
}
