﻿using OntologyClasses.BaseClasses;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.False,
        height = "100%")]

    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class ClipboardItem
    {
        [KendoColumn(hidden = true)]
        public string Id { get; set; }

        [KendoColumn(hidden = false, 
            Order = 0, 
            filterable = true, 
            title = "Apply", 
            template = "<input type=\"checkbox\" #= Apply ? 'checked=\"checked\"' : \"\" # class=\"chkbxApply\" />", 
            width = "80px", 
            type =ColType.BooleanType,
            headerTemplate = "<input type='checkbox' class='k-checkbox header-checkbox'><label class='k-checkbox-label' for='header-chb'></label>")]
        [KendoColumnSortable(sortable = false)]
        public string Apply { get; set; }

        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Name")]
        public string Name { get; set; }

        [KendoColumn(hidden = true)]
        public string IdParent { get; set; }

        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "Parent")]
        public string NameParent { get; set; }

        [KendoColumn(hidden = false, Order = 3, filterable = true, title = "Type")]
        public string Type { get; set; }

        [KendoColumn(hidden = false, Order = 4, filterable = true, title = "Order", type =ColType.NumberType)]
        public long OrderId { get; set; }

        public ClipboardItem(clsObjectRel relItem)
        {
            Id = relItem.ID_Other;
            Name = relItem.Name_Other;
            IdParent = relItem.ID_Parent_Other;
            NameParent = relItem.Name_Parent_Other;
            OrderId = relItem.OrderID.Value;
            Type = relItem.Ontology;
        }
    }
}
