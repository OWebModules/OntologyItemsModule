﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class CheckHierarchicalItemRequest : GetHierarchicalItemRequest
    {
        public CheckHierarchicalItemRequest(string idClass, string idRelationType, string fullName, string fullNameSplitter) : base(idClass, idRelationType, fullName, fullNameSplitter)
        {

        }
    }
}
