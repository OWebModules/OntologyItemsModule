﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Services;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule
{
    public class RelationTreeController : AppController
    {
        private ElasticAgentRelationTree elasticAgent;
        private clsRelationConfig relationConfig;

        public async Task<ResultRelationTree> GetRelationTreeFullQualifiedClass(string idClass, bool leftRight)
        {

            var result = new ResultRelationTree
            {
                Result = Globals.LState_Success.Clone(),
                NodeList = new List<ObjectRelNode>()
            };

            var resultClassRels = await elasticAgent.GetRelationTreeRelationFullQualifiedClass(idClass, leftRight);

            if (resultClassRels.Result.GUID == Globals.LState_Error.GUID)
            {
                result.Result = resultClassRels.Result;
                return result;
            }

            var clsRels = resultClassRels.ClassRels.OrderBy(clsRel => leftRight ? clsRel.ClassRel.Name_Class_Right : clsRel.ClassRel.Name_Class_Left);

            foreach (var clsRel in clsRels)
            {
                var treeNode = new ObjectRelNode(leftRight ? NodeType.NodeForwardFormal : NodeType.NodeBackwardFormal)
                {
                    IdNode = Globals.NewGUID,
                    IdLeft = leftRight ? idClass : clsRel.ClassRel.ID_Class_Left,
                    NameLeft = clsRel.ClassRel.Name_Class_Left,
                    IdRight = leftRight ? clsRel.ClassRel.ID_Class_Right : idClass,
                    NameRight = clsRel.ClassRel.Name_Class_Right,
                    IdRelationType = clsRel.ClassRel.ID_RelationType,
                    NameRelationType = clsRel.ClassRel.Name_RelationType,
                    Min = clsRel.ClassRel.Min_Forw ?? 0,
                    MaxForw = clsRel.ClassRel.Max_Forw ?? 0,
                    MaxBackw = clsRel.ClassRel.Max_Backw ?? 0,
                    Count = clsRel.CountItems
                };

                treeNode.SetClass1Auto();

                result.NodeList.Add(treeNode);


            }

            return result;
        }

        public async Task<ResultRelationTree> GetRelationTreeFullQualified(string idObject, bool leftRight)
        {
            
            var result = new ResultRelationTree
            {
                Result = Globals.LState_Success.Clone(),
                NodeList = new List<ObjectRelNode>()
            };

            var resultClassRels = await elasticAgent.GetRelationTreeRelationFullQualified(idObject, leftRight);

            if (resultClassRels.Result.GUID == Globals.LState_Error.GUID)
            {
                result.Result = resultClassRels.Result;
                return result;
            }

            var clsRels = resultClassRels.ClassRels.OrderBy(clsRel => leftRight ? clsRel.ClassRel.Name_Class_Right : clsRel.ClassRel.Name_Class_Left);

            foreach (var clsRel in clsRels)
            {
                var treeNode = new ObjectRelNode(leftRight ? NodeType.NodeForwardFormal : NodeType.NodeBackwardFormal)
                {
                    IdNode = Globals.NewGUID,
                    IdLeft =  leftRight ? idObject : clsRel.ClassRel.ID_Class_Left,
                    NameLeft = clsRel.ClassRel.Name_Class_Left,
                    IdRight = leftRight ? clsRel.ClassRel.ID_Class_Right : idObject,
                    NameRight = clsRel.ClassRel.Name_Class_Right,
                    IdRelationType = clsRel.ClassRel.ID_RelationType,
                    NameRelationType = clsRel.ClassRel.Name_RelationType,
                    Min = clsRel.ClassRel.Min_Forw ?? 0,
                    MaxForw = clsRel.ClassRel.Max_Forw ?? 0,
                    MaxBackw = clsRel.ClassRel.Max_Backw ?? 0,
                    Count = clsRel.CountItems
                };

                treeNode.SetClass1Auto();

                result.NodeList.Add(treeNode);

                
            }

            return result;
        }

        public async Task<ResultRelateItems> RelateItems(ObjectRelNode relNode, clsOntologyItem[] items, string itemType, bool setOrderId, long orderId)
        {
            var result = new ResultRelateItems
            {
                Result = Globals.LState_Success.Clone()
            };
            if (!items.Any())
            {
                result.Result = Globals.LState_Nothing.Clone();
                return result;
            }
            var itemList = new List<clsOntologyItem>();
            if (string.IsNullOrEmpty(itemType))
            {
                var oItemTask = await elasticAgent.GetOItems(items.ToList());
                result.Result = oItemTask.ResultState;
                if (result.Result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
                itemList = oItemTask.Result;
            }
            else if (itemType == Globals.Type_Object)
            {
                var oItemsTask = await elasticAgent.GetObjects(items);
                itemList = oItemsTask.OItems;
            }
            else if (itemType == Globals.Type_AttributeType)
            {
                var oItemsTask = await elasticAgent.GetAttributeTypes(items);
                itemList = oItemsTask.OItems;
            }
            else if (itemType == Globals.Type_RelationType)
            {
                var oItemsTask = await elasticAgent.GetAttributeTypes(items);
                itemList = oItemsTask.OItems;
            }
            else
            {
                var oItemsTask = await elasticAgent.GetClasses(items);
                itemList = oItemsTask.OItems;
            }

            if (relNode.NodeType == NodeType.NodeForwardFormal)
            {
                var oItemTask = await elasticAgent.GetOItem(relNode.IdLeft, Globals.Type_Object);

                var oItem = oItemTask.OItem;
                var relationItemTask = await elasticAgent.GetOItem(relNode.IdRelationType, Globals.Type_RelationType);

                var relationItem = relationItemTask.OItem;

                var toRelate = itemList.Where(obj => obj.GUID_Parent == relNode.IdRight);
                if (!toRelate.Any())
                {
                    result.Result = Globals.LState_Nothing.Clone();
                    result.RelationNode = relNode;
                    return result;
                }
                var checkExisting = toRelate.Select(obj => new clsObjectRel
                {
                    ID_Object = relNode.IdLeft,
                    ID_Other = obj.GUID,
                    ID_RelationType = relNode.IdRelationType
                }).ToList();

                var resultTask = await elasticAgent.GetExistingRelations(checkExisting);

                if (resultTask.Result.GUID == Globals.LState_Error.GUID)
                {
                    result.Result = resultTask.Result;
                    return result;
                }

                var toRelateRel = (from relateItem in toRelate
                                   join exists in resultTask.Related on relateItem.GUID equals exists.ID_Other into existItems
                                   from exists in existItems.DefaultIfEmpty()
                                   where exists == null
                                   select relationConfig.Rel_ObjectRelation(oItem,
                                    relateItem,
                                    relationItem, orderId: (setOrderId ? orderId++ : 1))).ToList();

                var resultRelateTask = await elasticAgent.RelateItems(toRelateRel);

                var count = resultRelateTask.Count;

                relNode.Count += count;

                result.RelationNode = relNode;
            }
            else if (relNode.NodeType == NodeType.NodeForwardInformal)
            {
                var oItemTask = await elasticAgent.GetOItem(relNode.IdLeft, Globals.Type_Object);

                var oItem = oItemTask.OItem;
                var relationItemTask = await elasticAgent.GetOItem(relNode.IdRelationType, Globals.Type_RelationType);

                var relationItem = relationItemTask.OItem;

                var checkExisting = items.Select(obj => new clsObjectRel
                {
                    ID_Object = relNode.IdLeft,
                    ID_Other = obj.GUID,
                    ID_RelationType = relNode.IdRelationType
                }).ToList();

                var resultTask = await elasticAgent.GetExistingRelations(checkExisting);

                if (resultTask.Result.GUID == Globals.LState_Error.GUID)
                {
                    result.Result = resultTask.Result;
                    return result;
                }

                var toRelateRel = (from relateItem in itemList
                                   join exists in resultTask.Related on relateItem.GUID equals exists.ID_Other into existItems
                                   from exists in existItems.DefaultIfEmpty()
                                   where exists == null
                                   select relationConfig.Rel_ObjectRelation(oItem,
                                    relateItem,
                                    relationItem)).ToList();
                if (!toRelateRel.Any())
                {
                    result.Result = Globals.LState_Nothing.Clone();
                    result.RelationNode = relNode;
                    return result;
                }
                var resultRelateTask = await elasticAgent.RelateItems(toRelateRel);

                var count = resultRelateTask.Count;

                relNode.Count += count;

                result.RelationNode = relNode;
            }
            else if (relNode.NodeType == NodeType.NodeBackwardFormal)
            {
                var oItemTask = await elasticAgent.GetOItem(relNode.IdRight, Globals.Type_Object);

                var oItem = oItemTask.OItem;
                var relationItemTask = await elasticAgent.GetOItem(relNode.IdRelationType, Globals.Type_RelationType);

                var relationItem = relationItemTask.OItem;

                var toRelate = itemList.Where(obj => obj.GUID_Parent == relNode.IdLeft);
                if (!toRelate.Any())
                {
                    result.Result = Globals.LState_Nothing.Clone();
                    result.RelationNode = relNode;
                    return result;
                }
                var checkExisting = toRelate.Select(obj => new clsObjectRel
                {
                    ID_Other = relNode.IdRight,
                    ID_Object = obj.GUID,
                    ID_RelationType = relNode.IdRelationType
                }).ToList();

                var resultTask = await elasticAgent.GetExistingRelations(checkExisting);

                if (resultTask.Result.GUID == Globals.LState_Error.GUID)
                {
                    result.Result = resultTask.Result;
                    return result;
                }

                var toRelateRel = (from relateItem in toRelate
                                   join exists in resultTask.Related on relateItem.GUID equals exists.ID_Object into existItems
                                   from exists in existItems.DefaultIfEmpty()
                                   where exists == null
                                   select relationConfig.Rel_ObjectRelation(relateItem,
                                    oItem,
                                    relationItem)).ToList();

                var resultRelateTask = await elasticAgent.RelateItems(toRelateRel);

                var count = resultRelateTask.Count;

                relNode.Count += count;

                result.RelationNode = relNode;
            }
            

            return result;
        }

        public async Task<ResultRelationTree> GetRelationTreeOther(string idObject, bool leftRight)
        {

            var result = new ResultRelationTree
            {
                Result = Globals.LState_Success.Clone(),
                NodeList = new List<ObjectRelNode>()
            };

            var objectItemTask = await elasticAgent.GetOItem(idObject, Globals.Type_Object);

            if (leftRight)
            {
                var resultRelatedTask = await elasticAgent.GetRelationTreeRelationOtherLeft(idObject);
                result.NodeList.Add(new ObjectRelNode(NodeType.NodeBackwardInformal)
                {
                    IdNode = $"{Globals.Type_Other}_backw",
                    NameLeft = $"{Globals.Type_Other} (Backward)",
                    SubNodes = new List<ObjectRelNode>(),
                    Class1 = "fa fa-caret-left",
                    Bold = true,
                    IsParent = true
                });
                foreach (var clsRel in resultRelatedTask.ClassRels)
                {
                    var treeNode = new ObjectRelNode(NodeType.NodeForwardInformal)
                    {
                        IdNode = Globals.NewGUID,
                        IdLeft = idObject,
                        NameLeft = objectItemTask.OItem.Name,
                        IdRelationType = clsRel.ClassRel.ID_RelationType,
                        NameRelationType = clsRel.ClassRel.Name_RelationType,
                        Ontology = clsRel.ClassRel.Ontology,
                        Min = clsRel.ClassRel.Min_Forw ?? 0,
                        MaxForw = clsRel.ClassRel.Max_Forw ?? 0,
                        MaxBackw = clsRel.ClassRel.Max_Backw ?? 0,
                        Count = clsRel.CountItems
                    };

                    treeNode.SetClass1Auto();


                    result.NodeList.Add(treeNode);
                }
                
                

                
            }
            else
            {
                var resultRelatedTask = await elasticAgent.GetRelationTreeRelationOtherRight(idObject);

                foreach (var clsRel in resultRelatedTask.ClassRels)
                {
                    var treeNode = new ObjectRelNode(NodeType.NodeBackwardInformal)
                    {
                        IdNode = Globals.NewGUID,
                        IdLeft = clsRel.ClassRel.ID_Class_Left,
                        NameLeft = clsRel.ClassRel.Name_Class_Left,
                        IdRight = idObject,
                        NameRight = objectItemTask.OItem.Name,
                        IdRelationType = clsRel.ClassRel.ID_RelationType,
                        NameRelationType = clsRel.ClassRel.Name_RelationType,
                        Count = clsRel.CountItems,
                        Ontology = clsRel.ClassRel.Ontology
                    };

                    treeNode.SetClass1Auto();

                    result.NodeList.Add(treeNode);
                }

            }

            return result;
        }

        public async Task<ResultRelationTree> GetRelationTreeAttribute(string idObject)
        {

            var result = new ResultRelationTree
            {
                Result = Globals.LState_Success.Clone(),
                NodeList = new List<ObjectRelNode>()
            };

            var resultClassAtts = await elasticAgent.GetRelationTreeAttribute(idObject);

            if (resultClassAtts.Result.GUID == Globals.LState_Error.GUID)
            {
                result.Result = resultClassAtts.Result;
                return result;
            }

            var clsAtts = resultClassAtts.ClassAtts.OrderBy(clsAtt => clsAtt.ClassAtt.Name_AttributeType);

            foreach (var clsAtt in clsAtts)
            {

                var treeNode = new ObjectRelNode(NodeType.AttributeRel)
                { 
                    IdNode = clsAtt.ClassAtt.ID_AttributeType,
                    IdLeft = idObject,
                    IdRight = clsAtt.ClassAtt.ID_AttributeType,
                    NameRight = clsAtt.ClassAtt.Name_AttributeType,
                    Count = clsAtt.CountItems,
                    Min = clsAtt.ClassAtt.Min ?? 0,
                    MaxForw = clsAtt.ClassAtt.Max ?? 0
                };

                treeNode.SetClass1Auto();

                result.NodeList.Add(treeNode);
            }


            return result;
        }

        public async Task<ResultRelationTree> GetRelationTreeAttributeClass(string idClass)
        {

            var result = new ResultRelationTree
            {
                Result = Globals.LState_Success.Clone(),
                NodeList = new List<ObjectRelNode>()
            };

            var resultClassAtts = await elasticAgent.GetRelationTreeAttributeClass(idClass);

            if (resultClassAtts.Result.GUID == Globals.LState_Error.GUID)
            {
                result.Result = resultClassAtts.Result;
                return result;
            }

            var clsAtts = resultClassAtts.ClassAtts.OrderBy(clsAtt => clsAtt.ClassAtt.Name_AttributeType);

            foreach (var clsAtt in clsAtts)
            {

                var treeNode = new ObjectRelNode(NodeType.AttributeRel)
                {
                    IdNode = clsAtt.ClassAtt.ID_AttributeType,
                    IdLeft = idClass,
                    IdRight = clsAtt.ClassAtt.ID_AttributeType,
                    NameRight = clsAtt.ClassAtt.Name_AttributeType,
                    Count = clsAtt.CountItems,
                    Min = clsAtt.ClassAtt.Min ?? 0,
                    MaxForw = clsAtt.ClassAtt.Max ?? 0
                };

                treeNode.SetClass1Auto();

                result.NodeList.Add(treeNode);
            }


            return result;
        }

        public async Task<ResultItem<string>> GetUrlEditAttribute(string urlEditBool, string urlEditDateTime, string urlEditDouble, string urlEditLong, string urlEditString, string idObject, string idAttributeType)
        {

            var result = new ResultItem<string>
            {
                ResultState = Globals.LState_Success.Clone()
            };

            var resultOItem = await elasticAgent.GetOItem(idAttributeType, Globals.Type_AttributeType);

            if (resultOItem.Result.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultOItem.Result;
                return result;
            }

            if (resultOItem.OItem.GUID_Parent == Globals.DType_Bool.GUID)
            {
                result.Result = $"{urlEditBool}?Object={idObject}&AttributeType={idAttributeType}"; 
            }
            else if (resultOItem.OItem.GUID_Parent == Globals.DType_DateTime.GUID)
            {
                result.Result = $"{urlEditDateTime}?Object={idObject}&AttributeType={idAttributeType}";
            }
            else if (resultOItem.OItem.GUID_Parent == Globals.DType_Int.GUID)
            {
                result.Result = $"{urlEditLong}?Object={idObject}&AttributeType={idAttributeType}";
            }
            else if (resultOItem.OItem.GUID_Parent == Globals.DType_Real.GUID)
            {
                result.Result = $"{urlEditDouble}?Object={idObject}&AttributeType={idAttributeType}";
            }
            else if (resultOItem.OItem.GUID_Parent == Globals.DType_String.GUID)
            {
                result.Result = $"{urlEditString}?Object={idObject}&AttributeType={idAttributeType}";
            }

            return result;

        }

        public async Task<ResultRelationTree> GetRelationTreeNodeOtherRightLeft()
        {
            var taskResult = await Task.Run<ResultRelationTree>(() =>
            {
                var result = new ResultRelationTree
                {
                    Result = Globals.LState_Success.Clone(),
                    NodeList = new List<ObjectRelNode>()
                };
                result.NodeList.Add(new ObjectRelNode(NodeType.NodeBackwardInformal)
                {
                    IdNode = $"{Globals.Type_Other}_backw",
                    NameLeft = $"{Globals.Type_Other} (Backward)",
                    SubNodes = new List<ObjectRelNode>(),
                    Class1 = "fa fa-arrow-left",
                    Bold = true,
                    IsParent = true
                });

                return result;
            });
            return taskResult;
        }

        public async Task<ResultRelationTree> GetRelationTreeInit()
        {
            var taskResult = await Task.Run<ResultRelationTree>(() =>
            {
                var result = new ResultRelationTree
                {
                    Result = Globals.LState_Success.Clone(),
                    NodeList = new List<ObjectRelNode>()
                };

                result.NodeList.Add(new ObjectRelNode(NodeType.AttributeRel)
                {
                    IdNode = Globals.Type_AttributeType,
                    NameLeft = Globals.Type_AttributeType,
                    SubNodes = new List<ObjectRelNode>(),
                    Class1 = "fa fa-share-alt",
                    Bold = true,
                    IsParent = true
                });

                result.NodeList.Add(new ObjectRelNode(NodeType.NodeForwardFormal)
                {
                    IdNode = Globals.Direction_LeftRight.GUID,
                    NameLeft = Globals.Direction_LeftRight.Name,
                    SubNodes = new List<ObjectRelNode>(),
                    Class1 = "fa fa-arrow-right",
                    Class2 = "fa fa-arrow-right",
                    Bold = true,
                    IsParent = true
                });

                result.NodeList.Add(new ObjectRelNode(NodeType.NodeBackwardFormal)
                {
                    IdNode = Globals.Direction_RightLeft.GUID,
                    NameLeft = Globals.Direction_RightLeft.Name,
                    SubNodes = new List<ObjectRelNode>(),
                    Class1 = "fa fa-arrow-left",
                    Class2 = "fa fa-arrow-left",
                    Bold = true,
                    IsParent = true
                });

                result.NodeList.Add(new ObjectRelNode(NodeType.NodeForwardInformal)
                {
                    IdNode = Globals.Type_Other,
                    NameLeft = Globals.Type_Other,
                    SubNodes = new List<ObjectRelNode>(),
                    Class1 = "fa fa-arrow-right",
                    Bold = true,
                    IsParent = true
                });

                

                return result;
            });

            return taskResult;
            
        }


        public RelationTreeController(Globals globals) : base(globals)
        {   
            Initialize();
        }

        private void Initialize()
        {
            elasticAgent = new ElasticAgentRelationTree(Globals);
            relationConfig = new clsRelationConfig(Globals);
        }
    }

    public class ResultRelationTree
    {
        public clsOntologyItem Result { get; set; }
        public List<ObjectRelNode> NodeList { get; set; } = new List<ObjectRelNode>();
    }

    public class ResultRelateItems
    {
        public clsOntologyItem Result { get; set; }
        public ObjectRelNode RelationNode { get; set; }
    }

}
