﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntologyItemsModule.Services;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule
{
    public class ClipboardController : AppController
    {
        public async Task<ResultItem<List<ClipboardItem>>> GetClipboardItems(clsOntologyItem refItem = null)
        {
            var serviceAgent = new ElasticAgentClipboard(Globals);
            var resultService = await serviceAgent.GetClipboardItemsRaw(refItem);

            var result = new ResultItem<List<ClipboardItem>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<ClipboardItem>()
            };

            if (resultService.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultService.ResultState;
                return result;
            }

            var taskResult = await Task.Run<ResultItem<List<ClipboardItem>>>(() =>
            {
                var resultCreate = new ResultItem<List<ClipboardItem>>
                {
                    ResultState = result.ResultState,
                    Result = resultService.Result.Select(relItem => new ClipboardItem(relItem)).ToList()
                };

                return resultCreate;
            });

            result = taskResult;

            return result;
        }

        public async Task<ResultItem<bool>> ClipboardItemsExist(clsOntologyItem refItem = null)
        {
            var serviceAgent = new ElasticAgentClipboard(Globals);
            var resultService = await serviceAgent.GetClipboardItemsRaw(refItem);

            var result = new ResultItem<bool>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = true
            };

            if (resultService.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultService.ResultState;
                return result;
            }

            var taskResult = await Task.Run<ResultItem<bool>>(() =>
            {
                var resultCreate = new ResultItem<bool>
                {
                    ResultState = result.ResultState,
                    Result = resultService.Result.Count > 0
                };

                return resultCreate;
            });

            result = taskResult;

            return result;
        }

        public async Task<ResultItem<List<ClipboardItem>>> AddClipboardItems(List<clsOntologyItem> refItems)
        {
            var serviceAgent = new ElasticAgentClipboard(Globals);

            var resultService = await serviceAgent.AddClipboardItems(refItems);

            var result = new ResultItem<List<ClipboardItem>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<ClipboardItem>()
            };

            if (resultService.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultService.ResultState;
                return result;
            }

            var taskResult = await Task.Run<ResultItem<List<ClipboardItem>>>(() =>
            {
                var resultCreate = new ResultItem<List<ClipboardItem>>
                {
                    ResultState = result.ResultState,
                    Result = resultService.Result.Select(relItem => new ClipboardItem(relItem)).ToList()
                };

                return resultCreate;
            });

            result = taskResult;

            return result;
        }

        public async Task<ResultItem<List<ClipboardItem>>> RemoveClipboardItems(List<ClipboardItem> clipboardItems)
        {
            var serviceAgent = new ElasticAgentClipboard(Globals);
            var taskResult = await Task.Run<List<clsObjectRel>>(() =>
            {

                var relItems = clipboardItems.Select(clipI => new clsObjectRel
                {
                    ID_Object = Config.LocalData.Object_Baseconfig.GUID,
                    ID_RelationType = GetIdRelationType(clipI.Type)
                }).ToList();

                return relItems;
            });
           

            var resultService = await serviceAgent.RemoveItems(taskResult);

            var result = new ResultItem<List<ClipboardItem>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<ClipboardItem>()
            };

            if (resultService.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultService.ResultState;
                return result;
            }

            result.Result = clipboardItems;

            return result;

        }

        public async Task<ResultOItem<List<clsOntologyItem>>> GetOItem(string idClass)
        {
            var serviceElastic = new ElasticAgentClipboard(Globals);
            var result = await serviceElastic.GetClasses(new List<clsOntologyItem> { new clsOntologyItem { GUID = idClass } });
            return result;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetOItems(List<string> idItems)
        {
            var serviceElastic = new ElasticAgentClipboard(Globals);
            var result = await serviceElastic.GetOItems(idItems);
            return result;
        }

        public async Task<clsOntologyItem> ClearClipbord(clsOntologyItem refItem = null)
        {
            var serviceAgent = new ElasticAgentClipboard(Globals);

            return await serviceAgent.ClearClipboard(refItem);
        }

        private string GetIdRelationType(string type)
        {
            if (type == Globals.Type_Class)
            {
                return Globals.RelationType_belongingClass.GUID;
            }

            if (type == Globals.Type_RelationType)
            {
                return Globals.RelationType_belongingRelationType.GUID;
            }

            if (type == Globals.Type_AttributeType)
            {
                return Globals.RelationType_belongingAttribute.GUID;
            }

            if (type == Globals.Type_Object)
            {
                return Globals.RelationType_belongingObject.GUID;
            }

            return null;
        }

       
        public ClipboardController(Globals globals) : base(globals)
        {
        }
    }
}
