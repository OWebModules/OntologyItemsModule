﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntologyItemsModule.Services;
using OntologyItemsModule.Validations;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule
{
    public class ObjectEditController : AppController
    {

        public async Task<ResultItem<DuplicateObjectResult>> DuplicateObject(DuplicateObjectRequest request)
        {
            var taskResult = await Task.Run<ResultItem<DuplicateObjectResult>>(async() =>
           {
               var result = new ResultItem<DuplicateObjectResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new DuplicateObjectResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               if (string.IsNullOrEmpty(request.IdObject))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The object-id is not valid! It's empty!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (!Globals.is_GUID(request.IdObject))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The object-id is not valid! It's no GUID!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");

               var elasticAgent = new Services.ElasticAgentObjectEdit(Globals);

               request.MessageOutput?.OutputInfo("Get object...");
               var getOItemResult = await elasticAgent.GetOItem(request.IdObject, Globals.Type_Object);

               result.ResultState = getOItemResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have object.");


               request.MessageOutput?.OutputInfo("Create new object...");

               if (string.IsNullOrEmpty(request.NewObjectName))
               {
                   request.NewObjectName = getOItemResult.Result.Name;
               }

               var newObject = new clsOntologyItem
               {
                   GUID = Globals.NewGUID,
                   Name = request.NewObjectName,
                   GUID_Parent = getOItemResult.Result.GUID_Parent,
                   Type = getOItemResult.Result.Type
               };

               var transactionController = new clsTransaction(Globals);

               var resultCreate = transactionController.do_Transaction(newObject);

               result.ResultState = resultCreate;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while saving the new object!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               result.Result.NewObject = newObject;

               request.MessageOutput?.OutputInfo("Created new object.");

               request.MessageOutput?.OutputInfo("Get object-relations left->right...");

               var relResult = await elasticAgent.GetRightRelations(request.IdObject);

               result.ResultState = relResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have left->right relations.");

               request.MessageOutput?.OutputInfo("Create new relation-list...");
               var newRelations = relResult.Result.Select(rel => new clsObjectRel
               {
                   ID_Object = newObject.GUID,
                   ID_Parent_Object = newObject.GUID_Parent,
                   ID_Other = rel.ID_Other,
                   ID_Parent_Other = rel.ID_Parent_Other,
                   ID_RelationType = rel.ID_RelationType,
                   OrderID = rel.OrderID,
                   Ontology = rel.Ontology
               });
               request.MessageOutput?.OutputInfo("Created new relation-list.");

               request.MessageOutput?.OutputInfo("Save relations...");
               foreach (var newRel in newRelations)
               {
                   var transRes = transactionController.do_Transaction(newRel);
                   result.ResultState = transRes;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving a relation!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       transactionController.rollback();
                       return result;
                   }

                   result.Result.NewRelations.Add(transactionController.OItem_Last.OItemObjectRel);
               }

               request.MessageOutput?.OutputInfo("Saved relations.");

               request.MessageOutput?.OutputInfo("Get object-attributes...");

               var objectAttributeResult = await elasticAgent.GetAllObjectAttributes(request.IdObject);

               result.ResultState = objectAttributeResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have object-attributes.");

               var newObjectAttributes = objectAttributeResult.Result.Select(rel => new clsObjectAtt
               {
                   ID_AttributeType = rel.ID_AttributeType,
                   ID_Object = newObject.GUID,
                   ID_DataType = rel.ID_DataType,
                   ID_Class = newObject.GUID_Parent,
                   OrderID = rel.OrderID,
                   Val_Bit = rel.Val_Bit,
                   Val_Date = rel.Val_Date,
                   Val_Double = rel.Val_Double,
                   Val_Int = rel.Val_Int,
                   Val_Name = rel.Val_Name,
                   Val_String = rel.Val_String
               });

               foreach (var objectAttribute in newObjectAttributes)
               {
                   var transRes = transactionController.do_Transaction(objectAttribute);
                   result.ResultState = transRes;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving a relation!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       transactionController.rollback();
                       return result;
                   }

                   result.Result.NewAttributes.Add(transactionController.OItem_Last.OItemObjectAtt);
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<CopyRelationsToDestResult>> CopyRelations(CopyRelationsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<CopyRelationsToDestResult>>(async () =>
            {
                request.MessageOutput?.OutputInfo("Validate request...");
                var result = new ResultItem<CopyRelationsToDestResult>
                {
                    ResultState = Globals.LState_Success.Clone()
                };
                result.ResultState = ValidationController.ValidateCopyRelationsRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated request.");

                var serviceAgent = new ElasticAgentObjectEdit(Globals);

                request.MessageOutput?.OutputInfo("Get model...");
                var modelResult = await serviceAgent.GetCopyRelationsModel(request, Globals);

                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Have model.");

                var ontologyController = new OntologyConnector(Globals);

                request.MessageOutput?.OutputInfo("Get ontology...");
                var getOntologyRequest = new GetOntologyRequest(modelResult.Result.Ontology);

                var getOntologyResult = await ontologyController.GetOntologies(getOntologyRequest);
                result.ResultState = getOntologyResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Ontology!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Have ontology.");


                request.MessageOutput?.OutputInfo("Get relations...");
                var joins = getOntologyResult.Result.SelectMany(ont => ont.OntologyJoins).Where(oJoin => oJoin.OItem1.RefItem.GUID != Globals.Class_Ontologies.GUID && (oJoin.OItem2 == null || oJoin.OItem2.RefItem.GUID != Globals.Class_Ontologies.GUID)).ToList();

                joins = joins.Where(join => join.OItem1.RefItem.GUID == modelResult.Result.SrcObject.GUID_Parent || (join.OItem2 != null && join.OItem2.RefItem.GUID == modelResult.Result.SrcObject.GUID_Parent) || join.OItem2 == null).ToList();

                var joinsLeft = joins.Where(join => join.OItem1.RefItem.GUID == modelResult.Result.SrcObject.GUID_Parent).ToList();
                var joinsAttribute = joinsLeft.Where(join => join.OItem2 != null && join.OItem2.RefItem.Type == Globals.Type_AttributeType).ToList();

                var joinsRight = joins.Where(join => join.OItem2 != null && join.OItem2.RefItem.GUID == modelResult.Result.SrcObject.GUID_Parent).ToList();

                var joinsRest = (from joinItem in joins
                                 join joinLeft in joinsLeft on joinItem.JoinEntity.GUID equals joinLeft.JoinEntity.GUID into joinsLeft1
                                 from joinLeft in joinsLeft1.DefaultIfEmpty()
                                 where joinLeft == null
                                 join joinRight in joinsRight on joinItem.JoinEntity.GUID equals joinRight.JoinEntity.GUID into joinsRight2
                                 from joinRight in joinsRight2.DefaultIfEmpty()
                                 where joinRight == null
                                 select joinItem).ToList();

                var joinsRestLeft = joinsRest.Where(join => join.OItem1 != null && join.OItem2 == null);
                var joinsRestRight = joinsRest.Where(join => join.OItem2 != null && join.OItem1 == null);

                var attRel = joinsAttribute.Select(join => new clsObjectAtt
                {
                    ID_Object = modelResult.Result.SrcObject.GUID,
                    ID_AttributeType = join.OItem2.RefItem.GUID
                }).ToList();

                var joinsRel = joinsLeft.Where(join => join.OItem2 == null || (join.OItem2 != null && join.OItem2.RefItem.Type != Globals.Type_AttributeType)).Select(join => new clsObjectRel
                {
                    ID_Object = modelResult.Result.SrcObject.GUID,
                    ID_RelationType = join.OItem3.RefItem.GUID,
                    ID_Parent_Other = join.OItem2 != null ? join.OItem2.RefItem.GUID : null
                }).ToList();

                joinsRel.AddRange( joinsRight.Select(join => new clsObjectRel
                {
                    ID_Other = modelResult.Result.SrcObject.GUID,
                    ID_RelationType = join.OItem3.RefItem.GUID,
                    ID_Parent_Object = join.OItem1 != null ? join.OItem1.RefItem.GUID : null
                }));


                joinsRel.AddRange(joinsRest.Select(join => new clsObjectRel
                {
                    ID_Object = join.OItem1 == null ? modelResult.Result.SrcObject.GUID : null,
                    ID_Parent_Object = join.OItem1 != null ? join.OItem1.RefItem.GUID : null,
                    ID_RelationType = join.OItem3.RefItem.GUID,
                    ID_Parent_Other = join.OItem2 != null ?  join.OItem2.RefItem.GUID : null,
                    ID_Other = join.OItem2 == null ? modelResult.Result.SrcObject.GUID : null
                }));

                request.MessageOutput?.OutputInfo($"Have {joinsAttribute.Count} attributes, {joinsRel.Count} relations");

                result = await serviceAgent.CopyRelationsToDest(modelResult.Result, attRel, joinsRel);

                return result;
            });

            return taskResult; ;
        }

        public async Task<ResultItem<MoveObjectListResult>> MoveObjectList(MoveObjectListRequest request)
        {
            var taskResult = await Task.Run<ResultItem<MoveObjectListResult>>(async() =>
            {
                var result = new ResultItem<MoveObjectListResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new MoveObjectListResult()
                };

                var elasticAgent = new Services.ElasticAgentObjectEdit(Globals);

                request.MessageOutput?.OutputInfo("Get Model...");
                var modelResult = await elasticAgent.GetMoveObjectListItems(request);

                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Model.");

                var ontologyBaseItemLists = new OntologyBaseItemLists
                {
                    OItems = modelResult.Result.ObjectList.Where(obj => obj.GUID_Parent != modelResult.Result.DestClass.GUID).ToList(),
                    ObjectAttributes = modelResult.Result.ObjectAttributes.Where(objAtt => objAtt.ID_Class != modelResult.Result.DestClass.GUID).ToList(),
                    ObjectRelations = modelResult.Result.ObjectRelationsLeftRight.Where(objRel => objRel.ID_Parent_Object != modelResult.Result.DestClass.GUID).ToList()
                };

                var objRelRightLeft = modelResult.Result.ObjectRelationsRightLeft.Where(objRel => objRel.ID_Parent_Other != modelResult.Result.DestClass.GUID).ToList();

                request.MessageOutput?.OutputInfo("Change Objects...");
                ontologyBaseItemLists.OItems.ForEach(obj => obj.GUID_Parent = modelResult.Result.DestClass.GUID);
                request.MessageOutput?.OutputInfo("Changed Objects.");
                request.MessageOutput?.OutputInfo("Change Attributes...");
                ontologyBaseItemLists.ObjectAttributes.ForEach(objAtt => objAtt.ID_Class = modelResult.Result.DestClass.GUID);
                request.MessageOutput?.OutputInfo("Changed Attributes.");
                request.MessageOutput?.OutputInfo("Change Relations (left-right)...");
                ontologyBaseItemLists.ObjectRelations.ForEach(objRel => objRel.ID_Parent_Object = modelResult.Result.DestClass.GUID);
                request.MessageOutput?.OutputInfo("Change Relations (left-right).");
                request.MessageOutput?.OutputInfo("Change Relations (right-left)...");
                objRelRightLeft.ForEach(objRel => objRel.ID_Parent_Other = modelResult.Result.DestClass.GUID);
                request.MessageOutput?.OutputInfo("Changed Relations (right-left).");

                ontologyBaseItemLists.ObjectRelations.AddRange(objRelRightLeft);

                request.MessageOutput?.OutputInfo("Save items...");
                var saveResult = await elasticAgent.SaveOntologyItems(ontologyBaseItemLists);

                result.ResultState = saveResult;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Saved items.");

                result.Result.MovedItems = ontologyBaseItemLists;

                request.MessageOutput?.OutputInfo($"Saved {ontologyBaseItemLists.GetClasses(Globals).Count} Classes");
                request.MessageOutput?.OutputInfo($"Saved {ontologyBaseItemLists.GetAttributeTypes(Globals).Count} AttributeTypes");
                request.MessageOutput?.OutputInfo($"Saved {ontologyBaseItemLists.GetRelationTypes(Globals).Count} RelationTypes");
                request.MessageOutput?.OutputInfo($"Saved {ontologyBaseItemLists.GetObjects(Globals).Count} Objects");
                request.MessageOutput?.OutputInfo($"Saved {ontologyBaseItemLists.ClassAttributes.Count} ClassAttributes");
                request.MessageOutput?.OutputInfo($"Saved {ontologyBaseItemLists.ClassRelations.Count} ClassRelations");
                request.MessageOutput?.OutputInfo($"Saved {ontologyBaseItemLists.ObjectAttributes.Count} ObjectAttributes");
                request.MessageOutput?.OutputInfo($"Saved {ontologyBaseItemLists.ObjectRelations.Count} ObjectRelations");

                return result;
            });

            return taskResult;
        }

        public ObjectEditController(Globals globals) : base(globals)
        {
        }
    }
}
