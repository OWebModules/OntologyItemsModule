﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Services;
using OntoMsg_Module.Models;
using OntoWebCore.Attributes;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule
{
    public class DropDownController : AppController
    {
        private ElasticAgentOItems serviceAgent;
        public Task<ResultItem<KendoDropDownConfig>> GetDropDownConfigRelationTypes()
        {
            var taskResult = Task.Run<ResultItem<KendoDropDownConfig>>(async () =>
            {
                var resultRelationTypes = await serviceAgent.GetRelationTypes();

                var result = new ResultItem<KendoDropDownConfig>
                {
                    ResultState = resultRelationTypes.ResultState,
                    Result = new KendoDropDownConfig
                    {
                        dataSource = resultRelationTypes.Result.OrderBy(lookItm => lookItm.Name).Select(attType => new KendoDropDownItem
                        {
                            Value = attType.GUID,
                            Text = attType.Name
                        }).ToList(),
                        optionLabel = "Relation-Type"
                    }
                };

                return result;
            });

            return taskResult;
        }

        public Task<ResultItem<KendoDropDownConfig>> GetDropCownConfigOItems(List<clsOntologyItem> oItems, string optionLabel, bool useAdditional)
        {
            var taskResult = Task.Run<ResultItem<KendoDropDownConfig>>(() =>
            {
                
                var result = new ResultItem<KendoDropDownConfig>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new KendoDropDownConfig
                    {
                        dataSource = oItems.OrderBy(lookItm => lookItm.Name).Select(attType => new KendoDropDownItem
                        {
                            Value = attType.GUID,
                            Text = useAdditional ? attType.Additional1 : attType.Name,
                            Additional = attType.Name
                        }).ToList(),
                        optionLabel = optionLabel
                    }
                };

                return result;
            });

            return taskResult;
        }

        public Task<ResultItem<KendoDropDownConfig>> GetDropCownConfigObjects(string idClass)
        {
            var taskResult = Task.Run<ResultItem<KendoDropDownConfig>>(async() =>
            {
                var elasticAgent = new ElasticAgentOItems(Globals);

                var result = new ResultItem<KendoDropDownConfig>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                var resultAgentClass = await serviceAgent.GetOItem(idClass, Globals.Type_Class);
                result.ResultState = resultAgentClass.ResultState;
                if (result.ResultState.GUID  == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var resultAgent = await serviceAgent.GetObjects(idClass);

                result.ResultState = resultAgent.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = new KendoDropDownConfig
                {
                    dataSource = resultAgent.Result.OrderBy(lookItm => lookItm.Name).Select(objectItem => new KendoDropDownItem
                    {
                        Value = objectItem.GUID,
                        Text = objectItem.Name,
                        Additional = objectItem.Name
                    }).ToList(),
                    optionLabel = resultAgentClass.Result.Name
                };

                return result;
            });

            return taskResult;
        }

        public Task<ResultItem<KendoMultiselectConfig>> GetMultiselectConfig(GetMultiselectConfigRequest request, CrudActionsRequest crudActionRequest)
        {
            var taskResult = Task.Run<ResultItem<KendoMultiselectConfig>>(async () =>
            {
                var elasticAgent = new ElasticAgentOItems(Globals);

                var result = new ResultItem<KendoMultiselectConfig>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new KendoMultiselectConfig()
                };

                if (!request.IsValid(Globals))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = request.ValidationMessage;

                    return result;
                }

                var resultAgentClassRel = await serviceAgent.GetClassRel(request);

                result.ResultState = resultAgentClassRel.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var resultAgentObjectRels = await serviceAgent.GetRelatedObjects(request);

                result.ResultState = resultAgentObjectRels.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var resultAgetnObjects = await GetObjects(request);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }


                result.Result = new KendoMultiselectConfig
                {
                    dataSource = new KendoDatasource
                    {
                        //pageSize = 10,
                        transport = new KendoTransport(),
                        schema = new KendoDataSourceSchema
                        {
                            data = "data",
                            total = "total",
                            model = new KendoDataSourceModel()
                        },
                        batch = true
                    },
                    //dataSource = resultAgetnObjects.Result,
                    autoBind = true,
                    //autoWidth = false,
                    //clearButton = true,
                    //autoClose = false,
                    dataValueField = "GUID",
                    dataTextField = "Name",
                    //delay = 200,
                    enable = true,
                    height = 250,
                    noDataTemplate = "noDataTemplate"
                };

                result.Result.dataSource.schema.model = new KendoDataSourceModel();
                result.Result.dataSource.schema.model.id = nameof(clsOntologyItem.GUID);
                result.Result.dataSource.schema.model.fields = new Dictionary<string, object>();
                var dictField1 = new Dictionary<string, object>();
                dictField1.Add(nameof(KendoColumnAttribute.type), "string");
                dictField1.Add(nameof(KendoModelAttribute.editable), false);
                result.Result.dataSource.schema.model.fields.Add(nameof(clsOntologyItem.GUID), dictField1);
                var dictField2 = new Dictionary<string, object>();
                dictField2.Add(nameof(KendoColumnAttribute.type), "string");
                dictField2.Add(nameof(KendoModelAttribute.editable), true);
                result.Result.dataSource.schema.model.fields.Add(nameof(clsOntologyItem.Name), dictField2);


                if (resultAgentObjectRels.Result.Any())
                {
                    result.Result.value = resultAgentObjectRels.Result.Select(obj => new { GUID = obj.GUID, Name = obj.Name }).ToList<object>();
                }

                if (resultAgentClassRel.Result.Max_Forw != -1)
                {
                    result.Result.maxSelectedItems = (int)resultAgentClassRel.Result.Max_Forw;
                }

                result.Result.dataSource.transport.read = new KendoTransportCRUD
                {
                    dataType = "json",
                    url = crudActionRequest.ActionRead
                };

                if (!string.IsNullOrEmpty(crudActionRequest.ActionCreate))
                {
                    result.Result.dataSource.transport.create = new KendoTransportCRUD
                    {
                        dataType = "json",
                        url = crudActionRequest.ActionCreate
                    };
                }

                if (!string.IsNullOrEmpty(crudActionRequest.ActionDelete))
                {
                    result.Result.dataSource.transport.destroy = new KendoTransportCRUD
                    {
                        dataType = "json",
                        url = crudActionRequest.ActionDelete
                    };
                }

                if (!string.IsNullOrEmpty(crudActionRequest.ActionUpdate))
                {
                    result.Result.dataSource.transport.update = new KendoTransportCRUD
                    {
                        dataType = "json",
                        url = crudActionRequest.ActionUpdate
                    };
                }


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjects(GetMultiselectConfigRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };

               var elasticAgent = new ElasticAgentOItems(Globals);

               var agentResult = await elasticAgent.GetObjects(request);

               result.ResultState = agentResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               result.Result = agentResult.Result;

               return result;
           });

            return taskResult;
        }

        public Task<ResultItem<KendoDropDownConfig>> GetDropDownConfigClasses()
        {
            var taskResult = Task.Run<ResultItem<KendoDropDownConfig>>(async () =>
            {
                var resultClasses = await serviceAgent.GetClasses();

                var result = new ResultItem<KendoDropDownConfig>
                {
                    ResultState = resultClasses.ResultState,
                    Result = new KendoDropDownConfig
                    {
                        dataSource = resultClasses.Result.OrderBy(lookItm => lookItm.Name).Select(attType => new KendoDropDownItem
                        {
                            Value = attType.GUID,
                            Text = attType.Name
                        }).ToList(),
                        optionLabel = "Classes"
                    }
                };

                return result;
            });

            return taskResult;
        }

        
        public DropDownController(Globals globals) : base(globals)
        {
            serviceAgent = new ElasticAgentOItems(globals);
        }
    }
}
