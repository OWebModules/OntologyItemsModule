﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntologyItemsModule.Services;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule
{
    public class ClassEditController : AppController
    {
        public async Task<OntologyAppDBConnector.Models.ResultOItem<clsOntologyItem>> GetClassItem(string idClass)
        {
            var taskResult = await Task.Run<OntologyAppDBConnector.Models.ResultOItem<clsOntologyItem>>(async() =>
            {

                var elasticAgent = new ElasticAgentClasses(Globals);

                var serviceResult = await elasticAgent.GetOItem(idClass, Globals.Type_Class);

                return serviceResult;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<ClassAttributeViewItem>>> GetClassAttributeViewItems(clsOntologyItem classItem)
        {
            var taskResult = await Task.Run<ResultItem<List<ClassAttributeViewItem>>>(async () =>
            {
                var result = new ResultItem<List<ClassAttributeViewItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<ClassAttributeViewItem>()
                };

                var serviceAgent = new ElasticAgentOItems(Globals);

                var searchClassAttributes = new List<clsClassAtt>
                {
                    new clsClassAtt
                    {
                        ID_Class = classItem.GUID
                    }
                };

                var getClassAttributesResult = await serviceAgent.GetClassAttributes(searchClassAttributes);

                result.ResultState = getClassAttributesResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = getClassAttributesResult.Result.Select(clsAtt => new ClassAttributeViewItem(clsAtt)).ToList();
                return result;
            });

            return taskResult;
        }



        public ClassEditController(Globals globals) : base(globals)
        {
        }
    }
}
