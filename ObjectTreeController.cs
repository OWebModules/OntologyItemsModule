﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Factories;
using OntologyItemsModule.Models;
using OntologyItemsModule.Services;
using OntologyItemsModule.Validations;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule
{
    public class ObjectTreeController : AppController
    {
        private object controllerLocker = new object();

        private ElasticAgentObjectTree elasticAgent;

        public async Task<ResultItem<clsOntologyItem>> GetMostImportantRelationType(string idClass)
        {
            return await elasticAgent.GetMostImportantRelationType(idClass);
        }

        public async Task<ResultItem<CheckHierarchicalItemResult>> CheckHierarchicalOItem(CheckHierarchicalItemRequest request)
        {
            var taskResult = await Task.Run<ResultItem<CheckHierarchicalItemResult>>(async() =>
            {
                var result = new ResultItem<CheckHierarchicalItemResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new CheckHierarchicalItemResult()
                    {
                        Request = request
                    }
                };

                var getResult = await GetHierarchicalItem((GetHierarchicalItemRequest)request);

                result.ResultState = getResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.HierarchicalItem = getResult.Result.HierarchicalItem;

                if (result.Result.HierarchicalItem == null)
                {
                    var names = request.FullName.Split(new string[] { request.FullNameSplitter }, StringSplitOptions.None).ToList();

                    for (int i = 0; i < names.Count; i++)
                    {
                        var name = names[i];


                    }
                }
                



                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetHierarchicalItemResult>> GetHierarchicalItem(GetHierarchicalItemRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetHierarchicalItemResult>>(async() =>
           {
               var result = new ResultItem<GetHierarchicalItemResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new GetHierarchicalItemResult()
                   {
                       Request = request
                   }
               };

               request.MessageOutput?.OutputInfo("Split Fullname...");
               var names = request.FullName.Split(new string[] { request.FullNameSplitter }, StringSplitOptions.None).ToList();
               request.MessageOutput?.OutputInfo($"Splited Fullname: {string.Join(" | ", names)}");

               var elasticAgent = new ElasticAgentObjectTree(Globals);

               request.MessageOutput?.OutputInfo("Get existing objects...");
               var objectsResult = await elasticAgent.GetObjects(names, request.IdClass);
               

               result.ResultState = objectsResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo($"Found objects: {objectsResult.Result.Count}");

               var objects = objectsResult.Result;
               var rels = new List<clsObjectRel>();
               if (objectsResult.Result.Any())
               {
                   request.MessageOutput?.OutputInfo("Get relations between objects...");
                   var relResult = await elasticAgent.GetJoinedRelList(objects, request.IdRelationType);

                   result.ResultState = relResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   request.MessageOutput?.OutputInfo($"Found relations: {relResult.Result.Count}");

                   rels = relResult.Result;

                   
               }



               if (names.Any())
               {
                   request.MessageOutput?.OutputInfo("Get hierarchical...");
                   var getHierarchical = await GetRelListRightLeft(names, rels, 0, request.NameSpaceIdsToExclude);
                   result.ResultState = getHierarchical.ResultState;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
                   request.MessageOutput?.OutputInfo($"Found hierarchical:{getHierarchical.Result.Count()}");

                   var hierarchicalItems = new List<HierarchicalOItem>();

                   request.MessageOutput?.OutputInfo("Compose hierarchical...");
                   foreach (var item in getHierarchical.Result)
                   {
                       var hierarchicalItem = new HierarchicalOItem(item)
                       {
                           HierarchicalParentItem = hierarchicalItems.LastOrDefault()?.Clone()
                       };

                       hierarchicalItems.Add(hierarchicalItem);
                   }

                   
                   result.Result.HierarchicalItem = hierarchicalItems.LastOrDefault();
                   request.MessageOutput?.OutputInfo($"Composed hierarchical: {result.Result.HierarchicalItem.GetFullPath(request.FullNameSplitter)} ({result.Result.HierarchicalItem.IdItem})");
               }

               return result;
           });

            return taskResult;
        }

        private async Task<ResultItem<List<clsOntologyItem>>> GetRelListRightLeft(List<string> names, List<clsObjectRel> relations, int index, List<string> excludeIds, clsOntologyItem parentItem = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };
               var itemCandidates = new List<clsObjectRel>();
               clsObjectRel item = null;

               if (parentItem == null)
               {
                   itemCandidates = (from rel in relations
                                     join excludeLeft in excludeIds on rel.ID_Object equals excludeLeft into excludesLeft
                                     from excludeLeft in excludesLeft.DefaultIfEmpty()
                                     where excludeLeft == null
                                     join excludeRight in excludeIds on rel.ID_Object equals excludeRight into excludesRight
                                     from excludeRight in excludesRight.DefaultIfEmpty()
                                     where excludeRight == null
                                     join relParent in relations on rel.ID_Object equals relParent.ID_Other into relParents
                                     from relParent in relParents.DefaultIfEmpty()
                                     where relParent == null
                                     select rel).ToList();
                   item = itemCandidates.FirstOrDefault(rel => rel.Name_Object == names[index]);
               }
               else
               {
                   itemCandidates = (from rel in relations.Where(rel => rel.ID_Object == parentItem.GUID)
                                     join excludeLeft in excludeIds on rel.ID_Object equals excludeLeft into excludesLeft
                                     from excludeLeft in excludesLeft.DefaultIfEmpty()
                                     where excludeLeft == null
                                     join excludeRight in excludeIds on rel.ID_Object equals excludeRight into excludesRight
                                     from excludeRight in excludesRight.DefaultIfEmpty()
                                     where excludeRight == null
                                     select rel).ToList();
                   item = itemCandidates.FirstOrDefault(rel => rel.Name_Other == names[index]);
               }
               

               if (item != null)
               {
                   result.Result.Add(new clsOntologyItem
                   {
                       GUID =index == 0 ? item.ID_Object : item.ID_Other,
                       Name = index == 0 ? item.Name_Object : item.Name_Other,
                       GUID_Parent = index == 0 ? item.ID_Parent_Object: item.ID_Parent_Other,
                       Type = Globals.Type_Object
                   });

                   if (index < names.Count - 1)
                   {
                       index++;
                       var subElement = await GetRelListRightLeft(names, relations, index, excludeIds, result.Result.Last());

                       result.ResultState = subElement.ResultState;
                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           return result;
                       }

                       if (subElement.Result != null)
                       {
                           result.Result.AddRange(subElement.Result);
                       }
                       else
                       {
                           result.ResultState = Globals.LState_Nothing.Clone();
                           result.Result = null;
                       }
                       
                    }
               }
               else
               {
                   result.ResultState = Globals.LState_Nothing.Clone();
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<KendoTreeNode>>> GetObjectTree(string idObject, string idClass, string idRelationType, string idDirection, int levelsCount)
        {
            var result = new ResultItem<List<KendoTreeNode>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<KendoTreeNode>()
            };

            if (string.IsNullOrEmpty(idClass))
            {
                result.ResultState = Globals.LState_Nothing.Clone();
                return result;
            }
            var resultTree = await elasticAgent.GetTree(idObject, idClass, idRelationType, idDirection);

            if (resultTree.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultTree.ResultState;
                return result;
            }
            var treeFactory = new Factories.ObjectTreeFactory();
            var resultTreeFactory = await treeFactory.CreateTree(resultTree.Result.Objects.OrderBy(obj => obj.Name).ToList(), resultTree.Result.ObjectRelations, idDirection, Globals, levelsCount);

            result.Result = resultTreeFactory.Result;

            return result;
        }

        public async Task<ResultItem<GetObjectRelationTreeResult>> GetObjectRelationTree(GetObjectRelationTreeRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<GetObjectRelationTreeResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GetObjectRelationTreeResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = ValidationController.ValidateGetObjectRelationTreeRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");
                request.MessageOutput?.OutputInfo("Get model...");

                var elasticAgent = new ElasticAgentObjectRelationTree(Globals);

                var getModelResult = await elasticAgent.GetObjectRelationTreeModel(request);

                result.ResultState = getModelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Have model: {getModelResult.Result.RelatedItems.Count} items.");

                result.Result.Model = getModelResult.Result;

                request.MessageOutput?.OutputInfo("Create Node-List...");

                var nodeListResult = await ObjectRelationTreeFactory.CreateKendoTreeNodeList(getModelResult.Result, Globals);
                result.ResultState = nodeListResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Node-List.");
                result.Result.TreeNodes = nodeListResult.Result;
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<KendoTreeNode>>> SaveObjectRelations(string idClass, KendoTreeNode parentNode, List<clsOntologyItem> childItems, clsOntologyItem relationType, bool nextOrderId, int levelsCount)
        {
            var result = new ResultItem<List<KendoTreeNode>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<KendoTreeNode>()
            };
            var oItem = await elasticAgent.GetOItem(parentNode.NodeId, Globals.Type_Object);
            if (oItem == null || oItem.GUID_Related == Globals.LState_Error.GUID)
            {
                result.ResultState = Globals.LState_Error.Clone();
                return result;
            }

            var relationConfig = new clsRelationConfig(Globals);
            long orderId = 1;

            if (nextOrderId)
            {
                var lastOrderId = await elasticAgent.GetNextOrderId(idClass, relationType.GUID);
                if (lastOrderId.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    return result;
                }
                orderId = lastOrderId.Result;
            }
            
            var relItems = childItems.Select(childItem => relationConfig.Rel_ObjectRelation(oItem, childItem, relationType, orderId: orderId++)).ToList();

            var resultSave = await elasticAgent.SaveRelations(relItems);

            if (resultSave.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = Globals.LState_Error.Clone();
                return result;
            }

            var factory = new Factories.ObjectTreeFactory();

            var factoryResult = await factory.CreateTree(childItems, new List<clsObjectRel>(), Globals.Direction_LeftRight.GUID, Globals, levelsCount);

            result.ResultState = factoryResult.ResultState;
            result.Result = factoryResult.Result;

            return result;

        }

        public async Task<ResultItem<OItemWithParent>> GetOItem(string id, string type)
        {
            var result = new ResultItem<OItemWithParent>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new OItemWithParent()
            };

            var oItem = await elasticAgent.GetOItem(id, type);
            oItem = oItem.Clone();
            if (oItem == null || oItem.GUID_Related == Globals.LState_Error.GUID)
            {
                result.ResultState = Globals.LState_Error.Clone();
                return result;
            }

            result.Result.OItem = oItem;
            result.Result.OItemParent = null;

            if (type == Globals.Type_Object)
            {
                var oItemParent = await elasticAgent.GetOItem(oItem.GUID_Parent, Globals.Type_Class);
                if (oItemParent == null || oItemParent.GUID_Related == Globals.LState_Error.GUID)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    return result;
                }

                result.Result.OItemParent = oItemParent;
            }

            return result;
        }

        public ObjectTreeController(Globals Globals) : base(Globals)
        {
            Initialize();
        }

        private void Initialize()
        {
            elasticAgent = new ElasticAgentObjectTree(Globals);
        }
    }

    public class ResultObjectTreeData
    {
        public clsOntologyItem Result { get; set; }
        public List<clsOntologyItem> RelationTypes { get; set; }
    }
}
