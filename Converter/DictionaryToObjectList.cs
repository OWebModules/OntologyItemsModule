﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntologyItemsModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Converter
{
    public static class DictionaryToObjectList
    {
        private static clsTypes types = new clsTypes();
        public static clsOntologyItem Convert(Dictionary<string, object> dictItem)
        {
            var relationItem = new clsOntologyItem();

            if (dictItem.ContainsKey(nameof(InstanceViewItem.IdInstance)))
            {
                relationItem.GUID = ((string[])dictItem[nameof(InstanceViewItem.IdInstance)])[0];
                relationItem.Name = dictItem.ContainsKey(nameof(InstanceViewItem.NameInstance)) ? ((string[])dictItem[nameof(InstanceViewItem.NameInstance)])[0] : null;
                relationItem.GUID_Parent = dictItem.ContainsKey(nameof(InstanceViewItem.IdClass)) ? ((string[])dictItem[nameof(InstanceViewItem.IdClass)])[0] : null;
                relationItem.Type = types.ObjectType;
                relationItem.Mark = dictItem.ContainsKey("ForceDelete") ? bool.Parse(((string[])dictItem["ForceDelete"])[0]) : false;
            }
            else
            {
                relationItem.GUID = ((string[])dictItem[nameof(clsOntologyItem.GUID)])[0];
                relationItem.Name = dictItem.ContainsKey(nameof(clsOntologyItem.Name)) ? ((string[])dictItem[nameof(clsOntologyItem.Name)])[0] : null;
                relationItem.GUID_Parent = dictItem.ContainsKey(nameof(clsOntologyItem.GUID_Parent)) ? ((string[])dictItem[nameof(clsOntologyItem.GUID_Parent)])[0] : null;
                relationItem.Type = ((string[])dictItem[nameof(clsOntologyItem.Type)])[0];
                relationItem.Mark = dictItem.ContainsKey("ForceDelete") ? bool.Parse(((string[])dictItem["ForceDelete"])[0]) : false;
            }
            
            return relationItem;
        }
    }
}
