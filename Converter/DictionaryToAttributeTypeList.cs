﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntologyItemsModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Converter
{
    public static class DictionaryToAttributeTypeList
    {
        private static clsTypes types = new clsTypes();
        public static clsOntologyItem Convert(Dictionary<string, object> dictItem)
        {
            var relationItem = new clsOntologyItem();

            if (dictItem.ContainsKey(nameof(AttributeTypeViewItem.IdAttributeType)))
            {
                relationItem.GUID = ((string[])dictItem[nameof(AttributeTypeViewItem.IdAttributeType)])[0];
                relationItem.Name = dictItem.ContainsKey(nameof(AttributeTypeViewItem.NameAttributeType)) ? ((string[])dictItem[nameof(AttributeTypeViewItem.NameAttributeType)])[0] : null;
                relationItem.GUID_Parent = dictItem.ContainsKey(nameof(AttributeTypeViewItem.IdDataType)) ? ((string[])dictItem[nameof(AttributeTypeViewItem.IdDataType)])[0] : null;
                relationItem.Type = types.AttributeType;
                relationItem.Mark = dictItem.ContainsKey("ForceDelete") ? bool.Parse(((string[])dictItem["ForceDelete"])[0]) : false;
            }
            else
            {
                relationItem.GUID = ((string[])dictItem[nameof(clsOntologyItem.GUID)])[0];
                relationItem.Name = dictItem.ContainsKey(nameof(clsOntologyItem.Name)) ? ((string[])dictItem[nameof(clsOntologyItem.Name)])[0] : null;
                relationItem.GUID_Parent = dictItem.ContainsKey(nameof(clsOntologyItem.GUID_Parent)) ? ((string[])dictItem[nameof(clsOntologyItem.GUID_Parent)])[0] : null;
                relationItem.Type = ((string[])dictItem[nameof(clsOntologyItem.Type)])[0];
                relationItem.Mark = dictItem.ContainsKey("ForceDelete") ? bool.Parse(((string[])dictItem["ForceDelete"])[0]) : false;
            }
            
            return relationItem;
        }
    }
}
