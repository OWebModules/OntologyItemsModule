﻿using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Converter
{
    public static class DictionaryToObjectObjectConscious
    {
        public static ObjectObject_Conscious Convert(Dictionary<string, object> dictItem)
        {
            var relationItem = new clsObjectRel();

            relationItem.ID_Object = ((string[])dictItem[nameof(ObjectObject_Conscious.IdObject)])[0];
            relationItem.Name_Object = dictItem.ContainsKey(nameof(ObjectObject_Conscious.NameObject)) ? ((string[])dictItem[nameof(ObjectObject_Conscious.NameObject)])[0] : null;
            relationItem.ID_Parent_Object = dictItem.ContainsKey(nameof(ObjectObject_Conscious.IdParentObject)) ? ((string[])dictItem[nameof(ObjectObject_Conscious.IdParentObject)])[0] : null;
            relationItem.Name_Parent_Object = dictItem.ContainsKey(nameof(ObjectObject_Conscious.NameParentObject)) ? ((string[])dictItem[nameof(ObjectObject_Conscious.NameParentObject)])[0] : null;
            relationItem.ID_Other = ((string[])dictItem[nameof(ObjectObject_Conscious.IdOther)])[0];
            relationItem.Name_Other = dictItem.ContainsKey(nameof(ObjectObject_Conscious.NameOther)) ? ((string[])dictItem[nameof(ObjectObject_Conscious.NameOther)])[0] : null;
            relationItem.ID_Parent_Other = dictItem.ContainsKey(nameof(ObjectObject_Conscious.IdParentOther)) ? ((string[])dictItem[nameof(ObjectObject_Conscious.IdParentOther)])[0] : null;
            relationItem.Name_Parent_Other = dictItem.ContainsKey(nameof(ObjectObject_Conscious.NameParentOther)) ? ((string[])dictItem[nameof(ObjectObject_Conscious.NameParentOther)])[0] : null;
            relationItem.ID_RelationType = ((string[])dictItem[nameof(ObjectObject_Conscious.IdRelationType)])[0];
            relationItem.Name_RelationType = dictItem.ContainsKey(nameof(ObjectObject_Conscious.NameRelationType)) ? ((string[])dictItem[nameof(ObjectObject_Conscious.NameRelationType)])[0] : null;
            relationItem.Ontology = dictItem.ContainsKey(nameof(ObjectObject_Conscious.Ontology)) ? ((string[])dictItem[nameof(ObjectObject_Conscious.Ontology)])[0] : null;
            if (dictItem.ContainsKey(nameof(ObjectObject_Conscious.OrderId)))
            {
                relationItem.OrderID = long.Parse((((string[])dictItem[nameof(ObjectObject_Conscious.OrderId)])[0]));
            }
            
            
            return new ObjectObject_Conscious(relationItem);
        }
    }
}
