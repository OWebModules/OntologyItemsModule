﻿using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Converter
{
    public static class DictionaryToObjectAttribute
    {
        public static ObjectAttributeViewItem Convert(Dictionary<string, object> dictItem)
        {
            var relationItem = new clsObjectAtt();

            relationItem.ID_Attribute = ((string[])dictItem[nameof(ObjectAttributeViewItem.IdAttribute)])[0];
            relationItem.ID_AttributeType = dictItem.ContainsKey(nameof(ObjectAttributeViewItem.IdAttributeType)) ? ((string[])dictItem[nameof(ObjectAttributeViewItem.IdAttributeType)])[0] : null;
            relationItem.ID_Class = dictItem.ContainsKey(nameof(ObjectAttributeViewItem.IdAttributeType)) ? ((string[])dictItem[nameof(ObjectAttributeViewItem.IdClass)])[0] : null;
            relationItem.ID_DataType = dictItem.ContainsKey(nameof(ObjectAttributeViewItem.IdAttributeType)) ? ((string[])dictItem[nameof(ObjectAttributeViewItem.IdDataType)])[0] : null;
            relationItem.ID_Object = dictItem.ContainsKey(nameof(ObjectAttributeViewItem.IdAttributeType)) ? ((string[])dictItem[nameof(ObjectAttributeViewItem.IdObject)])[0] : null;
            relationItem.Name_AttributeType = dictItem.ContainsKey(nameof(ObjectAttributeViewItem.IdAttributeType)) ? ((string[])dictItem[nameof(ObjectAttributeViewItem.NameAttributeType)])[0] : null;
            relationItem.Name_Class = dictItem.ContainsKey(nameof(ObjectAttributeViewItem.IdAttributeType)) ? ((string[])dictItem[nameof(ObjectAttributeViewItem.NameClass)])[0] : null;
            relationItem.Name_DataType = dictItem.ContainsKey(nameof(ObjectAttributeViewItem.IdAttributeType)) ? ((string[])dictItem[nameof(ObjectAttributeViewItem.NameDataType)])[0] : null;
            relationItem.Name_Object = dictItem.ContainsKey(nameof(ObjectAttributeViewItem.IdAttributeType)) ? ((string[])dictItem[nameof(ObjectAttributeViewItem.NameObject)])[0] : null;
            if (dictItem.ContainsKey(nameof(ObjectAttributeViewItem.OrderId)) )
            {
                relationItem.OrderID = long.Parse((((string[])dictItem[nameof(ObjectAttributeViewItem.OrderId)])[0]));
            }

            return new ObjectAttributeViewItem(relationItem);
        }
    }
}
