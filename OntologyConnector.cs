﻿using ImportExport_Module;
using ImportExport_Module.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntologyItemsModule.Services;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule
{
    public class OntologyConnector : AppController
    {
        public async Task<ImportExportResult> ExportOntologies(ExportOntologyRequest exportOntologyRequest)
        {
            var getOntologiesResult = await GetOntologies(exportOntologyRequest);

            var result = new ImportExportResult
            {
                ResultState = getOntologiesResult.ResultState
            };

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Ontologies cannot loaded!";
                return result;
            }

            var enrichResult = await EnrichOntologies(getOntologiesResult.Result);

            result.ResultState = enrichResult.ResultState;
            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Ontologies cannot be enriched with items!";
                return result;
            }

            var exporter = new JsonExporter(Globals);
            exporter.PacketCount = Globals.SearchRange;
            exporter.AttributeTypes = enrichResult.Result.SelectMany(res => res.AttributeTypes).ToList();
            exporter.RelationTypes = enrichResult.Result.SelectMany(res => res.RelationTypes).ToList();
            exporter.ClassItems = enrichResult.Result.SelectMany(res => res.Classes).ToList();
            exporter.ObjectItems = enrichResult.Result.SelectMany(res => res.Objects).ToList();

            exporter.ClassAttributes = enrichResult.Result.SelectMany(res => res.ClassAttributes).ToList();
            exporter.ClassRelations = enrichResult.Result.SelectMany(res => res.ClassRelations).ToList();

            exporter.ObjectAttributes = enrichResult.Result.SelectMany(res => res.ObjectAttributes).ToList();
            exporter.ObjectRelations = enrichResult.Result.SelectMany(res => res.ObjectRelations).ToList();


            var exportResult = await exporter.ExportItems(exportOntologyRequest.ExportPath, "");

            result.ResultState = exportResult.ResultState;
            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = "Error while exporting items";
            }

            if (exportOntologyRequest.ZipFiles)
            {

                var tempPath = Environment.ExpandEnvironmentVariables($"%TEMP%\\{Guid.NewGuid().ToString()}.zip");
                var streamWriter = new StreamWriter(tempPath);

                using (var zipArchive = new ZipArchive(streamWriter.BaseStream, ZipArchiveMode.Create))
                {
                    var fileItems = Directory.GetFiles(exportOntologyRequest.ExportPath);

                    foreach (var fileItem in fileItems)
                    {
                        zipArchive.CreateEntryFromFile(fileItem, Path.GetFileName(fileItem));
                    }
                    
                }

                var fileStream = new FileStream(tempPath, FileMode.Open);
                result.ZipStream = fileStream;

            }

            return result;
        }

        public async Task<ResultItem<List<Ontology>>> EnrichOntologies(List<Ontology> ontologies, bool ontologyElements = true)
        {
            var serviceAgent = new ElasticAgentOntologies(Globals);

            var taskResult = await Task.Run<ResultItem<List<Ontology>>>(() =>
            {
                var result = new ResultItem<List<Ontology>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = ontologies
                };

                foreach (var onto in ontologies)
                {
                    onto.AttributeTypes = onto.RefItems.Where(refItm => refItm.Type == Globals.Type_AttributeType).ToList();
                    onto.RelationTypes = onto.RefItems.Where(refItm => refItm.Type == Globals.Type_RelationType).ToList();
                    onto.Classes = onto.RefItems.Where(refItm => refItm.Type == Globals.Type_Class).ToList();
                    onto.Objects = onto.RefItems.Where(refItm => refItm.Type == Globals.Type_Object).ToList();

                    if (ontologyElements)
                    {
                        onto.Objects.AddRange(onto.OntologyItems.Select(ontoI => ontoI.OItem));
                        onto.Objects.AddRange(onto.OntologyJoins.Select(ontoJ => ontoJ.JoinEntity));
                        onto.Objects.Add(onto.OntologyEntity);
                        onto.Objects.AddRange(onto.OntologyItems.Where(ontoI => ontoI.OntologyRule != null).Select(ontoI => ontoI.OntologyRule));

                        onto.Classes.Add(Globals.Class_Ontologies);
                        onto.Classes.Add(Globals.Class_OntologyItems);
                        onto.Classes.Add(Globals.Class_OntologyJoin);
                        onto.Classes.Add(Globals.Class_OntologyRelationRule);
                    }
                    
                    var ontologItems = onto.OntologyItems.Where(ontoI => ontoI.OntologyRule != null && ontoI.OntologyRule.GUID == Globals.OntologyRelationRules.Rule_ChildToken.GUID && ontoI.RefItem.Type == Globals.Type_Class);
                    
                    if (ontologItems.Any())
                    {
                        var searchObjects = ontologItems.Select(ontoI => new clsOntologyItem
                        {
                            GUID_Parent = ontoI.RefItem.GUID
                        }).ToList();

                        var dbReaderGetChildObjects = new OntologyModDBConnector(Globals);

                        result.ResultState = dbReaderGetChildObjects.GetDataObjects(searchObjects);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "The Childobjects of Classes cannot be loaded!";
                            return result;
                        }

                        onto.Objects.AddRange(from objectItem in dbReaderGetChildObjects.Objects1
                                              join included in onto.Objects on objectItem.GUID equals included.GUID into includeds
                                              from included in includeds.DefaultIfEmpty()
                                              where included == null
                                              select objectItem);


                    }

                    var dbReaderClasses = new OntologyModDBConnector(Globals);

                    var resultClasses = dbReaderClasses.GetDataClasses();

                    var parentClassesNotPresent = (from cls in onto.Classes
                                                   join clsParent in onto.Classes on cls.GUID_Parent equals clsParent.GUID into clsParents
                                                   from clsParent in clsParents.DefaultIfEmpty()
                                                   where clsParent == null
                                                   select cls);

                    while (parentClassesNotPresent.Any())
                    {
                        var addClasses = (from cls in parentClassesNotPresent
                                         join clsParent in dbReaderClasses.Classes1 on cls.GUID_Parent equals clsParent.GUID
                                         select clsParent).ToList();
                        if (!addClasses.Any())
                        {
                            break;
                        }
                        onto.Classes.AddRange(addClasses);

                        parentClassesNotPresent = (from cls in onto.Classes
                                                   join clsParent in onto.Classes on cls.GUID_Parent equals clsParent.GUID into clsParents
                                                   from clsParent in clsParents.DefaultIfEmpty()
                                                   where clsParent == null
                                                   select cls);
                    }

                    var withChildClasses = onto.OntologyItems.Where(ontoItm => ontoItm.OntologyRule != null && ontoItm.OntologyRule.GUID == "5f39c4ce080d4f36b432f83d2892c841").Select(oItm => oItm.RefItem).ToList();

                    var dbReaderChildClasses = new OntologyModDBConnector(Globals);
                    foreach (var childClass in withChildClasses)
                    {
                        var searchChilds = new List<clsOntologyItem>
                        {
                            new clsOntologyItem
                            {
                                GUID_Parent = childClass.GUID
                            }
                        };

                        result.ResultState = dbReaderChildClasses.GetDataObjects(searchChilds);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting child-objects!";
                            return result;
                        }

                        var objectsToAdd = (from obj in dbReaderChildClasses.Objects1
                                            join exist in onto.Objects on obj.GUID equals exist.GUID into exists
                                            from exist in exists.DefaultIfEmpty()
                                            where exist == null
                                            select obj);

                        onto.Objects.AddRange(objectsToAdd);
                    }

                    var searchClassAttributes = (from cls in onto.Classes
                                                 from attType in onto.AttributeTypes
                                                 select new clsClassAtt
                                                 {
                                                     ID_Class = cls.GUID,
                                                     ID_AttributeType = attType.GUID
                                                 }).ToList();

                    if (searchClassAttributes.Any())
                    {
                        var dbReaderClassAttributes = new OntologyModDBConnector(Globals);
                        result.ResultState = dbReaderClassAttributes.GetDataClassAtts(searchClassAttributes, doIds:true);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting ClassAttributes";
                            return result;
                        }

                        onto.ClassAttributes = dbReaderClassAttributes.ClassAttsId;
                    }

                    var searchClassRelationsByRelations = (from cls in onto.Classes
                                                           from relType in onto.RelationTypes
                                                           select new clsClassRel
                                                           {
                                                               ID_Class_Left = cls.GUID,
                                                               ID_RelationType = relType.GUID
                                                           }).ToList();

                    if (searchClassRelationsByRelations.Any())
                    {
                        var dbReaderClassRelations = new OntologyModDBConnector(Globals);

                        result.ResultState = dbReaderClassRelations.GetDataClassRel(searchClassRelationsByRelations,doOr:true, doIds:true);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting ClassRelations";
                            return result;
                        }

                        onto.ClassRelations = dbReaderClassRelations.ClassRelsId.Where(clsRel => string.IsNullOrEmpty(clsRel.ID_Class_Right)).ToList();

                        onto.ClassRelations.AddRange((from clsLeft in onto.Classes
                                                     join clsRel in dbReaderClassRelations.ClassRelsId on clsLeft.GUID equals clsRel.ID_Class_Left
                                                     join clsRight in onto.Classes on clsRel.ID_Class_Right equals clsRight.GUID
                                                     select clsRel).Distinct());
                    }

                    
                    var searchObjectAttributes = (from obj in onto.Objects
                                                  from attType in onto.AttributeTypes
                                                  select new clsObjectAtt
                                                  {
                                                      ID_Object = obj.GUID,
                                                      ID_AttributeType = attType.GUID
                                                  }).ToList();

                    if (searchObjectAttributes.Any())
                    {
                        var dbReaderObjectAttributes = new OntologyModDBConnector(Globals);

                        var pos = 0;
                        int i;
                        for (i = 0; i < searchObjectAttributes.Count; i += 1000)
                        {
                            if (i > 0)
                            {
                                var range = searchObjectAttributes.GetRange(pos, i-pos);
                                result.ResultState = dbReaderObjectAttributes.GetDataObjectAtt(range, doIds: true);

                                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    result.ResultState.Additional1 = "Error while getting ObjectAttributes";
                                    return result;
                                }

                                onto.ObjectAttributes.AddRange(dbReaderObjectAttributes.ObjAttsId);
                                pos = i;


                            }
                        }

                        if (pos < searchObjectAttributes.Count-1)
                        {
                            var range = searchObjectAttributes.GetRange(pos, searchObjectAttributes.Count - pos);
                            result.ResultState = dbReaderObjectAttributes.GetDataObjectAtt(range, doIds: true);

                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = "Error while getting ObjectAttributes";
                                return result;
                            }

                            onto.ObjectAttributes.AddRange(dbReaderObjectAttributes.ObjAttsId);
                        }
                        
                    }

                    var searchObjectRelations = (from obj in onto.Objects
                                                 from relType in onto.RelationTypes
                                                 select new clsObjectRel
                                                 {
                                                     ID_Object = obj.GUID,
                                                     ID_RelationType = relType.GUID
                                                 }).ToList();


                    if (searchObjectRelations.Any())
                    {
                        var dbReaderObjectRelations = new OntologyModDBConnector(Globals);

                        result.ResultState = dbReaderObjectRelations.GetDataObjectRel(searchObjectRelations, doIds:true);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting ObjectRelations";
                            return result;
                        }

                        onto.ObjectRelations.AddRange(from objLeft in onto.Objects.Where(obj => obj.GUID_Parent != Globals.Class_Ontologies.GUID &&
                                                      obj.GUID_Parent != Globals.Class_OntologyItems.GUID &&
                                                      obj.GUID_Parent != Globals.Class_OntologyJoin.GUID &&
                                                      obj.GUID_Parent != Globals.Class_OntologyRelationRule.GUID &&
                                                      obj.GUID_Parent != Globals.Class_OntologyItemCreationRule.GUID)
                                                join rel in dbReaderObjectRelations.ObjectRelsId on objLeft.GUID equals rel.ID_Object
                                                join objRight in onto.Objects on rel.ID_Other equals objRight.GUID
                                                select rel);

                        var noObjectRel = dbReaderObjectRelations.ObjectRels.Where(relItm => relItm.Ontology != Globals.Type_Object).ToList();

                        var attributeTypeRels = noObjectRel.Where(rel => rel.Ontology == Globals.Type_AttributeType).ToList();
                        var relationTypeRels = noObjectRel.Where(rel => rel.Ontology == Globals.Type_RelationType).ToList();
                        var classRels = noObjectRel.Where(rel => rel.Ontology == Globals.Type_Class).ToList();

                        onto.ObjectRelations.AddRange(from obj in onto.Objects
                                                      join rel in attributeTypeRels on obj.GUID equals rel.ID_Object
                                                      join attType in onto.AttributeTypes on rel.ID_Other equals attType.GUID
                                                      select rel);

                        onto.ObjectRelations.AddRange(from obj in onto.Objects
                                                      join rel in relationTypeRels on obj.GUID equals rel.ID_Object
                                                      join relType in onto.RelationTypes on rel.ID_Other equals relType.GUID
                                                      select rel);

                        onto.ObjectRelations.AddRange(from obj in onto.Objects
                                                      join rel in relationTypeRels on obj.GUID equals rel.ID_Object
                                                      join clsItm in onto.Classes on rel.ID_Other equals clsItm.GUID
                                                      select rel);
                    }

                    

                };

                return result;
            });

            return taskResult;
        }

        

        public async Task<ResultItem<List<Ontology>>> GetOntologies(GetOntologyRequest getOntologyRequest)
        {
            var serviceAgent = new ElasticAgentOntologies(Globals);

            if (string.IsNullOrEmpty(getOntologyRequest.RefItem.Name))
            {
                var getOItemResult = serviceAgent.GetOItem(getOntologyRequest.RefItem.GUID, Globals.Type_Object);
                if (getOItemResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    var result = new ResultItem<List<Ontology>>
                    {
                        ResultState = getOItemResult.ResultState
                    };
                    return result;
                }
            }
            var resultRaw = await serviceAgent.GetOntology(getOntologyRequest);

            var taskResult = await Task.Run<ResultItem<List<Ontology>>>(() =>
            {
                var result = new ResultItem<List<Ontology>>
                {
                    Result = resultRaw.Result.Ontologies.Select(onto => new Ontology
                    {
                        OntologyEntity = onto,
                        RefItem = resultRaw.Result.RefItem
                    }).ToList()
                };
                result.ResultState = resultRaw.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var ontologyItems = (from ontologyToOItem in resultRaw.Result.OntologiesToOItems
                                     join oItemToRef in resultRaw.Result.OItemsToRefs on ontologyToOItem.ID_Other equals oItemToRef.ID_Object
                                     join ruleRel in resultRaw.Result.OItemsToRule on oItemToRef.ID_Object equals ruleRel.ID_Object into ruleRels
                                     from ruleRel in ruleRels.DefaultIfEmpty()
                                     select new OntologyAppDBConnector.Models.OntologyItem
                                     {
                                         IdOntology = ontologyToOItem.ID_Object,
                                         OItem = new clsOntologyItem { GUID = oItemToRef.ID_Object, Name = oItemToRef.Name_Object, GUID_Parent = oItemToRef.ID_Parent_Object, Type = Globals.Type_Object },
                                         RefItem = new clsOntologyItem { GUID = oItemToRef.ID_Other, Name = oItemToRef.Name_Other, GUID_Parent = oItemToRef.ID_Parent_Other, Type = oItemToRef.Ontology },
                                         OntologyRule = ruleRel != null ? new clsOntologyItem { GUID = ruleRel.ID_Other, Name = ruleRel.Name_Other, GUID_Parent = ruleRel.ID_Parent_Other, Type = ruleRel.Ontology } : null
                                     }).ToList();

                ontologyItems.AddRange(from ontologyToOJoin in resultRaw.Result.OntologiesToJoins
                                       join OJoinToOItem in resultRaw.Result.JoinsToOItems on ontologyToOJoin.ID_Other equals OJoinToOItem.ID_Object
                                       join oItemToRef in resultRaw.Result.OItemsToRefs on OJoinToOItem.ID_Other equals oItemToRef.ID_Object
                                       join ruleRel in resultRaw.Result.OItemsToRule on oItemToRef.ID_Object equals ruleRel.ID_Object into ruleRels
                                       from ruleRel in ruleRels.DefaultIfEmpty()
                                       select new OntologyAppDBConnector.Models.OntologyItem
                                       {
                                           IdOntology = ontologyToOJoin.ID_Object,
                                           OItem = new clsOntologyItem { GUID = oItemToRef.ID_Object, Name = oItemToRef.Name_Object, GUID_Parent = oItemToRef.ID_Parent_Object, Type = Globals.Type_Object },
                                           RefItem = new clsOntologyItem { GUID = oItemToRef.ID_Other, Name = oItemToRef.Name_Other, GUID_Parent = oItemToRef.ID_Parent_Other, Type = oItemToRef.Ontology },
                                           OntologyRule = ruleRel != null ? new clsOntologyItem { GUID = ruleRel.ID_Other, Name = ruleRel.Name_Other, GUID_Parent = ruleRel.ID_Parent_Other, Type = ruleRel.Ontology } : null
                                       });

                result.Result.ForEach(async onto =>
                {
                    var ontosToJoins = resultRaw.Result.OntologiesToJoins.Where(rel => rel.ID_Object == onto.OntologyEntity.GUID);
                    var ontosToOItems = resultRaw.Result.OntologiesToOItems.Where(rel => rel.ID_Object == onto.OntologyEntity.GUID);

                    // Ontologies to Ontology-Joins
                    onto.ObjectRelations.AddRange(ontosToJoins);

                    var oItemRels = ontosToOItems.ToList();
                    oItemRels.AddRange(from ontoToJoin in ontosToJoins
                                       join joinToOItem in resultRaw.Result.JoinsToOItems on ontoToJoin.ID_Other equals joinToOItem.ID_Object
                                       select joinToOItem);
                    // Ontologies and Joins to OItems
                    onto.ObjectRelations.AddRange(oItemRels);

                    var joinRules = (from joinItem in ontosToJoins
                                     join ruleRel in resultRaw.Result.JoinsToRule on joinItem.ID_Other equals ruleRel.ID_Object
                                     select ruleRel);

                    // Ontology-Joins to Rules
                    onto.ObjectRelations.AddRange(joinRules);

                    // RefItems of OItems
                    onto.ObjectRelations.AddRange(from oItemRel in oItemRels
                                                  join refItem in resultRaw.Result.OItemsToRefs on oItemRel.ID_Other equals refItem.ID_Object
                                                  select refItem);

                    onto.ObjectRelations.AddRange(from oItemRel in oItemRels
                                                  join oItemToRule in resultRaw.Result.OItemsToRule on oItemRel.ID_Other equals oItemToRule.ID_Object
                                                  select oItemToRule);

                    onto.OntologyJoins = (from joinItem in ontosToJoins
                                          join ruleRel in resultRaw.Result.JoinsToRule on joinItem.ID_Other equals ruleRel.ID_Object into ruleRels
                                          from ruleRel in ruleRels.DefaultIfEmpty()
                                          select new OntologyJoin
                                          {
                                              JoinEntity = new clsOntologyItem { GUID = joinItem.ID_Other, Name = joinItem.Name_Other, GUID_Parent = joinItem.ID_Parent_Other, Type = joinItem.Ontology },
                                              OntologyRule = ruleRel != null ? new clsOntologyItem { GUID = ruleRel.ID_Other, Name = ruleRel.Name_Other, GUID_Parent = ruleRel.ID_Parent_Other, Type = ruleRel.Ontology } : null
                                          }).ToList();

                    onto.OntologyJoins.ForEach(joinItm =>
                    {
                        joinItm.OItem1 = (from joinToOItem in resultRaw.Result.JoinsToOItems.Where(joinRel => joinRel.ID_Object == joinItm.JoinEntity.GUID && (joinRel.OrderID == 1 || joinRel.OrderID == 4))
                                       join oItem in ontologyItems on joinToOItem.ID_Other equals oItem.OItem.GUID
                                       select oItem).FirstOrDefault();

                        joinItm.OItem2 = (from joinToOItem in resultRaw.Result.JoinsToOItems.Where(joinRel => joinRel.ID_Object == joinItm.JoinEntity.GUID && joinRel.OrderID == 2)
                                       join oItem in ontologyItems on joinToOItem.ID_Other equals oItem.OItem.GUID
                                       select oItem).FirstOrDefault();

                        joinItm.OItem3 = (from joinToOItem in resultRaw.Result.JoinsToOItems.Where(joinRel => joinRel.ID_Object == joinItm.JoinEntity.GUID && joinRel.OrderID == 3)
                                       join oItem in ontologyItems on joinToOItem.ID_Other equals oItem.OItem.GUID
                                       select oItem).FirstOrDefault();
                    });

                    onto.OntologyItems = ontologyItems.Where(oItem => oItem.IdOntology == onto.OntologyEntity.GUID).ToList();

                    if (resultRaw.Result.ClassAttributes.Any())
                    {
                        var joinsToAdd = (from clsAtt in resultRaw.Result.ClassAttributes
                                          join oJoin in onto.OntologyJoins.Where(oJoin => oJoin.OItem1 != null && oJoin.OItem2 != null) on new { IdClass = clsAtt.ID_Class, IdAttributeType = clsAtt.ID_AttributeType } equals
                                                                                                                                                    new { IdClass = oJoin.OItem1.RefItem.GUID, IdAttributeType = oJoin.OItem2.RefItem.GUID } into oJoins
                                          from oJoin in oJoins.DefaultIfEmpty()
                                          where oJoin == null
                                          select clsAtt).ToList();

                        var joins = await GetOntologyJoins(joinsToAdd);
                        onto.OntologyJoins.AddRange(joins);
                    }

                    if (resultRaw.Result.ClassRelations.Any())
                    {
                        var joinsToAdd = (from clsRel in resultRaw.Result.ClassRelations
                                          join oJoin in onto.OntologyJoins.Where(oJoin => oJoin.OItem1 != null && oJoin.OItem2 != null && oJoin.OItem3 != null) on new { IdClassLeft = clsRel.ID_Class_Left, IdClassRight = clsRel.ID_Class_Right, IdRelationType = clsRel.ID_RelationType } equals
                                                                                                                                                    new { IdClassLeft = oJoin.OItem1.RefItem.GUID, IdClassRight = oJoin.OItem2.RefItem.GUID, IdRelationType = oJoin.OItem3.RefItem.GUID } into oJoins
                                          from oJoin in oJoins.DefaultIfEmpty()
                                          where oJoin == null
                                          select clsRel).ToList();

                        var joins = await GetOntologyJoins(joinsToAdd);
                        onto.OntologyJoins.AddRange(joins);
                    }
                });
                
                return result;
            });

            return taskResult;
            
        }

        public async Task<List<OntologyJoin>> GetOntologyJoins(List<clsClassAtt> classAttributes)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = classAttributes.Select(clsAtt => new OntologyJoin
                {
                    JoinEntity = new clsOntologyItem
                    {
                        GUID = Globals.NewGUID,
                        Name = (clsAtt.Name_AttributeType + clsAtt.Name_Class).Length > 255 ? (clsAtt.Name_AttributeType + clsAtt.Name_Class).Substring(0, 254) : (clsAtt.Name_AttributeType + clsAtt.Name_Class),
                        GUID_Parent = Globals.Class_OntologyJoin.GUID,
                        Type = Globals.Type_Object,
                        New_Item = true
                    },
                    OItem1 = new OntologyAppDBConnector.Models.OntologyItem
                    {
                        OItem = new clsOntologyItem
                        {
                            GUID = Globals.NewGUID,
                            Name = clsAtt.Name_Class,
                            GUID_Parent = Globals.Class_OntologyItems.GUID,
                            Type = Globals.Type_Object,
                            New_Item = true
                        },
                        RefItem = new clsOntologyItem
                        {
                            GUID = clsAtt.ID_Class,
                            Name = clsAtt.Name_Class,
                            Type = Globals.Type_Class
                        }
                    },
                    OItem2 = new OntologyAppDBConnector.Models.OntologyItem
                    {
                        OItem = new clsOntologyItem
                        {
                            GUID = Globals.NewGUID,
                            Name = clsAtt.ID_AttributeType,
                            GUID_Parent = Globals.Class_OntologyItems.GUID,
                            Type = Globals.Type_Object,
                            New_Item = true
                        },
                        RefItem = new clsOntologyItem
                        {
                            GUID = clsAtt.ID_AttributeType,
                            Name = clsAtt.Name_AttributeType,
                            GUID_Parent = clsAtt.ID_DataType,
                            Type = Globals.Type_AttributeType
                        }
                    }
                }).ToList();

                return result;
            });

            return taskResult;
        }

        public async Task<List<OntologyJoin>> GetOntologyJoins(List<clsClassRel> classRelations)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = classRelations.Select(clsRel => new OntologyJoin
                {
                    JoinEntity = new clsOntologyItem
                    {
                        GUID = Globals.NewGUID,
                        Name = (clsRel.Name_Class_Left + clsRel.Name_Class_Right + clsRel.Name_RelationType).Length > 255 ? (clsRel.Name_Class_Left + clsRel.Name_Class_Right + clsRel.Name_RelationType).Substring(0, 254) : (clsRel.Name_Class_Left + clsRel.Name_Class_Right + clsRel.Name_RelationType),
                        GUID_Parent = Globals.Class_OntologyJoin.GUID,
                        Type = Globals.Type_Object,
                        New_Item = true
                    },
                    OItem1 = new OntologyAppDBConnector.Models.OntologyItem
                    {
                        OItem = new clsOntologyItem
                        {
                            GUID = Globals.NewGUID,
                            Name = clsRel.Name_Class_Left,
                            GUID_Parent = Globals.Class_OntologyItems.GUID,
                            Type = Globals.Type_Object,
                            New_Item = true
                        },
                        RefItem = new clsOntologyItem
                        {
                            GUID = clsRel.ID_Class_Left,
                            Name = clsRel.Name_Class_Left,
                            Type = Globals.Type_Class
                        }
                    },
                    OItem2 =  string.IsNullOrEmpty( clsRel.ID_Class_Right) ? null : new OntologyAppDBConnector.Models.OntologyItem
                    {
                        OItem = new clsOntologyItem
                        {
                            GUID = Globals.NewGUID,
                            Name = clsRel.Name_Class_Right,
                            GUID_Parent = Globals.Class_OntologyItems.GUID,
                            Type = Globals.Type_Object,
                            New_Item = true
                        },
                        RefItem = new clsOntologyItem
                        {
                            GUID = clsRel.ID_Class_Right,
                            Name = clsRel.Name_Class_Right,
                            Type = Globals.Type_Class
                        }
                    },
                    OItem3 = new OntologyAppDBConnector.Models.OntologyItem
                    {
                        OItem = new clsOntologyItem
                        {
                            GUID = Globals.NewGUID,
                            Name = clsRel.Name_RelationType,
                            GUID_Parent = Globals.Class_OntologyItems.GUID,
                            Type = Globals.Type_Object,
                            New_Item = true
                        },
                        RefItem = new clsOntologyItem
                        {
                            GUID = clsRel.ID_RelationType,
                            Name = clsRel.Name_RelationType,
                            Type = Globals.Type_RelationType
                        }
                    }
                }).ToList();

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveOntologyJoins(List<OntologyJoin> joins, clsOntologyItem ontology = null)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = Globals.LState_Success.Clone();

                var relationConfig = new clsRelationConfig(Globals);

                var objectsToSave = new List<clsOntologyItem>();
                var relationsToSave = new List<clsObjectRel>();

                foreach (var join in joins)
                {
                    objectsToSave.Add(join.JoinEntity);
                    if (ontology != null)
                    {
                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(ontology, join.JoinEntity, Globals.RelationType_contains));
                    }

                    if (join.OntologyRule != null)
                    {
                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(join.JoinEntity, join.OntologyRule, Globals.RelationType_belonging));
                    }
                    
                    if (join.OItem1 != null)
                    {
                        objectsToSave.Add(join.OItem1.OItem);
                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(join.JoinEntity, join.OItem1.OItem, Globals.RelationType_contains, orderId: 1));
                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(join.OItem1.OItem, join.OItem1.RefItem, GetRelationType(join.OItem1)));
                        
                    }
                    if (join.OItem2 != null)
                    {
                        objectsToSave.Add(join.OItem2.OItem);
                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(join.JoinEntity, join.OItem2.OItem, Globals.RelationType_contains, orderId: 2));
                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(join.OItem2.OItem, join.OItem2.RefItem, GetRelationType(join.OItem2)));
                    }
                    if (join.OItem3 != null)
                    {
                        objectsToSave.Add(join.OItem3.OItem);
                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(join.JoinEntity, join.OItem2.OItem, Globals.RelationType_contains, orderId: 3));
                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(join.OItem3.OItem, join.OItem3.RefItem, GetRelationType(join.OItem3)));
                    }
                }

                var elasticAgent = new ElasticAgentOntologies(Globals);
                if (objectsToSave.Any())
                {
                    result = await elasticAgent.SaveObjects(objectsToSave);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Ontology-Join Objects!";
                        return result;
                    }
                }
                if (relationsToSave.Any())
                {
                    result = await elasticAgent.SaveRelations(relationsToSave);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Ontology-Join Relations!";
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        private clsOntologyItem GetRelationType(OntologyAppDBConnector.Models.OntologyItem ontologyItem)
        {
            if (ontologyItem.RefItem.Type == Globals.Type_AttributeType)
            {
                return Globals.RelationType_belongingAttribute;
            }
            if (ontologyItem.RefItem.Type == Globals.Type_RelationType)
            {
                return Globals.RelationType_belongingRelationType;
            }
            if (ontologyItem.RefItem.Type == Globals.Type_Class)
            {
                return Globals.RelationType_belongingClass;
            }
            if (ontologyItem.RefItem.Type == Globals.Type_Object)
            {
                return Globals.RelationType_belongingObject;
            }

            return null;
        }

        public async Task<clsOntologyItem> SaveClassesOfObjectsToOntology(Ontology ontology, List<clsOntologyItem> objectItems)
        {
            var serviceAgent = new ElasticAgentOntologies(Globals);

            var result = await serviceAgent.CheckOntology(ontology);
            
            if (result.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }


            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {

                var classIds = objectItems.GroupBy(obj => new { obj.GUID_Parent }).Select(grp => grp.Key.GUID_Parent).ToList();

                if (classIds.Any())
                {
                    var resultGetClasses = await serviceAgent.GetClassesByGuid(classIds);
                    result = resultGetClasses.ResultState;

                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var classesToSave = (from classItm in resultGetClasses.Result
                                         join oItemExists in ontology.OntologyItems on classItm.GUID equals oItemExists.RefItem.GUID into oItemsExists
                                         from oItemExists in oItemsExists.DefaultIfEmpty()
                                         where oItemExists == null
                                         select classItm).ToList();

                    result = await serviceAgent.RelateOItems(ontology, classesToSave);

                }

                return result;
            });

            return taskResult;
        }

        public OntologyConnector(OntologyAppDBConnector.Globals globals) : base(globals)
        {
        }
    }
}
