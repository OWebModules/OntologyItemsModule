﻿using Newtonsoft.Json;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Primitives;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Factories
{
    public class ClassTreeFactory : NotifyPropertyChange
    {
        private object factoryLocker = new object();

        private Globals globals;
        private ViewTreeNode searchNode;

        private List<clsOntologyItem> classList;

        

        private ResultPathNodes resultPathNodes;
        public ResultPathNodes ResultPathNodes
        {
            get
            {
                lock(factoryLocker)
                {
                    return resultPathNodes;
                }
                
            }
            set
            {
                lock(factoryLocker)
                {
                    resultPathNodes = value;
                }

                RaisePropertyChanged(nameof(ResultPathNodes));
            }
        }

        public async Task<ResultPathNodes> CreatePathNodes(List<clsOntologyItem> classList, string idNode)
        {
            var taskResult = await Task.Run<ResultPathNodes>(() =>
            {
                var result = new ResultPathNodes
                {
                    Result = globals.LState_Success.Clone()
                };

                var nodeList = CreateTree(classList: classList);

                result.PathNodes = nodeList;

                ResultPathNodes = result;
                return result;
            });

            return taskResult;
        }

        public async Task<ResultClassSelect> CreateTreeNodes(List<clsOntologyItem> classList, int level, string idParent = null)
        {
            var taskResult = await Task.Run<ResultClassSelect>(() =>
            {
                var result = new ResultClassSelect
                {
                    Result = globals.LState_Success.Clone()
                };

                if (level != -1 && idParent != searchNode.Id)
                {
                    var classItems = classList.Where(clsItem => clsItem.GUID_Parent == idParent).ToList();
                    this.classList = new List<clsOntologyItem>();
                    this.classList.AddRange(classItems);

                    do
                    {
                        this.classList.AddRange(from clsItm in classList
                                                join classItem in classItems on clsItm.GUID_Parent equals classItem.GUID
                                                select clsItm);
                        level--;
                    } while (level >= 0);

                    result.RootTreeNodes = new List<ViewTreeNode>();
                    result.TreeNodes = new List<ViewTreeNode>();
                    result.Result = CreateTreeWithSubHtml(result.RootTreeNodes, result.TreeNodes, idParent: idParent);
                }
                else if (idParent != searchNode.Id)
                {
                    this.classList = classList;
                    result.RootTreeNodes = new List<ViewTreeNode>();
                    result.TreeNodes = new List<ViewTreeNode>();
                    result.Result = CreateTreeWithSubHtml(result.RootTreeNodes, result.TreeNodes);
                    this.classList = classList;
                }
                else
                {
                    this.classList = new List<clsOntologyItem>();
                    this.classList.AddRange(classList);

                    result.RootTreeNodes = new List<ViewTreeNode>();
                    result.RootTreeNodes.Add(searchNode);

                    result.TreeNodes = new List<ViewTreeNode>();
                    result.Result = CreateTreeWithSubHtml(result.RootTreeNodes, result.TreeNodes, idParent: idParent);
                }



                return result;
            });

            return taskResult;
        }

        public clsOntologyItem CreateTreeWithSubHtml(List<ViewTreeNode> rootNodes, List<ViewTreeNode> treeNodes, ViewTreeNode parentItem = null, string idParent = null)
        {
            var result = globals.LState_Success.Clone();

            var nodes = new List<ViewTreeNode>();

            if (parentItem == null && idParent == searchNode.Id)
            {
                parentItem = new ViewTreeNode
                {
                    Id = searchNode.Id,
                    Name = searchNode.Id,
                    ShowAdd = true,
                    ShowApply = true
                };
            }
            if (parentItem != null)
            {
                if (parentItem.Id != searchNode.Id)
                {
                    nodes.Add(parentItem);
                }
                else
                {
                    nodes.AddRange(classList.Select(clsItem => new ViewTreeNode
                    {
                        Id = clsItem.GUID,
                        Name = clsItem.Name,
                        ShowAdd = true,
                        ShowApply = true
                    }));
                }




            }
            else if (parentItem == null && idParent != null)
            {

                nodes = classList.Where(clsItm => clsItm.GUID_Parent == idParent).Select(clsItm => new ViewTreeNode
                {
                    Id = clsItm.GUID,
                    Name = clsItm.Name,
                    ShowAdd = true,
                    ShowApply = true
                }).ToList();



            }
            else
            {
                nodes = classList.Where(clsItm => clsItm.GUID_Parent == globals.Root.GUID).Select(clsItm => new ViewTreeNode
                {
                    Id = clsItm.GUID,
                    Name = clsItm.Name,
                    ShowAdd = true,
                    ShowApply = true
                }).ToList();
            }


            treeNodes.AddRange(nodes);
            if (parentItem != null)
            {

                parentItem.SubNodes = nodes;


            }
            else
            {
                rootNodes.AddRange(nodes);
            }

            foreach (var node in nodes)
            {
                var addHtml = node.ShowAdd ? "<button class='btn btn-primary btn-xs btn-add-object' onclick='clickAddClass(this)'><i class='fa fa-plus-circle smallFont' aria-hidden='true'></i></button>" : "";
                var applyHtml = node.ShowApply ? "<button class='btn btn-primary btn-xs btn-apply-object' onclick='clickApplyClass(this)'><i class='fa fa-check-circle smallFont' aria-hidden='true'></i></button>" : "";
                node.ChildCount = classList.Where(clsItm => clsItm.GUID_Parent == node.Id).GroupBy(clsItm => clsItm.GUID).Count();
                if (node.ChildCount > 0)
                {
                    node.Html = string.Format(HTMLTemplates.ListDiv, 
                        System.Web.HttpUtility.HtmlEncode(node.Name),
                        addHtml,
                        applyHtml,
                        node.ChildCount.ToString(), 
                        "fa fa-plus-square-o");
                }
                else
                {
                    node.Html = string.Format(HTMLTemplates.ListDiv, 
                        System.Web.HttpUtility.HtmlEncode(node.Name),
                        addHtml,
                        applyHtml,
                        node.ChildCount.ToString(), 
                        "");
                }

            }

            return result;
        }

        public clsOntologyItem GetJson(List<ViewTreeNode> rootTreeNodes, List<ViewTreeNode> treeNodes)
        {
            
            try
            {
                
                using (var memoryStream = new MemoryStream())
                {
                    using (var streamWriter = new StreamWriter(memoryStream))
                    {
                        var result = globals.LState_Success.Clone();
                        bool success = true;
                        using (var jsonWriter = new JsonTextWriter(streamWriter))
                        {
                            jsonWriter.WriteStartArray();

                            var rootTreeNodesFiltered = rootTreeNodes.OrderBy(rootNode => rootNode.Name).ToList();
                            var searchCheckNode = rootTreeNodesFiltered.FirstOrDefault(node => node.Id == searchNode.Id);

                            if (searchCheckNode != null)
                            {
                                treeNodes.OrderBy(nodeItem => nodeItem.Name).ToList().ForEach(nodeItem =>
                                {
                                    nodeItem.WriteJSON(jsonWriter);
                                });
                            }
                            else
                            {
                                rootTreeNodes.OrderBy(rootNode => rootNode.Name).ToList().ForEach(rootNode =>
                                {
                                    rootNode.WriteJSON(jsonWriter);
                                });
                            }
                            
                            jsonWriter.WriteEndArray();
                            streamWriter.Flush();
                            result.Additional1 = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                        }

                        if (success)
                        {
                            return result;
                        }
                        else
                        {
                            return globals.LState_Error.Clone();
                        }
                    }
                }
                    
                

            }
            catch (Exception ex)
            {
                return globals.LState_Error.Clone();
            }
        }
        public clsOntologyItem WriteJsonDoc(StreamWriter streamWriter, List<ViewTreeNode> rootTreeNodes, List<ViewTreeNode> treeNodes)
        {
            try
            {
                bool success = true;
                using (var jsonWriter = new JsonTextWriter(streamWriter))
                {
                    jsonWriter.WriteStartArray();

                    var rootTreeNodesFiltered = rootTreeNodes.OrderBy(rootNode => rootNode.Name).ToList();
                    var searchCheckNode = rootTreeNodesFiltered.FirstOrDefault(node => node.Id == searchNode.Id);

                    if (searchCheckNode != null)
                    {
                        treeNodes.OrderBy(nodeItem => nodeItem.Name).ToList().ForEach(nodeItem =>
                        {
                            nodeItem.WriteJSON(jsonWriter);
                        });
                    }
                    else
                    {
                        rootTreeNodes.OrderBy(rootNode => rootNode.Name).ToList().ForEach(rootNode =>
                        {
                            rootNode.WriteJSON(jsonWriter);
                        });
                    }

                    jsonWriter.WriteEndArray();
                }
                if (success)
                {
                    return globals.LState_Success.Clone();
                }
                else
                {
                    return globals.LState_Error.Clone();
                }

            }
            catch (Exception ex)
            {
                return globals.LState_Error.Clone();
            }
        }

        private List<ViewTreeNode> CreateTree(ViewTreeNode parentItem = null, List<clsOntologyItem> classList = null)
        {
            var result = globals.LState_Success.Clone();

            var nodeList = new List<ViewTreeNode>();
            var classListForNodes = classList != null ? classList : this.classList;

            if (parentItem == null)
            {
                parentItem = new ViewTreeNode
                {
                    Id = globals.Root.GUID,
                    Name = globals.Root.Name,
                    ShowAdd = true,
                    ShowApply = true
                };
                nodeList.Add(parentItem);


            }
            var nodes = classListForNodes.Where(clsItm => clsItm.GUID_Parent == parentItem.Id).Select(clsItm => new ViewTreeNode
            {
                Id = clsItm.GUID,
                Name = clsItm.Name,
                ParentNode = parentItem,
                ShowAdd = true,
                ShowApply = true
            }).ToList();

            nodeList.AddRange(nodes);

            //parentItem.SubNodes = nodes;

            foreach (var node in nodes)
            {
                var newNodes = CreateTree(node, classList);
                if (newNodes == null) return null;
                nodeList.AddRange(newNodes);


            }

            return nodeList;
        }

        public ViewTreeNode CrateSearchNode()
        {
            return new ViewTreeNode
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Search"
            };
        }

        public ClassTreeFactory(Globals globals, ViewTreeNode searchNode)
        {
            this.globals = globals;
            this.searchNode = searchNode;
        }
    }

    

    public class ResultPathNodes
    {
        public clsOntologyItem Result { get; set; }
        public List<ViewTreeNode> PathNodes { get; set; }
        
    }
}
