﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Factories
{
    public class ObjectTreeFactory
    {
        public async Task<ResultItem<List<KendoTreeNode>>> CreateTree(List<clsOntologyItem> objects, List<clsObjectRel> nodeRelations, string idDirection, Globals globals, int levelsCount)
        {
            var taskResult = await Task.Run<ResultItem<List<KendoTreeNode>>>(() =>
            {
                var result = new ResultItem<List<KendoTreeNode>>
                {
                    ResultState = globals.LState_Success.Clone(),
                };

                List<KendoTreeNode> rootNodes = new List<KendoTreeNode>();
                if (idDirection == globals.Direction_LeftRight.GUID)
                {
                    result.Result = (from objectItm in objects
                                 join objRel in nodeRelations on objectItm.GUID equals objRel.ID_Other into objRels
                                 from objRel in objRels.DefaultIfEmpty()
                                 where objRel == null
                                 select new KendoTreeNode
                                 {
                                     NodeId = objectItm.GUID,
                                     NodeName = objectItm.Name
                                 }).ToList();

                    
                }
                else
                {
                    result.Result = (from objectItm in objects
                                     join objRel in nodeRelations on objectItm.GUID equals objRel.ID_Object into objRels
                                     from objRel in objRels.DefaultIfEmpty()
                                     where objRel == null
                                     select new KendoTreeNode
                                     {
                                         NodeId = objectItm.GUID,
                                         NodeName = objectItm.Name
                                     }).ToList();

                    if (!result.Result.Any() &&
                        objects.Any())
                    {
                        result.Result = objects.Select(obj => new KendoTreeNode
                        {
                            NodeId = obj.GUID,
                            NodeName = obj.Name
                        }).ToList();
                    }
                }

                GetSubNodes(result.Result, nodeRelations, idDirection, globals, levelsCount);

                return result;
            });

            return taskResult;
        }

        public void GetSubNodes(List<KendoTreeNode> treeNodes, List<clsObjectRel> nodeRelations, string idDirection, Globals globals, int levelsCount)
        {

            foreach (var treeNode in treeNodes)
            {
                var subNodes = nodeRelations.Where(nodeRel => idDirection == globals.Direction_LeftRight.GUID ? nodeRel.ID_Object == treeNode.NodeId : nodeRel.ID_Other == treeNode.NodeId).Select(nodeRel => new KendoTreeNode
                {
                    NodeId = idDirection == globals.Direction_LeftRight.GUID ? nodeRel.ID_Other : nodeRel.ID_Object,
                    NodeName = idDirection == globals.Direction_LeftRight.GUID ? nodeRel.Name_Other : nodeRel.Name_Object
                }).ToList();
                treeNode.SubNodes = subNodes;
                treeNode.HasChildren = subNodes.Any();
                if (levelsCount != -1)
                {
                    levelsCount--;
                }

                if (levelsCount == -1 || levelsCount > 0)
                {
                    GetSubNodes(treeNode.SubNodes, nodeRelations, idDirection, globals, levelsCount);
                }

            }
        }
    }
}
