﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Factories
{
    public class DataGridJsonFactory : NotifyPropertyChange
    {
        private Globals globals;

        private object factoryLocker = new object();

        private DataGridResult dataGridResult;

        public DataGridResult DataGridResult
        {
            get
            {
                lock(factoryLocker)
                {
                    return dataGridResult;
                }
            }
            set
            {
                lock(factoryLocker)
                {
                    dataGridResult = value;
                }
                RaisePropertyChanged(nameof(DataGridResult));
            }
        }

        public async Task<DataGridResult> GetDataGridItems(ResultOItemList itemList, bool isVisibleApply)
        {
            

            var result = new DataGridResult
            {
                Result = globals.LState_Success.Clone()
            };

            List<PropAttItem> propertyAttribs = null;

            Type typeItem = null;
            if (itemList.ListType == ListType.Classes)
            {
                typeItem = typeof(ClassViewItem);
                
            }
            else if (itemList.ListType == ListType.AttributeTypes)
            {
                typeItem = typeof(AttributeTypeViewItem);
            }
            else if (itemList.ListType == ListType.ClassAttributeRelation)
            {
                typeItem = typeof(ClassAttributeViewItem);
            }
            else if (itemList.ListType == ListType.ClassClassRelation_Conscious)
            {
                typeItem = typeof(ClassRelationConsciousViewItem);
                
            }
            else if (itemList.ListType == ListType.ClassClassRelation_Subconscious)
            {
                typeItem = typeof(ClassRelationSubconsciousViewItem);
            }
            else if (itemList.ListType == ListType.ClassClassRelation_Omni)
            {
                typeItem = typeof(ClassRelationOmniViewItem);
                
            }
            else if (itemList.ListType == ListType.ClassOtherRelation_Conscious)
            {
                typeItem = typeof(ClassRelationConsciousViewItem);
            }
            else if (itemList.ListType == ListType.InstanceAttribute_Conscious)
            {
                typeItem = typeof(ObjectAttributeViewItem);
            }
            else if (itemList.ListType == ListType.InstanceInstance_Conscious)
            {
                typeItem = typeof(ObjectObject_Conscious);
                
            }
            else if (itemList.ListType == ListType.InstanceInstance_Omni)
            {
                typeItem = typeof(ObjectObject_Omni);
            }
            else if (itemList.ListType == ListType.InstanceInstance_Subconscious)
            {
                typeItem = typeof(ObjectObject_Subconscious);
            }
            else if (itemList.ListType == ListType.InstanceOther_Conscious)
            {
                typeItem = typeof(ObjectObject_Conscious);
            }
            else if (itemList.ListType == ListType.InstanceOther_Subconscious)
            {
                typeItem = typeof(ObjectObject_Subconscious);
            }
            else if (itemList.ListType == ListType.Instances)
            {
                typeItem = typeof(InstanceViewItem);
            }
            else if (itemList.ListType == ListType.RelationTypes)
            {
                typeItem = typeof(RelationTypeViewItem);
            }

            propertyAttribs = typeItem.GetProperties().Select(propItem =>
            {
                var attribute = (DataViewColumnAttribute)propItem.GetCustomAttributes(true).FirstOrDefault(attrib => attrib.GetType() == typeof(DataViewColumnAttribute));

                if (attribute == null) return null;

                return new PropAttItem { PropInfo = propItem, AttributeItem = attribute };
            }).Where(propAtt => propAtt != null).OrderBy(propAtt => propAtt.AttributeItem.DisplayOrder).ToList();

            if (itemList.ListType == ListType.Classes)
            {
                result.GridItemsClass = itemList.ClassViewItems;
            }
            else if (itemList.ListType == ListType.AttributeTypes)
            {
                result.GridItemsAttributeType = itemList.AttributeTypeViewItems;
            }
            else if (itemList.ListType == ListType.ClassAttributeRelation)
            {
                result.GridItemsClassAtt = itemList.ClassAttViewItems;
            }
            else if (itemList.ListType == ListType.ClassClassRelation_Conscious)
            {
                result.GridItemsClassRelConcious = itemList.ClassRelConciousViewItems;
            }
            else if (itemList.ListType == ListType.ClassClassRelation_Subconscious)
            {
                result.GridItemsClassRelSubconscious = itemList.ClassRelSubconsciousViewItems;
            }
            else if (itemList.ListType == ListType.ClassClassRelation_Omni)
            {
                result.GridItemsClassRelOmni = itemList.ClassRelOmniViewItems;
            }
            else if (itemList.ListType == ListType.ClassOtherRelation_Conscious)
            {
                result.GridItemsClassRelConcious = itemList.ClassRelConciousViewItems;
            }
            else if (itemList.ListType == ListType.InstanceAttribute_Conscious)
            {
                result.GridItemsObjectAttribute = itemList.ObjectAttributeViewItems;
            }
            else if (itemList.ListType == ListType.InstanceInstance_Conscious)
            {
                result.GridItemsObjectObjectConscious = itemList.ObjectObjectConsciousViewItems;
            }
            else if (itemList.ListType == ListType.InstanceInstance_Omni)
            {
                result.GridItemsObjectObjectOmni = itemList.ObjectObjectOmniViewItems;
            }
            else if (itemList.ListType == ListType.InstanceInstance_Subconscious)
            {
                result.GridItemsObjectObjectSubconscious = itemList.ObjectObjectSubconsciousViewItems;
            }
            else if (itemList.ListType == ListType.InstanceOther_Conscious)
            {
                result.GridItemsObjectOtherConscious = itemList.ObjectOtherConsciousViewItems;
            }
            else if (itemList.ListType == ListType.InstanceOther_Subconscious)
            {
                result.GridItemsObjectOtherSubconscious = itemList.ObjectOtherSubconsciousViewItems;
            }
            else if (itemList.ListType == ListType.Instances)
            {
                result.GridItemsInstance = itemList.InstanceViewItems;
            }
            else if (itemList.ListType == ListType.RelationTypes)
            {
                result.GridItemsRelationType = itemList.RelationTypeViewItems;
            }

            DataGridResult = result;
            return result;
        }

        private List<Dictionary<string,object>> GetDictionary(List<object> items, List<PropAttItem> propAttItems, bool isVisibleApply)
        {
            var result = new List<Dictionary<string, object>>();

            items.ForEach(item =>
            {
                var dict = new Dictionary<string, object>();
                propAttItems.ForEach(propAtt =>
                {
                    dict.Add(propAtt.AttributeItem.Caption == null ? propAtt.PropInfo.Name : propAtt.AttributeItem.Caption, propAtt.PropInfo.GetValue(item));
                });

                if (isVisibleApply)
                {
                    dict.Add("ItemApply", false);
                }

                result.Add(dict);
            });

            return result;
        }

        public DataGridJsonFactory(Globals globals)
        {
            this.globals = globals;
        }

        private class PropAttItem
        {
            public PropertyInfo PropInfo { get; set; }
            public DataViewColumnAttribute AttributeItem { get; set; }
        }
    }

    
    
    public class DataGridResult
    {
        public clsOntologyItem Result { get; set; }

        public Dictionary<string,object> GridConfig { get; set; }
        public List<ClassViewItem> GridItemsClass { get; set; }
        public List<AttributeTypeViewItem> GridItemsAttributeType { get; set; }
        public List<RelationTypeViewItem> GridItemsRelationType { get; set; }
        public List<InstanceViewItem> GridItemsInstance { get; set; }
        public List<ClassAttributeViewItem> GridItemsClassAtt { get; set; }
        public List<ClassRelationConsciousViewItem> GridItemsClassRelConcious { get; set; }
        public List<ClassRelationSubconsciousViewItem> GridItemsClassRelSubconscious { get; set; }
        public List<ClassRelationOmniViewItem> GridItemsClassRelOmni { get; set; }
        public List<ObjectAttributeViewItem> GridItemsObjectAttribute { get; set; }
        public List<ObjectObject_Conscious> GridItemsObjectObjectConscious { get; set; }
        public List<ObjectObject_Subconscious> GridItemsObjectObjectSubconscious { get; set; }
        public List<ObjectObject_Conscious> GridItemsObjectOtherConscious { get; set; }
        public List<ObjectObject_Subconscious> GridItemsObjectOtherSubconscious { get; set; }

        public List<ObjectObject_Omni> GridItemsObjectObjectOmni { get; set; }
    }
}
