﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using OntoWebCore.Converter;
using OntoWebCore.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OntologyItemsModule.Factories
{
    public static class ObjectRelationTreeFactory
    {
        public static async Task<ResultItem<List<KendoTreeNode>>> CreateKendoTreeNodeList(ObjectRelationTreeModel model, Globals globals)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<List<KendoTreeNode>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<KendoTreeNode>()
                };

                var showObjectRelationAndClass = (from referenceItemToShowRelationObject in model.ReferenceItemsToShowRelationObjects
                                                  join showRelationObject in model.ShowRelationObjects on referenceItemToShowRelationObject.ID_Object equals showRelationObject.GUID
                                                  join relationObjectToRelationClass in model.ShowRelationToRelationClassesSecond on showRelationObject.GUID equals relationObjectToRelationClass.ID_Object
                                                  join showRelationObjectToCss in model.ShowRelationToCssClasses on showRelationObject.GUID equals showRelationObjectToCss.ID_Object into showRelationObjectToCsss
                                                  from showRelationObjectToCss in showRelationObjectToCsss.DefaultIfEmpty()
                                                  select new { referenceItemToShowRelationObject, showRelationObject, relationObjectToRelationClass, showRelationObjectToCss }).ToList();

                if (model.ReferenceItem != null)
                {
                    result.Result.Add(new KendoTreeNode()
                    {
                        Bold = true,
                        NodeId = model.ReferenceItem.GUID,
                        NodeName = model.ReferenceItem.Name,
                        SelectChannel = ItemTypeToChannelConverter.ConvertToSelectChannel(model.ReferenceItem.Type, globals),
                        ApplyChannel = ItemTypeToChannelConverter.ConvertToApplyChannel(model.ReferenceItem.Type, globals),
                        ReferenceType = model.ReferenceItem.Type
                    });
                }
                else
                {
                    result.Result.AddRange(showObjectRelationAndClass.Select(rel => new KendoTreeNode
                    {
                        Bold = true,
                        NodeId = rel.referenceItemToShowRelationObject.ID_Object,
                        NodeName = rel.referenceItemToShowRelationObject.Name_Object,
                        SelectChannel = ItemTypeToChannelConverter.ConvertToSelectChannel(globals.Type_Object, globals),
                        ApplyChannel = ItemTypeToChannelConverter.ConvertToApplyChannel(globals.Type_Object, globals),
                        ReferenceType = globals.Type_Class
                    }));
                }

                var providers = NameTransformManager.GetNameTransformProviders(globals, true);

                var relatedItemsForNameTransform = (from rel in model.RelatedItems
                                                     join useNameProv in model.UseNameprovider.Where(use => use.Val_Bool.Value) on rel.ShowObjectItem.GUID equals useNameProv.ID_Object
                                                     select rel.Related).ToList();

                var classesOfRelatedItemsForNameTransform = relatedItemsForNameTransform.GroupBy(rel => rel.GUID_Parent).Select(relPar => relPar.Key).ToList();

                var providerForTransform = (from prov in providers
                                            from cls in classesOfRelatedItemsForNameTransform
                                            where prov.IsResponsible(cls)
                                            select new { cls, prov }).ToList();

                foreach (var prov in providerForTransform)
                {
                    var items = relatedItemsForNameTransform.Where(rel => rel.GUID_Parent == prov.cls).ToList();
                    await prov.prov.TransformNames(items, true, prov.cls);
                    items.ForEach(item => item.Mark = true);
                }
                
                foreach (var showObjectAndClass in showObjectRelationAndClass.OrderBy(obj => obj.relationObjectToRelationClass.Name_Other))
                {
                    KendoTreeNode rootNode = null;
                    if (model.ReferenceItem != null)
                    {
                        rootNode = result.Result.First();
                    }
                    else
                    {
                        rootNode = result.Result.First(node => node.NodeId == showObjectAndClass.referenceItemToShowRelationObject.ID_Object);
                    }

                    var nodeL1 = rootNode.SubNodes.FirstOrDefault(node => node.NodeId == showObjectAndClass.relationObjectToRelationClass.ID_Other);

                    if (nodeL1 == null)
                    {
                        nodeL1 = new KendoTreeNode
                        {
                            NodeId = showObjectAndClass.relationObjectToRelationClass.ID_Other,
                            NodeName = showObjectAndClass.relationObjectToRelationClass.Name_Other
                        };

                        if (showObjectAndClass.showRelationObjectToCss != null)
                        {
                            nodeL1.FontClass1 = showObjectAndClass.showRelationObjectToCss.Name_Other;
                        }

                        rootNode.SubNodes.Add(nodeL1);
                        rootNode.HasChildren = rootNode.SubNodes.Any();
                        nodeL1.SelectChannel = ItemTypeToChannelConverter.ConvertToSelectChannel(showObjectAndClass.relationObjectToRelationClass.Ontology, globals);
                        nodeL1.ApplyChannel = ItemTypeToChannelConverter.ConvertToApplyChannel(showObjectAndClass.relationObjectToRelationClass.Ontology, globals);
                        nodeL1.ReferenceType = showObjectAndClass.relationObjectToRelationClass.Ontology;
                        nodeL1.IdReference = showObjectAndClass.showRelationObject.GUID;
                        nodeL1.EnableAddObjects = true;
                    }

                    foreach (var rel in model.RelatedItems.Where(itm => itm.ShowObjectItem.GUID == nodeL1.IdReference).OrderBy(rel => rel.OrderId).ThenBy(rel => rel.ReferenceObject.Name).ToList())
                    {
                        var nodeL2 = new KendoTreeNode
                        {
                            NodeId = rel.Related.GUID,
                            NodeName = (rel.Related.Mark ?? false) ? rel.Related.Val_String : rel.Related.Name,
                            FontClass1 = nodeL1.FontClass1,
                            SelectChannel = ItemTypeToChannelConverter.ConvertToSelectChannel(rel.Related.Type, globals),
                            ApplyChannel = ItemTypeToChannelConverter.ConvertToApplyChannel(rel.Related.Type, globals),
                            IsEncoded = rel.Related.Mark ?? false
                        };
                        nodeL1.SubNodes.Add(nodeL2);
                        nodeL1.HasChildren = nodeL1.SubNodes.Any();
                    }
                }

                return result;
            });

            return taskResult;
        }

    }
}
